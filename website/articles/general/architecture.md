---
sidebar_position: 1
---

# Architecture

A simplified representation of the D2 Smartview(D2SV) runtime.

![D2 Architecture](/images/architecture-d2.png)

A central component of the runtime is D2 Plugins a.k.a D2FS Plugins. 
A D2 Plugin is loaded dynamically in the D2SV Ecosystem and it can primarily augment
functions of D2 Foundation Services(D2FS).

The D2SV SDK API is built around the same D2 Plugins architecture and additionally
it can augment functions of D2SV UI & D2FS-REST runtime.

The SDK deals with hybrid Maven + NodeJS project which has both Java & Javascript code
along with other static resources organized in a certain structure. Upon build, 
the SDK compiles and packages the built output into a Jar. This Jar file can then be dropped
inside the `lib` folder of a D2 Smartview runtime. The D2SV runtime loads the pluggable components
from within the Jar dynamically.

## Technology

The D2 Smartview is a web application and requires a hybrid middleware runtime. JVM runs the Java written
back-end code and an Internet browser's Javascript VM runs the front-end. All the communication
between FE & BE happens through AJAX request-response.

* The Java based back-end uses the Spring WebMVC Framework, configured to run in an
application container, along with other proprietary and open-source libraries.
* The Javascript front-end uses Backbone & Marionette UI framework along with jQuery, 
RequireJS, Underscore, Handlebars etc. libraries.


