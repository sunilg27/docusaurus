---
sidebar_label: Catalog of built-in action icons
---

# Action icons catalog
Here is a list of built-in action icons from D2 Smartview runtime and its
framework.

:::caution
Action icons listed under CSUI & SVF may change without notice.
:::

