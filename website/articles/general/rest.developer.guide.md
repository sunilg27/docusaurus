# D2FS REST services developer guide

This document helps to familiarize one with the existing D2FS REST endpoints and learn about
the standards and conventions used in developing those endpoints.

<iframe src="/sdk/pdf/OpenText Documentum D2FS REST Services Development Guide.pdf" width="100%" style={{height: '450px'}}></iframe> 

