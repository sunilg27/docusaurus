# Where to start?

Well, D2 Smartview UI has many UI constructs like command, shotcut tile, list tile, application scope
perspective, rest-controller etc. Answer to the question, depends on what you're trying to accomplish.

A good starting point might be to look at the packaged samples. `D2-AdminGroups` sample specifically covers all
of the above stated constructs.

After extracting the sample, go thorugh [D2 Admin Groups Sample](./samples/3_01_admin_groups) documentation to know about 
the key concepts and strctures implemented in the sample. Then -

1. Build it once
2. Deploy the compiled artifact in `WEB-INF\lib` folder of a running D2-Smartview
3. Follow [How to debug](./howto/1_01_debug_sv_ui) to run the sample in debug mode and put break points(in internet browser's developer console) at
   at different javascript modules in the project.
   
Once familiarized, try exploring different workspace assistant options to add new components to the sample
or create a fresh plugin project and add it there to see how it works.

:::tip
Familiarize yourself with the rest of "How to" topics.
The [API documentation](../api_overview) helps getting to know different parts of the front-end & back-end components.
:::