---
sidebar_position: 1.02
sidebar_label: Extend/Override D2FS services
---

# Extending/Overriding D2FS service through Service Plugin


If developer wants to create a customization which needs to override/extend the existing functionality of a D2FS service, the developer can create custom class
with the <b>"(D2FS Services Name)Plugin.java'</b> which extends the D2FS service class and implements **ID2fsPlugin** class. For an example -
```
package com.opentext.d2.smartview.d2svdialogs.webfs.dialog;

import com.emc.d2fs.dctm.web.services.ID2fsPlugin;
import com.emc.d2fs.dctm.web.services.dialog.D2DialogService;
import com.emc.d2fs.models.context.Context;
import com.emc.d2fs.models.attribute.Attribute;
import com.emc.d2fs.models.dialog.Dialog;
import java.util.List;

public class D2DialogServicePlugin extends D2DialogService implements ID2fsPlugin {
    ...
    public Dialog validDialog(Context context, String id, String dialogName, List<Attribute> parameters) throws Exception {
        //custom logic
        
        //If the following line is executed during an invocation then it becomes an extension, otherwise it becomes an override.
        return super.validDialog(context, id, dialogName, parameters);
    }
    ...
}
```

Following services can only be extended/overriden for the supported methods. Below table lists both overidable 
and non-overridable methods for D2-Smartview 


| Services      | Overridable Methods           | Non-Overridable  Methods |
| ------------- |:-------------:| -----:|
| D2CreationService | applyVdTemplate<br/>getVDTemplates<br/>setTemplate<br/>getTemplates<br/>getTemplates<br/>updateTemplatesListwithFilter<br/>getConvertStructureConfig<br/>createTemplateFromServer<br/>createProperties<br/>getRecentlyUsedVDTemplates<br/>getImportStructureConfigs<br/>getRecentlyUsedTemplates| hasAnyAttachments<br/>removeAttachments<br/>getUIMaxSize<br/>hasAttachments<br/>hasAttachments<br/>getFilteredTemplates<br/>isAFolderOrACabinet<br/>getTemplateFilterOptions<br/>isNoCreationProfile<br/>isNoContentAuthorized|
| D2DialogService | getOptions<br/>getDialog<br/>validDialog<br/>cancelDialog| getTaxonomy<br/>getLabels<br/>isMemberOfGroup<br/>getImportValuesUrl<br/>getSubforms<br/>getExportValuesUrl|
| D2PropertyService | dump<br/>saveProperties<br/>saveProperties| getProperties|
| D2WorkflowService | rerunAutoActivity<br/>resumeTask<br/>pauseTask<br/>setTaskPriority<br/>updateWorkflowSupervisor<br/>getWorkflowTemplatesByWidgetName<br/>removeWorkflowSupportingDocuments<br/>isTaskQueueItemRead<br/>addWorkflowSupportingDocuments<br/>getWorkflowUsersByWidgetName<br/>getWorkflowWorkingDocumentsCount<br/>getWorkflowSupportingDocumentsCount<br/>completeAutoActivity<br/>getWorkflowAttachments<br/>checkLifeCycle<br/>pauseWorkflow<br/>processTask<br/>addNoteToTask<br/>launchWorkflow<br/>abortWorkflow<br/>canAddTaskNote<br/>delegateTask<br/>setTaskReadState<br/>resumeWorkflow<br/>updatePerformer<br/>fetchWorkflowConfig<br/>checkAttachmentLockState<br/>getWorkflowStatusSummary<br/>verifyEntryCriterias<br/>verifyEntryCriteria<br/>launchScheduledWorkflow<br/>acquireTaskAndGetState<br/>getUnreadTasks<br/>delegateTaskEx<br/>addNoteToWorkflow<br/>doAutoTaskAction| isTaskAcquired<br/>getTaskMode<br/>acquireTask<br/>getTaskPermissions<br/>canRejectTask<br/>canForwardTask<br/>canDelegateTask<br/>checkPropertyPage<br/>checkPropertyPage<br/>getTaskFolderLabel<br/>canAbortWorkflow<br/>getConfigurationsNames<br/>getWorkflowDisplayName<br/>checkWorkflowAliases<br/>delegateTaskOnError<br/>isManualAcquisitionTask|
| D2CheckoutService | checkout<br/>cancelCheckout<br/>testCheckout<br/>testControlledPrint| cancelCheckoutAll<br/>checkoutAsNew<br/>getNumberOfCheckoutDocument|
| D2CheckinService | getCheckinConfig| checkin|
| D2DownloadService | checkin<br/>getUploadUrls<br/>getUploadUrls<br/>getUploadUrls<br/>getPageServingUrl<br/>getCheckinUrls<br/>getDownloadUrls<br/>getExtractUrls<br/>getFile<br/>setFile<br/>hasAnyValidC2ExportAndRenditionConfig<br/>getDefaultServerInfo<br/>checkinAndLifeCycle<br/>extractDcoumentProperty<br/>getImportStructureUrls<br/>setDocumentProperty<br/>extractProperties<br/>getDownloadObjectId| getFileInfo<br/>getObjectsDownloadUrls<br/>canPrintControlledView<br/>getDispatchDownloadUrl<br/>getDownloadFileDetails<br/>isProtectedInControlledView<br/>addRendition|

:::info
In case any unsupported methods are overriden by a plugin, it will be shown as warning in `D2-Smartview.log` during startup of the application
:::