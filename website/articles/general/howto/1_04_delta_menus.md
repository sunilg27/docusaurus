---
sidebar_position: 1.04
sidebar_label: Use Delta Menus
---

# Delta Menus in D2


Developers can create default menus as part of any context from the plugin. 
Any menu item can be appended in context.

Following are the currently supported contexts in D2 Smartview.
<ul>
<li>MenuContext</li>
<li>MenuContextDetailRelations</li>
<li>MenuContextDetailRenditions</li>
<li>MenuContextDetailVersions</li>
<li>MenuContextSavedSearchObject</li>
<li>MenuContextTaskDocument</li>
<li>MenuContextTaskPriority</li>
<li>MenuContextTasksList</li>
<li>MenuContextVD</li>
<li>MenuContextVDAddChildOption</li>
<li>MenuContextVDReplaceChildOption</li>
<li>MenuContextWorkflowOverview</li>
<li>MenuDocumentD2LifeCycleAttach</li>
<li>MenuDocumentLifeCycle</li>
<li>MenuDocumentLifeCycleAttach</li>
<li>MenuDocumentShare</li>
<li>MenuDocumentWorkflow</li>
<li>MenuNewObject</li>
<li>MenuToolsMassUpdate</li>
<li>MenuUser</li>
</ul>

If the develop wants to define a OOTB default menu in the following path and format.
 `resources/xml/unitymenu/<context_name>Delta.xml`.
 
 Menu item in the D2 will follow the below structure
 ```  
 <menuitem id="MenuId">
      <dynamic-action class="ClassName"/>
      <condition class="ClassName" equals="value"/>
   </menuitem>
```
 
Here we have mainly 3 part for the menu as follows
- **menuitem**: define the menu item which will have a id attribute. The id attribute can be used
 to define label and is also a unique identifier.
- **dynamic-action**:  developer can define a class which is extended from the `IDynamicAction`. 
This tag is used to define the action that has to be performed when the menu item is clicked.
- **condition**: developer can define condition which has to be extended from `ICondition`

:::tip 
Depending on the class custom attributes can be passed to the tag for both `dynamic-action`
and `condition`
:::
 

In order to define the menu item we need to understand the root tag as `delta` tag.
There are mainly 3 types of operation done on the menu item
- **insert** - This will be used to insert a new item. There are a couple of attribute
which are `position-before` and `position-after` which will contain attribute value to 
menu id before or after which the current node has to be placed.
- **append** - This will be used to append item to the end of the.
- **modify** - this will need an id attribute which is a reference to any 
existing menu id which has to be modified. This can can be used to `delete`, `insert`,
`append` and `insert-before`.
- **delete** - This will need an id attribute which will refer to the menu id that has to deleted.
- **insert-before** - This can be used along with the `modify` to add new conditions.
- **move**- This can be used to move and  exiting menu by using the menu id and 
`position-before` and `position-after`.


:::tip 
1. `position-before` and `position-after` attributes containes the menu id of other menus
2. menu id, class names for the dynamic actions and conditions can be identified by 
creating menu items in D2-Config and exporting the menus.
:::

Find below some of the ways to use delta menus:
1. Insert new menu
  ```  
  <delta>
    <insert position-before="menuToolsMassUpdate">
      <menuitem id="menuContextViewPermission">
        <dynamic-action class="com.emc.d2fs.dctm.ui.dynamicactions.actions.U4Generic" eMethod="getPermissions" eMode="SINGLE" eService="PermissionActionService" rAction="${pluginNamespace}/dialogs/permissions/permissions.dialog:showPermissions" rType="JS"/>
        <condition class="com.emc.d2fs.dctm.ui.conditions.interfaces.IsMultipleSelection" equals="false"/>
        <condition class="com.emc.d2fs.dctm.ui.conditions.options.IsPluginActivated" value="${pluginName}"/>
      </menuitem>
    </insert>
    <insert position-before="menuToolsMassUpdate">
      <separator/>
    </insert>
  </delta>
 ```
2. Modify an existing menu with new conditions
  ``` 
<delta>
	<modify id="menuDocumentEdit">
		<insert-before>
			<condition class="com.emc.d2fs.dctm.ui.conditions.typeparts.IsObjectType" value="d2c_bin_deleted_document¬d2c_bin_deleted_folder¬d2c_bin_deleted_folder_dump¬d2c_bin_deleted_rendition" equals="false"/>
		</insert-before>
	</modify>
</delta>
  ``` 