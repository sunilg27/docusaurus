---
sidebar_position: 1.01
sidebar_label: Debug Smartview code
---

# Debugging D2 Smartview UI

D2 Smartview UI being written purely in Javascript, HTML, CSS has the benefit of debugging its client-side source code
directly from an Internet Browser. Debugging can happen in either mode 
1. with the source code as is
2. with compiled & minifed version of the source code 

In distribution, D2 Smartview front-end and back-end is packaged as a single web archive
however these two parts are very loosely coupled and communicates via HTTP request-responses.

This loosely coupled nature helps keep the front-end and back-end
clearly separate even upto an extent where these two parts are hosted and served by two 
different application servers. This technique is recommended and also used by us to debug D2 Smartview UI such as
we setup and configure D2 Smartview UI to make it talk to a running Smartview instance as backend and then host
only the UI part on a lightweight NodeJS server.

:::info
The following method only allows debugging client-side Javascript code. If a plugin has some server-side
component like REST-Controller, D2FS dialog, D2FS service plugin, menu confguration etc. then the plugin has to be built and 
deployed on the D2 Smartview application server followed by a server restart in order to make them available to use for
the corresponding client-side code.
:::

## How to setup?

For D2SV plugin developers, the heavy lifting is already done for you. All you need is to -
1. Open to edit the `server.conf.js` from `src/main/smartview` directory of your plugin.
2. For `D2SV_APP_SERVER_URL` property, set an appripriate URL to a running instance of D2 Smartview
   <p> E.g. http://my.domain.com:port/D2-Smartview</p>
   
   :::tip
   The D2-Smartview installation, which is being referred to by the URL, must be setup to produce either relative 
   linkrels by setting <code>rest.use.relative.url=true</code> in its rest-api-runtime.properties file, or alternatively it should be configured to allow 
   Cross-Origin requests by setting other appropriate properties(refer to rest-api-runtime.properties.template file from D2-Smartview distribution).
   :::
   
3. Save `server.conf.js`
4. Open a Terminal/Command Prompt in the same `src/main/smartview` directory of the plugin.
5. Execute command `npm start`
6. Navigate to URL `http://localhost:6989/ui/pages/debug/app.html` in a browser to start debugging code in as-is format.
   <p>Or, alternatively navigate to <code>http://localhost:6989/ui/pages/release/app.html</code> to debug the compiled & minified code.
   Please remember that command <code>grunt compile</code> from <code>src/main/smartview</code> or <code>npm run build</code> from 
   workspace root directory has to be executed before you can debug the compiled and minified code.</p>
   
  