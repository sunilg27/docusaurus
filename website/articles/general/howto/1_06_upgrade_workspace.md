---
sidebar_position: 1.06
sidebar_label: Upgrade SDK workspace/plugins
---

# Upgrading Documentum D2 Smartview SDK to the latest version

With Documentum D2 Smartview SDK, new features will be added and delivered in each release. 
So,the SDK workspace need to be updated for consuming newly delivered feature.

With the Current version of D2 Smartview SDK,the existing workspace cannot be upgraded with the new version of SDK automatically.
Developer need to migrate to the new version of SDK manually.

For manual migration, extract the latest version of D2 Smartview SDK for follow the steps for creating new development workspace.
Once the new workspace is created, then copy the source code from old workspace to new workspace. 

Ideally all the plugin project are created inside the "plugins" folder by default. Same workspace plugins folder name can be changed while creating the new plugin.

While creating a new plugin project, it's recommended to provide the same workspace assistance inputs which is captured in the prior version of SDK. 
But its not mandatory to provide the same value. Providing the same value will make upgrade process easy.

Please refer the following list for finding the workspace assistance inputs provided in prior version of SDK.

* Directory name to save this plugin project in  - Refer the "modules" tag in workspace root "pom.xml"
* Maven group-id of the plugin - Refer the "groupId" tag in first level within the plugin's pom.xml.
* Name(maven artifact-id) of the plugin - Refer the "artifactId" tag in first level within the plugin's pom.xml.
* Version of the plugin - Refer the "version" tag in first level within the plugin's pom.xml.
* One liner description of the plugin - Refer the "name" and "description" tag in first level within the plugin's pom.xml.
* Enter package namespace for the plugin - Refer the content of property file in [plugin_name]/src/main/resources/D2Plugin.properties
* Include support for D2SV UI - Check if "smartview" folder exist in [plugin_name]/src/main

Once the new workspace is created with new version of D2 Smartview SDK, few files needs to be copied from old to new workspace and few files need to be merged.

* Files/Folders to be copied from [plugin_name]/src/main/resources
    * [plugin_name]/src/main/resources/strings
    * [plugin_name]/src/main/resources/xml

* Files/Folders to be copied from [plugin_name]/src/main/java
    * [plugin_name]/src/main/java/com/opentext/d2/smartview

* Files/Folders to be merged in [plugin_name]/src/main/java
    * [plugin_name]/src/main/java/com/opentext/d2/rest/context/jc
    * Spring bean configuration need to be merged in java configuration file which is located in mentioned path.

* Files/Folders to be copied from [plugin_name]/src/main/smartview
    * Following items must be ignored while moving the source code from old workspace to new workspace.
      * [plugin_name]/src/main/smartview/src/bundles
      * [plugin_name]/src/main/smartview/src/component.js
      * [plugin_name]/src/main/smartview/src/config-build.js
      * [plugin_name]/src/main/smartview/src/Gruntfile.js
      * [plugin_name]/src/main/smartview/src/extensions.json.js
      * [plugin_name]/src/main/smartview/src/[plugin_namespace]-init.js
    * Copy the rest of the files and folders from old workspace to new workspace.

* Files/Folders to be merged in [plugin_name]/src/main/smartview
    * [plugin_name]/src/main/smartview/src/extensions.json
    * UI extension need to be merged. So that customization from previous version will be included in latest version as well.

After moving and merging the source code to new workspace, open workspace assistance to compile all the plugins in the SDK workspace.
If build is successfull, then the workspace upgrade is done.