---
sidebar_position: 1.03
sidebar_label: Declare custom types for D2SV landing page shortcuts
---

# Custom shortcut type in D2
D2-Smartview landing page configuration elements like <code>&lt;tile&gt;</code> requires a `type` attribute to be
set. By default all "Widget type" found in D2-Config are accepted as valid values. However, while defining new shortcuts for the landing page
tile one might need to use a value that is not a "Widget type" at all or in other words the value is not pre-defined. 
Additionally, while defining new application scope perspective one might
need to declare a default widget name for the corresponding perspective to use when a direct URL based navigation is taking place in the UI.

To facilitate this, an SDK developer can create
a properties file **{Plugin}/resources/string/config/U4landing.properties**. 
There are two properties that can be defined in the file - 
- <i>default_widget_tags</i>
    : This allows declaring new valid XML tag names to go under the <code>&lt;default-widgets&gt;</code> section in the D2SV landing page file. Multiple comma separated
    values could be specified.    
- <i>shortcut_types</i>
    : This allows declaring valid values for `type` attribute of <code>&lt;tile&gt;</code>. Multiple comma separated values could be specified. 
    
#### Example
```
default_widget_tags=abcd,defg
shortcut_types=custom1,custom2
```

If this is the content of the **U4Landing.properties** file, then the following hypothetical landing page structure is accepted as valid configuration
```
<root>
    <space>
        <flow-layout-container>
            <content>
                <tile-container size="full">
                    <tile name="one" type="custom1">
                        <theme color="shade1" type="stone1"/>
                    </tile>
                    <tile name="two" type="custom2">
                        <theme color="shade1" type="indigo1"/>
                    </tile>
                </tile-container>
            </content>
        </flow-layout-container>
    </space>
    <default-widgets>
        <abcd>SOME_NAME</abcd>
        <defg>ANOTHER</defg>
    </default-widgets>
</root>
``` 