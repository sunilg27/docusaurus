---
sidebar_position: 1.05
sidebar_label: Use icons in D2SV
---

# Icons in D2 Smartview

Like any other application, D2 Smartview also uses different icons for visual context of data, infromation
or action.

D2 Smartview uses Scalable Vector Graphic(svg) images to implement an icon. All the icons used
in D2SV can be broadly classified into
* Non interactive
  <p>Used to attach a context to a piece of data or information.</p>
* Interactive a.k.a action icons
  <p>Used to represent an action </p>
  
Action icons are different semantically compared to the other type in the sense that action icons
are reactive and respond to keyboard focus, blur or mosue events. Implementation wise, the SVG behind
an action icon has a certain element structure whereas the other type don't have any such restriction.

## Specification

Being SVG, a D2SV icon can be upscaled or downscaled to any size to fit in a UI element's boundary, however
we recommend and follow that each SVG is defined w.r.t a view box of size `32px x 32px`.

Action icons must have 3 svg sub-elements with id `state`, `metaphor` and `focus`. The `state` element
reacts to mouse position w.r.t to icon itself and changes its color accent. The `metaphor` element holds 
the visual graphic of the icon. And the `focus` element reacts to keyboard focus gain, giving itself a highlighted
border.

## How to use an icon

* For non-interactive icons, place the svg file at any source folder location and refer to it
as <code>background-image</code> property in any CSS file using relative(from css file location to svg file locaiton) url.
  <p>We follow a convention where an svg file is placed inside <code>impl/images</code> folder w.r.t to CSS 
  file's location where the svg image will be used.</p>
  
* For interactive icons, drop the svg file inside <code>utils/theme/action_icons</code> folder w.r.t the
smartview source code directory. Then run <code>grunt compile</code> in the smartview source code
directory. And finally, at the place of use (which is mostly inside node.actions.rules module), set property
<code> iconName: '&lt;pluginNamespace&gt;_&lt;svg_file_name_without_extension&gt;' </code> after replacing
the format with appropriate values.

## Sample non-interactive icon

    <?xml version="1.0" encoding="utf-8"?>
    <!-- Generator: Adobe Illustrator 19.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    	 viewBox="-379 291 32 32" enable-background="new -379 291 32 32" xml:space="preserve">
    <g>
    	<circle fill="#7E929F" cx="-363" cy="307" r="16"/>
    </g>
    <circle fill="#FFFFFF" cx="-363" cy="315" r="2"/>
    <path fill="#FFFFFF" d="M-361,309c0,1.1-0.9,2-2,2l0,0c-1.1,0-2-0.9-2-2v-11c0-1.1,0.9-2,2-2l0,0c1.1,0,2,0.9,2,2V309z"/>
    </svg>
    
## Sample action icon

    <?xml version="1.0" encoding="utf-8"?>
    <!-- Generator: Adobe Illustrator 24.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    	 viewBox="0 0 32 32" style="enable-background:new 0 0 32 32;" xml:space="preserve">
    <style type="text/css">
    	.st0{fill:none;}
    	.st1{fill-rule:evenodd;clip-rule:evenodd;fill:#333333;}
    	.st2{fill:none;stroke:#2E3D98;}
    </style>
    <circle id="state" class="st0" cx="16" cy="16" r="14"/>
    <path id="metaphor" class="st1" d="M22.4,12.5H9.6c-0.331,0-0.6-0.336-0.6-0.75S9.269,11,9.6,11h12.8c0.331,0,0.6,0.336,0.6,0.75
    	S22.731,12.5,22.4,12.5z M23.53,16.5H9.685C9.307,16.5,9,16.164,9,15.75S9.307,15,9.685,15H23.53c0.378,0,0.684,0.336,0.684,0.75
    	S23.908,16.5,23.53,16.5z M9.628,20.5h10.785c0.346,0,0.628-0.336,0.628-0.75S20.759,19,20.412,19H9.628C9.281,19,9,19.336,9,19.75
    	S9.281,20.5,9.628,20.5z"/>
    <circle id="focus" class="st2" cx="16" cy="16" r="15.5"/>
    </svg>

:::tip
For a list of built-in action icons refer to the [Catalog](../builtin.icons)
:::