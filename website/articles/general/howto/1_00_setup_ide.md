---
sidebar_position: 1.00
sidebar_label: Use IDE for plugin development
---

# Setup SDK workspace in IDE

### Preface

All D2SV plugin projects are made of Java & Javascript source codes. Naturally the project setup
also has to be hybrid to compile and package all parts of the source code. This is the reason the SDK
uses a mixed tooling approach towards the same. 

All the plugins inside the workspace, are layed out in a Maven project structure where the <code>pom.xml</code>
found at the workspace root directory serves as the aggregator-parent and each plugin is linked to it as Maven module. 

The NodeJS specific portion does not require a parent-child relationship between the plugins and the workspace.
However to optimize dependency management, the workspace declares itself as a NodeJS project through its 
<code>package.json</code> and each plugin in turn uses directory shortcuts to refer to the same set of 
dependencies even though the plugin declares a separate NodeJS project for its Javascript code through 
<code>package.json</code> found in its <code>src/main/smartview</code> directory.

While building, Maven is used as the primary tool to trigger it. Internally Maven uses <code>maven-antrun-plugin</code>
to execute NodeJS script through shell and that builds the Javascript portion.

Any IDE that works with Maven projects, can recognize this hybrid setup. All that it takes is the support to be able to import
a Maven project from existing source code.

:::tip
Before trying any of the following steps to setup IDE, make sure to either create a plugin project or extract
a packaged sample in the SDK workspace by running either [Create a plugin project](../ws_assistant/2_01_create.plugin) or 
[Checkout some samples](../ws_assistant/2_08_extract.sample) option from [Workspace Assistant](../ws_assistant/2_00_ws_overview).
:::

## Steps to setup workspace in IntelliJ IDE

* Select **File -> New -> Project from Existing Sources** from menu.<p>Note. If starting with a fresh IDE installation select <b>Import Project</b> option from welcome screen.</p>
* In **Select File or Directory to Import** dialog, locate and select <code>pom.xml</code> from the workspace root.
* In **Import Project from Maven** dialog, keep the default values and deselect <i>Search for projects recursively</i>
  checkbox if it is selected. Then, click **Next** button to go to next screen.
* In the current screen select the checkbox against a group-id and artifact-id combination that correctly represents
  the workspace root pom. By default it may look something like <i>com.opentext.d2.smartview:D2-Plugin-Projects:1.0.0</i>.
  After selection, click **Next** button to proceed to the next screen.
* Select an available JDK to use for the imported projects and click **Next** to proceed.
* In current screen keep default values for **Project name** and **Project file location** input fields and click **Finish**
  to start import.<p>Note. It might ask whether to open the project in current window or new window, please select an appropriate option. <b>New window</b> is could be a preferred option.</p>
* After the project import completes, it might take a while for the IDE to index files from the workspace. To cut short on the indexing time, it is advised
  to mark all directories from workspace root except **plugins**(or whichever directory you chose to store plugins) as **Excluded**.
  
:::info
As you keep adding/removing new plugin projects in the workspace using the workspace assistant, the IDE automatically catches up with the change.
:::
  
## Steps to setup workspace in Eclipse IDE

* Select **File -> Import** from menu.
* In **Import** dialog, expand **Maven** and select **Existing Maven Projects** and click **Next**.
* In **Import Maven Projects** dialog, Click **Browse...** button beside the **Root Directory** field.
* In **Select Root Folder** file-selection dialog, navigate to SDK workspace root directory and click **Select Folder** button to go back to **Import Maven Projects** dialog.
* In **Import Maven Projects** dialog, click **Select All** to select all discovered projects. Then click **Finish** to close the dialog and start importing the projects.
* Once the project import completes, right-click on root project's **pom.xml** and select **Run as -> Maven build** from the menu to open **Edit Configuration** dialog.
* In **Edit Configuration** dialog type <code>clean install</code> in **Goals** field then click **Run** button to start building the plugins in the workspace.

:::caution
Eclipse is unable to automatically detect plugin projects added to/removed from the workspace using the workspace assistant.
::: 

###### To detect newly added project, in Eclipse **Project Explorer**
* On the root project, **right click -> Refresh**.
* Select the **plugins** folder(or whichever folder you're using to store plugins) and select **Right click -> New -> Project**
* In **New Project** dialog, select **General -> Project** and click **Next**.
* In **New Project** dialog, deselect **Use default location** checkbox and click **Browse**
* In **Select Folder** dialog, navigate inside the root folder of newly created plugin project on disk and click 
**Select Folder** button
* In **New Project** dialog, copy the last part of path value from **Location** field and paste it into **Project name** field.
* Select the chekcbox **Add project to working sets** and select value **D2-Plugin-Projects** for the field **Working sets** 
then click **Finish** to close the dialog.
* Once the project creation completes, in **Project Explorer** of Eclipse, expand the **plugins**(or whichever folder you're using)
folder and gesture **Right-click -> Configure -> Convert to Maven Project** to finish setting up the new plugin project.

###### To detect removed project, in Eclipse **Project Explorer**
* Gesture **Right-click -> Maven -> Update Project** on the root project's **pom.xml**
* In the **Update Maven Project** dialog, deselect every project except the root one and click **OK**
* If the above steps do not automatically remove the plugin project entry, then you can safely **Right-click -> Delete** the project.
  


