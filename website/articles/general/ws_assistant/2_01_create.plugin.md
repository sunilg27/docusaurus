---
sidebar_position: 2.01
---

# Creating a plugin

This option is used to initiate a fresh D2SV plugin project. 
A plugin project is basically a maven project with all its dependencies pre-declared from the workspace `lib` folder.
Based on selected options a plugin project may optionally have a Smartview UI component.
If there is a D2SV UI component in a plugin, it requires NodeJS runtime to compile and package that specific component.
All the relevant NodeJS and Javascript dependencies will be initialized upon plugin project creation.

To create a new plugin, a developer has to choose the **Create a new plugin project** option from the
D2SV SDK workspace assistant.

Upon selecting the specific option in workspace assistant, a developer has to answer a few questions before the assistant can
create and initialize the plugin project. For some of these questions asked, the workspace assistant will provide
a meaningful contextual default answer based on usage, the default answer is enclosed within a pair of 
parentheses `()`, to choose the default value, one has to only press *Enter* key on the keyboard.
These questions are self-explanatory however, here are a list of those questions and their meaning -

* Directory name to save this plugin project in
  <br/><p>Where to save the newly created project, defaults to `plugins` directory within the workspace.</p>
  
* Maven group-id of the plugin
  <br/><p>Since all the plugin projects are maven projects, each project requires a group-id to be specified.</p>
  
* Name(maven artifact-id) of the plugin
  <br/><p>Artifact identifier of the maven project to uniquely identify this plugin within the provided maven group ID.</p>
  
* Version of the plugin
  <br/><p>Version of the plugin project.</p>
  
* One liner description
  <br/><p>Used as the name and description for the underlying maven project. This is also shown as
  part of installed plugin data in D2 runtime.</p>
  
* Package namespace
  <br/><p>A unique name used as prefix/suffix for generating the source code & properties in the maven project.
  The lowercase version of the given package name is used as part of the Java package name and also used as
  unique identifier for the Smartview UI code in the project, if any. For an example, if a Plugin project is created
  with Maven group-id `a.b.c` and it is given a package name `MyPlugin` then the base package for all Java source code 
  becomes `a.b.c.myplugin` and the Smartview specific UI code is identified by `myplugin`.</p>
  
* Include support for D2SV UI
  <br/><p>Whether to include D2 Smartview UI specific code infrastructure in the created plugin project. This question should
  be answered with an `Yes (Y)` only if the plugin is meant to develop, override or complement any D2SV front-end functionality.
  However, even for a plugin project initially created to develop or complement back-end oriented functionality, D2SV UI support
  can be added later through [Add smartview UI support to an existing plugin project](./add.sv.ui) option.</p>
  
  
