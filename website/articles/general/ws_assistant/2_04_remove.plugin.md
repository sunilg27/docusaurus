---
sidebar_position: 2.04
---

# Remove a plugin from workspace

This option is used to remove a plugin project from the corresponding workspace.

:::caution
Use this option when at least one plugin project exists in the workspace.
:::

**Associated questions and their meanings** -

* Select plugin to remove from workspace
  <p>Select which plugin project is to be removed from the workspace.</p>

* Remove it completely from file system?
  <p>Whether to remove the project completely from filesystem. In case of a soft removal, the project is left intact
on the disk, however its entry from the aggregator POM in workspace is removed to exclude it from build order.</p>
