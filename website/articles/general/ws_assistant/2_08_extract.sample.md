---
sidebar_position: 2.08
---

# Checkout samples

The D2SV SDK packs a few working samples to demonstrate the building blocks of D2SV runtime APIs. 
This option of the workspace assitant is used to unpack a sample into the workspace such that
subsequently it can be built and deployed on a running D2 Smartview or simly the source code
could be checked out as tutorial.

Upon selecting this option, the only additional question to be answered is to pick the sample
that is to be extracted.
