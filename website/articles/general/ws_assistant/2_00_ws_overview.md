---
sidebar_position: 2.00
sidebar_label: Overview
---

# Workspace & Assistant

## What is a workspace?
A workspace is the collection of tools, libraries, documentations put together in a specific directory structure 
that acts as a **Development Environment** and facilitates creation, management and build of D2 Plugin projects. 
D2SV SDK distributable comes in the form of a zip file named as `D2SV-SDK-<Version>.zip`. After extracting the zip and
subsequently executing `ws-init.bat` successfully in the extracted folder turns it into a workspace.


## What is the Assistant?
The workspace assistant is a command line utility that lives within a workspace & provides the functional aspects of the
workspace with help of `NodeJS` & `Apache Maven` runtimes. 
While running, the assistant presents users with options to get something done within the corresponding workspace.

To run the assistant, open a command prompt/terminal in a workspace root directory and run
    
    $npm start
    
Things that the workspace assistant is capable of doing are:

* [Create a new plugin project](./2_01_create.plugin)
* [Add smartview application scope perspective](./2_02_add.sv.app.scope.perspective)
* [Add smartview UI support to an existing plugin](./2_03_add.sv.ui)
* [Remove a plugin from workspace](./2_04_remove.plugin)
* [Add D2-REST controller to a plugin](./2_05_add.rest.controller)
* [Build all plugins in the workspace](./2_06_build.plugins)
* [Check out the documentation](./2_07_open.sdk.documentation)
* [Check out some samples](./2_08_extract.sample)
* [Add smartview shortcut behavior](./2_09_add.sv.shortcut.behavior)
* [Add smartview list tile](./2_10_add.sv.tile.list)
* [Add smartview shortcut tile](./2_11_add.sv.tile.shortcut)
* [Add D2FS dialog to a plugin](./2_12_add.d2fs.dialog)


