---
sidebar_position: 2.07
---

# Checkout documentation

This option starts the embedded documentation server and opens the home page of it in the default browser.

