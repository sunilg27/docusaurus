---
sidebar_position: 2.06
---

# Build all plugins in the workspace

This option builds all the plugin projects in the workspace whose entries are found in the aggregator
`pom.xml` file in the workspace.

Basically it runs `mvn clean package` command in the workspace root directory. As part of this option,
final build output from each of the plugin project is collected in the `dist` directory right under the
workspace root directory.

:::caution
Use this option when at least one plugin project exists in the workspace.
:::
