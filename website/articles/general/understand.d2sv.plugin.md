---
sidebar_label: Understanding D2SV plugin project
---

# Understanding D2SV plugin project

Each D2SV plugin project is a hybrid Maven + NodeJS project having some Java, Javascript and a few static resources as part of
its source code. On the outer side, the source code is organized in a Maven project layout and an additional source directory `src/main/smartview`
is used to house Javascript source code and directory follows an NPM project structure.

Here we list the directories and files found in a bare-minimum project and outline their purpose. For the purpose of listing, we create a new plugin
project by following -
* Execute `npm start` in the workspace root directory to fire up the [Workspace Assistant](./ws_assistant/2_00_ws_overview)
* Select option **Create a new plugin project**
* Answer the follow-up questions as -
  * Directory name to save this plugin project in: **plugins**
  * Maven group-id of the plugin: **com.opentext.d2.smartview**
  * Name(maven artifact-id) of the plugin: **D2SV-TEST**
  * Version of the plugin: **1.0.0**
  * One liner description of the plugin(shows up everywhere in D2 runtime): **D2SV-TEST**
  * Enter package namespace for the plugin(used as prefix/suffix to generate Java classes & properties, also its lowercase format is used as base Java package
    name for the plugin & D2SV UI bundle): **D2SVTEST**
  * Include support for D2SV UI: **Yes**
  
After the assistant runs successfully, it would create a new plugin project in `plugins/D2SV-TEST` directory.

### Plugin project layout
```
D2SV-TEST
|
|   pom.xml
|   
+---src
|   \---main
|       +---java
|       |   \---com
|       |       +---emc
|       |       |       D2PluginVersion.java
|       |       |       
|       |       \---opentext
|       |           \---d2
|       |               +---rest
|       |               |   \---context
|       |               |       \---jc
|       |               |               PluginRestConfig_D2SVTEST.java
|       |               |               
|       |               \---smartview
|       |                   \---d2svtest
|       |                       |   D2SVTESTPlugin.java
|       |                       |   
|       |                       +---api
|       |                       |       D2SVTESTVersion.java
|       |                       |       
|       |                       \---rest
|       |                               package-info.java
|       |                               
|       +---resources
|       |   |   D2Plugin.properties
|       |   |   d2svtest-version.properties
|       |   |   
|       |   \---smartview
|       |           SmartView.properties
|       |           
|       \---smartview
|           |   .csslintrc
|           |   .eslintrc-html.yml
|           |   .eslintrc.yml
|           |   .npmrc
|           |   config-editor.js
|           |   d2svtest.setup.js
|           |   esmhelper.js
|           |   Gruntfile.js
|           |   package.json
|           |   proxy.js
|           |   server.conf.js
|           |   server.js
|           |   
|           +---lib (shortcut to javascript & java libraries)
|           +---node_modules (shortcut to NPM based dependencies used to build/serve the Javascript portion of code)
|           |           
|           +---pages
|           |   |   config-d2.js
|           |   |   config-dev.js
|           |   |   favicon.ico
|           |   |   initialize.js
|           |   |   loader.css
|           |   |   otds.html
|           |   |   redirect.html
|           |   |   
|           |   +---debug
|           |   |       app.html
|           |   |       
|           |   \---release
|           |           app.html
|           |           
|           +---src
|           |   |   component.js
|           |   |   config-build.js
|           |   |   d2svtest-init.js
|           |   |   extensions.json
|           |   |   Gruntfile.js
|           |   |   
|           |   +---bundles
|           |   |       d2svtest-bundle.js
|           |   |       
|           |   +---test
|           |   |       extensions.spec.js
|           |   |       
|           |   \---utils
|           |       |   startup.js
|           |       |   
|           |       \---theme
|           |           |   action.icons.js
|           |           |   
|           |           \---action_icons
|           |                   action_sample_icon.svg
|           |                   
|           \---test
|                   Gruntfile.js
|                   karma.js
|                   test-common.js
|                   
\---target
```

### Files and their purpose
* `/`
  * pom.xml - Maven build file. Used to build the plugin from source code to its distributable format.
* `src/main/java/` - Directory containing all Java source code
  * com/emc/D2PluginVersion.java - Declares identification information for the entire plugin using `D2SVTESTVersion` class.
  * com/opentext/d2/rest/context/jc/PluginRestConfig_D2SVTEST.java - Java configuration for spring components like Beans, Interceptor, Filter etc. This class also
declares a component scanner for Spring runtime to automatically load REST Controller and related components.
  * com/opentext/d2/smartview/d2svtest/D2SVTESTPlugin.java - Declares a blanket plugin. Additional code could be put inside this class to implement any functional
service plugin.
  * com/opentext/d2/smartview/d2svtest/api/D2SVTESTVersion.java - Holder for plugin identification information. Loads relevant data from `d2svtest-version.properties` file resource.
  * com/opentext/d2/smartview/d2svtest/rest/package-info.java - Declares package metadata for JDK and IDE.
* `src/main/resources/` - Directory containing all plugin related metadata and other static resources
  * smartview/SmartView.properties - Descriptor for D2SV UI runtime. Content of this file is read by D2SV UI runtime and appropriate UI artifacts from this plugin are discovered and linked
to the UI.
:::info
This file won't be present for the plugins where Smartview UI support is not enabled.
:::
:::danger
Changing content of this file will bring about runtime incompatibility and the UI artifacts will never get discovered by D2SV UI runtime.
:::
  * D2Plugin.properties - Another descriptor for D2SV plugin system to identify this plugin separately from other plugins deployed in the same runtime. Content of this file is 
interpreted as a uniqueue namespace identifier.
:::danger
Changing content of this file will lose the namespace convention used throughout the source code and makes this plugin unmanageable by the SDK as well as D2SV runtime.
:::
  * d2svtest-version.properties - Contains name, version and base package for Java classes defined by this plugin. This file is read by `D2SVTESTVersion` class and supplies
the metadata information to D2SV plugin system.
:::danger
Changing content of this file may render D2SV runtime to correctly load class files from the deployed plugin jar.
:::

* `src/main/smartview/` - Home of the NPM project that represents the Javascript and related source code for D2SV UI.
:::danger
Even though the source code here is layed out as an NPM project but you should never execute `npm install` command in this
directory. Doing so will completely break the setup as the `node_modules` folder is a softlinked directory and managed by
the `package.json` from SDK workspace root.
:::
:::tip
If for some reason an additional NPM based dependency is required for a plugins UI code, the dependency should be added in the `package.json` from
SDK workspace root, subsequently followed by `npm install` command execution in the workspace root directory itself. After that the plugin specific
Javascript code can refer to it in usual manner.
:::
  * `lib` - Shortcut to the `lib` directory from SDK workspace root. The directory hosts all D2SV UI dependency libraries, that are used while running the Javascript only portion through a NodeJS light-server.
  * `node_modules` - Shortcut to the `node_modules` directory from SDK workspace root. The directory hosts all NPM packages used to test, build the Javascript source code and pack those into distributable format, besides
  serving them through NodeJS server for debugging/testing purposes.
  * pages/debug/app.html -  HTML page used to run the application with as-is source code using NodeJS server.
  * pages/release/app.html - HTML page used to run the application with "built" source code using NodeJS server.
  * pages/config-d2.js - Primary `RequireJS` configuration used to run the application using NodeJS server.
  * pages/config-dev.js - Additional `RequireJS` configuration to run the application using NodeJS server.
  * pages/favicon.ico - Tab icon for the browser to use while accessing the application started using NodeJS server.
  * pages/initialize.js - Primary Javascript loaded by the HTML pages to kick-off the bootstrapping of application.
  * pages/loader.css - Basic styling using during bootstrap phase of the application.
  * pages/otds.html - Token capturer page used when the back-end is configured with OTDS for authentication.
  * config-editor.js - Temporary RequireJS configuration override file used while building the UI code.
  * d2svtest.setup.js - NodeJS module to setup/re-instate the directory softlinks for `lib` and `node_modules`.
  * esmhelper.js - ESM to AMD conversion helper used while running the application using NodeJS server.
  * Gruntfile.js - Master task definition file used by `Grunt` while testing/building the source code.
  * package.json - NPM package manifest for the UI code.
  * proxy.js - Creates local equivalent of a foreign URL through HTTP proxy.
  * server.conf.js - Lets configure the remote URL to use as back-end while serving the application through NodeJS.
  * server.js - Javascript module to spawn a NodeJS server that supplies D2SV application to the browser.
  * .csslintrc - Linter rules for CSS files. Used to validate if all CSS source files meet D2SV standard.
  * .eslintrc.yml - Linter rules for Javascript files. Used to validate if all JS source code meet D2SV standard.
  * .eslintrc-html.yml - Linter rules for HTML & HBS(Handlebars HTML template) files. Used to validate if matching source files meet D2SV standard.
  * .npmrc - Local NPM configuration, used by NodeJS engine before running any NPM/NodeJS scripts.
  :::info
  All these HTML, JS, JSON, CSS etc files are not part of the actual plugin source code. Some of them facilitate build, test, packaging of the actual
  source code whereas, others enable serving the entire front-end of D2SV application including this plugin and connects it to a remote back-end
  for testing/debugging purposes. Any of these files should not be modified.
  :::
  * src/bundles/d2svtest-bundle.js - Plugin UI bundle, it must contain direct/indirect reference to all AMD modules defined in this plugin so that they are correctly packaged during build.
  :::info
  AMD modules, that are referred by atleast one other AMD module through `define()`, are called statically referred modules. Dynamically referred AMD modules are
  never referred by any other module using `define()` rather they are referred only through `require()`. The UI bundle must contain direct reference to all dynamically referred modules.
  It's very important to know that for an entry, all other statically referred modules from that module are automatically added e.g. suppose, A statically refers to B and that statically refers to C but C dynamically refers to D,
  in this scenario, you should put an entry for A and an entry for C to completely refer all the modules A, B, C, D.
  :::
  * src/test/extensions.spec.js - Sample unit test. Shows how to write unit tests for each JS module in this plugin.
  * src/utils/theme/action_icons/action_sample_icon.svg - A sample icon resource, could be used to represent a menu action. To know more about 
  action icons and how to use them in UI refer to [Use icons in D2SV](./howto/1_05_use_icons)
  * src/utils/theme/action.icons.js - Holder of all the action icons. This file is auto-generated everytime
  the UI code is being built. 
  :::tip
  After adding a new svg file in `action_icons` directory, you need to build the UI code once to re-generate this
  holder file.
  :::
  * src/utils/startup.js - Hooks to D2SV application startup phase. Generally used to run additional
  custom logic that has to happen during the startup i.e before any of the UI components starts to render on the page.
  Provides two hook points, namely `beforeStartup()` and `afterStartup()` their purpose is pretty much self explanatory.
  * src/component.js - Declares the UI bundles in this plugin. Used while building the UI code as well as used 
  by `esmhelper.js` while serving the code through NodeJS server. This file normally wouldn't require any change unless the UI source code
  declares a RequireJS plugin.
  :::caution
  At this time only one UI bundle per D2SV plugin is supported by the runtime. So you must not try to split the `d2svtest-bundle.js` file into multiple
  smaller bundles.
  :::
  * src/config-build.js - RequireJS configuration used while building the UI code. This file is auto-generated
  every time the UI code is being built. So any changes done to this file gets automatically discarded.
  * src/d2svtest-init.js - Used to supply additional RequireJS configuration to AMD modules in this plugin. Also can be
  used to re-configure any other AMD modules defined by the D2SV UI itself or any of its dependencies.
  :::info
  All the plugins deployed on D2 Smartview do not have a mechanism to specify their loading order. So if multiple plugins try to configure the same AMD
  module then whichever plugin is loaded last, configuration from that plugin will apply.
  :::
  * src/extensions.json - Single file to register all the D2SV UI API extensions defined by this plugin.
  * src/Gruntfile.js - Task definition file used to build the source code.
  * test/Gruntfile.js - Task definition file used to start karma server to run unit test on source code/build output.
  * test/karma.js - Karma configuration file used while running unit tests.
  * test/test-common.js - Defines pre-conditions and init configuration used to run unit tests.