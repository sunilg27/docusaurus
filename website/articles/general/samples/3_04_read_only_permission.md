---
sidebar_position: 3.01
sidebar_label: D2SV Read-Only Permission View Sample
---

# D2SV Read-Only Permission View Sample

D2 Read-Only permission view sample plugin fills-in the gap functionally and serves as a complete example of how to use SDK to

* Define a custom action services 
* Define a custom menu by default and initiate a dialog
* Define a custom dialog view to display the information and show the form view of the data

### Instruction to try out the sample

Developer can extract the sample and build it using the workspace assistant. Once built, the distribution is collected in 'dist' folder as **D2SV-ReadOnlyPermission-View-1.0.0.jar**
which can placed in `WEB-INF/lib` directory of a deployed D2 Smartview. The application server needs to be restarted post deployment.

As result of deploying this plugin, it will introduce a new menu in `Doclist` widget as `View Permission` 


### Source code structure

```
D2SV-ReadOnlyPermission-View
    |   pom.xml
    |   
    +---src
    |   \---main
    |       +---java
    |       |   \---com
    |       |       +---emc
    |       |       |       D2PluginVersion.java
    |       |       |       
    |       |       \---opentext
    |       |           \---d2
    |       |               +---rest
    |       |               |   \---context
    |       |               |       \---jc
    |       |               |               PluginRestConfig_D2SVROPView.java
    |       |               |               
    |       |               \---smartview
    |       |                   \---d2svropview
    |       |                       |   D2SVROPViewPlugin.java
    |       |                       |   
    |       |                       +---api
    |       |                       |       D2SVROPViewVersion.java
    |       |                       |       
    |       |                       +---rest
    |       |                       |       package-info.java
    |       |                       |       
    |       |                       \---webfs
    |       |                           \---custom
    |       |                                   PermissionActionService.java
    |       |                                   
    |       +---resources
    |       |   |   D2Plugin.properties
    |       |   |   d2svropview-version.properties
    |       |   |   
    |       |   +---smartview
    |       |   |       SmartView.properties
    |       |   |       
    |       |   +---strings
    |       |   |   \---menu
    |       |   |       \---MenuContext
    |       |   |               MenuContext_en.properties
    |       |   |               
    |       |   \---xml
    |       |       \---unitymenu
    |       |               MenuContextDelta.xml
    |       |               
    |       \---smartview
    |           |   .csslintrc
    |           |   .eslintrc-html.yml
    |           |   .eslintrc.yml
    |           |   .npmrc
    |           |   config-editor.js
    |           |   d2svropview.setup.js
    |           |   esmhelper.js
    |           |   Gruntfile.js
    |           |   package.json
    |           |   proxy.js
    |           |   server.conf.js
    |           |   server.js
    |           |   
    |           +---pages
    |           |   |   config-d2.js
    |           |   |   config-dev.js
    |           |   |   favicon.ico
    |           |   |   initialize.js
    |           |   |   loader.css
    |           |   |   otds.html
    |           |   |   
    |           |   +---debug
    |           |   |       app.html
    |           |   |       
    |           |   \---release
    |           |           app.html
    |           |           
    |           +---src
    |           |   |   component.js
    |           |   |   config-build.js
    |           |   |   d2svropview-init.js
    |           |   |   extensions.json
    |           |   |   Gruntfile.js
    |           |   |   
    |           |   +---bundles
    |           |   |       d2svropview-bundle.js
    |           |   |       
    |           |   +---commands
    |           |   |   |   node.actions.rules.js
    |           |   |   |   view.permission.js
    |           |   |   |   
    |           |   |   \---impl
    |           |   |       \---nls
    |           |   |           |   lang.js
    |           |   |           |   
    |           |   |           \---root
    |           |   |                   lang.js
    |           |   |                   
    |           |   +---dialogs
    |           |   |   \---permissions
    |           |   |       |   permissions.dialog.js
    |           |   |       |   permissions.view.js
    |           |   |       |   
    |           |   |       \---impl
    |           |   |           |   permission.collection.js
    |           |   |           |   permissions.css
    |           |   |           |   permissions.hbs
    |           |   |           |   table.columns.js
    |           |   |           |   
    |           |   |           \---nls
    |           |   |               |   lang.js
    |           |   |               |   
    |           |   |               \---root
    |           |   |                       lang.js
    |           |   |                       
    |           |   +---test
    |           |   |       extensions.spec.js
    |           |   |       
    |           |   \---utils
    |           |       |   startup.js
    |           |       |   
    |           |       \---theme
    |           |           |   action.icons.js
    |           |           |   
    |           |           \---action_icons
    |           |                   action_sample_icon.svg
    |           |                   action_view_perms32.svg
    |           |                   
    |           \---test
    |                   Gruntfile.js
    |                   karma.js
    |                   test-common.js
    |                   
    \---target


```

### Files and their purpose

Following are the list of function oriented source files and their purpose. Other source files present within the plugin are part of 
common infrastructure code and explained in [Understanding D2SV plugin project](../understand.d2sv.plugin).

### REST Implementations
* pom.xml - Defines the maven project for this plugin.
* src/main/java/com/emc/D2PluginVersion.java - Declares identification information for the entire plugin using `D2SVROPViewVersion` class
* src/main/java/com/opentext/d2/rest/context/jc/PluginRestConfig_D2SVROPView.java - Java configuration for spring components like REST controller, Beans etc.
* src/main/java/com/opentext/d2/smartview/d2svropview/D2SVROPViewPlugin.java - Declares a plugin component for D2FS.
* src/main/java/com/opentext/d2/smartview/d2svropview/api/D2SVROPViewVersion.java - Holder for plugin identification information. Loads relevant data from `d2svropview-version.properties` file resource.
* src/main/java/com/opentext/d2/smartview/d2svropview/rest/package-info.java - Declares package metadata for JDK and IDE.
* src/main/java/com/opentext/d2/smartview/d2svropview/rest/webfs.custom/PermissionActionService.java - Defines a custom service to fetch the basic permissions for the given object id.
So the menu will have reference to the method 'getPermissions' which will triggered from the menu action.

### View permission menu configuration in back-end and its display & execution on the front-end
* src/main/resources/strings/menu/MenuContext/MenuContext_en.properties - Labels associated with the dynamically configured menu.
* src/main/resources/xml/unitymenu/MenuContextDelta.xml - This delta menu will be used to dynamically load the custom OOTB menu to view permissions in the D2 Smartview for the default MenuContext.
```
<delta>
  <insert position-before="menuToolsMassUpdate">
    <menuitem id="menuContextViewPermission">
      <dynamic-action class="com.emc.d2fs.dctm.ui.dynamicactions.actions.U4Generic" eMethod="getPermissions" eMode="SINGLE" eService="PermissionActionService" rAction="d2svropview/dialogs/permissions/permissions.dialog:showPermissions" rType="JS"/>
      <condition class="com.emc.d2fs.dctm.ui.conditions.interfaces.IsMultipleSelection" equals="false"/>
      <condition class="com.emc.d2fs.dctm.ui.conditions.options.IsPluginActivated" value="D2SV-ReadOnlyPermission-View"/>
    </menuitem>
  </insert>
  <insert position-before="menuToolsMassUpdate">
    <separator/>
  </insert>
</delta>
```
Here the `dynamic-action` is used to map the method `getPermissions` in `PermissionActionService` when the menuAction is triggered from UI. 
`dynamic-action` will also have reference to the target UI action to perform using the `rAction`


* src/main/smartview/src/bundles/d2svropview-bundle.js - A portion of this file is used to refer to key RequireJS modules that define the extensions to the toolbar and menu related D2SV UI API.
```
define([
    'd2svropview/utils/theme/action.icons',
    'd2svropview/utils/startup',
    'd2svropview/commands/node.actions.rules',
    'd2svropview/commands/view.permission',
], {});
```
* src/main/smartview/src/commands/view.permission.js - A `CommandModel` that implements the executable logic when a user clicks the `View Permission` menu on the UI. 
It is an extension of `CallServiceCommand` which is used to take care of the forming the service method request
* src/main/smartview/src/commands/node.actions.rules.js - Defines client side filtering and association logic to attach the above `view.permission.js` command model implementaion to a UI toolbar item.
* src/main/smartview/src/utils/startup.js - Runs as part of D2 Smartview client-side startup flow. This flow is executed everytime endusers reload the D2 Smartview application in their internet browser.
* src/main/smartview/src/extensions.json - A portion of this file registers extensions to the toolbar and menu related D2SV UI API. The corresponding portion is highlighted below
```
  "d2/sdk/controls/action.icons/action.icons": {
    "extensions": {
      "d2svropview": [
        "d2svropview/utils/theme/action.icons"
      ]
    }
  },
  "d2/sdk/commands/node.actions.rules": {
    "extensions": {
      "d2svropview": [
        "d2svropview/commands/node.actions.rules"
      ]
    }
  },
  "d2/sdk/utils/commands": {
    "extensions": {
      "d2svropview": [
        "d2svropview/commands/view.permission"
      ]
    }
  }
```


### The side-panel dialog that displays permissions


* src/main/smartview/src/bundles/d2svropview-bundle.js - A portion of this file is used to refer to key RequireJS modules that define the extensions to dialog used for the side panel as part of the response from the menu action.
```
define([
    ...
    'd2svropview/dialogs/permissions/permissions.dialog'
], {});
```
* src/main/smartview/src/dialogs/permissions/permissions.dialog.js - This dialog will be used to show a stepper wizard view.
The stepper wizard will be a single step having `permissions.view`. 
* src/main/smartview/src/dialogs/permissions/permissions.view.js - The view will be used to render data as table view. Data returned 
as part fo the response from the `PermissionActionService` be managed by a `MarionetteJS` `Collection` `permission.collection.js`.
* src/main/smartview/src/dialogs/permissions/impl/table.columns.js -  Its a `Backbone JS` collection which is used to map the columns 
information such as the key,title etc

Example:
```
{
      key: 'base_permission',
      column_key: 'base_permission',
      sequence: 3,
      sort: false,
      definitions_order: 3,
      title: lang.colNameBasePermissions,
      type: -1,
      widthFactor: 0.7,
      permanentColumn: true // don't wrap column due to responsiveness into details row
}
```
* src/main/smartview/src/extensions.json - A portion of this file registers extensions toward collections used in handling the list of permissions. The corresponding portion is highlighted below
```
"d2/sdk/models/module.collection": {
    "modules": {
      "d2svropview": {
        "title": "D2SV-ReadOnlyPermission-View",
        "version": "1.0.0"
      }
    }
  }
```
* src/main/smartview/src/dialogs/permissions/impl/permissions.collection.js - Collection is used to parse the unformatted response data from the
`PermissionActionService` to collection of models. This collection is included in `BrowsableMixin` to have filter and sorting capability of the result set.
 
