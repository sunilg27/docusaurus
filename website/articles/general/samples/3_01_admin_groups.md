---
sidebar_position: 3.01
sidebar_label: D2 Admin-Groups Sample
---

# D2 Admin-Groups Sample

D2 Smartview does not ship with an Out-Of-The-Box(OOTB) group management widget like D2 Classic. However, the 
D2 Admin-Groups sample plugin fills-in the gap functionally and serves as a complete example of how to use SDK to

* Define a landing page widget tile.
* Define a perspective and stich it up with the landing page widget.
* Define and use a custom menu type to go with the widget.
* Define a few REST endpoints to serve data to the widget.

### Instruction to try out the sample
As this plugin implements a landing page tile as part of it, some configuration changes are required in the D2 Smartview landing page
before the tile is made available for the end users. Here are the list of steps required to completely deploy and configure this plugin.

* Build the plugin using `npm run build` from SDK workspace root.
* Copy `D2-AdminGroups-1.0.0.jar` from 'dist' folder in workspace root and paste it inside `WEB-INF/lib` folder of a deployed D2 Smartview application.
* Restart application server on which D2 Smartview is deployed.
* Open D2-Config web application in browser, login and then navigate to **Widget view -> Widget...** from menu.
* Create a new widget configuration, and put "Manage Groups"(or whatever you wish) as **Name** and select "AdminGroupsWidget" for **Widget type** field.
* Fill-in other fields as necessary and save the configuration.
* From the toolbar, click **Matrix** to go to D2-config matrix and enable the widget configuration, you just created, against appropriate contexts.
* Select **Widget view -> Smart View Landing Page...** from menubar to navigate to landing page configurations.
* Select an applicable configuration from the left side and click **Download** to get the structure file locally and then open in notepad to edit.
:::tip
If a pre-created Smartview landing page configuration does not exist, then refer to D2 Administration Guide documentation to create the same and learn basics of
landing page structure file.
:::
* Paste the following piece of xml anywhere right under the `<content>` tag
```
<widget-container>
    <widget name="Manage Groups" type="AdminGroupsWidget"/>
</widget-container>
```
:::tip
If you've used a different name while creating the widget configuration, use that name as the value for **name** attribute.
:::
* Save the landing structure xml file and upload it to D2-Config under the same landing page configuration from where we downloaded it before.
* Save the configuration change in D2-Config and click **Tools -> Refresh cache** from menubar.
* Reloading the D2 Smartview at this point should show an additional widget in the landing page, similar to following
![Admin Groups Widget](/images/D2-AdminGroups.png)

### Source code structure

```
D2-AdminGroups
|
|
|   pom.xml
|   
+---src
|   \---main
|       +---java
|       |   \---com
|       |       +---emc
|       |       |       D2PluginVersion.java
|       |       |       
|       |       \---opentext
|       |           \---d2
|       |               +---rest
|       |               |   \---context
|       |               |       \---jc
|       |               |               PluginRestConfig_AdminGroups.java
|       |               |               
|       |               \---smartview
|       |                   \---admingroups
|       |                       |   AdminGroupsPlugin.java
|       |                       |   
|       |                       +---api
|       |                       |       AdminGroupsVersion.java
|       |                       |       
|       |                       \---rest
|       |                           |   package-info.java
|       |                           |   
|       |                           +---controller
|       |                           |       AdminGroupsController.java
|       |                           |       AdminGroupsMembersController.java
|       |                           |       
|       |                           +---dfc
|       |                           |   |   AdminGroupsManager.java
|       |                           |   |   
|       |                           |   \---impl
|       |                           |           AdminGroupsManagerImpl.java
|       |                           |           
|       |                           +---model
|       |                           |       GroupMembers.java
|       |                           |       GroupModel.java
|       |                           |       UserModel.java
|       |                           |       
|       |                           \---view
|       |                                   GroupMembersFeedView.java
|       |                                   GroupsFeedView.java
|       |                                   GroupView.java
|       |                                   UsersFeedView.java
|       |                                   UserView.java
|       |                                   
|       +---resources
|       |   |   admingroups-version.properties
|       |   |   D2Plugin.properties
|       |   |   
|       |   +---smartview
|       |   |       SmartView.properties
|       |   |       
|       |   +---strings
|       |   |   \---menu
|       |   |       \---PMenuContextAdminGroups
|       |   |               PMenuContextAdminGroups_en.properties
|       |   |               
|       |   \---xml
|       |       \---unitymenu
|       |               PMenuContextAdminGroups.xml
|       |               
|       \---smartview
|           |   .csslintrc
|           |   .eslintrc-html.yml
|           |   .eslintrc.yml
|           |   .npmrc
|           |   admingroups.setup.js
|           |   config-editor.js
|           |   esmhelper.js
|           |   Gruntfile.js
|           |   package.json
|           |   proxy.js
|           |   server.conf.js
|           |   server.js
|           |           
|           +---pages
|           |   |   config-d2.js
|           |   |   config-dev.js
|           |   |   favicon.ico
|           |   |   initialize.js
|           |   |   loader.css
|           |   |   otds.html
|           |   |   redirect.html
|           |   |   
|           |   +---debug
|           |   |       app.html
|           |   |       
|           |   \---release
|           |           app.html
|           |           
|           +---src
|           |   |   admingroups-init.js
|           |   |   component.js
|           |   |   config-build.js
|           |   |   extensions.json
|           |   |   Gruntfile.js
|           |   |   
|           |   +---bundles
|           |   |       admingroups-bundle.js
|           |   |       
|           |   +---commands
|           |   |   |   manage.group.js
|           |   |   |   node.actions.rules.js
|           |   |   |   
|           |   |   \---nls
|           |   |       |   lang.js
|           |   |       |   
|           |   |       \---root
|           |   |               lang.js
|           |   |               
|           |   +---dialogs
|           |   |   \---manage.group
|           |   |       |   manage.group.dialog.js
|           |   |       |   manage.group.view.js
|           |   |       |   
|           |   |       \---impl
|           |   |           |   group.members.form.view.js
|           |   |           |   manage.group.css
|           |   |           |   manage.group.hbs
|           |   |           |   
|           |   |           \---nls
|           |   |               |   lang.js
|           |   |               |   
|           |   |               \---root
|           |   |                       lang.js
|           |   |                       
|           |   +---extensions
|           |   |   |   admin.groups.icon.sprites.js
|           |   |   |   admin.groups.perspective.js
|           |   |   |   admin.groups.tile.js
|           |   |   |   
|           |   |   \---nls
|           |   |       |   lang.js
|           |   |       |   
|           |   |       \---root
|           |   |               lang.js
|           |   |               
|           |   +---models
|           |   |       admin.groups.collection.js
|           |   |       group.members.collection.js
|           |   |       group.model.js
|           |   |       member.model.js
|           |   |       
|           |   +---test
|           |   |       extensions.spec.js
|           |   |       
|           |   +---utils
|           |   |   |   alert.util.js
|           |   |   |   constants.js
|           |   |   |   menu.utils.js
|           |   |   |   startup.js
|           |   |   |   
|           |   |   +---contexts
|           |   |   |   \---factories
|           |   |   |           admin.groups.collection.factory.js
|           |   |   |           next.group.factory.js
|           |   |   |           
|           |   |   +---perspectives
|           |   |   |       admin.groups.perspective.json
|           |   |   |       
|           |   |   \---theme
|           |   |       |   action.icons.js
|           |   |       |   
|           |   |       \---action_icons
|           |   |               action_sample_icon.svg
|           |   |               
|           |   \---widgets
|           |       +---admin.groups
|           |       |   |   admin.groups.manifest.json
|           |       |   |   admin.groups.view.js
|           |       |   |   toolitems.js
|           |       |   |   
|           |       |   \---impl
|           |       |       |   admin.groups.css
|           |       |       |   
|           |       |       +---images
|           |       |       |       group-svgrepo-com.svg
|           |       |       |       
|           |       |       \---nls
|           |       |           |   admin.groups.manifest.js
|           |       |           |   lang.js
|           |       |           |   
|           |       |           \---root
|           |       |                   admin.groups.manifest.js
|           |       |                   lang.js
|           |       |                   
|           |       \---admin.groups.members
|           |           |   admin.groups.members.view.js
|           |           |   
|           |           \---impl
|           |               |   admin.groups.members.css
|           |               |   
|           |               \---nls
|           |                   |   lang.js
|           |                   |   
|           |                   \---root
|           |                           lang.js
|           |                           
|           \---test
|                   Gruntfile.js
|                   karma.js
|                   test-common.js
|                   
\---target

```

#### Files and their purpose

Following are the list of function oriented source files and their purpose. Other source files present within the plugin are part of 
common infrastructure code and explained in [Understanding D2SV plugin project](../understand.d2sv.plugin).

###### REST Controller
* src/main/java/com/opentext/d2/rest/context/jc/PluginRestConfig_AdminGroups.java - Declares Spring Bean `AdminGroupsManager` through `AdminGroupsManagerImpl`.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/controller/AdminGroupsController.java - Defines a REST controller with two endpoints
to list all the users and groups from Documentum.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/controller/AdminGroupsMembersController.java - Defines a REST controller with two endpoints
to list and edit members of a group.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/dfc/AdminGroupsManager.java - Declares the data manager interface used by above REST controllers
to get/set the data they deal with.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/dfc/impl/AdminGroupsManagerImpl.java - Data manager that interacts with Documentum
through DQL and exchanges data as per `AdminGroupsManager` interface.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/model/GroupMembers.java - Serializable POJO that represents members of a group while editing.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/model/GroupModel.java - Serializable POJO that represents a single group.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/model/UserModel.java - Serializable POJO that represents a single user.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/view/GroupMembersFeedView.java - Spring view used to wrap and serialize a list of group member data. Uses `UserView` in turn to serialize each individual member.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/view/GroupsFeedView.java - Spring view used to wrap and serialize a list of group data. Uses `GroupView` in turn to serialize each individual group.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/view/GroupView.java - Spring view used to seralize a single group data.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/view/UsersFeedView.java - Spring view used to wrap and serialize a list of user data. Uses `UserView` in turn to serialize each individual user.
* src/main/java/com/opentext/d2/smartview/admingroups/rest/view/UserView.java - Spring view used to serialize a single user data.

###### Group-manage menu configuration in back-end and its display & execution on the front-end
* src/main/resources/strings/menu/PMenuContextAdminGroups/PMenuContextAdminGroups_en.properties - Labels associated with the dynamically configured menu.
* src/main/resources/xml/unitymenu/PMenuContextAdminGroups.xml - The menu definition file that dynamically adds a new type(PMenuContextAdminGroups) of menu for the D2FS `D2MenuService` to discover and return for D2 Smartview.
* src/main/smartview/src/bundles/admingroups-bundle.js - A portion of this file is used to refer to key RequireJS modules that define the extensions to the toolbar and menu related D2SV UI API.
```
define([
    ...
    'admingroups/utils/startup',
    'admingroups/commands/node.actions.rules',
    'admingroups/commands/manage.group',
    ...
], {});
```
* src/main/smartview/src/commands/manage.group.js - A `CommandModel` that implements the executable logic when a user clicks the `Manage` menu on the UI. It dynamically loads and displays `manage.group.dialog.js` dialog.
When the dialog closes(without cancel flag), it makes an AJAX call to group-update related REST endpoint created by the Java code from above and then finally shows a toast message on successful
completion.
* src/main/smartview/src/commands/node.actions.rules.js - Defines client side filtering and association logic to attach the above `manage.group.js` command model implementaion to a UI toolbar item.
* src/main/smartview/src/utils/menu.utils.js - Parses the data for `PMenuContextAdminGroups` type of menu into a client-side toolbar definition that in turn is used by landing page tile & perspective.
* src/main/smartview/src/utils/startup.js - Runs as part of D2 Smartview client-side startup flow. This flow is executed everytime endusers reload the D2 Smartview application in their internet browser.
As part of this startup hook, an AJAX call is made to get the menu configuration data for type `PMenuContextAdminGroups`, then the response is trasformed into a toolbar definition by `menu.utils.js`.
* src/main/smartview/src/extensions.json - A portion of this file registers extensions to the toolbar and menu related D2SV UI API. The corresponding portion is highlighted below
```
"d2/sdk/commands/node.actions.rules": {
    "extensions": {
      "admingroups": [
        "admingroups/commands/node.actions.rules"
      ]
    }
  },
  "d2/sdk/utils/commands": {
    "extensions": {
      "admingroups": [
        "admingroups/commands/manage.group"
      ]
    }
  }
```

###### Tile on the landing page
* src/main/smartview/src/bundles/admingroups-bundle.js - A portion of this file refers to the the RequireJS modules that 
implement the landing page tile. These references are only used for RequireJS modules that are otherwise not referenced statically 
from any other RequireJS module.
```
define([
   ...
   'admingroups/extensions/admin.groups.icon.sprites',
   ...
   'admingroups/extensions/admin.groups.tile',
   'admingroups/widgets/admin.groups/admin.groups.view'
   ...
   'json!admingroups/widgets/admin.groups/admin.groups.manifest.json',
   'i18n!admingroups/widgets/admin.groups/impl/nls/admin.groups.manifest'
], {});
```
* src/main/smartview/src/extensions/admin.groups.icon.sprites.js - Defines the icon to represent a user and a group by means of extension.
* src/main/smartview/src/extensions/admin.groups.tile.js - Declares a new widget type handler for the landing page tiles by means of extension.
* src/main/smartview/src/extensions.json - A portion of this file registers extensions to the landing page tile & icon related D2SV UI API. The corresponding portion is highlighted below
```
"d2/sdk/utils/landingpage/tiles": {
    "extensions": {
      "admingroups": [
        "admingroups/extensions/admin.groups.tile"
      ]
    }
  },
  ...
  "d2/sdk/controls/icon.sprites/node.icon.sprites": {
      "extensions": {
        "admingroups": [
          "admingroups/extensions/admin.groups.icon.sprites"
        ]
      }
    }
```
* src/main/smartview/src/models/admin.groups.collection.js - A `BackboneJS` collection that holds all available groups data. 
Uses `group.model.js` to represent each group in the collection. Makes an AJAX call to one of the REST endpoint, created by Java code from above, to get the available groups data.
* src/main/smartview/src/models/group.model.js - A `BackboneJS` model that holds data for a single group.
* src/main/smartview/src/utils/contexts/factories/admin.groups.collection.factory.js - A factory to control creation of initialized/uninitialized group collection instances.
* src/main/smartview/src/utils/contexts/factories/next.group.factory.js - A factory to control creation of group-model instances. Purpose
of this factory is to create a single instance of group model and use that to reflect current selected group item in the UI. This instance is used by `admin.groups.view.js` to constantly
update the selected group data as user clicks an item in the list of visible groups in UI.
* src/main/smartview/src/utils/constants.js - A portion of the file defines a few frequently used constant values in the context of landing tile implementation.
* src/main/smartview/src/widgets/admin.groups/admin.groups.view.js - A `MarionetteJS` view that implements the UI and function for the widget. An instance of this view
is dynamically created by the D2SV runtime and this instance manages and renders DOM elements to represent the list of all available groups. It also handles user interaction with the DOM elements.
* src/main/smartview/src/widgets/admin.groups/toolitems.js - The toolbar configuration used by `admin.groups.view.js` to display inline `Manage` menu. An instance of it is
manipulated by `menu.utils.js` to dynamically inject the menu items in the toolbar at the time of D2SV UI startup.

###### The perspective, landing tile expands into
The perspective is defined in a two panel layout where the left-side re-uses the same `admin.groups.view.js` from landing page tile. Apart from the RequireJS modules
and other source code resources referred by the landing page tile, here's the other files involved in defining the perspective itself besides the right-side part of it.

* src/main/smartview/src/bundles/admingroups-bundle.js - A portion of this file refers to the the RequireJS modules that 
implement the perspective and right-side part of it. These references are only used for RequireJS modules that are otherwise not referenced statically 
from any other RequireJS module.
```
define([
   ...
   'admingroups/extensions/admin.groups.perspective',
   ...
   'admingroups/widgets/admin.groups.members/admin.groups.members.view',
   'json!admingroups/utils/perspectives/admin.groups.perspective.json',
   ...
], {});
```
* src/main/smartview/src/extensions/admin.groups.perspective.js - Declares an application scope handler and associates a perspective definition file with it.
* src/main/smartview/src/extensions.json - A portion of this file registers extensions toward application scope perspective related D2SV UI API. The corresponding portion is highlighted below
```
"d2/sdk/utils/perspectives/app.scope.perspectives": {
    "extensions": {
      "admingroups": [
        "admingroups/extensions/admin.groups.perspective"
      ]
    }
  }
```
* src/main/smartview/src/models/group.members.collection.js - A `BackboneJS` collection that holds membership data for a given group. Uses `member.model.js` to
represent each member within the group. Makes an AJAX call to one of the REST endpoint, created by Java code from above, to get the members data for a selected group.
* src/main/smartview/src/models/member.model.js - A `BackboneJS` model that holds data for a member within a group.
* src/main/smartview/src/utils/contexts/factories/next.group.factory.js - A factory to control creation of group-model instances. Purpose
  of this factory is to create a single instance of group model and use that to reflect current selected group item in UI. This instance is used by `admin.groups.members.view.js` to constantly
  monitor change to the selected group in UI.
* src/main/smartview/src/utils/perspectives/admin.groups.perspective.json - Defines the layout for the perspective and associates `admin.groups` & `admin.groups.members`
as the widgets to go on the left and right side respectively. The D2SV runtime dynamically creates the associated view instances when the perspective comes alive.
* src/main/smartview/src/widgets/admin.groups.members/admin.groups.members.view.js - A `MarionetteJS` view implementation that displays the members of a 
selected group. An instance of this view is dynamically created by D2SV runtime to show list of members in the perspective. The instance automatically updates itself as a result
of end users selecting a group from the left-side by means of constant watch over the group model instance acquired using `next.group.factory.js`.

###### The side-panel dialog that manages group member
* src/main/smartview/src/bundles/admingroups-bundle.js - A portion of this file refers to the the RequireJS modules that 
implements the dialog. These references are only used for RequireJS modules that are otherwise not referenced statically 
from any other RequireJS module.
```
define([
   ...
   'admingroups/dialogs/manage.group/manage.group.dialog',
   ...
], {});
```
* src/main/smartview/src/dialogs/manage.group/manage.group.dialog.js - Uses D2SV UI API to create a side panel and host an instance of `manage.group.view.js` to show the related UI.
Also collects information about updated group members and relays that to the caller.
* src/main/smartview/src/dialogs/manage.group/manage.group.view.js - A `MarionetteJS` view that wraps an instance of `group.members.form.view.js` to make it renderable within the side panel and 
defers the instance creation & rendering until required membership data has been fetched through an instance of `group.members.collection.js`. Also defines utility methods to have a check on whether the membership data has changed from what is loaded initially.
Uses `manage.group.hbs` & `manage.group.css` files for HTML templating and CSS styling respectively.
* src/main/smartview/src/dialogs/manage.group/impl/group.members.form.view.js - Uses D2SV UI API to create a statically defined form with multi-select list field to show membership information for the selcted group.
Also makes an AJAX call to one of the REST endpoint, created by Java code from above, to get all available users data that serves as the options shown while editing the membership data. Also defines utility method to get the membership
information for the selected group at any time.