---
sidebar_position: 3.00
sidebar_label: Overview
---

# Packaged Samples

D2SV SDK includes a few sample plugins as part of its distribution. They could be 
extracted in a workspace by using [Checkout some samples](../ws_assistant/2_08_extract.sample) option of
the workspace assistant.

## List of samples

* [D2 Admin Groups Sample](./3_01_admin_groups)
* [D2SV client to server logging](./3_02_client_to_server_logging)
* [D2SV Cutom Dialogs(D2FS) sample](./3_03_custom_dialog)
* [D2SV Read-Only permission display](./3_04_read_only_permission)
