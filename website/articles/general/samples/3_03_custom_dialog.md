---
sidebar_position: 3.03
sidebar_label: D2SV Cutom Dialogs(D2FS) sample
---

# D2SV Cutom Dialogs(D2FS) sample

D2 Custom Dialog sample provide an option to modify the metadata for a document with any available properties page created in D2-Config.
As out of the box, the document metadata can be modified only using the properties page which is resolved after configuration matrix against the context.

This sample shows
* How to define a D2 Dialog service plugin which implements ID2fsPlugin.
* How to define a D2FS state method to make dialog chaining as context less. So that the last step Submit will be performed on OOTB property dialog service instead of original dialog service.

### Instruction to try out the sample

* Build the plugin using `npm run build` from SDK workspace root.
* Copy `D2SV-Custom-Dialogs-1.0.0.jar` from 'dist' folder in workspace root and paste it inside `WEB-INF/lib` folder of a deployed D2 Smartview application.
* Restart application server on which D2 Smartview is deployed.
* Open D2-Config web application in browser, login and then create few properties page configuration.

### Source code structure

```
D2SV-Custom-Dialogs
|   pom.xml
|   
+---src
|   \---main
|       +---java
|       |   \---com
|       |       +---emc
|       |       |       D2PluginVersion.java
|       |       |       
|       |       \---opentext
|       |           \---d2
|       |               +---rest
|       |               |   \---context
|       |               |       \---jc
|       |               |               PluginRestConfig_D2SVDialogs.java
|       |               |               
|       |               \---smartview
|       |                   \---d2svdialogs
|       |                       |   D2SVDialogsPlugin.java
|       |                       |   
|       |                       +---api
|       |                       |       D2SVDialogsVersion.java
|       |                       |       
|       |                       +---dialogs
|       |                       |       SelectivePropertyDisplay.java
|       |                       |       
|       |                       +---rest
|       |                       |       package-info.java
|       |                       |       
|       |                       \---webfs
|       |                           \---dialog
|       |                                   D2DialogServicePlugin.java
|       |                                   
|       +---resources
|       |   |   D2Plugin.properties
|       |   |   d2svdialogs-version.properties
|       |   |   
|       |   +---smartview
|       |   |       SmartView.properties
|       |   |       
|       |   +---strings
|       |   |   +---dialog
|       |   |   |   \---SelectivePropertyDisplay
|       |   |   |           SelectivePropertyDisplay_en.properties
|       |   |   |           
|       |   |   \---menu
|       |   |       \---MenuContext
|       |   |               MenuContext_en.properties
|       |   |               
|       |   \---xml
|       |       +---dialog
|       |       |       SelectivePropertyDisplay.xml
|       |       |       
|       |       \---unitymenu
|       |               MenuContextDelta.xml
|       |               
|       \---smartview
|           |   .csslintrc
|           |   .eslintrc-html.yml
|           |   .eslintrc.yml
|           |   .npmrc
|           |   config-editor.js
|           |   d2svdialogs.setup.js
|           |   esmhelper.js
|           |   Gruntfile.js
|           |   package.json
|           |   proxy.js
|           |   server.conf.js
|           |   server.js
|           |   
|           +---pages
|           |   |   config-d2.js
|           |   |   config-dev.js
|           |   |   favicon.ico
|           |   |   initialize.js
|           |   |   loader.css
|           |   |   otds.html
|           |   |   
|           |   +---debug
|           |   |       app.html
|           |   |       
|           |   \---release
|           |           app.html
|           |           
|           +---src
|           |   |   component.js
|           |   |   config-build.js
|           |   |   d2svdialogs-init.js
|           |   |   extensions.json
|           |   |   Gruntfile.js
|           |   |   
|           |   +---bundles
|           |   |       d2svdialogs-bundle.js
|           |   |       
|           |   +---dialogs
|           |   |   \---d2fs
|           |   |           context.less.d2fs.state.method.js
|           |   |           
|           |   +---extensions
|           |   |       dialog.state.methods.js
|           |   |       
|           |   +---test
|           |   |       extensions.spec.js
|           |   |       
|           |   \---utils
|           |       |   startup.js
|           |       |   
|           |       \---theme
|           |           |   action.icons.js
|           |           |   
|           |           \---action_icons
|           |                   action_sample_icon.svg
|           |                   
|           \---test
|                   Gruntfile.js
|                   karma.js
|                   test-common.js
|                   
\---target
```

#### Files and their purpose

Following are the list of function oriented source files and their purpose. Other source files present within the plugin are part of 
common infrastructure code and explained in [Understanding D2SV plugin project](../understand.d2sv.plugin).

###### Java Classes
* src/main/java/com/opentext/d2/smartview/d2svdialogs/dialogs/SelectivePropertyDisplay.java - Dialog class which implements ID2Dialog to serve the dialog for selecting the properties page configuration.
* src/main/java/com/opentext/d2/smartview/d2svdialogs/webfs/dialog/D2DialogServicePlugin.java - Dialog service class which implements ID2fsPlugin interface  for validating the dialog request.

###### Dialog form definition
* src/main/resources/xml/dialog/SelectivePropertyDisplay.xml - Defines the form structure for rendering "SelectivePropertyDisplay" dialog. The same file will be processed in "src/main/java/com/opentext/d2/smartview/d2svdialogs/dialogs/SelectivePropertyDisplay.java"
* src/main/resources/strings/dialog/SelectivePropertyDisplay/SelectivePropertyDisplay_en.properties - Label associated with the dialog.

###### Custom dialog menu configuration in back-end
* src/main/resources/strings/menu/MenuContext/MenuContext_en.properties - Labels associated with the dynamically configured menu.
* src/main/resources/xml/unitymenu/MenuContextDelta.xml - The menu definition file that dynamically adds a new type(MenuContext) of menu for the D2FS `D2MenuService` to discover and return for D2 Smartview.

###### Dialog state method override
As part of dialog state customization added extension for dialog state methods. 
This state method will be resolved based on "SelectivePropertyDisplay" dialog name. 
Intension of having custom dialog state method to override the default behavior of dialog state. 
With this override dialog state is decoupled between first form and second form. 
* src/main/smartview/src/dialogs/d2fs/context.less.d2fs.state.method.js - This is a client side JavaScript file extends "d2/sdk/controls/dialogs/generic/d2fs.state.method". Dialog context is decoupled by having dummy override for "setDialogContextForm()" method.
* src/main/smartview/src/extensions/dialog.state.methods.js - This file is having rule for resolving the dialog state method based on dialog name. 
* src/main/smartview/src/extensions.json - Adding the rule for dialog.state.method.
```
  "d2/sdk/controls/dialogs/generic/dialog.state.methods": {
    "extensions": {
      "d2svdialogs": [
        "d2svdialogs/extensions/dialog.state.methods"
      ]
    }
  }
```