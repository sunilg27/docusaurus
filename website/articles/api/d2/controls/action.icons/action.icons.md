<a name="module_ActionIconRegistry"></a>

## ActionIconRegistry : <code>d2/sdk/controls/action.icons/action.icons</code>
Action icon registry. Allows registering new action icons through extension.Also provides managed API to look up a registered action icon given its name.New action icons can be registered through extension

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(to register an extension, add in extension.json)*  
```js

{
  "d2/sdk/controls/action.icons/action.icons": {
     "extensions": {
        "mybundle": {
           "mybundle/utils/theme/action.icons"
        }
     }
   }
}
```
**Example** *(whereas action.icons.js is defined as)*  
```js

define([], function(){
   'use strict';

   return {
     'mybundle_action_1': '<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32" class="csui-icon-v2 csui-icon-v2__d2_action_abort_workflow"><path d="M16,30c7.732,0,14-6.268,14-14c0-7.732-6.268-14-14-14C8.268,2,2,8.268,2,16 C2,23.732,8.268,30,16,30z" fill="none" class="csui-icon-v2-state"/><path d="M16,8c-4.418,0-8,3.582-8,8c0,4.418,3.582,8,8,8s8-3.582,8-8C24,11.582,20.418,8,16,8z M9.778,16 c0-3.436,2.786-6.222,6.222-6.222c1.373,0,2.641,0.444,3.67,1.197l-8.695,8.695C10.222,18.641,9.778,17.372,9.778,16z M16,22.222 c-1.423,0-2.734-0.478-3.783-1.281l8.724-8.724c0.804,1.049,1.281,2.36,1.281,3.783C22.222,19.436,19.436,22.222,16,22.222z" fill="#333333" class="csui-icon-v2-metaphor0"/><path d="M16,31.5c8.56,0,15.5-6.94,15.5-15.5c0-8.56-6.94-15.5-15.5-15.5C7.44,0.5,0.5,7.44,0.5,16 C0.5,24.56,7.44,31.5,16,31.5z" fill="none" stroke="#2E3D98" class="csui-icon-v2-focus"/></svg>',
     'mybunle_action_2': '<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 32 32" class="csui-icon-v2 csui-icon-v2__d2_action_view_notes"><circle cx="16" cy="16" r="14" fill="none" class="csui-icon-v2-state"/><g><path d="M20,7h-9C9.897,7,9,7.888,9,8.98v13.859c0,1.092,0.897,1.979,2,1.979h10c1.103,0,2-0.888,2-1.979V9.97L20,7z M22,22.839c0,0.54-0.448,0.979-1,0.979H11c-0.551,0-1-0.439-1-0.979V8.98C10,8.439,10.449,8,11,8h8.588L22,10.387V22.839z" fill-rule="evenodd" clip-rule="evenodd" fill="#333333" class="csui-icon-v2-metaphor0"/><path d="M12.5,11.75c-0.414,0-0.75,0.336-0.75,0.75s0.336,0.75,0.75,0.75h7c0.414,0,0.75-0.336,0.75-0.75 s-0.336-0.75-0.75-0.75H12.5z" fill-rule="evenodd" clip-rule="evenodd" fill="#333333" class="csui-icon-v2-metaphor0"/><path d="M19.5,15.75h-7c-0.414,0-0.75,0.336-0.75,0.75s0.336,0.75,0.75,0.75h7c0.414,0,0.75-0.336,0.75-0.75 S19.914,15.75,19.5,15.75z" fill-rule="evenodd" clip-rule="evenodd" fill="#333333" class="csui-icon-v2-metaphor0"/><path d="M18,19.75h-5.5c-0.414,0-0.75,0.336-0.75,0.75s0.336,0.75,0.75,0.75H18c0.414,0,0.75-0.336,0.75-0.75 S18.414,19.75,18,19.75z" fill-rule="evenodd" clip-rule="evenodd" fill="#333333" class="csui-icon-v2-metaphor0"/></g><circle cx="16" cy="16" r="15.5" fill="none" stroke="#2E3D98" class="csui-icon-v2-focus"/></svg>'
   };
});
```

* [ActionIconRegistry](#module_ActionIconRegistry) : <code>d2/sdk/controls/action.icons/action.icons</code>
    * [.registerIcons(icons)](#module_ActionIconRegistry.registerIcons) ⇒ <code>void</code>
    * [.getIconByName(iconName)](#module_ActionIconRegistry.getIconByName) ⇒ <code>string</code>
    * [.getIconByNameWithOptions(iconOptions)](#module_ActionIconRegistry.getIconByNameWithOptions) ⇒ <code>string</code>

<a name="module_ActionIconRegistry.registerIcons"></a>

### ActionIconRegistry.registerIcons(icons) ⇒ <code>void</code>
Register action icons.

**Kind**: static method of [<code>ActionIconRegistry</code>](#module_ActionIconRegistry)  
**Throws**:

- Error If an icon name matches with any of already registered icons.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>icons</td><td><code>object</code></td><td><p>A hash of key value pairs, such that key represents an icon name and value is the string serialized svg content.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ActionIconRegistry.getIconByName"></a>

### ActionIconRegistry.getIconByName(iconName) ⇒ <code>string</code>
Find an action icon by name.

**Kind**: static method of [<code>ActionIconRegistry</code>](#module_ActionIconRegistry)  
**Returns**: <code>string</code> - Matched icon content or a generalized missing icon if given name is not found in the registry.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>iconName</td><td><code>string</code></td><td><p>Name of the action icon being looked up.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ActionIconRegistry.getIconByNameWithOptions"></a>

### ActionIconRegistry.getIconByNameWithOptions(iconOptions) ⇒ <code>string</code>
Find an icon and return decored copy with supplied options

**Kind**: static method of [<code>ActionIconRegistry</code>](#module_ActionIconRegistry)  
**Returns**: <code>string</code> - Matched icon content or the missin icon after decorating it with given options.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>iconOptions</td><td><code>object</code></td><td></td>
    </tr><tr>
    <td>iconOptions.iconName</td><td><code>string</code></td><td><p>Find the icon by this name.</p>
</td>
    </tr><tr>
    <td>iconOptions.theme</td><td><code>string</code></td><td><p>Decorate found icon with css class <code>csui-icon-v2-theme-&lt;theme&gt;</code></p>
</td>
    </tr><tr>
    <td>iconOptions.allStateByElement</td><td><code>boolean</code></td><td><p>Whether to decorate icon with all possible state CSS classes.</p>
</td>
    </tr><tr>
    <td>iconOptions.hoverStateByElement</td><td><code>boolean</code></td><td><p>Decorate with mouse hover state CSS class.</p>
</td>
    </tr><tr>
    <td>iconOptions.activeStateByElement</td><td><code>boolean</code></td><td><p>Decorate with mosue left button down state CSS class.</p>
</td>
    </tr><tr>
    <td>iconOptions.focusStateByElement</td><td><code>boolean</code></td><td><p>Decorate with keyboard focus state CSS class.</p>
</td>
    </tr><tr>
    <td>iconOptions.disabledStateByElement</td><td><code>boolean</code></td><td><p>Decorate with disabled state CSS class.</p>
</td>
    </tr><tr>
    <td>iconOptions.states</td><td><code>boolean</code></td><td><p>Whether the icon is capable of showing different states.</p>
</td>
    </tr><tr>
    <td>iconOptions.handleRTL</td><td><code>boolean</code></td><td><p>Whether the icon is capable of handling RTL.</p>
</td>
    </tr><tr>
    <td>iconOptions.on</td><td><code>boolean</code></td><td><p>Decorate icon with on state CSS class.</p>
</td>
    </tr><tr>
    <td>iconOptions.filter</td><td><code>string</code></td><td><p>Apply filter. Only supported value is <code>grayscale</code>. <code>theme</code>, <code>states</code> &amp; <code>on</code> must be false.</p>
</td>
    </tr><tr>
    <td>iconOptions.colorTheme</td><td><code>string</code></td><td><p>Apply a coloring scheme. Applies only when <code>theme</code>, <code>states</code> &amp; <code>on</code> are <code>false</code>. Possible values are
<code>&#39;tree&#39;, &#39;outline-light&#39;, &#39;outline-dark&#39;, &#39;ot-navy&#39;, &#39;ot-indigo&#39;, &#39;ot-plum&#39;, &#39;ot-teal&#39;, &#39;ot-light-blue&#39;, &#39;ot-steal&#39;, &#39;ot-cloud&#39;</code>.</p>
</td>
    </tr>  </tbody>
</table>

