<a name="module_GlobalMessage"></a>

## GlobalMessage : <code>d2/sdk/controls/globalmessage/globalmessage</code>
UI component used to show different types of toast messages.

**Extends**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)  

* [GlobalMessage](#module_GlobalMessage) : <code>d2/sdk/controls/globalmessage/globalmessage</code>
    * _static_
        * [.showMessage(type, message, [details], [options])](#module_GlobalMessage.showMessage) ⇒ [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
        * [.showSuccessMessageWithLink(options)](#module_GlobalMessage.showSuccessMessageWithLink)
    * _inner_
        * [~GMConfigOptions](#module_GlobalMessage..GMConfigOptions) : <code>Object</code>

<a name="module_GlobalMessage.showMessage"></a>

### GlobalMessage.showMessage(type, message, [details], [options]) ⇒ [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
Show a toast message of particular type

**Kind**: static method of [<code>GlobalMessage</code>](#module_GlobalMessage)  
**Returns**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html) - The toast message view instance.  
**Note**: `success` messages automatically disappear after some timeout.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>string</code></td><td><p>Type of the message. Possible values are <code>&#39;info&#39;, &#39;success&#39;, &#39;warning&#39;, &#39;error&#39;</code>.</p>
</td>
    </tr><tr>
    <td>message</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[details]</td><td><code>string</code></td><td><p>Details associated with the message.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>object</code></td><td><p>Control options</p>
</td>
    </tr><tr>
    <td>[options.configOption]</td><td><code>GMConfigOptions</code></td><td><p>Message display configuration.</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
define([ 'd2/sdk/controls/globalmessage/globalmessage'], function(GlobalMessage){ 'use strict'; //To show information message GlobalMessage.showMessage('info', 'Hello world!'); //To show success message GlobalMessage.showMessage('success', 'This is a sample success message');});
```
<a name="module_GlobalMessage.showSuccessMessageWithLink"></a>

### GlobalMessage.showSuccessMessageWithLink(options)
Show a success toast message with a clickable link that can execute a command model implementaion.

**Kind**: static method of [<code>GlobalMessage</code>](#module_GlobalMessage)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Message options</p>
</td>
    </tr><tr>
    <td>options.showLink</td><td><code>Boolean</code></td><td><p>Whether to show a link</p>
</td>
    </tr><tr>
    <td>options.node</td><td><code><a href="../../models/node.model">NodeModel</a></code></td><td><p>A NodeModel instance whose details is used to format the message. Also used later to execute the linked command when user clicks
on the link.</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td><p>Reference to the application context.</p>
</td>
    </tr><tr>
    <td>options.messageFormat</td><td><code>string</code></td><td><p>A string template. First placeholder in the string will be replaced with the name attribute value from <code>options.node</code>.
Can also be used to pass a complete message disregarding the fact that it will try to interpret as a template.</p>
</td>
    </tr><tr>
    <td>options.linkLabel</td><td><code>string</code></td><td><p>Text to be shown as link.</p>
</td>
    </tr><tr>
    <td>options.linkCommandSignature</td><td><code>string</code></td><td><p>Signature of the command to be executed when user clicks on the displayed link.</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
define([   'd2/sdk/controls/globalmessage/globalmessage',   'd2/sdk/models/node.model',   'd2/sdk/utils/contexts/context.utils'], function(GlobalMessage, NodeModel, contextUtil){ 'use strict'; GlobalMessage.showSuccessMessageWithLink({   showLink: true,   node: new NodeModel({id: '090015fa80002d49', name: 'SampleDocument'}),   context: contextUtil.getPerspectiveContext(),   messageFormat: 'Hello "{0}"',   linkLabel: 'click to open overview',   linkCommandSignature: 'Browse' });});
```
<a name="module_GlobalMessage..GMConfigOptions"></a>

### GlobalMessage~GMConfigOptions : <code>Object</code>
**Kind**: inner typedef of [<code>GlobalMessage</code>](#module_GlobalMessage)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>enablePermanentHeaderMessages</td><td><code>Boolean</code></td><td><p>Whether to show the toast message without auto-dismiss. Defaults to <code>false</code>.</p>
</td>
    </tr>  </tbody>
</table>

