<a name="module_FormView"></a>

## FormView : <code>d2/sdk/controls/form/form.view</code>
FormView. Create and render an arbitrary HTML form using JSON schema. Form also provides manage API to validate & collect form data.Uses `FormModel` to hold the JSON schema that drives the form rendering. See [FormModel](./form.model) for examples of how tocreate a `FormModel` from literal JSON data and dialog-like D2-REST API response.

**Extends**: [<code>Marionette.ItemView</code>](https://marionettejs.com/docs/v2.4.7/marionette.itemview.html)  
**Emits**: [<code>d2:change:formValidation</code>](#module_FormView+event_d2_change_formValidation), [<code>change:field</code>](#module_FormView+change_field), [<code>valid:field</code>](#module_FormView+valid_field), [<code>invalid:field</code>](#module_FormView+invalid_field), [<code>form:changed</code>](#module_FormView+form_changed), [<code>render:form</code>](#module_FormView+render_form)  
**Example** *(To create a form view)*  
```js
define([
 'd2/sdk/controls/form/form.view',
 'd2/sdk/controls/form/form.model'
], function(FormView, FormModel){
 'use strict';

 var formOptions = {
   topAlignedLabel: true,
   mode: 'create',
   layoutMode: 'singleCol',
   model: createFormModel()
 };

 function createFormModel() {
   var formSchema;

   // populate form schema

   return new FormModel(formSchema);
 }

 var formView = new FormView(formOptions); //For controlling any business specific, behavior FormView could be extended to attach business specific logic

 formView.on('render:form', function(){
   console.log('Form is rendered');
 });

 new Marionette.Region({el: '.my-form-container'}).show(formView);
});
```

* [FormView](#module_FormView) : <code>d2/sdk/controls/form/form.view</code>
    * _instance_
        * [.resetForm()](#module_FormView+resetForm) ⇒ <code>void</code>
        * [.validateForm(refreshState, validateOnlyRequiredFields)](#module_FormView+validateForm) ⇒ <code>jQyery.Promise</code>
        * [.getValues()](#module_FormView+getValues) ⇒ <code>object</code>
        * [.getFlatFormValueMap(valueMap)](#module_FormView+getFlatFormValueMap) ⇒ <code>object</code>
        * ["d2:change:formValidation"](#module_FormView+event_d2_change_formValidation)
        * ["change:field"](#module_FormView+change_field)
        * ["valid:field"](#module_FormView+valid_field)
        * ["invalid:field"](#module_FormView+invalid_field)
        * ["form:changed"](#module_FormView+form_changed)
        * ["render:form"](#module_FormView+render_form)
    * _inner_
        * [~FormView](#module_FormView..FormView)
            * [new FormView(options)](#new_module_FormView..FormView_new)
        * [~FormValidationCallback](#module_FormView..FormValidationCallback) : <code>function</code>

<a name="module_FormView+resetForm"></a>

### formView.resetForm() ⇒ <code>void</code>
Reset the current form removing all user inputs.

**Kind**: instance method of [<code>FormView</code>](#module_FormView)  
<a name="module_FormView+validateForm"></a>

### formView.validateForm(refreshState, validateOnlyRequiredFields) ⇒ <code>jQyery.Promise</code>
Validate current form values and ensure form contraints are met.

**Kind**: instance method of [<code>FormView</code>](#module_FormView)  
**Returns**: <code>jQyery.Promise</code> - - Promise is resolved with validation status. A `FormValidationDone` callback could be invoked with the validation result.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>refreshState</td><td><code>Boolean</code></td><td><p>Whether to clear previous validation status before running this one.</p>
</td>
    </tr><tr>
    <td>validateOnlyRequiredFields</td><td><code>Boolean</code></td><td><p>Whether to perform only a required field validation or all other contraints as well.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_FormView+getValues"></a>

### formView.getValues() ⇒ <code>object</code>
Get current field values

**Kind**: instance method of [<code>FormView</code>](#module_FormView)  
**Returns**: <code>object</code> - A JSON in the same structure as the form's field layout definition, it contains key-value pairs at each level where key correspondsto form field name and value corresponds to the data collected from the field.  
<a name="module_FormView+getFlatFormValueMap"></a>

### formView.getFlatFormValueMap(valueMap) ⇒ <code>object</code>
Get current field values formatted as a flattened single level key-value hash.

**Kind**: instance method of [<code>FormView</code>](#module_FormView)  
**Returns**: <code>object</code> - Flattened key-value map.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valueMap</td><td><code>object</code></td><td><p>The hierarchical form value map. Basically the return value of <code>getValues()</code> could be passed as this parameter.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_FormView+event_d2_change_formValidation"></a>

### "d2:change:formValidation"
Indicates form validation status has changed.

**Kind**: event emitted by [<code>FormView</code>](#module_FormView)  
<a name="module_FormView+change_field"></a>

### "change:field"
Indicates a field value has been updated.

**Kind**: event emitted by [<code>FormView</code>](#module_FormView)  
<a name="module_FormView+valid_field"></a>

### "valid:field"
Indicates a field cotains a valid value.

**Kind**: event emitted by [<code>FormView</code>](#module_FormView)  
<a name="module_FormView+invalid_field"></a>

### "invalid:field"
Indicates a field contains an invalid value.

**Kind**: event emitted by [<code>FormView</code>](#module_FormView)  
<a name="module_FormView+form_changed"></a>

### "form:changed"
Indicates that form data has changed somehow.

**Kind**: event emitted by [<code>FormView</code>](#module_FormView)  
<a name="module_FormView+render_form"></a>

### "render:form"
Indicates that form has been fully rendered in UI

**Kind**: event emitted by [<code>FormView</code>](#module_FormView)  
<a name="module_FormView..FormView"></a>

### FormView~FormView
**Kind**: inner class of [<code>FormView</code>](#module_FormView)  
<a name="new_module_FormView..FormView_new"></a>

#### new FormView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options</p>
</td>
    </tr><tr>
    <td>options.model</td><td><code><a href="./form.model">FormModel</a></code></td><td><p>The data holder having schema definition for this form instance.</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td><p>Reference to the application context.</p>
</td>
    </tr><tr>
    <td>[options.topAlignedLabel]</td><td><code>Boolean</code></td><td><p>Whether to place label at the top of form field or side-by-side fashion. Defaults to <code>false</code>.</p>
</td>
    </tr><tr>
    <td>[options.mode]</td><td><code>string</code></td><td><p>Controls rendering of the form. Possible values are <code>&#39;create&#39;, &#39;read&#39;, &#39;update&#39;</code>. Defaults to <code>update</code>. The
<code>read</code> mode renders a readonly form. Whereas, the <code>update</code> &amp; <code>create</code> mode are equivalent and allows form field value modification. The difference
between <code>update</code> and <code>create</code> is that form is initially not validated for required values and other conditions for <code>create</code> mode until user starts to
interact with the form.</p>
</td>
    </tr><tr>
    <td>[options.layoutMode]</td><td><code>string</code></td><td><p>Whether to layout the form in a single or double column fashion. Supported values are <code>&#39;singleCol&#39;, &#39;doubleCol&#39;</code>.
Defaults to <code>singleCol</code>.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_FormView..FormValidationCallback"></a>

### FormView~FormValidationCallback : <code>function</code>
**Kind**: inner typedef of [<code>FormView</code>](#module_FormView)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>isValid</td><td><code>Boolean</code></td><td><p>Whether the form is valid.</p>
</td>
    </tr>  </tbody>
</table>

