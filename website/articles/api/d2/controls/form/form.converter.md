<a name="module_FormConverter"></a>

## FormConverter : <code>d2/sdk/controls/form/form.converter</code>
Form converter. Utility to transform D2 Form metadata.

**Extends**: <code>Object</code>  

* [FormConverter](#module_FormConverter) : <code>d2/sdk/controls/form/form.converter</code>
    * _static_
        * [.convertConfig(formJSON, options)](#module_FormConverter.convertConfig) ⇒ <code>ConversionReturnType</code>
        * [.generateFormModelConfig(formArray, [options])](#module_FormConverter.generateFormModelConfig) ⇒ <code>void</code>
    * _inner_
        * [~ConversionReturnType](#module_FormConverter..ConversionReturnType) : <code>Object</code>
        * [~AlpacaSchema](#module_FormConverter..AlpacaSchema) : <code>Object</code>

<a name="module_FormConverter.convertConfig"></a>

### FormConverter.convertConfig(formJSON, options) ⇒ <code>ConversionReturnType</code>
Convert a D2 Form metadata into an Alpaca form schema.

**Kind**: static method of [<code>FormConverter</code>](#module_FormConverter)  
**Returns**: <code>ConversionReturnType</code> - `propertiesConfig` & `signOffConfig` are two separate alpaca form schema to collect properties and user's electronic signature.  
**Note**: Alpaca form schema generated through this method does not have to go through `generateFormModelConfig()` method below.  
**See**: [Alpaca Forms](http://www.alpacajs.org/docs/api/forms.html)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formJSON</td><td><code>JSON</code></td><td><p>Raw dialog-like response received from a D2-REST API or an equivalent object constructed literally.</p>
</td>
    </tr><tr>
    <td>options</td><td><code>Object</code></td><td><p>Holder of transformer parameters</p>
</td>
    </tr><tr>
    <td>[options.actionMode]</td><td><code>string</code></td><td><p>How to process the fields. Can be one of &#39;create&#39;, &#39;read&#39;, &#39;edit&#39;. &#39;read&#39; is for readonly mode, other two are editable. Value of this option
influences the list of fields and their conditions processed as per equivalent D2-config property page field modes.</p>
</td>
    </tr><tr>
    <td>[options.layoutMode]</td><td><code>string</code></td><td><p>By default fields are laid out in a double column fashion, if a single-column is desired instead set this option to &#39;singleCol&#39;.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_FormConverter.generateFormModelConfig"></a>

### FormConverter.generateFormModelConfig(formArray, [options]) ⇒ <code>void</code>
Inject additional data into an Alpaca form schema and make it compatible for [FormModel](./form.model) & [FormView](./form.view).If more than one form is passed in the input, the method also generates cross-reference data if one form uses value from other, for any reason.

**Kind**: static method of [<code>FormConverter</code>](#module_FormConverter)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formArray</td><td><code>Array.&lt;AlpacaSchema&gt;</code></td><td><p>Array of Alpaca form schemas. The parameter is also the output parameter, each element in the array
is modified with additional data.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>object</code></td><td><p>Additional data/control flag used during the conversion.</p>
</td>
    </tr><tr>
    <td>[options.initialFieldValueMap]</td><td><code>object</code></td><td><p>Initial data reference for all the fields across forms.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_FormConverter..ConversionReturnType"></a>

### FormConverter~ConversionReturnType : <code>Object</code>
**Kind**: inner typedef of [<code>FormConverter</code>](#module_FormConverter)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>propertiesConfig</td><td><code>Object</code></td><td><p>Alpaca schema for the main form</p>
</td>
    </tr><tr>
    <td>signOffConfig</td><td><code>Object</code></td><td><p>Alpaca schema for D2 Electronic signature form</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_FormConverter..AlpacaSchema"></a>

### FormConverter~AlpacaSchema : <code>Object</code>
**Kind**: inner typedef of [<code>FormConverter</code>](#module_FormConverter)  
**See**: [Alpaca Forms](http://www.alpacajs.org/docs/api/forms.html)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>object</code></td><td><p>Object hash of initial values for each field property</p>
</td>
    </tr><tr>
    <td>schema</td><td><code>object</code></td><td><p>Container of schema properties</p>
</td>
    </tr><tr>
    <td>schema.properties</td><td><code>object</code></td><td><p>Object hash defining schema for each field property.</p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>Container of additional options</p>
</td>
    </tr><tr>
    <td>options.fields</td><td><code>object</code></td><td><p>Object hash defining options for each field property.</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
var form = {   data: {     field1: 'Hello world!'   },   schema: {     properties: {       field1: {         type: 'string',         required: true       }     }   },   options: {     fields: {       field1: {         type: 'text',         label: 'Greetings'       }     }   }};
```
