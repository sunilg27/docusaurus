<a name="module_LayoutViewEventPropagationMixin"></a>

## LayoutViewEventPropagationMixin : <code>d2/sdk/controls/mixins/layoutview.events.propagation.mixin</code>
Event propagation mixin for Marionette LayoutView. Marionette Layout view fails to propagate view-lifecycle events to differentchild views that it hosts in its regions.This mixin makes it possible to establish the broken chain in a declarative style.

**Extends**: <code>Object</code>  
**Example** *(To use the mixin in a layout view)*  
```js
define([
 'nuc/lib/marionette',
 'd2/sdk/controls/mixins/layoutview.events.propagation.mixin',
], function(Marionette, LayoutViewEventsPropagationMixin){
 'use strict';

 var MyView = Marionette.LayoutView.extend({
   regions: {
     header: '.header',
     footer: '.footer'
   },
   constructor: function MyView(options) {
     options = options || {};
     MyView.__super__.constructor.call(this, options);

     this.propagateEventsToRegions();
   }
 });

 LayoutViewEventsPropagationMixin.mixin(MyView.prototype);

 var view = new MyView();
 new Marionette.Region({el: '.host-element'}).show(view); // With this default setup, whenever MyView instance receives 'dom:refresh' event, it is also relayed to views hosted in regions 'header' & 'footer'.
});
```

* [LayoutViewEventPropagationMixin](#module_LayoutViewEventPropagationMixin) : <code>d2/sdk/controls/mixins/layoutview.events.propagation.mixin</code>
    * _instance_
        * [._eventsToPropagateToRegions](#module_LayoutViewEventPropagationMixin+_eventsToPropagateToRegions) : <code>Array.&lt;string&gt;</code>
        * [.propagateEventsToRegions()](#module_LayoutViewEventPropagationMixin+propagateEventsToRegions) ⇒ <code>void</code>
    * _static_
        * [.mixin(prototype)](#module_LayoutViewEventPropagationMixin.mixin) ⇒ <code>void</code>

<a name="module_LayoutViewEventPropagationMixin+_eventsToPropagateToRegions"></a>

### layoutViewEventPropagationMixin.\_eventsToPropagateToRegions : <code>Array.&lt;string&gt;</code>
This property gets mixed with the view prototype. It should contain a list of Marionette view lifecycle events that are desired tobe propagated to each hosted child view when those events occur at the LayoutView level.

**Kind**: instance property of [<code>LayoutViewEventPropagationMixin</code>](#module_LayoutViewEventPropagationMixin)  
**Default**: <code>[&#x27;dom:refresh&#x27;]</code>  
<a name="module_LayoutViewEventPropagationMixin+propagateEventsToRegions"></a>

### layoutViewEventPropagationMixin.propagateEventsToRegions() ⇒ <code>void</code>
This method gets mixed with any Marionette.View prototype. This method should be called from within a view instance post construction.

**Kind**: instance method of [<code>LayoutViewEventPropagationMixin</code>](#module_LayoutViewEventPropagationMixin)  
<a name="module_LayoutViewEventPropagationMixin.mixin"></a>

### LayoutViewEventPropagationMixin.mixin(prototype) ⇒ <code>void</code>
Method to add this mixin to a view prototype.

**Kind**: static method of [<code>LayoutViewEventPropagationMixin</code>](#module_LayoutViewEventPropagationMixin)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>any</code></td><td><p>The view prototype where is mixin is to be merged.</p>
</td>
    </tr>  </tbody>
</table>

