<a name="module_WidgetViewStateMixin"></a>

## WidgetViewStateMixin : <code>d2/sdk/controls/mixins/widget.view.state.mixin</code>
Mixin to faciliate storage and retrieval of view specific parameters especially for those views that are backed by a widget configuration.To be mixed on the `prototype` of a view.

**Extends**: <code>Object</code>  
**Example**  
```js
define([ 'nuc/lib/marionette', 'd2/sdk/controls/mixins/widget.view.state.mixin', 'd2/sdk/utils/contexts/context.utils'], function(Marionette, WidgetViewStateMixin, contextUtil){ 'use strict'; var MyView = Marionette.ItemView.extend({   constructor: function MyView(options) {     options = options || {};     this.context = contextUtil.getPerspectiveContext(); //A context reference on self object is required for WidgetViewStateMixin to work.     MyView.__super__.constructor.call(this, options);   },   onRender: function() {     this.$el.text('My widget name is: ' + this.getViewStateWidgetName());   } }); WidgetViewStateMixin.mixin(MyView.prototype); var view = new MyView(); view.setViewStateWidgetName('hello world'); //Also, the view itself could call it to set the value new Marionette.Region({el: '.host-el'}).show(view); //Shows the widget name in view element's text.});
```

* [WidgetViewStateMixin](#module_WidgetViewStateMixin) : <code>d2/sdk/controls/mixins/widget.view.state.mixin</code>
    * _instance_
        * [.setViewStateWidgetName(widgetName)](#module_WidgetViewStateMixin+setViewStateWidgetName) ⇒ <code>void</code>
        * [.getViewStateWidgetName()](#module_WidgetViewStateMixin+getViewStateWidgetName) ⇒ <code>string</code> \| <code>undefined</code>
        * [.getInitialUrlParamsList()](#module_WidgetViewStateMixin+getInitialUrlParamsList) ⇒ <code>Array.&lt;string&gt;</code>
    * _static_
        * [.mixin(prototype)](#module_WidgetViewStateMixin.mixin) ⇒ <code>\*</code>

<a name="module_WidgetViewStateMixin+setViewStateWidgetName"></a>

### widgetViewStateMixin.setViewStateWidgetName(widgetName) ⇒ <code>void</code>
Set the widget configuration name to be associated with this `widget.view`

**Kind**: instance method of [<code>WidgetViewStateMixin</code>](#module_WidgetViewStateMixin)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>widgetName</td><td><code>string</code></td><td><p>Stores the supplied value against key <code>widget-name</code> in the <code>ViewStateModel</code></p>
</td>
    </tr>  </tbody>
</table>

<a name="module_WidgetViewStateMixin+getViewStateWidgetName"></a>

### widgetViewStateMixin.getViewStateWidgetName() ⇒ <code>string</code> \| <code>undefined</code>
Get the widget configuration name associated with this `widget.view`

**Kind**: instance method of [<code>WidgetViewStateMixin</code>](#module_WidgetViewStateMixin)  
**Returns**: <code>string</code> \| <code>undefined</code> - previously stored widget name from `ViewStateModel`  
<a name="module_WidgetViewStateMixin+getInitialUrlParamsList"></a>

### widgetViewStateMixin.getInitialUrlParamsList() ⇒ <code>Array.&lt;string&gt;</code>
Get the initial list of parameters to be serialized/deserialized from the HTML document location

**Kind**: instance method of [<code>WidgetViewStateMixin</code>](#module_WidgetViewStateMixin)  
**Returns**: <code>Array.&lt;string&gt;</code> - should return name of the parameter to (de)serialize from the HTML page URL  
<a name="module_WidgetViewStateMixin.mixin"></a>

### WidgetViewStateMixin.mixin(prototype) ⇒ <code>\*</code>
Mix function from this mixin into supplied prototype.

**Kind**: static method of [<code>WidgetViewStateMixin</code>](#module_WidgetViewStateMixin)  
**Returns**: <code>\*</code> - The same prototype  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>any</code></td><td><p>A view prototype.</p>
</td>
    </tr>  </tbody>
</table>

