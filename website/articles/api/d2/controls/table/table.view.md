<a name="module_TableView"></a>

## TableView : <code>d2/sdk/controls/table/table.view</code>
A `Marionette.view` to render `Backbone.Collection` data in tabular format.

**Extends**: [<code>Marionette.CollectionView</code>](https://marionettejs.com/docs/v2.4.7/marionette.collectionview.html)  

* [TableView](#module_TableView) : <code>d2/sdk/controls/table/table.view</code>
    * [~TableView](#module_TableView..TableView)
        * [new TableView(options)](#new_module_TableView..TableView_new)
    * [~TableColumnCollection](#module_TableView..TableColumnCollection) : <code>Backbone.Collection</code>
    * [~ColumnModel](#module_TableView..ColumnModel) : <code>Object</code>

<a name="module_TableView..TableView"></a>

### TableView~TableView
**Kind**: inner class of [<code>TableView</code>](#module_TableView)  
**Prams**: <code>TableColumnCollection</code> options.thumbnailColumns] - Columns used while rendering items as thumbnails. Can be kept as same as `tableColumns`.  
<a name="new_module_TableView..TableView_new"></a>

#### new TableView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options. Following are the options specifically supported by the list apart from those from its supertype.</p>
</td>
    </tr><tr>
    <td>options.tableColumns</td><td><code>TableColumnCollection</code></td><td><p>Column definition. Each model must have <code>ColumnModel</code> attrutes.</p>
</td>
    </tr><tr>
    <td>options.columns</td><td><code>TableColumnCollection</code></td><td><p>Server side columns. Can be kept as same as <code>tableColumns</code>.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>The connector instance.</p>
</td>
    </tr><tr>
    <td>options.collection</td><td><code>NodeCollection</code></td><td><p>The collection holding data for the table.</p>
</td>
    </tr><tr>
    <td>options.columnsWithSearch</td><td><code>Array.&lt;string&gt;</code></td><td><p>Which columns should have search enabled on them.</p>
</td>
    </tr><tr>
    <td>options.originatingView</td><td><code>Marionette.View</code></td><td><p>Reference to a parent view where this table is hosted.</p>
</td>
    </tr><tr>
    <td>options.selectedChildren</td><td><code>NodeCollection</code></td><td><p>Collection to hold list of items selected.</p>
</td>
    </tr><tr>
    <td>options.inlineBar</td><td><code>ToolitemsFactory</code></td><td><p>Toolbar configuration for this table items.</p>
</td>
    </tr><tr>
    <td>options.selectRows</td><td><code>string</code></td><td><p>Row selection model. Set to &#39;none&#39;.</p>
</td>
    </tr><tr>
    <td>options.enableSorting</td><td><code>boolean</code></td><td><p>Enable column sorting</p>
</td>
    </tr><tr>
    <td>[options.customLabels]</td><td><code>object</code></td><td><p>Customized state messages as follows</p>
</td>
    </tr><tr>
    <td>[options.customLabels.emptySearchTable]</td><td><code>string</code></td><td><p>Shown when search in the table returns no result.</p>
</td>
    </tr><tr>
    <td>[options.customLabels.zeroRecords]</td><td><code>string</code></td><td><p>Shown when the underlying collection has no items in it.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TableView..TableColumnCollection"></a>

### TableView~TableColumnCollection : <code>Backbone.Collection</code>
**Kind**: inner typedef of [<code>TableView</code>](#module_TableView)  
**Note**: Each TableColumnCollection must implement a method `getColumnKeys()` that returns the `key` attribute values from each of it's models  
**Note**: The model used for TableColumnCollection must use `key` as the `idAttribute`.  
**Example**  
```js
define([ 'nuc/lib/backbone'], function(Backbone){ 'use strict'; var TableModel = Backbone.Model.extend({   idAttribute: "key",   defaults: {     key: null,     sequence: 0   } }); var TableColumnCollection = Backbone.Collection.extend({   model: TableColumnModel,   comparator: 'sequence',   getColumnKeys: function () {     return this.pluck('key');   } }); //Then finally an instance might look like var columnCollection = new TableColumnCollection([   {     key: 'attr1',     column_key: 'attr1',     sequence: 2, // Data column sequence should always start from 2, by design position 1 is reserved for displaying a selection checkbox.     definitions_order: 2,     sort: false,     title: 'Cell 1',     type: -1, //string data     permanentColumn: true, //Don't wrap if screen size reduces     isNaming: true   },   {     key: 'attr2',     column_key: 'attr2',     sequence: 3,     definitions_order: 3,     sort: false,     title: 'Cell 2',     type: -1,     permanentColumn: true,     isNaming: true   } ]); return columnCollection;});
```
<a name="module_TableView..ColumnModel"></a>

### TableView~ColumnModel : <code>Object</code>
**Kind**: inner typedef of [<code>TableView</code>](#module_TableView)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>key</td><td><code>string</code></td><td><p>Data attribute name</p>
</td>
    </tr><tr>
    <td>column_key</td><td><code>string</code></td><td><p>Column identifier. Usually kept similar to <code>key</code> above.</p>
</td>
    </tr><tr>
    <td>sequence</td><td><code>number</code></td><td><p>Order of this model in the associated column collection</p>
</td>
    </tr><tr>
    <td>definitions_order</td><td><code>number</code></td><td><p>Visible order of this column. Usually kept same as <code>sequence</code> above.</p>
</td>
    </tr><tr>
    <td>sort</td><td><code>boolean</code></td><td><p>Enable sorting on this column</p>
</td>
    </tr><tr>
    <td>title</td><td><code>string</code></td><td><p>Text to be displayed for this column</p>
</td>
    </tr><tr>
    <td>type</td><td><code>number</code></td><td><p>data type associated with this column. See <a href="./cells/cell.registry">TableCellRegistry</a>.</p>
</td>
    </tr><tr>
    <td>widthFactor</td><td><code>number</code></td><td><p>A decimal number to control this column&#39;s visual width in percentage.</p>
</td>
    </tr><tr>
    <td>permanentColumn</td><td><code>boolean</code></td><td><p>Whether this is a permanent column. Permanent columns are not wrapped if screen size reduces.</p>
</td>
    </tr><tr>
    <td>isNaming</td><td><code>boolean</code></td><td><p>Whether this is the naming column. At least one column should be marked as naming column in a column collection. Naming column
is where the table renders its inline actionbar, if configured any.</p>
</td>
    </tr>  </tbody>
</table>

