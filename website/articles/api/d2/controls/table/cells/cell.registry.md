<a name="module_TableCellRegistry"></a>

## TableCellRegistry : <code>d2/sdk/controls/table/cells/cell.registry</code>
Register a new cell renderer, to be used for a specified column in a TableView or against a particular data type.

**Extends**: <code>Object</code>  

* [TableCellRegistry](#module_TableCellRegistry) : <code>d2/sdk/controls/table/cells/cell.registry</code>
    * [.registerByColumnKey(columnKey, handlerClass)](#module_TableCellRegistry.registerByColumnKey) ⇒ <code>void</code>
    * [.registerByDataType(dataType, handlerClass)](#module_TableCellRegistry.registerByDataType) ⇒ <code>void</code>

<a name="module_TableCellRegistry.registerByColumnKey"></a>

### TableCellRegistry.registerByColumnKey(columnKey, handlerClass) ⇒ <code>void</code>
Register a cell renderer by column key, i.e. an attribute name

**Kind**: static method of [<code>TableCellRegistry</code>](#module_TableCellRegistry)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>columnKey</td><td><code>string</code></td><td><p>Attribute name to bind this renderer to</p>
</td>
    </tr><tr>
    <td>handlerClass</td><td><code><a href="./templated.cell.view">TemplatedCellView</a></code></td><td><p>View class implementing the renderer</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TableCellRegistry.registerByDataType"></a>

### TableCellRegistry.registerByDataType(dataType, handlerClass) ⇒ <code>void</code>
Register a renderer by data type

**Kind**: static method of [<code>TableCellRegistry</code>](#module_TableCellRegistry)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>dataType</td><td><code>number</code></td><td><p>A non-zero number to represent a data type. Pre-declared types are <code>-1(string)</code>, <code>-4(double)</code>, <code>-7(date)</code>, <code>2(integer)</code>, <code>5(boolean)</code>.</p>
</td>
    </tr><tr>
    <td>handlerClass</td><td><code><a href="./templated.cell.view">TemplatedCellView</a></code></td><td><p>View class implementing the renderer</p>
</td>
    </tr>  </tbody>
</table>

