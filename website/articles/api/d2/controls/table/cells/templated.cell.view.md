<a name="module_TableCellView"></a>

## TableCellView : <code>d2/sdk/controls/table/cells/templated.cell.view</code>
Interface for classes that want to implement a table's cell view.After defining a new cell implementation, it has to be registered with `CellRegistry`.

**Extends**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)  
**See**: [TableCellRegistry](./cell.registry)  
**Example**  
```js
//In mybundle/table/cells/template/my.cell.hbs define the template as<div class="my-cell-wrapper">  <a class="my-cell-container" title="{{title}}">     <span class="my-cell-data">{{data}}</span>  </a></div>//Then in a cell implementation moduledefine([ 'nuc/lib/underscore', 'd2/sdk/controls/table/cells/cell.registry', 'd2/sdk/controls/table/cells/templated.cell.view', 'hbs!mybundle/table/cells/my.cell'], function(_, CellRegistry, TemplatedCellView, template){ 'use strict'; var MyCellView = TemplatedCellView.extend({   template: template,   renderValue: function() {     var tmplData = {       data: this.options.column ? TemplatedCellView.getValue(this.model, this.options.column): '',       title: this.getValueText()     };     var html = '';     if(!_.isEmpty(tmplData.data)) {       tmplData.data = '' + tmplData.data;       html = this.template(tmplData);     }     this.$el.html(html);   } }); CellRegistry.registerByColumnKey('d2_custom_column', MyCellView); //Whenever a table is rendered where the backing collection has 'd2_custom_column' in its column definition, this cell view implementation willl be linked. return MyCellView;});
```

* [TableCellView](#module_TableCellView) : <code>d2/sdk/controls/table/cells/templated.cell.view</code>
    * _static_
        * [.columnClassName](#module_TableCellView.columnClassName) : <code>string</code>
        * [.getValue(model, column)](#module_TableCellView.getValue)
    * _inner_
        * [~TemplatedCellView](#module_TableCellView..TemplatedCellView)
            * [new TemplatedCellView(options)](#new_module_TableCellView..TemplatedCellView_new)
        * *[~renderValue()](#module_TableCellView..renderValue) ⇒ <code>void</code>*
        * *[~getValueText()](#module_TableCellView..getValueText) ⇒ <code>string</code>*

<a name="module_TableCellView.columnClassName"></a>

### TableCellView.columnClassName : <code>string</code>
CSS classname to be added for this column

**Kind**: static property of [<code>TableCellView</code>](#module_TableCellView)  
<a name="module_TableCellView.getValue"></a>

### TableCellView.getValue(model, column)
**Kind**: static method of [<code>TableCellView</code>](#module_TableCellView)  
**Decription**: Get value of this cell  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>model</td><td><code>Backbone.Model</code></td><td><p>The model holding data for this cell</p>
</td>
    </tr><tr>
    <td>column</td><td><code>*</code></td><td><p>Column definition of this cell.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TableCellView..TemplatedCellView"></a>

### TableCellView~TemplatedCellView
**Kind**: inner class of [<code>TableCellView</code>](#module_TableCellView)  
<a name="new_module_TableCellView..TemplatedCellView_new"></a>

#### new TemplatedCellView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options. Following are the options specifically supported by the list apart from those from its supertype.</p>
</td>
    </tr><tr>
    <td>options.column</td><td><code>*</code></td><td><p>Column definition for this table cell.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TableCellView..renderValue"></a>

### *TableCellView~renderValue() ⇒ <code>void</code>*
Render the value associated with this cell. Contains logic to render and sytlize the corresponding DOM element

**Kind**: inner abstract method of [<code>TableCellView</code>](#module_TableCellView)  
<a name="module_TableCellView..getValueText"></a>

### *TableCellView~getValueText() ⇒ <code>string</code>*
Get renderable formatted value for this table cell.

**Kind**: inner abstract method of [<code>TableCellView</code>](#module_TableCellView)  
