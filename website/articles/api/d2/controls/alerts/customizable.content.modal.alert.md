<a name="module_CustomizableContentModalAlert"></a>

## CustomizableContentModalAlert : <code>d2/sdk/controls/customizable.content.modal.alert</code>
Shows a modal alert with customized body content.The `options` parameter can contain a property `contentView` that is used as the modal's body. If the `contentView` is not present thenin it's all sense it behaves like a regular [ModalAlert](./modal.alert)

**Extends**: [<code>ModalAlert</code>](./modal.alert)  
**See**: [ModalAlert](./modal.alert) for a list of static methods available to instantiate a modal alert with custom content.  
**Example** *(To show a ModalAlert with another Marionette.View as content)*  
```js
define([
 'nuc/lib/marionette',
 'd2/controls/alerts/customizable.content.modal.alert'
], function(Marionette, ModalAlert){
 'use strict';

 var ContentView = Marionette.LayoutView.extend({
   template: false,
   className: 'sample-modal-content',
   constructor: function ContentView(options) {
     options = options || {};
     ContentView.__super__.constructor.call(this, options);
   },

   onRender: function() {
     this.$el.text('Hello World!');
   }
 });

 ModalAlert.showInformation(function(result){
   console.log(result);
 }, '' , 'Greetings', {
   buttons: {
     showYes: true,
     labelYes: 'OK',
     showNo: false,
     showCancel: false
   },
   contentView: new ContentView(),
   modalClass: 'my-sample-greeting-modal'
 });
});
```
