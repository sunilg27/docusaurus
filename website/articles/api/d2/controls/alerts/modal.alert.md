<a name="module_ModalAlert"></a>

## ModalAlert : <code>d2/sdk/controls/modal.alert</code>
Shows a modal alert.Different static methods could be used to show different type of modal alerts.

**Extends**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)  
**Example** *(A sample invocation to show a confirmation alert)*  
```js
define([
 'd2/sdk/controls/modal.alert'
], function(ModalAlert){
 'use strict';

 var buttonOptions = {
   showYes: true,
   labelYes: 'Ok',
   showNo: false,
   showCancel: true,
   labelCancel: 'Cancel'
 };

 ModalAlert.confirmQuestion(function(result){
   if(result) {
     console.log('Clicked OK');
   } else {
     console.log('Clicked Cancel');
   }
 }, 'sample message', 'Sample Title', {
    buttons: buttonOptions
 });
});
```

* [ModalAlert](#module_ModalAlert) : <code>d2/sdk/controls/modal.alert</code>
    * _static_
        * [.ButtonOptions](#module_ModalAlert.ButtonOptions)
        * [.showSuccess([callback], [message], [title], [options])](#module_ModalAlert.showSuccess) ⇒ <code>PromiseReturnType</code>
        * [.showInformation([callback], [message], [title], [options])](#module_ModalAlert.showInformation) ⇒ <code>PromiseReturnType</code>
        * [.showWarning([callback], [message], [title], [options])](#module_ModalAlert.showWarning) ⇒ <code>PromiseReturnType</code>
        * [.showError([callback], [message], [title], [options])](#module_ModalAlert.showError) ⇒ <code>PromiseReturnType</code>
        * [.showMessage([callback], [message], [title], [options])](#module_ModalAlert.showMessage) ⇒ <code>PromiseReturnType</code>
        * [.confirmSuccess([callback], [message], [title], [options])](#module_ModalAlert.confirmSuccess) ⇒ <code>PromiseReturnType</code>
        * [.confirmInformation([callback], [message], [title], [options])](#module_ModalAlert.confirmInformation) ⇒ <code>PromiseReturnType</code>
        * [.confirmWarning([callback], [message], [title], [options])](#module_ModalAlert.confirmWarning) ⇒ <code>PromiseReturnType</code>
        * [.confirmError([callback], [message], [title], [options])](#module_ModalAlert.confirmError) ⇒ <code>PromiseReturnType</code>
        * [.confirmQuestion([callback], [message], [title], [options])](#module_ModalAlert.confirmQuestion) ⇒ <code>PromiseReturnType</code>
        * [.confirmMessage([callback], [message], [title], [options])](#module_ModalAlert.confirmMessage) ⇒ <code>PromiseReturnType</code>
    * _inner_
        * [~AlertDissolveCallback](#module_ModalAlert..AlertDissolveCallback) ⇒ <code>void</code>
        * [~PromiseReturnType](#module_ModalAlert..PromiseReturnType) : <code>jQuery.Promise</code>
        * [~OptionsType](#module_ModalAlert..OptionsType) : <code>Object</code>

<a name="module_ModalAlert.ButtonOptions"></a>

### ModalAlert.ButtonOptions
A few pre-configured button options to ease up modal creation with preset

**Kind**: static property of [<code>ModalAlert</code>](#module_ModalAlert)  
**Todo**

- Describe different button options

<a name="module_ModalAlert.showSuccess"></a>

### ModalAlert.showSuccess([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows a success modal alert

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.showInformation"></a>

### ModalAlert.showInformation([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows a info modal alert

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.showWarning"></a>

### ModalAlert.showWarning([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows a warning modal alert

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.showError"></a>

### ModalAlert.showError([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows an error modal alert

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.showMessage"></a>

### ModalAlert.showMessage([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows a generic modal alert without associating with a type

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.confirmSuccess"></a>

### ModalAlert.confirmSuccess([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows an interrogative modal alert representing success

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.confirmInformation"></a>

### ModalAlert.confirmInformation([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows an interrogative modal alert representing information

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.confirmWarning"></a>

### ModalAlert.confirmWarning([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows an interrogative modal alert representing warning

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.confirmError"></a>

### ModalAlert.confirmError([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows an interrogative modal alert representing error

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.confirmQuestion"></a>

### ModalAlert.confirmQuestion([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows a generic interrogative modal alert

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert.confirmMessage"></a>

### ModalAlert.confirmMessage([callback], [message], [title], [options]) ⇒ <code>PromiseReturnType</code>
Shows a generic interrogative modal alert representing a message

**Kind**: static method of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>AlertDissolveCallback</code></td><td><p>Executed with a hint about user interaction.</p>
</td>
    </tr><tr>
    <td>[message]</td><td><code>string</code></td><td><p>The message to be shown.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code></td><td><p>Title of the alert.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>OptionsType</code></td><td><p>A single option hash to customize the modal title, message &amp; button options</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert..AlertDissolveCallback"></a>

### ModalAlert~AlertDissolveCallback ⇒ <code>void</code>
Callback type invoked, when the modal dissolves.

**Kind**: inner typedef of [<code>ModalAlert</code>](#module_ModalAlert)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>result</td><td><code>boolean</code> | <code>undefined</code></td><td><p><code>true</code> indicates &#39;Yes&#39; button, <code>false</code> indicates &#39;No&#39; and <code>undefined</code> indicates the modal was cancelled.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ModalAlert..PromiseReturnType"></a>

### ModalAlert~PromiseReturnType : <code>jQuery.Promise</code>
Return type. Associated `jQuery.Promise` instance is  `resolve()`-ed or `reject()`-ed to indicate whether user pressed 'Yes' or some other button respectively.

**Kind**: inner typedef of [<code>ModalAlert</code>](#module_ModalAlert)  
<a name="module_ModalAlert..OptionsType"></a>

### ModalAlert~OptionsType : <code>Object</code>
Customize modal's title, message, button labels and configuration

**Kind**: inner typedef of [<code>ModalAlert</code>](#module_ModalAlert)  
**Todo**

- Enumerate different properties

