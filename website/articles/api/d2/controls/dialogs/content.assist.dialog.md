<a name="module_ContentAssistDialog"></a>

## ContentAssistDialog : <code>d2/sdk/controls/dialogs/content.assist.dialog</code>
Content Assist Dialog. Helps navigating through the repository while selecting an object.Content assist configuration is to be fetched first to control constructor options. See [ContentAssistDialogUtil](../../utils/content.assist).

**Extends**: <code>Object</code>  
**Example** *(A typical usage example)*  
```js
define([
 'nuc/lib/underscore',
 'd2/sdk/controls/dialogs/content.assist.dialog',
 'd2/sdk/models/node.model',
 'd2/sdk/utils/contexts/context.utils',
 'd2/sdk/utils/contexts/factories/connector',
 'd2/sdk/utils/content.assist'
], function(_, NodePicker, NodeModel, contextUtils, ConnectorFactory, contentAssistUtil){
 'use strict';

  var context = contextUtils.getPerspectiveContext(),
   connector = context.getObject(ConnectorFactory),
   container = new NodeModel({id: '0c0015fa8000291e'}, {connector: connector});

   container.fetch()
     .done(function(){
       showNodePicker(context, connector, container);
     })
     .fail(function(){
       console.log('failed fetching container');
     });

   function showNodePicker(context, connector, container) {
     contentAssistUtil.getContentAssistOptions({context: context})
     .done(function(config){
       var title = config ? config.label_value : "",
         includeBrowse = (config && config.properties) ? config.properties.has_browse_assist : true,
         includeFavorites = (config && config.properties) ? config.properties.has_favorites_assist : false,
         globalSearch = (config && config.properties) ? config.properties.has_search_assist : false;

       // Enable browsing if nothing is enabled
       if (!includeBrowse && !includeFavorites && !globalSearch) {
         includeBrowse = true;
       }

       var nodePicker = new NodePicker({
         dialogTitle: title,
         startLocations: [],
         includeBrowse: includeBrowse,
         includeFavorites: includeFavorites,
         globalSearch: globalSearch,
         context: context,
         connector: connector,
         initialContainer: container,
         selectMultiple: false
       });

       nodePicker.show()
         .done(function(selection){
           console.log('Selected ' + selection.nodes.length + ' items.');
         })
         .fail(function(){
           console.log('User cancelled');
         });
     })
     .fail(function(){
       console.log('failed to load options for the node picker');
     });
   }

});
```

* [ContentAssistDialog](#module_ContentAssistDialog) : <code>d2/sdk/controls/dialogs/content.assist.dialog</code>
    * _instance_
        * [.show()](#module_ContentAssistDialog+show) ⇒ <code>jQuery.Promise</code>
    * _inner_
        * [~ContentAssistDialog](#module_ContentAssistDialog..ContentAssistDialog)
            * [new ContentAssistDialog(options)](#new_module_ContentAssistDialog..ContentAssistDialog_new)
        * [~ContentAssistFinishCallback](#module_ContentAssistDialog..ContentAssistFinishCallback) : <code>function</code>

<a name="module_ContentAssistDialog+show"></a>

### contentAssistDialog.show() ⇒ <code>jQuery.Promise</code>
Shows the dialog

**Kind**: instance method of [<code>ContentAssistDialog</code>](#module_ContentAssistDialog)  
**Returns**: <code>jQuery.Promise</code> - resolves & invokes `ContentAssistFinishCallback` on positive (select/add) user interaction, rejects otherwise (user cancels).  
<a name="module_ContentAssistDialog..ContentAssistDialog"></a>

### ContentAssistDialog~ContentAssistDialog
**Kind**: inner class of [<code>ContentAssistDialog</code>](#module_ContentAssistDialog)  
<a name="new_module_ContentAssistDialog..ContentAssistDialog_new"></a>

#### new ContentAssistDialog(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td></td><td><p>Construction options holder</p>
</td>
    </tr><tr>
    <td>[options.dialogTitle]</td><td><code>string</code></td><td></td><td><p>Title of the dialog.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td></td><td><p>The connector instance. See <a href="../../utils/contexts/factories/connector">ConnectorFactory</a>.</p>
</td>
    </tr><tr>
    <td>options.initialContainer</td><td><code><a href="../../models/node.model">NodeModel</a></code></td><td></td><td><p>Begin browsing from this folder/cabinet. The passed object must be fetched before hand.</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td></td><td><p>Active application context. See <a href="../../utils/contexts/context.utils">ContextUtils</a></p>
</td>
    </tr><tr>
    <td>options.startLocations</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>Set the supported content display locations. Supported values are <code>&#39;search.location&#39;, &#39;current.location&#39;, &#39;favorites&#39;. At least an empty array must be passed if following shorthand flags </code>globalSearch<code>, </code>includeBrowse<code>, </code>includeFavorites` are used to control
the start locations.</p>
</td>
    </tr><tr>
    <td>[options.globalSearch]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Show searchable locations. Short hand for setting <code>search.location</code> as part of <code>startLocations</code>.</p>
</td>
    </tr><tr>
    <td>[options.includeBrowse]</td><td><code>Boolean</code></td><td><code>true</code></td><td><p>Show browsable locations from Documentum repository. Short hand for setting <code>current.location</code> as
part of <code>startLocations</code>.</p>
</td>
    </tr><tr>
    <td>[options.includeFavorites]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Show favorited contents. Short hand for setting <code>favorites</code> as part of <code>startLocations</code>.</p>
</td>
    </tr><tr>
    <td>[options.selectMultiple]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Enable multiple selection.</p>
</td>
    </tr><tr>
    <td>[options.selectType]</td><td><code>string</code></td><td><code>null</code></td><td><p>Attribute to check while enabling/diabling selectability of a node. <code>&#39;typeName&#39;</code> could be used as a value.</p>
</td>
    </tr><tr>
    <td>[options.selectTypeValue]</td><td><code>string</code></td><td></td><td><p>A node is selectable only if its <code>selectType</code> attribute&#39;s value mathes with this value.</p>
</td>
    </tr><tr>
    <td>[options.excludeTypes]</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>Exclude these node types from display.</p>
</td>
    </tr><tr>
    <td>[options.url_filter]</td><td><code>string</code></td><td><code>null</code></td><td><p>An additional fileter query parameter to be used while populating content assist dialog with objects.</p>
</td>
    </tr><tr>
    <td>[options.selectButtonLabel]</td><td><code>string</code></td><td></td><td><p>Text to be shown on the affirmative button.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ContentAssistDialog..ContentAssistFinishCallback"></a>

### ContentAssistDialog~ContentAssistFinishCallback : <code>function</code>
**Kind**: inner typedef of [<code>ContentAssistDialog</code>](#module_ContentAssistDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>selectedNodes</td><td><code>Object</code></td><td><p>Selected nodes holder</p>
</td>
    </tr><tr>
    <td>selectedNodes.nodes</td><td><code><a href="../../models/node.model">Array.&lt;NodeModel&gt;</a></code></td><td><p>Collection of nodes that were selected.</p>
</td>
    </tr>  </tbody>
</table>

