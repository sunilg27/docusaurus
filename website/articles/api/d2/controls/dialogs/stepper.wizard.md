<a name="module_StepperWizard"></a>

## StepperWizard : <code>d2/sdk/controls/dialogs/stepper.wizard</code>
Create a modal wizard that navigates through a configured set of steps to achieve some arbitrary functionality.Steps can be navigated in a simple-sequential manner or on-demand hopping manner.

**Extends**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)  
**Emits**: [<code>wizard:shown</code>](#module_StepperWizard+wizard_shown), [<code>wizard:minimized</code>](#module_StepperWizard+wizard_minimized), [<code>wizard:restored</code>](#module_StepperWizard+wizard_restored), [<code>wizard:closed</code>](#module_StepperWizard+wizard_closed), [<code>wizard:forceclose</code>](#module_StepperWizard+wizard_forceclose), [<code>step:before:active</code>](#module_StepperWizard+step_before_active), [<code>dom:refresh</code>](#module_StepperWizard+dom_refresh), [<code>step:active</code>](#module_StepperWizard+step_active), [<code>step:before:inactive</code>](#module_StepperWizard+step_before_inactive), [<code>step:inactive</code>](#module_StepperWizard+step_inactive)  
**Example** *(Minimal configuration for a two-step wizard)*  
```js
define([
 'nuc/lib/backbone',
 'nuc/lib/marionette',
 'd2/sdk/controls/dialogs/stepper.wizard'
 ],function(Backbone, Marionette, StepperWizard){
   'use strict';

   new StepperWizard({
     steps: [
       {
         id: 'first',
         viewClass: Marionette.View.extend({
           template: '<div>First Step View</div>'
         }),
         title: 'First Step Title',
       },
       {
         id: 'last',
         viewClass: Marionette.View.extend({
           template: '<div>Last Step View</div>'
         }),
         title: 'Last Step Title'
       }
    ]
  }).show();
});
```
**Example** *(Sample 3-step wizard)*  
```js
define([
 'nuc/lib/backbone',
 'nuc/lib/marionette',
 'd2/sdk/controls/dialogs/stepper.wizard'
],function(Backbone, Marionette, StepperWizard){
 'use strict';

 new StepperWizard({
   steps: [
     {
       id: 'first',
       viewClass: Marionette.View.extend({
         template: '<div>First Step View</div>'
       }),
       nextStepId: 'second',
       title: 'First Step Title',
       subTitle: 'First Step Subtitle'
     },
     {
       id: 'second',
       viewClass: Marionette.View.extend({
         template: '<div>Second Step View</div>'
       }),
       buttons: [
         {
           id: 'additional', //An extra button for second step
           label: 'Additional',
           align: 'begin',
           clickHandler: function(){
             console.log('additional button clicked at step 2');
           }
         }
       ],
       nextStepId: 'third',
       previousStepId: 'first',
       title: 'Second Step Title'
     },
     {
       id: 'third',
       viewClass: Marionette.View.extend({
         template: '<div>Third Step View</div>',
         onStepActive: function(){
           //Refresh all footer buttons
           this.options.wizard.refreshFooter();
         },
         isValid: function() {
           //some custom logic to determine the return value here will control 'disabled' state of the 'next' button
           return true;
         }
       }),
       buttons: [
         {
           id: 'next',
           label: 'Done',
           disabled: function() {
             // Enable/Disable the 'Done' button based on validation state after the step has become active
             return !this.isValid(); //'this' refers to the view instance of this step
           }
         }
       ],
       previousStepId: 'second',
       title: 'Third step title',
       subTitle: 'This is the last step'
     }
   ],
   data: { //This data will be shared accross all step views.
     model: new Backbone.Model({})
   }
 }).show();
});
```

* [StepperWizard](#module_StepperWizard) : <code>d2/sdk/controls/dialogs/stepper.wizard</code>
    * _instance_
        * [.show([stepId])](#module_StepperWizard+show) ⇒ <code>void</code>
        * [.close([...arg])](#module_StepperWizard+close) ⇒ <code>void</code>
        * [.cancel([...arg])](#module_StepperWizard+cancel) ⇒ <code>void</code>
        * [.minimize([promise], [notificationTooltip])](#module_StepperWizard+minimize) ⇒ <code>Boolean</code>
        * [.restore()](#module_StepperWizard+restore)
        * [.prevStep()](#module_StepperWizard+prevStep)
        * [.nextStep()](#module_StepperWizard+nextStep)
        * [.gotoStep(stepId, [animateFromEnd])](#module_StepperWizard+gotoStep)
        * [.refreshHeader()](#module_StepperWizard+refreshHeader)
        * [.refreshFooter([...buttonId])](#module_StepperWizard+refreshFooter)
        * [.getData()](#module_StepperWizard+getData) ⇒ <code>\*</code>
        * [.getId()](#module_StepperWizard+getId) ⇒ <code>string</code>
        * [.blockActions()](#module_StepperWizard+blockActions)
        * [.unblockActions()](#module_StepperWizard+unblockActions)
        * ["wizard:shown"](#module_StepperWizard+wizard_shown)
        * ["wizard:minimized"](#module_StepperWizard+wizard_minimized)
        * ["wizard:restored"](#module_StepperWizard+wizard_restored)
        * ["wizard:closed" (isCancel, [...args])](#module_StepperWizard+wizard_closed)
        * ["wizard:forceclose"](#module_StepperWizard+wizard_forceclose)
        * ["step:before:active"](#module_StepperWizard+step_before_active)
        * ["dom:refresh"](#module_StepperWizard+dom_refresh)
        * ["step:active"](#module_StepperWizard+step_active)
        * ["step:before:inactive"](#module_StepperWizard+step_before_inactive)
        * ["step:inactive"](#module_StepperWizard+step_inactive)
    * _inner_
        * [~StepperWizard](#module_StepperWizard..StepperWizard)
            * [new StepperWizard(options)](#new_module_StepperWizard..StepperWizard_new)
        * [~StepConfig](#module_StepperWizard..StepConfig) : <code>object</code>
        * [~ButtonConfig](#module_StepperWizard..ButtonConfig) : <code>object</code>
        * [~StepCancellationCallback](#module_StepperWizard..StepCancellationCallback) ⇒ <code>jQuery.Promise</code> \| <code>void</code>
        * [~ButtonClickHandler](#module_StepperWizard..ButtonClickHandler) ⇒ <code>void</code>

<a name="module_StepperWizard+show"></a>

### stepperWizard.show([stepId]) ⇒ <code>void</code>
Show the wizard after construction. The wizard becomes visible on screen only after this method is called.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
**Note**: By default the wizard shows up on the first step as specified in constructor options.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[stepId]</td><td><code>string</code></td><td><p>Instead of starting from the first step, start from this particular step. If a matching step is not found the wizard
disregards this parameter and starts from the first step.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+close"></a>

### stepperWizard.close([...arg]) ⇒ <code>void</code>
Close and finally destroy the wizard.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[...arg]</td><td><code>any</code></td><td><p>This method could be invoked with any number of parameters. All those parameters will be passed as event parameters for &quot;wizard:closed&quot;
event.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+cancel"></a>

### stepperWizard.cancel([...arg]) ⇒ <code>void</code>
Try closing the wizard with a CANCEL signal. If the signal is not interfered with, it eventually causes the wizard to close and destroy itself.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[...arg]</td><td><code>any</code></td><td><p>All arguments that this method is invoked with, will be passed as event parameters for &quot;wizard:closed&quot;</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+minimize"></a>

### stepperWizard.minimize([promise], [notificationTooltip]) ⇒ <code>Boolean</code>
Hide the wizard from screen without destroying it.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
**Returns**: <code>Boolean</code> - Returns `true` to indicate that the wizard is actually minimized. otherwise returns `false`.  
**Note**: Only one wizard can be minimized with a notification, every other instance that tries to do the same at the same time will fail to minimize itself  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[promise]</td><td><code>jQuery.Promise</code></td><td><p>Whether or not the wizard needs to be minimized with a notification on the toolbar.
Usually used when wizard is performing long-running background task on some step and it doesn&#39;t want to keep the end user blocked on the modal wizard.
Resolve or reject the promise to change status of the notification icon.</p>
</td>
    </tr><tr>
    <td>[notificationTooltip]</td><td><code>string</code></td><td><p>What should be the tooltip of the notification icon if wizard is minimized with a notification</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+restore"></a>

### stepperWizard.restore()
Restore a minimized hidden wizard and bring it back on screen.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+prevStep"></a>

### stepperWizard.prevStep()
Go to previous step as determined by 'steps' configuration of the wizard

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+nextStep"></a>

### stepperWizard.nextStep()
Go to next step as determined by 'steps' configuration of the wizard.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+gotoStep"></a>

### stepperWizard.gotoStep(stepId, [animateFromEnd])
Go to specific step with given animation direction. Allows random jump from one step to another.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>stepId</td><td><code>string</code></td><td></td><td><p>Id of the step to go to</p>
</td>
    </tr><tr>
    <td>[animateFromEnd]</td><td><code>Boolean</code></td><td><code>true</code></td><td><p>Whether or not the next step to be sweeped-in from end edge of the wizard.
If <code>false</code>, the step is sweeped-in from beginning of the wizard.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+refreshHeader"></a>

### stepperWizard.refreshHeader()
Refresh header to show updated data (title & subTitle) for the step if they were changed

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+refreshFooter"></a>

### stepperWizard.refreshFooter([...buttonId])
Refresh footer to show updated button states

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
**Note**: If the method is called without any parameter then all buttons of the active step are refreshed.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[...buttonId]</td><td><code>string</code></td><td><p>Id of the button to refresh. Multiple id could be passed as parameters.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+getData"></a>

### stepperWizard.getData() ⇒ <code>\*</code>
Returns a reference to the data shared accross steps

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
**Returns**: <code>\*</code> - The data that was set as part of constructor options.  
<a name="module_StepperWizard+getId"></a>

### stepperWizard.getId() ⇒ <code>string</code>
Returns a specifier to uniquely identify a wizard instance. Can be used later for inbound radio channel events

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
**Returns**: <code>string</code> - Unique identifier of this wizard instance  
<a name="module_StepperWizard+blockActions"></a>

### stepperWizard.blockActions()
Creates an indeterminate progress view over the wizard to block user interaction. The blocker remains active until `unblockActions()` is called.

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+unblockActions"></a>

### stepperWizard.unblockActions()
Withdraws an active user interaction blocker from over the wizard

**Kind**: instance method of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+wizard_shown"></a>

### "wizard:shown"
Triggered on the wizard instance after it appears on screen

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+wizard_minimized"></a>

### "wizard:minimized"
Triggered on the wizard instance after it is minimized

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+wizard_restored"></a>

### "wizard:restored"
Triggered on the wizard instance after it is restored from minimized state.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+wizard_closed"></a>

### "wizard:closed" (isCancel, [...args])
Triggered on the wizard instance after it is closed.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>isCancel</td><td><code>Boolean</code></td><td><p>Whether the wizard was closed or cancelled.</p>
</td>
    </tr><tr>
    <td>[...args]</td><td><code>any</code></td><td><p>Any additional parameters paassed to <code>cancel()</code> or <code>close()</code></p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard+wizard_forceclose"></a>

### "wizard:forceclose"
Triggered on the wizard instance just before it is about to be force closed due to unplanned page navigation e.g. reload

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+step_before_active"></a>

### "step:before:active"
Triggered on StepView instance when the correspoding step is about to appear on screen.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+dom_refresh"></a>

### "dom:refresh"
Triggered on the StepView when it appears on screen.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+step_active"></a>

### "step:active"
Triggered on the StepView after the step has become active and DOM is updated with corresponding view.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+step_before_inactive"></a>

### "step:before:inactive"
Triggered on the StepView when it is about to disappear from screen.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard+step_inactive"></a>

### "step:inactive"
Triggered on the StepView after the view has been removed from DOM.

**Kind**: event emitted by [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="module_StepperWizard..StepperWizard"></a>

### StepperWizard~StepperWizard
**Kind**: inner class of [<code>StepperWizard</code>](#module_StepperWizard)  
<a name="new_module_StepperWizard..StepperWizard_new"></a>

#### new StepperWizard(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td></td><td><p>Constructor options</p>
</td>
    </tr><tr>
    <td>options.steps</td><td><code>Array.&lt;StepConfig&gt;</code></td><td></td><td><p>Step configuration of the wizard. Each step is represented with a different screen and a transition animation
is played when the wizard moves from one step to another due to user interaction.</p>
</td>
    </tr><tr>
    <td>[options.showPageLeavingWarning]</td><td><code>Boolean</code></td><td><code>true</code></td><td><p>Whether to show a page blocker when user tries to close the wizard.</p>
</td>
    </tr><tr>
    <td>[options.suppressHeader]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Indicate whether to hide the header.</p>
</td>
    </tr><tr>
    <td>[options.suppressFooter]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Indicate whether to hide the footer.</p>
</td>
    </tr><tr>
    <td>[options.usePreviousStepTitleAsBackButtonTooltip]</td><td><code>Boolean</code></td><td><code>true</code></td><td><p>Whether to show previous step&#39;s title as tooltip for <code>Back</code> button shown on current step.</p>
</td>
    </tr><tr>
    <td>[options.data]</td><td><code>object</code></td><td></td><td><p>Any arbitrary piece of data that is shared among all the steps i.e views representing the steps.</p>
</td>
    </tr><tr>
    <td>[options.className]</td><td><code>string</code></td><td></td><td><p>An additional CSS class name to be used along with its own styling classes. Useful, when specific styling
classes are required to be written for step views or customize overall appearance of the wizard.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard..StepConfig"></a>

### StepperWizard~StepConfig : <code>object</code>
Configuration for each step

**Kind**: inner typedef of [<code>StepperWizard</code>](#module_StepperWizard)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>id</td><td><code>string</code></td><td></td><td><p>A unique identifier for the step</p>
</td>
    </tr><tr>
    <td>viewClass</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td></td><td><p>View definition(not an instance, the class itself) which will be used to create presentation for the corresponding step.</p>
</td>
    </tr><tr>
    <td>[nextStepId]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Id of the step, which the wizard should show next. Defaults to &#39;id&#39; of the next step in config, if any.</p>
</td>
    </tr><tr>
    <td>[previousStepId]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Id of the step, which the wizard should go back to from present step. Defaults to &#39;id&#39; of the previous step in config, if any.</p>
</td>
    </tr><tr>
    <td>[title]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Used to set title for the step</p>
</td>
    </tr><tr>
    <td>[subTitle]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Used to set sub-title for the step</p>
</td>
    </tr><tr>
    <td>[buttons]</td><td><code>Array.&lt;ButtonConfig&gt;</code></td><td></td><td><p>Configuration to control behavior of default and/or any additional button for this step. Three customizable buttons are automatically created by the wizard to facilitate back &amp; forth navigation between steps and cancellation.
Any other buttons configured here will be created upon activation of corresponding step</p>
</td>
    </tr><tr>
    <td>[onCancel]</td><td><code>StepCancellationCallback</code></td><td></td><td><p>Callback to decide whether or not to close a wizard upon receiving CANCEL signal from either <code>close</code>,<code>cancel</code> button press or <code>cancel()</code> API call.</p>
</td>
    </tr><tr>
    <td>[viewOptions]</td><td><code>object</code></td><td></td><td><p>An unmanaged JS Object that will be passed only onto the view&#39;s constructor for this step</p>
</td>
    </tr><tr>
    <td>[transient]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Indicates whether or not this step is to be considered a transient one. A transient step is never kept in step navigation history. In effect, with default navigation setup, clicking back on the step next to a transient step always brings the user back to the step before the transient one</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard..ButtonConfig"></a>

### StepperWizard~ButtonConfig : <code>object</code>
Configuration for a button

**Kind**: inner typedef of [<code>StepperWizard</code>](#module_StepperWizard)  
**Note**: Default `clickHandler` for button with id='previous', id='next' & id='cancel' are already defined by the wizard to control back & forth navigation and cancellation. Overriding those handlers may be desired for an alternative behavior for a step  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>id</td><td><code>string</code></td><td></td><td><p>Identifier for the button. Buttons with <code>id=&#39;next&#39;</code> &amp; <code>id=&#39;previous&#39;</code> &amp; <code>id=&#39;cancel&#39;</code> are already added to the set by the wizard.</p>
</td>
    </tr><tr>
    <td>[className]</td><td><code>string</code></td><td></td><td><p>Name of CSS class to be applied to the button</p>
</td>
    </tr><tr>
    <td>[align]</td><td><code>string</code></td><td><code>&quot;end&quot;</code></td><td><p>Alignment of the button. &#39;previous&#39;, &#39;next&#39;, &#39;cancel&#39; buttons come with pre-defined but customizable alignment.
Buttons aligned on the same side will be placed according to their order of definition</p>
</td>
    </tr><tr>
    <td>[label]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Label for the button</p>
</td>
    </tr><tr>
    <td>[ariaLabel]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Accessible title for the button. Defaults to same as <code>label</code>, if omitted or kept blank</p>
</td>
    </tr><tr>
    <td>[toolTip]</td><td><code>string</code> | <code>function</code></td><td></td><td><p>Tooltip for the button</p>
</td>
    </tr><tr>
    <td>[hidden]</td><td><code>Boolean</code> | <code>function</code></td><td><code>false</code></td><td><p>Whether this button should be hidden upon step activation or not</p>
</td>
    </tr><tr>
    <td>[disabled]</td><td><code>Boolean</code> | <code>function</code></td><td><code>false</code></td><td><p>Whether this button should be disabled upon step activation or not. The &#39;hidden&#39; attribute takes precedence over this &#39;disabled&#39; attribute</p>
</td>
    </tr><tr>
    <td>[clickHandler]</td><td><code>ButtonClickHandler</code></td><td></td><td><p>Default click handler for this button. This handler gets executed in context of the step&#39;s view</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_StepperWizard..StepCancellationCallback"></a>

### StepperWizard~StepCancellationCallback ⇒ <code>jQuery.Promise</code> \| <code>void</code>
**Kind**: inner typedef of [<code>StepperWizard</code>](#module_StepperWizard)  
**Returns**: <code>jQuery.Promise</code> \| <code>void</code> - Return a `JQuery Promise` object to control the decision. The wizard will close or discard the CANCEL signal depending on whether the `Promise` is resolved or rejected.If anything else is returned from the Function or the callback is not defined at all, the default 'CANCEL' signal handler will close the wizard.  
**Note**: This callback is executed in the context of view instance associated with the current srep.  
<a name="module_StepperWizard..ButtonClickHandler"></a>

### StepperWizard~ButtonClickHandler ⇒ <code>void</code>
**Kind**: inner typedef of [<code>StepperWizard</code>](#module_StepperWizard)  
**Note**: This callback is executed in the context of view instance associated with the current srep.  
