<a name="module_SimpleDialog"></a>

## SimpleDialog : <code>d2/sdk/controls/dialogs/simple.dialog</code>
Create a simple modal dialog. Has customizable header, footer and primary content.

**Extends**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)  
**Emits**: <code>event:&quot;shown.binf.modal&quot;</code>, <code>event:&quot;hidden.binf.modal&quot;</code>  
**Example**  
```js
define([ 'd2/sdk/controls/dialogs/simple.dialog'], function(SimpleDialog){ 'use strict'; var dialog = new SimpleDialog({title: 'Greetings', bodyMessage: 'Hello World!'}); dialog.show(); //Dialog is rendered with a close button at top-right corner of the header. dialog = new SimpleDialog({title: 'Greetings', bodyMessage: 'Hello World!', buttons: [{label: 'OK'}]}); dialog.show();//Dialog header does not show a close button anymore, however SimpleDialog automatically adds a 'Cancel' button to close it.});
```

* [SimpleDialog](#module_SimpleDialog) : <code>d2/sdk/controls/dialogs/simple.dialog</code>
    * _instance_
        * [.show()](#module_SimpleDialog+show) ⇒ <code>any</code>
        * [.updateButton(id, [attributes])](#module_SimpleDialog+updateButton) ⇒ <code>void</code>
        * [.destroy()](#module_SimpleDialog+destroy) ⇒ <code>void</code>
        * ["event:shown.binf.modal"](#module_SimpleDialog+event_shown.binf.modal)
        * ["event:hidden.binf.modal"](#module_SimpleDialog+event_hidden.binf.modal)
    * _inner_
        * [~SimpleDialog](#module_SimpleDialog..SimpleDialog)
            * [new SimpleDialog(options)](#new_module_SimpleDialog..SimpleDialog_new)
        * [~FooterButton](#module_SimpleDialog..FooterButton) : <code>Object</code>
        * [~FooterButtonClickCallback](#module_SimpleDialog..FooterButtonClickCallback) : <code>function</code>

<a name="module_SimpleDialog+show"></a>

### simpleDialog.show() ⇒ <code>any</code>
Shows the dialog after it has been instantiated

**Kind**: instance method of [<code>SimpleDialog</code>](#module_SimpleDialog)  
**Returns**: <code>any</code> - The dialog instance  
<a name="module_SimpleDialog+updateButton"></a>

### simpleDialog.updateButton(id, [attributes]) ⇒ <code>void</code>
Updates the dialog's footer button state.

**Kind**: instance method of [<code>SimpleDialog</code>](#module_SimpleDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>id</td><td><code>string</code></td><td><p>Identifier of the button to be updated.</p>
</td>
    </tr><tr>
    <td>[attributes]</td><td><code>object</code></td><td><p>New button state properties</p>
</td>
    </tr><tr>
    <td>[attributes.hidden]</td><td><code>Boolean</code></td><td><p>Whether is button is to be hidden.</p>
</td>
    </tr><tr>
    <td>[attributes.disabled]</td><td><code>Boolean</code></td><td><p>Whether this button should be disabled.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_SimpleDialog+destroy"></a>

### simpleDialog.destroy() ⇒ <code>void</code>
Hide & close the dialog

**Kind**: instance method of [<code>SimpleDialog</code>](#module_SimpleDialog)  
<a name="module_SimpleDialog+event_shown.binf.modal"></a>

### "event:shown.binf.modal"
Triggers on the dialog's HTML element when it is shown.

**Kind**: event emitted by [<code>SimpleDialog</code>](#module_SimpleDialog)  
<a name="module_SimpleDialog+event_hidden.binf.modal"></a>

### "event:hidden.binf.modal"
Trigger on the dialog's HTML element when it becomes hidden.

**Kind**: event emitted by [<code>SimpleDialog</code>](#module_SimpleDialog)  
<a name="module_SimpleDialog..SimpleDialog"></a>

### SimpleDialog~SimpleDialog
**Kind**: inner class of [<code>SimpleDialog</code>](#module_SimpleDialog)  
<a name="new_module_SimpleDialog..SimpleDialog_new"></a>

#### new SimpleDialog(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options</p>
</td>
    </tr><tr>
    <td>options.title</td><td><code>string</code></td><td><p>Title of the dialog.</p>
</td>
    </tr><tr>
    <td>[options.fullSize]</td><td><code>Boolean</code></td><td><p>Whether to show a full page dialog.</p>
</td>
    </tr><tr>
    <td>[options.largeSize]</td><td><code>Boolean</code></td><td><p>Shows a large dialog.</p>
</td>
    </tr><tr>
    <td>[options.midSize]</td><td><code>Boolean</code></td><td><p>Shows a medium dialog.</p>
</td>
    </tr><tr>
    <td>[options.smallSize]</td><td><code>Boolean</code></td><td><p>Shows a small dialog.</p>
</td>
    </tr><tr>
    <td>[options.bodyMessage]</td><td><code>string</code></td><td><p>A message to be shown at the primary content area.</p>
</td>
    </tr><tr>
    <td>[options.buttons]</td><td><code>Array.&lt;FooterButton&gt;</code></td><td><p>Buttons to show in the footer if footer is not customized with <code>footerView</code> options.
<code>SimpleDialog</code> automatically adds an additional button to close the dialog.</p>
</td>
    </tr><tr>
    <td>[options.view]</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>Customize the primary content area with this view instance.</p>
</td>
    </tr><tr>
    <td>[options.headerView]</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>Customize the dialog&#39;s header completely with this view instance.</p>
</td>
    </tr><tr>
    <td>[options.footerView]</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>Customize the dialog&#39;s footer completely with this view instance.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_SimpleDialog..FooterButton"></a>

### SimpleDialog~FooterButton : <code>Object</code>
**Kind**: inner typedef of [<code>SimpleDialog</code>](#module_SimpleDialog)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>label</td><td><code>string</code></td><td><p>Text to be displayed on the button</p>
</td>
    </tr><tr>
    <td>[toolTip]</td><td><code>string</code></td><td><p>Tooltip associated with the button.</p>
</td>
    </tr><tr>
    <td>[id]</td><td><code>string</code></td><td><p>Unique identifier of this button.</p>
</td>
    </tr><tr>
    <td>[default]</td><td><code>Boolean</code></td><td><p>Whether this button is default button in the footer. Default button is drawn with a visual distinction compared
to other buttons.</p>
</td>
    </tr><tr>
    <td>[separate]</td><td><code>Boolean</code></td><td><p>Whether this button is to be drawn separately.</p>
</td>
    </tr><tr>
    <td>[click]</td><td><code>FooterButtonClickCallback</code></td><td><p>Callback to handle click event from the button.</p>
</td>
    </tr><tr>
    <td>[close]</td><td><code>Boolean</code></td><td><p>Whether this button closes the dialog.</p>
</td>
    </tr><tr>
    <td>[hidden]</td><td><code>Boolean</code></td><td><p>Whether this button should be hidden</p>
</td>
    </tr><tr>
    <td>[disabled]</td><td><code>Boolean</code></td><td><p>Renders a button in disabled state.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_SimpleDialog..FooterButtonClickCallback"></a>

### SimpleDialog~FooterButtonClickCallback : <code>function</code>
**Kind**: inner typedef of [<code>SimpleDialog</code>](#module_SimpleDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td></td>
    </tr><tr>
    <td>dialog</td><td><code>*</code></td><td><p>The dialog itself.</p>
</td>
    </tr><tr>
    <td>button</td><td><code>HTMLElement</code></td><td><p>DOM element of the button that is clicked.</p>
</td>
    </tr><tr>
    <td>buttonAttributes</td><td><code>Object</code></td><td><p>Attributes of the button.</p>
</td>
    </tr>  </tbody>
</table>

