<a name="module_DialogActions"></a>

## DialogActions : <code>d2/sdk/controls/dialogs/generic/dialog.actions</code>
Collection of actionables for D2SV generic dialogs. Each screens of D2SV generic dialog offers a few footer buttons for the end user tointeract with in order to change state of the dialog. Items from this collection acts as handler of button clicks for D2SV generic dialogsuch that each item could be loosely attached to any of generic dialog's footer button after evaluating the action's matching conditions.By default, D2SV runtime registers actions that matches buttons with id 'buttonOk', 'buttonCancel', 'cancel' & 'previous'. New actions couldbe registered by means of extensions.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**See**: [DialogAction](./dialog.action)  
**Example** *(To register new actions, in extensions.json)*  
```js
{
 "d2/sdk/controls/dialogs/generic/dialog.actions": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/generic.dialog.actions"
     ]
   }
 }
}
```
**Example** *(whereas generic.dialog.actions.js could be defined as)*  
```js
import ActionA from 'mybundle/generic.dialog/action.a';
import ActionB from 'mybundle/generic.dialog/action.b';

const actionRules = [
 {equals: {"button.id": "custom1"}, action: ActionA},
 {equals: {"button.id": "custom2"}, action: ActionB}
];

export default actionRules;
```
