<a name="module_DialogSchemaBuilders"></a>

## DialogSchemaBuilders : <code>d2/sdk/controls/dialogs/generic/dialog.schema.builders</code>
D2SV offers registration of custom transformers that work to transform a D2FS dialog structure intoUI compatible form schema. This DialogSchemaBuilders collection holds all such registered transformers.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(To register a new transformer extensions.json is to be modified as below)*  
```js
{
 "d2/sdk/controls/dialog/generic/dialog.schema.builder": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/dialog.transformers"
     ]
   }
 }
}
```
**Example** *(whereas dialog.transformers.js may be defined as )*  
```js
define([
  'mybundle/dialogs/generic/my.dialog.transformer'
], function(MyDialogTransformer){
 return [{
   sequence: 1, //defines order of execution across all registered plugins of D2SV, transformers with same sequence number will get executed as per their corresponding D2SV plugin discovery order which is indeterministic in nature.
   builderClass: MyDialogTransformer
 }];
});
```
**Example** *(whereas my.dialog.transformer.js may be defined as )*  
```js
import DialogSchemaBuilder from 'd2/sdk/controls/dialogs/generic/dialog.schema.builder';

class MyDialogTransformer extends DialogSchemaBuilder {
  buildSchema(formName, formData) {
    //formName: D2FS dialog name
     //formData: The dialog structure after its initial conversion into UI compatible schema
     //this function should return the modified 'formData' after applying its own logic
     //this function can also return a Promise which could later be resolve with the formData post transformation.
  }
}

export default MyDialogTransformer;
```
