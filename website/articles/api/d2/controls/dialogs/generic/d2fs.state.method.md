<a name="module_D2FSStateMethod"></a>

## D2FSStateMethod : <code>d2/sdk/controls/dialogs/generic/d2fs.state.method</code>
D2FSStateMethod. A default, contextful implementation of state methods that makes it possible to retrieve and transition throughdifferent states of D2FS dialogs. To customize the default implementation with lesser amount of coding, this class canbe extended into scenario specific subclass and added to state methods collection by means of extension with proper binding rules.Also see [DialogStateMethods](./dialog.state.methods)

**Extends**: [<code>DialogStateMethod</code>](./dialog.state.method)  

* [D2FSStateMethod](#module_D2FSStateMethod) : <code>d2/sdk/controls/dialogs/generic/d2fs.state.method</code>
    * [.execute(options, result)](#module_D2FSStateMethod+execute) ⇒ <code>Promise.&lt;boolean&gt;</code>
    * [.setDialogContextForm(dialogId, action, currentFormName)](#module_D2FSStateMethod+setDialogContextForm) ⇒ <code>void</code>
    * [.getDialogContextForm(dialogId, action)](#module_D2FSStateMethod+getDialogContextForm) ⇒ <code>string</code> \| <code>null</code>
    * [.clearDialogContextForm(dialogId)](#module_D2FSStateMethod+clearDialogContextForm) ⇒ <code>void</code>

<a name="module_D2FSStateMethod+execute"></a>

### d2FSStateMethod.execute(options, result) ⇒ <code>Promise.&lt;boolean&gt;</code>
Implements contextful state transition logic for D2FS dialog.

**Kind**: instance method of [<code>D2FSStateMethod</code>](#module_D2FSStateMethod)  
**Returns**: <code>Promise.&lt;boolean&gt;</code> - Resolve with `false` to stop executing any other matching state methods that are supposed to execute after it.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>MethodMetadata</code></td><td><p>holder for various current state related metadata</p>
</td>
    </tr><tr>
    <td>result</td><td><code>OutputFormData</code></td><td><p>Output parameter, should be filled in with metadata pertaining to next possible state of the dialog.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_D2FSStateMethod+setDialogContextForm"></a>

### d2FSStateMethod.setDialogContextForm(dialogId, action, currentFormName) ⇒ <code>void</code>
Set the context before applying this method to current state of the dialog.

**Kind**: instance method of [<code>D2FSStateMethod</code>](#module_D2FSStateMethod)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>dialogId</td><td><code>string</code></td><td><p>A uniqueue identifier associated with current dialog.</p>
</td>
    </tr><tr>
    <td>action</td><td><code>string</code></td><td><p>Name of the method getting applied</p>
</td>
    </tr><tr>
    <td>currentFormName</td><td><code>string</code></td><td><p>Present state&#39;s metadata name for the dialog.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_D2FSStateMethod+getDialogContextForm"></a>

### d2FSStateMethod.getDialogContextForm(dialogId, action) ⇒ <code>string</code> \| <code>null</code>
Get context before applying this method to current state of the dialog.

**Kind**: instance method of [<code>D2FSStateMethod</code>](#module_D2FSStateMethod)  
**Returns**: <code>string</code> \| <code>null</code> - Should return contextual state name, however can return `null` to implement a contextless flow.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>dialogId</td><td><code>string</code></td><td><p>A uniqueue identifier associated with current dialog.</p>
</td>
    </tr><tr>
    <td>action</td><td><code>string</code></td><td><p>Name of the method about to be applied.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_D2FSStateMethod+clearDialogContextForm"></a>

### d2FSStateMethod.clearDialogContextForm(dialogId) ⇒ <code>void</code>
Clears context for given dialog when the dialog has reached a terminal state.

**Kind**: instance method of [<code>D2FSStateMethod</code>](#module_D2FSStateMethod)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>dialogId</td><td><code>string</code></td><td><p>A uniqueue identifier associated with current dialog.</p>
</td>
    </tr>  </tbody>
</table>

