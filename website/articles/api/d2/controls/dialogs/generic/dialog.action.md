<a name="module_DialogAction"></a>

## DialogAction : <code>d2/sdk/controls/dialogs/generic/dialog.action</code>
DialogAction. Base class for defining handler of footer buttons in a D2SV generic dialog screen. All derived classes should override theasynchronous `execute()` method to implement appropriate handler logic. In its abstract notion, A dialog action should collect/validate the datastate of currently showing screen of D2SV generic dialog and then call 'DialogState' method to trigger transition to a next possible state.A dialog action could be thought of as a 'controller' in an MVC pattern.

**Extends**: [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)  
**See**: [DialogActions](./dialog.actions)  
**Example** *(A sample action implementation(goto.search.js) that triggers non-default state transition in generic dialog)*  
```js
import DialogAction from 'd2/sdk/controls/dialogs/generic/dialog.action';

class GotoSearch extends DialogAction {
 constructor(options={}) {
   super(options);
 }

 async execute(options) {
   const {formView, dialogState, dialog} = options;
   const formValues = formView.getFlatFormValueMap(formView.getValues());
   const {formName, formSchema: {generalConfig}} = await dialogState.applyFormStateMethod({
     method: 'SEARCH',
     formData: formValues
   });

   if(generalConfig) {//Go to search step only if the metadata indicates it.
     dialog.gotoStep(formName);
   }
 }
}
```
**Example** *(To associate this action with binding rule, an extension(my.dialog.actions.js) could be defined as)*  
```js
import GotoSearch from 'mybundle/dialogs/generic/actions/goto.search';

const rules = [
 {equals:{"button.id": "search"}, action: GotoSearch}
];

export default rules;
```
**Example** *(To finally register the extension, in extensions.json)*  
```js
{
 "d2/sdk/controls/dialogs/generic/dialog.actions": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/my.dialog.actions"
     ]
   }
 }
}
```

* [DialogAction](#module_DialogAction) : <code>d2/sdk/controls/dialogs/generic/dialog.action</code>
    * _instance_
        * [.execute(options)](#module_DialogAction+execute) ⇒ <code>Promise</code>
    * _inner_
        * [~DynamicDialog](#module_DialogAction..DynamicDialog) ⇐ [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
            * [.show(formName)](#module_DialogAction..DynamicDialog+show) ⇒ <code>this</code>
            * [.gotoStep(formName)](#module_DialogAction..DynamicDialog+gotoStep) ⇒ <code>void</code>
            * [.handleError(e)](#module_DialogAction..DynamicDialog+handleError) ⇒ <code>void</code>
            * [.cancel()](#module_DialogAction..DynamicDialog+cancel) ⇒ <code>void</code>
            * [.close()](#module_DialogAction..DynamicDialog+close) ⇒ <code>void</code>
        * [~DialogState](#module_DialogAction..DialogState) ⇐ [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)
            * [.gotoForm(formName)](#module_DialogAction..DialogState+gotoForm) ⇒ <code>Promise</code>
            * [.validateForm(formData)](#module_DialogAction..DialogState+validateForm) ⇒ <code>Promise</code>
            * [.cancelForm(formData)](#module_DialogAction..DialogState+cancelForm) ⇒ <code>Promise</code>
            * [.applyFormStateMethod(options)](#module_DialogAction..DialogState+applyFormStateMethod) ⇒ <code>Promise</code>
            * [.getCurrentFormName()](#module_DialogAction..DialogState+getCurrentFormName) ⇒ <code>string</code>
            * [.getRelativeFormPosition(srcForm, destForm)](#module_DialogAction..DialogState+getRelativeFormPosition) ⇒ <code>boolean</code>
            * [.nextForm()](#module_DialogAction..DialogState+nextForm) ⇒ <code>Promise</code>
            * [.previousForm()](#module_DialogAction..DialogState+previousForm) ⇒ <code>Promise</code>
        * [~StateMethodInput](#module_DialogAction..StateMethodInput) : <code>Object</code>
        * [~ButtonProperties](#module_DialogAction..ButtonProperties) : <code>Object</code>
        * [~ActionExecutionData](#module_DialogAction..ActionExecutionData) : <code>Object</code>

<a name="module_DialogAction+execute"></a>

### dialogAction.execute(options) ⇒ <code>Promise</code>
Implement controlling logic behind this action

**Kind**: instance method of [<code>DialogAction</code>](#module_DialogAction)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code><a href="#module_DialogAction..ActionExecutionData">ActionExecutionData</a></code></td><td><p>holder for various contextual data related to the action&#39;s execution</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DynamicDialog"></a>

### DialogAction~DynamicDialog ⇐ [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
Dynamic dialog UI interface.

**Kind**: inner class of [<code>DialogAction</code>](#module_DialogAction)  
**Extends**: [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)  

* [~DynamicDialog](#module_DialogAction..DynamicDialog) ⇐ [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
    * [.show(formName)](#module_DialogAction..DynamicDialog+show) ⇒ <code>this</code>
    * [.gotoStep(formName)](#module_DialogAction..DynamicDialog+gotoStep) ⇒ <code>void</code>
    * [.handleError(e)](#module_DialogAction..DynamicDialog+handleError) ⇒ <code>void</code>
    * [.cancel()](#module_DialogAction..DynamicDialog+cancel) ⇒ <code>void</code>
    * [.close()](#module_DialogAction..DynamicDialog+close) ⇒ <code>void</code>

<a name="module_DialogAction..DynamicDialog+show"></a>

#### dynamicDialog.show(formName) ⇒ <code>this</code>
Shows a generic dialog post construction.

**Kind**: instance method of [<code>DynamicDialog</code>](#module_DialogAction..DynamicDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formName</td><td><code>string</code></td><td><p>Show the generic dialog starting from this step</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DynamicDialog+gotoStep"></a>

#### dynamicDialog.gotoStep(formName) ⇒ <code>void</code>
Triggers transition of the dialog to given step.

**Kind**: instance method of [<code>DynamicDialog</code>](#module_DialogAction..DynamicDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formName</td><td><code>string</code></td><td><p>Id of the dialog state to transition into</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DynamicDialog+handleError"></a>

#### dynamicDialog.handleError(e) ⇒ <code>void</code>
handles error during the dialog operations. DynamicDialog internally uses delegation pattern to to handoff the error into a fitting UI representation.

**Kind**: instance method of [<code>DynamicDialog</code>](#module_DialogAction..DynamicDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>e</td><td><code>Error</code></td><td><p>The error to be handled.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DynamicDialog+cancel"></a>

#### dynamicDialog.cancel() ⇒ <code>void</code>
Execute cancellation flow on this dialog. Upon successful cancellation the dialog is finally closed & disposed off.

**Kind**: instance method of [<code>DynamicDialog</code>](#module_DialogAction..DynamicDialog)  
<a name="module_DialogAction..DynamicDialog+close"></a>

#### dynamicDialog.close() ⇒ <code>void</code>
Close the dialog and dispose it off.

**Kind**: instance method of [<code>DynamicDialog</code>](#module_DialogAction..DynamicDialog)  
<a name="module_DialogAction..DialogState"></a>

### DialogAction~DialogState ⇐ [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)
State model of the current dialog

**Kind**: inner class of [<code>DialogAction</code>](#module_DialogAction)  
**Extends**: [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)  

* [~DialogState](#module_DialogAction..DialogState) ⇐ [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)
    * [.gotoForm(formName)](#module_DialogAction..DialogState+gotoForm) ⇒ <code>Promise</code>
    * [.validateForm(formData)](#module_DialogAction..DialogState+validateForm) ⇒ <code>Promise</code>
    * [.cancelForm(formData)](#module_DialogAction..DialogState+cancelForm) ⇒ <code>Promise</code>
    * [.applyFormStateMethod(options)](#module_DialogAction..DialogState+applyFormStateMethod) ⇒ <code>Promise</code>
    * [.getCurrentFormName()](#module_DialogAction..DialogState+getCurrentFormName) ⇒ <code>string</code>
    * [.getRelativeFormPosition(srcForm, destForm)](#module_DialogAction..DialogState+getRelativeFormPosition) ⇒ <code>boolean</code>
    * [.nextForm()](#module_DialogAction..DialogState+nextForm) ⇒ <code>Promise</code>
    * [.previousForm()](#module_DialogAction..DialogState+previousForm) ⇒ <code>Promise</code>

<a name="module_DialogAction..DialogState+gotoForm"></a>

#### dialogState.gotoForm(formName) ⇒ <code>Promise</code>
Retrieve metadata of the given form name & update state accordingly.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>Promise</code> - Resolves with form's metadata or rejects with error.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formName</td><td><code>string</code></td><td><p>Name of the form whose metadata is to be retrieved</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DialogState+validateForm"></a>

#### dialogState.validateForm(formData) ⇒ <code>Promise</code>
Apply validation flow on current dialog state & auto-transition to next possible state.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>Promise</code> - Resolves with next possible state's metadata or rejects with error.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formData</td><td><code>*</code></td><td><p>Key value pairs representing data from currently visible screen.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DialogState+cancelForm"></a>

#### dialogState.cancelForm(formData) ⇒ <code>Promise</code>
Apply cancellation flow on current dialog state & auto-transition to next possible state.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>Promise</code> - Resolves with next possible state's metadata or rejects with error.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formData</td><td><code>*</code></td><td><p>Key value pairs representing data from currently visible screen.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DialogState+applyFormStateMethod"></a>

#### dialogState.applyFormStateMethod(options) ⇒ <code>Promise</code>
Apply an arbitrary flow on current dialog state & auto-transition to next possible state.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>Promise</code> - Resolves with next possible state's metadata or rejects with error.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code><a href="#module_DialogAction..StateMethodInput">StateMethodInput</a></code></td><td><p>holder of input parameters.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DialogState+getCurrentFormName"></a>

#### dialogState.getCurrentFormName() ⇒ <code>string</code>
Get form name of the current state of dialog.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>string</code> - Returns current form name.  
<a name="module_DialogAction..DialogState+getRelativeFormPosition"></a>

#### dialogState.getRelativeFormPosition(srcForm, destForm) ⇒ <code>boolean</code>
Given two form names, calculates relative positioning between them and hints whether the second from comes after/before the first one.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>boolean</code> - Returns `true` if destForm comes after srcForm, returns `false` otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>srcForm</td><td><code>string</code></td><td><p>Calculate positioning w.r.t this form.</p>
</td>
    </tr><tr>
    <td>destForm</td><td><code>string</code></td><td><p>Name of the form whose positioning is being calculated.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..DialogState+nextForm"></a>

#### dialogState.nextForm() ⇒ <code>Promise</code>
Go to the next form state. Works only if the next form was already retrieved earlier as part of an earlier state transition.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>Promise</code> - Resolves with next form's metadata, if exists locally otherwise rejects with error.  
<a name="module_DialogAction..DialogState+previousForm"></a>

#### dialogState.previousForm() ⇒ <code>Promise</code>
Go to the previous form state. Works only if the previous form was already retrieved earlier as part of an earlier state transition.

**Kind**: instance method of [<code>DialogState</code>](#module_DialogAction..DialogState)  
**Returns**: <code>Promise</code> - Resolves with previous form's metadata, if exists locally otherwise rejects with error.  
<a name="module_DialogAction..StateMethodInput"></a>

### DialogAction~StateMethodInput : <code>Object</code>
Holder of input parameters while applying an arbitrary flow on dialog's current state. Any number of arbitrary properties along with the onesdocumented below could be passed, they are in turn handed off to matching method implementors. See [DialogStateMethod](./dialog.state.method)

**Kind**: inner typedef of [<code>DialogAction</code>](#module_DialogAction)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>method</td><td><code>string</code></td><td><p>Name of the state transition method to apply</p>
</td>
    </tr><tr>
    <td>[formName]</td><td><code>string</code></td><td><p>Apply state transition method w.r.t this form name. Defaults to form name of the current state.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..ButtonProperties"></a>

### DialogAction~ButtonProperties : <code>Object</code>
Button properties. Additional attributes apart from the ones documented below might be present.

**Kind**: inner typedef of [<code>DialogAction</code>](#module_DialogAction)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>id</td><td><code>string</code></td><td><p>Unique identifier of the button that was clicked. Scope of uniqueness is limited to current state/visible screen of the dialog only.</p>
</td>
    </tr><tr>
    <td>label</td><td><code>string</code></td><td><p>Display name of the button clicked.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogAction..ActionExecutionData"></a>

### DialogAction~ActionExecutionData : <code>Object</code>
Holder for data relevant to this action's execution

**Kind**: inner typedef of [<code>DialogAction</code>](#module_DialogAction)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>formName</td><td><code>string</code></td><td><p>Dialog&#39;s visible screen&#39;s unique name</p>
</td>
    </tr><tr>
    <td>button</td><td><code><a href="#module_DialogAction..ButtonProperties">ButtonProperties</a></code></td><td><p>Properties of the the UI button which was clicked.</p>
</td>
    </tr><tr>
    <td>formView</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>Instance of the view associated with currently visible screen</p>
</td>
    </tr><tr>
    <td>dialogState</td><td><code><a href="#module_DialogAction..DialogState">DialogState</a></code></td><td><p>Reference to the state model of the dialog.</p>
</td>
    </tr><tr>
    <td>nodes</td><td><code><a href="../../models/node.collection">NodeCollection</a></code></td><td><p>Set of objects on which this dialog is operating.</p>
</td>
    </tr><tr>
    <td>dialog</td><td><code><a href="#module_DialogAction..DynamicDialog">DynamicDialog</a></code></td><td><p>Reference to the overall dialog UI.</p>
</td>
    </tr>  </tbody>
</table>

