<a name="module_DialogStateMethod"></a>

## DialogStateMethod : <code>d2/sdk/controls/dialogs/generic/dialog.state.method</code>
DialogStateMethod. Base class for implementing D2SV generic dialog state transition logic. All derived classes should override theasynchronous `execute()` method to actually implement the transition logic.Also see [DialogStateMethods](./dialog.state.methods)

**Extends**: [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)  
**Example** *(A sample method implementation module that constructs state metadata by literal definition for a certain given state name)*  
```js
import StateMethod from 'd2/sdk/controls/dialogs/generic/dialog.state.method';

class LiteralGetMethod extends StateMethod {
 async execute(options, result) {
   if(options.formName === 'MyFormName') {
     result['page-content'] = {
       dialog: {
         id: 'user_dialog',
         label: 'Name Form',
         content: {
           text: {
             order: 1,
             control: true,
             id: 'full_name',
             label: 'Full name',
             value: 'John Doe'
           }
         },
         buttons: {
           button: [
             {id: 'buttonOk', action: 'validDialog()', label: 'OK', order: 1}
           ]
         }
       }
     };
     return false; //So that no other succeeeding state method applies
   }
   return true;
 }
}

export default LiteralGetMethod;
```

* [DialogStateMethod](#module_DialogStateMethod) : <code>d2/sdk/controls/dialogs/generic/dialog.state.method</code>
    * _instance_
        * [.execute(options, result)](#module_DialogStateMethod+execute) ⇒ <code>Promise.&lt;boolean&gt;</code>
    * _inner_
        * [~MethodMetadata](#module_DialogStateMethod..MethodMetadata) : <code>Object</code>
        * [~OutputFormData](#module_DialogStateMethod..OutputFormData) : <code>Object</code>
        * [~DialogMetadata](#module_DialogStateMethod..DialogMetadata) : <code>Object</code>
        * [~ButtonWrapper](#module_DialogStateMethod..ButtonWrapper) : <code>Object</code>
        * [~ButtonMetadata](#module_DialogStateMethod..ButtonMetadata) : <code>Object</code>

<a name="module_DialogStateMethod+execute"></a>

### dialogStateMethod.execute(options, result) ⇒ <code>Promise.&lt;boolean&gt;</code>
Implement state transition logic for D2SV generic dialog

**Kind**: instance method of [<code>DialogStateMethod</code>](#module_DialogStateMethod)  
**Returns**: <code>Promise.&lt;boolean&gt;</code> - Resolve with `false` to stop executing any other matching state methods that are supposed to execute after it.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>MethodMetadata</code></td><td><p>holder for various current state related metadata</p>
</td>
    </tr><tr>
    <td>result</td><td><code>OutputFormData</code></td><td><p>Output parameter, should be filled in with metadata pertaining to next possible state of the dialog.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogStateMethod..MethodMetadata"></a>

### DialogStateMethod~MethodMetadata : <code>Object</code>
Metadata related to current state of the dialog

**Kind**: inner typedef of [<code>DialogStateMethod</code>](#module_DialogStateMethod)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>method</td><td><code>string</code></td><td><p>Name of the transition method which is getting applied</p>
</td>
    </tr><tr>
    <td>formName</td><td><code>string</code></td><td><p>Form name of the current dialog state.</p>
</td>
    </tr><tr>
    <td>nodes</td><td><code><a href="../../models/node.collection">NodeCollection</a></code></td><td><p>The object selection set on which this method is being applied.</p>
</td>
    </tr><tr>
    <td>actionParams</td><td><code>Object</code></td><td><p>If the dialog was invoked through a menu, all the parameters associated with the menu are collection in this part of options.</p>
</td>
    </tr><tr>
    <td>[formData]</td><td><code>Object</code></td><td><p>Data collected from currently visible screen of the dialog.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogStateMethod..OutputFormData"></a>

### DialogStateMethod~OutputFormData : <code>Object</code>
Type of form data to be populated by the state method. The overall structure should match response structure returned by D2-REST 'property-page' endpoint.

**Kind**: inner typedef of [<code>DialogStateMethod</code>](#module_DialogStateMethod)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>"page-content"</td><td><code>Object</code></td><td><p>wrapper for the form data</p>
</td>
    </tr><tr>
    <td>["page-content".dialog]</td><td><code>DialogMetadata</code></td><td><p>Actual form data</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogStateMethod..DialogMetadata"></a>

### DialogStateMethod~DialogMetadata : <code>Object</code>
Metadata for a single screen in a dialog.

**Kind**: inner typedef of [<code>DialogStateMethod</code>](#module_DialogStateMethod)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>id</td><td><code>string</code></td><td><p>unique identifier of the form</p>
</td>
    </tr><tr>
    <td>label</td><td><code>string</code></td><td><p>Displayable title of the form</p>
</td>
    </tr><tr>
    <td>[buttons]</td><td><code>ButtonWrapper</code></td><td><p>Interaction buttons for the form</p>
</td>
    </tr><tr>
    <td>[content]</td><td><code>*</code></td><td><p>Structure of the form.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogStateMethod..ButtonWrapper"></a>

### DialogStateMethod~ButtonWrapper : <code>Object</code>
Wrapper for buttons

**Kind**: inner typedef of [<code>DialogStateMethod</code>](#module_DialogStateMethod)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>button</td><td><code>Array.&lt;ButtonMetadata&gt;</code></td><td><p>collection of buttons</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DialogStateMethod..ButtonMetadata"></a>

### DialogStateMethod~ButtonMetadata : <code>Object</code>
Metadata definition of a button

**Kind**: inner typedef of [<code>DialogStateMethod</code>](#module_DialogStateMethod)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>id</td><td><code>string</code></td><td><p>Uniqueue identifier of the button</p>
</td>
    </tr><tr>
    <td>label</td><td><code>string</code></td><td><p>Display text for the button</p>
</td>
    </tr><tr>
    <td>order</td><td><code>number</code></td><td><p>Sequential position of the button.</p>
</td>
    </tr><tr>
    <td>action</td><td><code>string</code></td><td><p>The action this button represents</p>
</td>
    </tr><tr>
    <td>[isPrimary]</td><td><code>boolean</code></td><td><p>Whether to visually indicate this button as primary button for the current form.</p>
</td>
    </tr>  </tbody>
</table>

