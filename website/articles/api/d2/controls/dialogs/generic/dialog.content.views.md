<a name="module_DialogContentViews"></a>

## DialogContentViews : <code>d2/sdk/controls/dialogs/generic/dialog.content.views</code>
Collection of content view for D2SV generic dialogs. Each screen of the D2SV generic dialog offers the content area for the end user tointeract with in order to view and enter the data for the dialog. Items from this collection act as handlers for resolving the viewin D2SV generic dialog such that views are loosely attached to the generic dialog's view content after evaluating the view's matching conditions.By default, D2SV runtime registers default form content view for dynamic dialog. New dialog content view couldbe registered by means of extensions.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**See**: [DialogContentView](./dialog.content.views)  
**Example** *(To register new dialog content view, in extensions.json)*  
```js
{
 "d2/sdk/controls/dialogs/generic/dialog.content.views": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/generic.dialog.content.view"
     ]
   }
 }
}
```
**Example** *(whereas generic.dialog.content.view.js could be defined as)*  
```js
import DialogContentViewA from 'mybundle/generic.dialog/dialog.content.view.a';
import DialogContentViewB from 'mybundle/generic.dialog/dialog.content.view.b';

const dialogContentViewRules = [
  {equals: {"formName": "FormA"}, viewClass: DialogContentViewA},
  {equals: {"formName": "FormB"}, viewClass: DialogContentViewB}
];

export default dialogContentViewRules;
```
