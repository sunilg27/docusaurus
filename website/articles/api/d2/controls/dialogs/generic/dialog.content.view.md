<a name="module_DialogContentView"></a>

## DialogContentView : <code>d2/sdk/controls/dialogs/generic/dialog.content.view</code>
DialogContentView. Base class for defining handler of dialog content view in a D2SV generic dialog screen. All derived classes should override the`resolveView()` method to return appropriate view content. The `resolveView()` method can be synchronous or asynchronous.The `resolveView()` method must return either instance object of 'Marionette View' (or) Marionette View Class.

**Extends**: [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)  
**See**: [DialogContentViews](./dialog.content.views)  
**Example** *(A sample dialog content view implementation(dialog.content.view.a.js) that returns the Marionette View instance for generic dialog asynchronously)*  
```js
import DialogContentView from 'd2/sdk/controls/dialogs/generic/dialog.content.view';
import CustomFormView from 'mybundle/generic.dialog/custom.form.view';

class DialogContentViewA extends DialogContentView {
 constructor(options={}) {
   super(options);
 }

 async resolveView(options = {}) {
   let customFormView = new CustomFormView({...options});
   return customFormView;
 }
}
```
**Example** *(A sample dialog content view implementation(dialog.content.view.a.js) that returns the Marionette View instance for generic dialog synchronously)*  
```js
import DialogContentView from 'd2/sdk/controls/dialogs/generic/dialog.content.view';
import CustomFormView from 'mybundle/generic.dialog/custom.form.view';

class DialogContentViewA extends DialogContentView {
 constructor(options={}) {
   super(options);
 }

 resolveView(options = {}) {
   let customFormView = new CustomFormView({...options});
   return customFormView;
 }
}
```
**Example** *(To associate this dialog content view with binding rule, an extension(generic.dialog.content.view.js) could be defined as)*  
```js
import DialogContentViewA from 'mybundle/generic.dialog/dialog.content.view.a';
import DialogContentViewB from 'mybundle/generic.dialog/dialog.content.view.b';

const dialogContentViewRules = [
  {equals: {"formName": "FormA"}, viewClass: DialogContentViewA},
  {equals: {"formName": "FormB"}, viewClass: DialogContentViewB}
];

export default dialogContentViewRules;
```
**Example** *(To finally register the extension, in extensions.json)*  
```js
{
 "d2/sdk/controls/dialogs/generic/dialog.content.views": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/generic.dialog.content.view"
     ]
   }
 }
}
```
