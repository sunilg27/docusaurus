<a name="module_NodeIconSprites"></a>

## NodeIconSprites : <code>d2/sdk/controls/icon.sprites/node.icon.sprites</code>
Node icons collection. Used to visually represent a [NodeModel](../../models/node.model) within a `NodeIconView`.The collection can be looked up to discover possible represenation or it can be augmented by extension

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(To extend the node icons collection)*  
```js
//In mybundle/extensions/node.icons.js
define([], function(){
 'use strict';

 return [
   {
     decides: function(node) {
       return node.get('type') === 'group';
     },
     className: 'csui-icon d2-icon-type-group-user',
     mimeType: 'Group',
     sequence: 20
   },
   {
     decides: function(node) {
       return node.get('type') === 'user';
     },
     className: 'csui-icon d2-icon-type-single-user',
     mimeType: 'User',
     sequence: 20
   }
 ];
});

//To register the extension, put in extensions.json
 "d2/sdk/controls/icon.sprites/node.icon.sprites": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/node.icons"
     ]
   }
 }
```
