<a name="module_TreeIconSprites"></a>

## TreeIconSprites : <code>d2/sdk/controls/icon.sprites/tree.icon.sprites</code>
Tree icons collection. Used to visually represent a [NodeModel](../../models/node.model) within a `TreeView`.The collection can be looked up to discover possible represenation or it can be augmented by extensionSee [NodeIconSprites](./node.icon.sprites) for sample extension definition. Registering the extension is alsosimilar except the target module

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(To register an extension)*  
```js
// Assuming that mybundle/extensions/tree.icons.js is the extension
// Add to extension.json as shown below

"d2/sdk/controls/icon.sprites/tree.icon.sprites": {
   "extensions": {
     "mybundle": [
       "mybundle/extensions/tree.icons"
     ]
   }
 }
```
