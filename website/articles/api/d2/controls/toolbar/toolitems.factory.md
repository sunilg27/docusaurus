<a name="module_ToolitemsFactory"></a>

## ToolitemsFactory : <code>d2/sdk/controls/toolbar/toolitems.factory</code>
Create a new toolbar definiftion. The definition could be used to create a menubar in UI.

**Extends**: <code>Object</code>  
**Example** *(Static toolbar configuration)*  
```js
define([
 'd2/sdk/controls/toolbar/toolitem.factory'
], function(ToolitemsFactory){
 'use strict';

 return {
   rightToolbar: new ToolitemsFactory({
     main: [
       {
         signature: 'D2-RefreshList',
         name: 'Refresh',
         iconName: 'csui_action_refresh32',
         enabled: true
       }
     ]
   }, {
     maxItemsShown: 1
     dropDownText: 'More',
     dropDownIconName: 'csui_action_more32'
   });
 };
});
```

* [ToolitemsFactory](#module_ToolitemsFactory) : <code>d2/sdk/controls/toolbar/toolitems.factory</code>
    * [~ToolitemsFactory](#module_ToolitemsFactory..ToolitemsFactory)
        * [new ToolitemsFactory(toolItems, options)](#new_module_ToolitemsFactory..ToolitemsFactory_new)
    * [~ToolitemType](#module_ToolitemsFactory..ToolitemType) : <code>Object</code>

<a name="module_ToolitemsFactory..ToolitemsFactory"></a>

### ToolitemsFactory~ToolitemsFactory
**Kind**: inner class of [<code>ToolitemsFactory</code>](#module_ToolitemsFactory)  
<a name="new_module_ToolitemsFactory..ToolitemsFactory_new"></a>

#### new ToolitemsFactory(toolItems, options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>toolItems</td><td><code>object</code></td><td><p>Key-value pairs where key represents a group name and value represents menu items under that group. Each value should be an array of <code>ToolitemType</code></p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>Controls options</p>
</td>
    </tr><tr>
    <td>options.maxItemsShown</td><td><code>number</code></td><td><p>Number of menus to be shown at max if this toolbar config is used to draw a menubar. Setting it to <code>0</code> shows the menu as dropdown</p>
</td>
    </tr><tr>
    <td>options.dropDownText</td><td><code>string</code></td><td><p>Text shown for dropdown button</p>
</td>
    </tr><tr>
    <td>options.dropDownIconName</td><td><code>string</code></td><td><p>Action icon name for the dropdown button.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ToolitemsFactory..ToolitemType"></a>

### ToolitemsFactory~ToolitemType : <code>Object</code>
**Kind**: inner typedef of [<code>ToolitemsFactory</code>](#module_ToolitemsFactory)  
**Propety**: <code>Boolean</code> [enabled=false] - Whether this menu should be always enabled.  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>signature</td><td><code>string</code></td><td></td><td><p>Signature of the command bound to this menu item.</p>
</td>
    </tr><tr>
    <td>name</td><td><code>string</code></td><td></td><td><p>Text displayed for this menu item.</p>
</td>
    </tr><tr>
    <td>iconName</td><td><code>string</code></td><td></td><td><p>Action icon name for this menu</p>
</td>
    </tr><tr>
    <td>commandData</td><td><code>*</code></td><td></td><td><p>Arbitrary data. Must have a <code>menuId</code> attribute for uniqueness and equality check.</p>
</td>
    </tr><tr>
    <td>[subItemOf]</td><td><code>string</code></td><td></td><td><p>ID of the menu item for which the current item is a sub-menu</p>
</td>
    </tr><tr>
    <td>[actionProperties]</td><td><code>*</code></td><td></td><td><p>Additional menu configuration data later used by matching command implementations.</p>
</td>
    </tr><tr>
    <td>[promoted]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Whether this menu is promoted. Promoted menus represent priority actions that are fetched and shown before others.</p>
</td>
    </tr><tr>
    <td>type</td><td><code>string</code></td><td></td><td><p>Menu type. Should correlate with action types from D2-REST <code>actions</code> API.</p>
</td>
    </tr><tr>
    <td>[dynamic]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Whether this menu is a dynamic menu.</p>
</td>
    </tr>  </tbody>
</table>

