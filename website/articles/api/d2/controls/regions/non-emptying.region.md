<a name="module_NonEmptyingRegion"></a>

## NonEmptyingRegion : <code>d2/sdk/controls/regions/non-emptying.region</code>
A `Marionette.Region` that does not empty it's DOM element on rendering new instance of `Marionette.View`.

**Extends**: [<code>Marionette.Region</code>](https://marionettejs.com/docs/v2.4.7/marionette.region.html)  
**Example**  
```js
define([ 'nuc/lib/underscore', 'nuc/lib/marionette', 'd2/sdk/controls/regions/non-emptying.region'], function(_, Marionette, NonEmptyingRegion){ 'use strict'; var MyView = Marionette.LayoutView.extend({   template: false,   constructor: function MyView(options) {     options = options || {};     MyView.__super__.constructor.call(this, options);   },   onRender: function() {     if(_.isString(this.options.greet) && !_.isEmpty(this.options.greet)) {       this.$el.text(this.options.greet);     }   } }); var view1 = new MyView({greet: 'Hello'}); var view2 = new MyView({greet: 'World'}); var region = new NonEmptyingRegion({el: '.host-element'}); //can also set prependChild: true if element for view added later should show before previously added element in DOM. region.show(view1); //Adds element with 'Hello' in the DOM region.show(view2, {preventDestroy: true}); //Adds element with 'World' in the DOM, however element with 'Hello' is not removed and still exists in the DOM. //With normal Marionette.Region, after adding view2, the HTML element specific to view1 would have been removed.});
```

* [NonEmptyingRegion](#module_NonEmptyingRegion) : <code>d2/sdk/controls/regions/non-emptying.region</code>
    * _instance_
        * [.show(view, [options])](#module_NonEmptyingRegion+show)
    * _inner_
        * [~NonEmptyingRegion](#module_NonEmptyingRegion..NonEmptyingRegion)
            * [new NonEmptyingRegion([options])](#new_module_NonEmptyingRegion..NonEmptyingRegion_new)

<a name="module_NonEmptyingRegion+show"></a>

### nonEmptyingRegion.show(view, [options])
Shows a `Marionette.View` in the region.

**Kind**: instance method of [<code>NonEmptyingRegion</code>](#module_NonEmptyingRegion)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>view</td><td><code>Maionette.View</code></td><td><p>The view to be shown</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>object</code></td><td><p>Display control options</p>
</td>
    </tr><tr>
    <td>[options.preventDestroy]</td><td><code>Boolean</code></td><td><p>Indicates whether previous view should be destroyed before showing current view or not. This flag is
an important indication that tells the <code>NonEmptyingRegion</code> to keep adding view content as children of it own element. Without this flag seto to <code>true</code>
the previously shown view is destroyed and its element is removed from DOM as usual.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_NonEmptyingRegion..NonEmptyingRegion"></a>

### NonEmptyingRegion~NonEmptyingRegion
**Kind**: inner class of [<code>NonEmptyingRegion</code>](#module_NonEmptyingRegion)  
<a name="new_module_NonEmptyingRegion..NonEmptyingRegion_new"></a>

#### new NonEmptyingRegion([options])
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[options]</td><td><code>object</code></td><td><p>Constructor options. Usual constructor options as per <code>Marionette.Region</code> specification are all supported but not
explictly documented here. Below are the special parameter supported by this region type.</p>
</td>
    </tr><tr>
    <td>[options.prependChild]</td><td><code>Boolean</code></td><td><p>Flag to indicate whether current view&#39;s element is to be placed before or after
previous view&#39;s element in DOM when <code>show()</code> is called with <code>options.preventDestroy=true</code>. Defaults to <code>false</code>.</p>
</td>
    </tr><tr>
    <td>[options.index]</td><td><code>number</code></td><td><p>Positive integer to indicate any specific ordered spot for current view&#39;s element in the DOM
when a previous view exists and current view is being shown with <code>options.preventDestroy=true</code>.</p>
</td>
    </tr>  </tbody>
</table>

