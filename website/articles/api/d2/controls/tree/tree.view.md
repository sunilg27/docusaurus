<a name="module_TreeView"></a>

## TreeView : <code>d2/sdk/controls/tree/tree.view</code>
Tree View, renders data into a tree structure. Internally it is implemented using [Fancytree](https://wwwendt.de/tech/fancytree/doc/jsdoc/Fancytree.html).

**Extends**: [<code>Marionette.ItemView</code>](https://marionettejs.com/docs/v2.4.7/marionette.itemview.html)  
**Example** *(A sample tree construction may look like)*  
```js
define([
 'nuc/lib/marionette',
 'd2/sdk/controls/tree/tree.view',
 'hbs!mybundle/test/test.tree'
], function(Marionette, TreeView, template){

 var MyTreeView = Marionette.LayoutView.extend({
   template: template,

   regions: {
     rgnTree: '.rgnTree'
   },

   constructor: function MyTreeView(options) {
     options = options || {};
     MyTreeView.__super__.constructor.call(this, options);
   },

   onRender: function() {
     if(!this.treeView) {
       this.treeView = new TreeView(this._buildTreeOptions());
       this.rgnTree.show(this.treeView);
     }
   },

   _buildTreeOptions: function() {
     var treeOption = {
       source: [
         //This demonstrates how to initialize a tree with literal data only
         {title: "Node 1", key: "1"},
         {title: "Folder 2", key: "2", folder: true, children: [
           {title: "Node 2.1", key: "3"},
           {title: "Node 2.2", key: "4"}
         ]}
       ]
     };

     return treeOption;
   }
 });

 return MyTreeView;
});
```
**Example** *( where the content of mybundle/test/test.tree.hbs is as follows )*  
```js
<div class="rgnTree"></div>
```

* [TreeView](#module_TreeView) : <code>d2/sdk/controls/tree/tree.view</code>
    * [~TreeView](#module_TreeView..TreeView)
        * [new TreeView(options)](#new_module_TreeView..TreeView_new)
    * [~FancytreeEventCallback](#module_TreeView..FancytreeEventCallback) : <code>function</code>

<a name="module_TreeView..TreeView"></a>

### TreeView~TreeView
**Kind**: inner class of [<code>TreeView</code>](#module_TreeView)  
<a name="new_module_TreeView..TreeView_new"></a>

#### new TreeView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Construction options holder. Only a subset of</p>
</td>
    </tr><tr>
    <td>[options.activate]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when some node is activated.</p>
</td>
    </tr><tr>
    <td>[options.beforeActivate]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when some node is about to become active.</p>
</td>
    </tr><tr>
    <td>[options.click]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a node is clicked.</p>
</td>
    </tr><tr>
    <td>[options.clickPaging]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a paging status node is clicked. A paging status node is used to render more siblings at given depth.</p>
</td>
    </tr><tr>
    <td>[options.collapse]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a node is collapsed.</p>
</td>
    </tr><tr>
    <td>[options.dblclick]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a node is clicked twice.</p>
</td>
    </tr><tr>
    <td>[options.expand]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a node is expanded.</p>
</td>
    </tr><tr>
    <td>[options.focus]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a node receives keyboard focus.</p>
</td>
    </tr><tr>
    <td>[options.init]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when the tree reaches a particular state, when tree is initialized, source data is loaded &amp; visible nodes are rendered.</p>
</td>
    </tr><tr>
    <td>[options.keydown]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when a node has received a keyboard event.</p>
</td>
    </tr><tr>
    <td>[options.lazyLoad]</td><td><code>FancytreeEventCallback</code></td><td><p>Called when <code>data.node</code> is expanded for the first time. The children data must be returned in <code>data.results</code> property.</p>
</td>
    </tr><tr>
    <td>[options.source]</td><td><code>*</code></td><td><p>Used to compute/collect data for the initial tree rendering. Can return a jQuery promise which can then later be resolved with tree data. See <a href="https://wwwendt.de/tech/fancytree/doc/jsdoc/global.html#NodeData">NodeData</a>.</p>
</td>
    </tr><tr>
    <td>[options.strings]</td><td><code>Object</code></td><td><p>Key-value pairs used for translations.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TreeView..FancytreeEventCallback"></a>

### TreeView~FancytreeEventCallback : <code>function</code>
**Kind**: inner typedef of [<code>TreeView</code>](#module_TreeView)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>event</td><td><code><a href="https://api.jquery.com/category/events/event-object/">jQueryEvent</a></code></td><td><p>A jquery event representing interaction.</p>
</td>
    </tr><tr>
    <td>data</td><td><code><a href="https://wwwendt.de/tech/fancytree/doc/jsdoc/global.html#EventData">FancytreeEventData</a></code></td><td><p>Data associated with the event.</p>
</td>
    </tr>  </tbody>
</table>

