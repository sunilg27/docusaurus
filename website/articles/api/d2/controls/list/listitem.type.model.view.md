<a name="module_ListItemTypeModel"></a>

## ListItemTypeModel : <code>d2/sdk/controls/list/listitem.type.model.view</code>
A list item view that can work with any [Backbone.Model](https://backbonejs.org/#Model).

**Extends**: [<code>Marionette.ItemView</code>](https://marionettejs.com/docs/v2.4.7/marionette.itemview.html)  
**Example** *(Typical usage)*  
```js
define([
 'nuc/lib/backbone',
 'nuc/lib/marionette',
 'd2/sdk/controls/list/simple.list.view',
 'd2/sdk/controls/list/listitem.type.model.view',
 'd2/sdk/utils/contexts/context.utils',
], function(Backbone, Marionette, ListView, ListItemTypeModelView, contextUtils){
 'use strict';

 var collection = new Backbone.Collection([
   {
     id: 1,
     name: 'One'
   },
   {
     id: 2,
     name: 'Two'
   },
   {
     id: 3,
     name: 'Three'
   }
 ]), context = contextUtils.getPerspectiveContext();

 var MyListView = ListView.extend({
   childView: ListItemTypeModelView,
   childViewOptions: function() {
     var opts = _.extend({}, this.options);
     opts.templateHelpers = function () {
       return {
         name: this.model.get('name'),
         itemLabel: this.model.get('name'),
         enableIcon: false,
         showInlineActionBar: false
       };
     };
     return opts;
   },
   constructor: function MyListView(options) {
     options = options || {};
     MyListView.__super__.constructor.call(this, options);
   }
 });

 var myList = new MyListView({collection: collection, context: context});
 new Marionette.Region({el: '.host-el'}).show(myList);
});
```

* [ListItemTypeModel](#module_ListItemTypeModel) : <code>d2/sdk/controls/list/listitem.type.model.view</code>
    * _instance_
        * [.templateHelpers()](#module_ListItemTypeModel+templateHelpers) ⇒ <code>TemplateOptions</code>
    * _inner_
        * [~ListItemTypeModel](#module_ListItemTypeModel..ListItemTypeModel)
            * [new ListItemTypeModel(options)](#new_module_ListItemTypeModel..ListItemTypeModel_new)
        * [~TemplateOptions](#module_ListItemTypeModel..TemplateOptions) : <code>object</code>

<a name="module_ListItemTypeModel+templateHelpers"></a>

### listItemTypeModel.templateHelpers() ⇒ <code>TemplateOptions</code>
**Kind**: instance method of [<code>ListItemTypeModel</code>](#module_ListItemTypeModel)  
**Returns**: <code>TemplateOptions</code> - That controls how each item is rendered.  
<a name="module_ListItemTypeModel..ListItemTypeModel"></a>

### ListItemTypeModel~ListItemTypeModel
**Kind**: inner class of [<code>ListItemTypeModel</code>](#module_ListItemTypeModel)  
<a name="new_module_ListItemTypeModel..ListItemTypeModel_new"></a>

#### new ListItemTypeModel(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options</p>
</td>
    </tr><tr>
    <td>[options.context]</td><td><code>Context</code></td><td><p>Reference to the active application context. Needed only if the listitem is going to have a dropdown menu.</p>
</td>
    </tr><tr>
    <td>[options.toolbarData]</td><td><code>object</code></td><td></td>
    </tr><tr>
    <td>[options.toolbarData.toolbaritems]</td><td><code><a href="../toolbar/toolitems.factory">ToolitemsFactory</a></code></td><td><p>Menu configuration</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ListItemTypeModel..TemplateOptions"></a>

### ListItemTypeModel~TemplateOptions : <code>object</code>
**Kind**: inner typedef of [<code>ListItemTypeModel</code>](#module_ListItemTypeModel)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>name</td><td><code>string</code></td><td></td><td><p>Displayable text for the item</p>
</td>
    </tr><tr>
    <td>itemLabel</td><td><code>string</code></td><td></td><td><p>Aria-label associated with the item</p>
</td>
    </tr><tr>
    <td>[enableIcon]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Whether to show an icon for the item</p>
</td>
    </tr><tr>
    <td>[icon]</td><td><code>string</code></td><td></td><td><p>CSS class name of an icon to be shown for the item. Ignored if <code>enableIcon</code> is set to <code>false</code>.</p>
</td>
    </tr><tr>
    <td>[showInlineActionBar]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Whether the item will have a dropdown menu</p>
</td>
    </tr>  </tbody>
</table>

