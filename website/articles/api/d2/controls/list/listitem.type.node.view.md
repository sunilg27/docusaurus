<a name="module_ListItemTypeNode"></a>

## ListItemTypeNode : <code>d2/sdk/controls/list/listitem.type.node.view</code>
A list item view that can work with any [NodeModel](../../models/node.model).

**Extends**: [<code>ListItemTypeModel</code>](./listitem.type.model.view)  
**See**: [ListItemTypeNode](./listitem.type.model.view)  
**Example** *(Sample usage)*  
```js
define([
 'nuc/lib/backbone',
 'nuc/lib/marionette',
 'd2/sdk/controls/list/simple.list.view',
 'd2/sdk/controls/list/listitem.type.node.view',
 'd2/sdk/models/node.model'
], function(Backbone, Marionette, ListView, ListItemTypeNodeView, NodeModel){
 'use strict';

  var MyListView = ListView.extend({
   childView: ListItemTypeNodeView,
   constructor: function MyListView(options) {
     options = options || {};
     MyListView.__super__.constructor.call(this, options);
   }
 });

 var MyCollection = Backbone.Collection.extend({
   model: NodeModel
 });

 var collection = new MyCollection([
   {id: '1', name: 'One', mime_type: 'application/pdf'},
   {id: '2', name: 'Two', mime_type: 'image/jpg'}
 ]);

 new Marionette.Region({el: '.host-el'}).show(new MyListView({
   collection: collection
 }));
});
```
