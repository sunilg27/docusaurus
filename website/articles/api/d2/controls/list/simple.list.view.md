<a name="module_SimpleListView"></a>

## SimpleListView : <code>d2/sdk/controls/list/simple.list.view</code>
A flat list view backed by a [Backbone.Collection](https://backbonejs.org/#Collection).`Backbone.Model` instances could be added/removed from the backing collection to update the view dynamically.

**Extends**: [<code>Marionette.CollectionView</code>](https://marionettejs.com/docs/v2.4.7/marionette.collectionview.html)  
**See**

- [ListItemTypeModel](./listitem.type.model.view)
- [ListItemTypeNode](./listitem.type.node.view)

**Example** *(Sample list with a selection)*  
```js
define([
 'nuc/lib/backbone',
 'nuc/lib/marionette',
 'd2/sdk/controls/list/simple.list.view',
 'd2/sdk/controls/list/listitem.type.node.view',
 'd2/sdk/models/node.model'
], function(Backbone, Marionette, ListView, ListItemTypeNodeView, NodeModel){
 'use strict';

  var MyListView = ListView.extend({
   childView: ListItemTypeNodeView,
   constructor: function MyListView(options) {
     options = options || {};
     MyListView.__super__.constructor.call(this, options);
   }
 });

 var MyCollection = Backbone.Collection.extend({
   model: NodeModel
 });

 var collection = new MyCollection([
   {id: '1', name: 'One', mime_type: 'application/pdf'},
   {id: '2', name: 'Two', mime_type: 'image/jpg'}
 ]);

 new Marionette.Region({el: '.host-el'}).show(new MyListView({
   collection: collection, selectedModel: new NodeModel({id: '2'})
 }));
});
```

* [SimpleListView](#module_SimpleListView) : <code>d2/sdk/controls/list/simple.list.view</code>
    * _instance_
        * [.mergeAttributesFromSelectedModelOnIDChange](#module_SimpleListView+mergeAttributesFromSelectedModelOnIDChange) : <code>Boolean</code>
        * [.skippableAttributesOnMerge](#module_SimpleListView+skippableAttributesOnMerge) : <code>Array.&lt;string&gt;</code>
        * [.attributesUsedForEqualityComparison](#module_SimpleListView+attributesUsedForEqualityComparison) : <code>Array.&lt;string&gt;</code>
    * _inner_
        * [~SimpleListView](#module_SimpleListView..SimpleListView)
            * [new SimpleListView(options)](#new_module_SimpleListView..SimpleListView_new)

<a name="module_SimpleListView+mergeAttributesFromSelectedModelOnIDChange"></a>

### simpleListView.mergeAttributesFromSelectedModelOnIDChange : <code>Boolean</code>
Whether to merge attributes of constructor option `selectedModel` to matching model within the collectionwhenever `id` attribute of the `selectedModel` changes.

**Kind**: instance property of [<code>SimpleListView</code>](#module_SimpleListView)  
**Default**: <code>true</code>  
<a name="module_SimpleListView+skippableAttributesOnMerge"></a>

### simpleListView.skippableAttributesOnMerge : <code>Array.&lt;string&gt;</code>
Which attributes to skip while merging from `selectedModel` to a matching model within the collection

**Kind**: instance property of [<code>SimpleListView</code>](#module_SimpleListView)  
**Default**: <code>[&#x27;name&#x27;]</code>  
<a name="module_SimpleListView+attributesUsedForEqualityComparison"></a>

### simpleListView.attributesUsedForEqualityComparison : <code>Array.&lt;string&gt;</code>
Which attributes are used to compare for equality of `selectedModel` and models within the collection

**Kind**: instance property of [<code>SimpleListView</code>](#module_SimpleListView)  
**Default**: <code>[&#x27;id&#x27;]</code>  
<a name="module_SimpleListView..SimpleListView"></a>

### SimpleListView~SimpleListView
**Kind**: inner class of [<code>SimpleListView</code>](#module_SimpleListView)  
<a name="new_module_SimpleListView..SimpleListView_new"></a>

#### new SimpleListView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options. Following are the options specifically supported by the list apart from those from its supertype.</p>
</td>
    </tr><tr>
    <td>[options.emptyStateMessage]</td><td><code>string</code></td><td><p>Message to show when there are no items in the associated collection.</p>
</td>
    </tr><tr>
    <td>[options.selectedModel]</td><td><code><a href="../../models/node.model">NodeModel</a></code></td><td><p>Show a selection highlight for this model</p>
</td>
    </tr>  </tbody>
</table>

