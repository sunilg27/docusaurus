<a name="module_SortableListView"></a>

## SortableListView : <code>d2/sdk/controls/list/sortable.list.view</code>
A flat list view backed by a [Backbone.Collection](https://backbonejs.org/#Collection). Items in the list view could bedragged around to re-order them within the list.

**Extends**: [<code>Marionette.CompositeView</code>](https://marionettejs.com/docs/v2.4.7/marionette.compositeview.html)  
**Example** *(Typical usage)*  
```js
//In mybundle/trial/list/sortable.listitem.hbs
<div class="item-label" data-d2-key="{{name}}">{{label}}</div>

//In a test module
define([
 'nuc/lib/backbone',
 'nuc/lib/marionette',
 'd2/sdk/controls/list/sortable.list.view',
 'hbs!mybundle/trial/list/sortable.listitem'
], function(Backbone, Marionette, SortableListView, template){
 'use strict';

 var ItemView = Marionette.ItemView.extend({
   template: template,
   templateHelpers: function() {
     return {
       label: this.model.get('label'),
       name: this.model.get('id')
     };
   }
 });

 var collection = new Backbone.Collection([
   {id: '1', label: 'One'},
   {id: '2', label: 'Two'},
   {id: '3', label: 'Three'}
 ]);

 var listView = new SortableListView({itemView: ItemView, collection: collection, hasNonRemovableItem: true, isItemNonRemovable: function(){return true;}});
 new Marionette.Region({el: '.el-host'}).show(listView);

});
```

* [SortableListView](#module_SortableListView) : <code>d2/sdk/controls/list/sortable.list.view</code>
    * [~SortableListView](#module_SortableListView..SortableListView)
        * [new SortableListView(options)](#new_module_SortableListView..SortableListView_new)
    * [~RemovableDetectionCallback](#module_SortableListView..RemovableDetectionCallback) ⇒ <code>Boolean</code>

<a name="module_SortableListView..SortableListView"></a>

### SortableListView~SortableListView
**Kind**: inner class of [<code>SortableListView</code>](#module_SortableListView)  
<a name="new_module_SortableListView..SortableListView_new"></a>

#### new SortableListView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td></td><td><p>Constructor options. Following are the options specifically supported by the list apart from those from its supertype.</p>
</td>
    </tr><tr>
    <td>options.itemView</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.itemview.html">Marionette.ItemView</a></code></td><td></td><td><p>View class to use for rendering each item in the list.</p>
</td>
    </tr><tr>
    <td>[options.hasNonRemovableItem]</td><td><code>Boolean</code></td><td><code>false</code></td><td><p>Whether the list has non-removable item. If set to <code>true</code> then <code>isItemNonRemovable()</code> callback is executed
for each item to decide removability.</p>
</td>
    </tr><tr>
    <td>[options.isItemNonRemovable]</td><td><code>RemovableDetectionCallback</code></td><td></td><td><p>Callback to decide if a particular item is non-removable</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_SortableListView..RemovableDetectionCallback"></a>

### SortableListView~RemovableDetectionCallback ⇒ <code>Boolean</code>
**Kind**: inner typedef of [<code>SortableListView</code>](#module_SortableListView)  
**Returns**: <code>Boolean</code> - Return `true` to indicate the item shouldn't be removable.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>model</td><td><code>Backbone.Model</code></td><td><p>Model behind the item being evaluated.</p>
</td>
    </tr>  </tbody>
</table>

