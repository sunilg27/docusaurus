<a name="module_ClientsideInfiniteScrollingBehavior"></a>

## ClientsideInfiniteScrollingBehavior : <code>d2/sdk/controls/behaviors/clientside.infinite.scrolling.behavior</code>
A `Marionette.view` behavior to implement infinite scrolling for views operating on `Backbone.Collection`.The collection prototype has to be mixed with [BrowsableCollectionMixin](../../models/mixins/browsable.collection.mixin)

**Extends**: [<code>Marionette.Behavior</code>](https://marionettejs.com/docs/v2.4.7/marionette.behavior.html)  
**Example** *(To use the behavior on a CompositeView)*  
```js
define([
 'nuc/lib/marionette',
 'd2/sdk/controls/behaviors/clientside.infinite.scrolling.behavior',
 'hbs!mybundle/some/path/template.hbs'
], function(Marionette, InfiniteScrollingBehavior, template){
 'use strict';

 var ChildView = Marionette.ItemView.extend({
   className: 'child-view'
 });

 var MyListView = Marionette.CompositeView.extend({
   template: template, //template content simply is <div class="child-container"></div>
   childViewContainer: '.child-container',
   childView: ChildView,
   behaviors: {
     IScroll: {
       behaviorClass: InfiniteScrollingBehavior,
       contentParent: '.child-container', //The scroll parent element
       content: '.child-view', // outermost element of each item view
       fetchMoreItemsThreshold: 95, //Offset percentage in integer value that indicates when to consider a potential end of the visible list
       fetchMoreItemLength: 10 //How many more items to be fetched when scroll position has reached potential end
     }
   }
 });
});
```

* [ClientsideInfiniteScrollingBehavior](#module_ClientsideInfiniteScrollingBehavior) : <code>d2/sdk/controls/behaviors/clientside.infinite.scrolling.behavior</code>
    * [~ClientsideInfiniteScrollingBehavior](#module_ClientsideInfiniteScrollingBehavior..ClientsideInfiniteScrollingBehavior)
        * [new ClientsideInfiniteScrollingBehavior(options, view)](#new_module_ClientsideInfiniteScrollingBehavior..ClientsideInfiniteScrollingBehavior_new)

<a name="module_ClientsideInfiniteScrollingBehavior..ClientsideInfiniteScrollingBehavior"></a>

### ClientsideInfiniteScrollingBehavior~ClientsideInfiniteScrollingBehavior
**Kind**: inner class of [<code>ClientsideInfiniteScrollingBehavior</code>](#module_ClientsideInfiniteScrollingBehavior)  
<a name="new_module_ClientsideInfiniteScrollingBehavior..ClientsideInfiniteScrollingBehavior_new"></a>

#### new ClientsideInfiniteScrollingBehavior(options, view)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options, automatically passed by Marionette from behavior configuration of a view</p>
</td>
    </tr><tr>
    <td>options.contentParent</td><td><code>string</code></td><td><p>CSS selector to find the scrollable parent element</p>
</td>
    </tr><tr>
    <td>options.content</td><td><code>string</code></td><td><p>CSS selector to find children of scrollable parent</p>
</td>
    </tr><tr>
    <td>options.fetchMoreItemsThreshold</td><td><code>number</code></td><td><p>Threshold in percentage, used to calculate whether scrolling has reached end of list.</p>
</td>
    </tr><tr>
    <td>options.fetchMoreItemLength</td><td><code>number</code></td><td><p>Number of new items to be fetched when scrolling has reached end of list.</p>
</td>
    </tr><tr>
    <td>view</td><td><code>Marionette.View</code></td><td><p>The view instance associated with this behavior instance. Automatically set by Marionette.</p>
</td>
    </tr>  </tbody>
</table>

