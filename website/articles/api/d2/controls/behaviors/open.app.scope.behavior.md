<a name="module_OpenAppScopeBehavior"></a>

## OpenAppScopeBehavior : <code>d2/sdk/controls/behaviors/open.app.scope.behavior</code>
A `Marionette.view` behavior to navigate to an application scope perspective. Whichever view usesthis behavior, waits for a click event to happen. As a reponse to the click event, it opens the applicationscope perspective.A view that has already mixed this behavior, can set a Boolean flag `disableAppScopeBehavior=true` to disbale the behavior.

**Extends**: [<code>Marionette.Behavior</code>](https://marionettejs.com/docs/v2.4.7/marionette.behavior.html)  
**Example** *(To use this behavior in a sample view)*  
```js

define([
 'nuc/lib/marionette',
 'd2/sdk/controls/behaviors/open.app.scope.behavior',
 'd2/sdk/utils/contexts/context.utils'
], function(Marionette, OpenAppScopeBehavior, contextUtil){
 'use strict';

 var context = contextUtil.getPerspectiveContext();

 var MyView = Marionette.LayoutView.extend({
   behaviors: {
     AppScope: {
       behaviorClass: OpenAppScopeBehavior,
       context: context,
       applicationScope: 'favorites'
     }
   },
   template: false,
   onRender: function() {
     this.$el.text('Click me to open favorites');
   }
 });
});
```

* [OpenAppScopeBehavior](#module_OpenAppScopeBehavior) : <code>d2/sdk/controls/behaviors/open.app.scope.behavior</code>
    * [~OpenAppScopeBehavior](#module_OpenAppScopeBehavior..OpenAppScopeBehavior)
        * [new OpenAppScopeBehavior(options, view)](#new_module_OpenAppScopeBehavior..OpenAppScopeBehavior_new)

<a name="module_OpenAppScopeBehavior..OpenAppScopeBehavior"></a>

### OpenAppScopeBehavior~OpenAppScopeBehavior
**Kind**: inner class of [<code>OpenAppScopeBehavior</code>](#module_OpenAppScopeBehavior)  
<a name="new_module_OpenAppScopeBehavior..OpenAppScopeBehavior_new"></a>

#### new OpenAppScopeBehavior(options, view)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Constructor options, automatically passed by Marionette from behavior configuration of a view</p>
</td>
    </tr><tr>
    <td>options.applicationScope</td><td><code>string</code></td><td><p>Name of the application scope to be opened on click.</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td><p>Reference to the runtime context. See <a href="../../utils/contexts/context.utils#Context">Context</a>.</p>
</td>
    </tr><tr>
    <td>[options.registerClickEventListener]</td><td><code>Boolean</code></td><td><p>Whether the view should add a separate <code>click</code> event listener. Defaults to <code>true</code>.
Some views which has already added a click event listener on self element for some purpose, can be reused automatically for this behavior, in that case
this flag needs to be set to <code>false</code> explicitly.</p>
</td>
    </tr><tr>
    <td>view</td><td><code>Marionette.View</code></td><td><p>The view instance associated with this behavior instance. Automatically set by Marionette.</p>
</td>
    </tr>  </tbody>
</table>

