<a name="module_BlockingView"></a>

## BlockingView : <code>d2/sdk/controls/blocker/blocker</code>
UI blocker. Used to temporarily make the whole or part of Smart View UI non-interactive to block any unexpected userinput while executing a critical piece of logic either in the background or foreground.

**Extends**: <code>Object</code>  

* [BlockingView](#module_BlockingView) : <code>d2/sdk/controls/blocker/blocker</code>
    * [.imbue(view)](#module_BlockingView.imbue)
    * [.delegate(view, delegateToView)](#module_BlockingView.delegate)

<a name="module_BlockingView.imbue"></a>

### BlockingView.imbue(view)
Mix progrss blocking APIs as well as well as blocker instance to a view.

**Kind**: static method of [<code>BlockingView</code>](#module_BlockingView)  
**Mixes**: <code>BlockerAPI</code>  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>view</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>The view instance where blocing APIs are to be mixed.</p>
</td>
    </tr>  </tbody>
</table>

**Example** *(An item view showing/hiding blocker when its model syncs)*  
```js
define([
   'nuc/lib/marionette',
   'd2/sdk/controls/blocker/blocker'
   'd2/sdk/models/node.model',
], function(Marionette, BlockingView, NodeModel){
   'use strict';

   var NodeView = Marionette.ItemView.extend({
       constructor: function NodeView(options) {
           options = options || {};
           options.model = new NodeModel({id: options.id}, options);

           BlockingView.imbue(this);
       },

       onRender: function() {
         this.blockAction(); // Blocker API that got mixed into this instance due to BlockingView.imbue() above
         this.model.fetch().always(this.unblockActions.bind(this));
       }
   });

   return NodeView;
});
```
<a name="module_BlockingView.delegate"></a>

### BlockingView.delegate(view, delegateToView)
Mix progress blocking APIs to a view where the blocker instance is delegated to a different view.Useful when there are more than one view instance with parent-child relationship such that each of the views want to mixblocker APIs in it without creating duplicate blocker instances.

**Kind**: static method of [<code>BlockingView</code>](#module_BlockingView)  
**Mixes**: <code>BlockerAPI</code>  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>view</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>The view instance where blocing APIs are to be mixed.</p>
</td>
    </tr><tr>
    <td>delegateToView</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>The view which already has blocker APIs mixed and is going to be used as source of blocker instance.</p>
</td>
    </tr>  </tbody>
</table>

**Example** *(An item view showing/hiding blocker when its model syncs)*  
```js
define([
   'nuc/lib/marionette',
   'd2/sdk/controls/blocker/blocker'
   'd2/sdk/models/node.model',
], function(Marionette, BlockingView, NodeModel){
   'use strict';

   var NodeView = Marionette.ItemView.extend({
       constructor: function NodeView(options) {
           options = options || {};
           options.model = new NodeModel({id: options.id}, options);

           BlockingView.delegate(this, options.parentView); // Assuming 'parentView' already has blocker API mixed to it
       },

       onRender: function() {
         this.blockAction(); // Blocker API that got mixed into this instance due to BlockingView.imbue() above
         this.model.fetch().always(this.unblockActions.bind(this));
       }
   });

   return NodeView;
});
```
