<a name="module_NodesTableView"></a>

## NodesTableView : <code>d2/sdk/widgets/nodestable/nodestable.view</code>
Base class for all table-like widgets. Typically a subclass of this type is used whenever an[ApplicationScopePerspective](../../utils/perspectives/app.scope.perspectives) requires a table-like rendering.

**Extends**: [<code>Marionette.CollectionView</code>](https://marionettejs.com/docs/v2.4.7/marionette.collectionview.html)  
**Example** *(A minimum sample derivative may look like)*  
```js
//In mybundle/widgets/sample.table/sample.table.view.js

define([
 'd2/sdk/widgets/nodestable/nodestable.view',
 'mybundle/utils/contexts/factories/sample.table.collection.factory'
], function(NodesTableView, SampleTableCollectionFactory){
   'use strict';

   var SampleTableView = NodesTableView.extend({
     constructor: function SampleTableView(options) {
       options = options || {};
       options.className = 'my-table-view';
       options.d2collectionFactory = SampleTableCollectionFactory;

       SampleTableView.__super__.constructor.call(this, options);
     }
   });

   return SampleTableView;
});
```
**Example** *(To link it to a perspective)*  
```js
//In mybundle/utils/perspectives/table.perspecitve.json

 {
     "type": "left-center-right",
     "options": {
       "center": {
         "type": "mybundle/widgets/sample.table"
       }
     }
 }


//In mybundle/extensions/perspectives.js
define([], function(){
 'use strict';

 return [{
   ...,
   perspectiveJson: 'mybundle/utils/perspectives/table.perspecitve.json',
   ...
 }];
});
```
**Example** *(Finally, to register the perspective definition as an application scope perspective)*  
```js
//In extensions.json
{
  "d2/sdk/utils/perspectives/app.scope.perspectives": {
     "extensions": {
        "mybundle": {
           "mybundle/extensions/perspectives"
        }
     }
   }
}
```

* [NodesTableView](#module_NodesTableView) : <code>d2/sdk/widgets/nodestable/nodestable.view</code>
    * _instance_
        * [.getZeroRecordText()](#module_NodesTableView+getZeroRecordText) ⇒ <code>string</code>
        * [.getTitle()](#module_NodesTableView+getTitle) ⇒ <code>string</code>
        * [.getContainerNode()](#module_NodesTableView+getContainerNode) ⇒ [<code>NodeModel</code>](../../models/node.model)
        * [.getExtraIconConfig()](#module_NodesTableView+getExtraIconConfig) ⇒ <code>object</code>
        * [.getCollectionOptions()](#module_NodesTableView+getCollectionOptions) ⇒ <code>object</code>
    * _static_
        * [.extend(protoProperties, staticProperties)](#module_NodesTableView.extend) ⇒ <code>function</code>
    * _inner_
        * [~NodesTableView](#module_NodesTableView..NodesTableView)
            * [new NodesTableView([options])](#new_module_NodesTableView..NodesTableView_new)
        * [~NodesTableToolbar](#module_NodesTableView..NodesTableToolbar) : <code>Object</code>

<a name="module_NodesTableView+getZeroRecordText"></a>

### nodesTableView.getZeroRecordText() ⇒ <code>string</code>
Hizibizi

**Kind**: instance method of [<code>NodesTableView</code>](#module_NodesTableView)  
**Returns**: <code>string</code> - Text to be shown when there are no items in the data holder collection.  
<a name="module_NodesTableView+getTitle"></a>

### nodesTableView.getTitle() ⇒ <code>string</code>
**Kind**: instance method of [<code>NodesTableView</code>](#module_NodesTableView)  
**Returns**: <code>string</code> - Title to show at header of this table  
<a name="module_NodesTableView+getContainerNode"></a>

### nodesTableView.getContainerNode() ⇒ [<code>NodeModel</code>](../../models/node.model)
**Kind**: instance method of [<code>NodesTableView</code>](#module_NodesTableView)  
**Returns**: [<code>NodeModel</code>](../../models/node.model) - A node model to hold represenation data for the table header.  
<a name="module_NodesTableView+getExtraIconConfig"></a>

### nodesTableView.getExtraIconConfig() ⇒ <code>object</code>
**Kind**: instance method of [<code>NodesTableView</code>](#module_NodesTableView)  
**Returns**: <code>object</code> - Additional attributes to be merged with the NodeModel returned from `getContainerNode()`.  
<a name="module_NodesTableView+getCollectionOptions"></a>

### nodesTableView.getCollectionOptions() ⇒ <code>object</code>
**Kind**: instance method of [<code>NodesTableView</code>](#module_NodesTableView)  
**Returns**: <code>object</code> - Additional option to be used while constructing the collection from `collectionClass` constructor option.  
<a name="module_NodesTableView.extend"></a>

### NodesTableView.extend(protoProperties, staticProperties) ⇒ <code>function</code>
To define a derived type from `NodesTableView`.

**Kind**: static method of [<code>NodesTableView</code>](#module_NodesTableView)  
**Returns**: <code>function</code> - The derived type.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>protoProperties</td><td><code>object</code></td><td><p>Properties attached to the prototype of derived type.</p>
</td>
    </tr><tr>
    <td>protoProperties.constructor</td><td><code>function</code></td><td><p>The function to be used to construct instance of the derived type.</p>
</td>
    </tr><tr>
    <td>staticProperties</td><td><code>object</code></td><td><p>Properties statically attached to the derived type.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_NodesTableView..NodesTableView"></a>

### NodesTableView~NodesTableView
**Kind**: inner class of [<code>NodesTableView</code>](#module_NodesTableView)  
<a name="new_module_NodesTableView..NodesTableView_new"></a>

#### new NodesTableView([options])
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[options]</td><td><code>object</code></td><td><p>Constructor options</p>
</td>
    </tr><tr>
    <td>[options.cssClass]</td><td><code>string</code></td><td><p>CSS class name to be added to HTML element representing this whole table view. <code>options.className</code> will have a higher precedence over the same purpose.</p>
</td>
    </tr><tr>
    <td>[options.d2collectionFactory]</td><td><code>Factory</code></td><td><p>Collection factory to be used to instantiate the collection that will hold data for this table.</p>
</td>
    </tr><tr>
    <td>[options.collectionClass]</td><td><code>function</code></td><td><p>A constructor type to be used to instantiate the data holder collection.</p>
</td>
    </tr><tr>
    <td>[options.collection]</td><td><code>Backbone.Collection</code></td><td><p>A collection instance to be used as data holder for the table. One of <code>d2collectionFactory</code>, <code>collectionClass</code> or
<code>collection</code> has to be provided as part of constructor options. Otherwise, it fails with error.</p>
</td>
    </tr><tr>
    <td>[options.toolbarItems]</td><td><code>NodesTableToolbar</code></td><td><p>Menu configuration for the table view.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_NodesTableView..NodesTableToolbar"></a>

### NodesTableView~NodesTableToolbar : <code>Object</code>
Toolbar type for NodesTable.

**Kind**: inner typedef of [<code>NodesTableView</code>](#module_NodesTableView)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>inlineActionbar</td><td><code><a href="../../controls/toolbar/toolitems.factory">ToolitemsFactory</a></code></td><td><p>Menu configuration used at inidividual item level.</p>
</td>
    </tr><tr>
    <td>tableHeaderToolbar</td><td><code><a href="../../controls/toolbar/toolitems.factory">ToolitemsFactory</a></code></td><td><p>Menu configuration for the header.</p>
</td>
    </tr>  </tbody>
</table>

