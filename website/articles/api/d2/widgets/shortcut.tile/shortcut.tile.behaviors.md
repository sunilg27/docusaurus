<a name="module_ShortcutTileBehaviorCollection"></a>

## ShortcutTileBehaviorCollection : <code>d2/sdk/widgets/shortcut.tile/shortcut.tile.behaviors</code>
Collection of all registered shortcut behaviors. Used by shortcut tiles on D2 Smart View landing perspective to define what happens when a particularshortcut tile is clicked.To implement a new shortcut behavior and get it registered with this collection, see -* [ShortcutBehavior](./shortcut.tile.behavior)

**Extends**: <code>Array</code>  
