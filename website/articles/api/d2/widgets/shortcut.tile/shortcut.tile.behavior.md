<a name="module_ShortcutTileBehavior"></a>

## ShortcutTileBehavior : <code>d2/sdk/widgets/shortcut.tile/shortcut.tile.behavior</code>
Interface for all classes that want to implement a shortcut tile's behavior.Defines what should happen when user clicks on a particular shortcut tile in D2 Smart View landing perspective and also lets customizeappearance of the shortcut tile upto some extent.

**Extends**: [<code>Marionette.Behavior</code>](https://marionettejs.com/docs/v2.4.7/marionette.behavior.html)  
**Example** *(A new shortcut behavior url.shortcut.behavior.js could be defined as)*  
```js
define([
   'd2/sdk/widgets/shortcut.tile/shortcut.tile.behavior',
   'd2/sdk/utils/utils'
], function(BaseShortcutBehavior, utils){
   'use strict';

   var URLShortcutBehavior = BaseShortcutBehavior.extend({
       onClick: function() {
         if(this.options.data.url) {
           utils.openUrlInWindow(this.options.data.url, window.open(''));
         }
       }
   });

   return URLShortcutBehavior;
});
```
**Example** *(to register this shortcut behavior with ShortcutTileBehaviorCollection, add in extension.json)*  
```js
{
  "d2/sdk/widgets/shortcut.tile/shortcut.tile.behaviors": {
     "extensions": {
       "mybundle": {
         "mybundle/extensions/my.shortcut.behaviors"
       }
     }
   }
}
```
**Example** *(where my.shortcut.behaviors.js is defined as)*  
```js
define([
   'mybundle/widgets/shortcut.tile/url.shortcut.behavior'
], function(URLShortcutBehavior){
   'use strict';

   // see 'ShortcutBehaviorType' documentation below for all possible properties and their usage.
   return [
     {type: 'OpenURLWidget', behaviorClass: URLShortcutBehavior}
     // Additional behaviors can also be registered
   ];
});
```

* [ShortcutTileBehavior](#module_ShortcutTileBehavior) : <code>d2/sdk/widgets/shortcut.tile/shortcut.tile.behavior</code>
    * _instance_
        * [.view](#module_ShortcutTileBehavior+view) : [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
        * [.viewModel](#module_ShortcutTileBehavior+viewModel) : [<code>Backbone.Model</code>](https://backbonejs.org/#Model)
        * [.context](#module_ShortcutTileBehavior+context) : <code>Context</code>
        * [.options](#module_ShortcutTileBehavior+options) : <code>Object</code>
        * *[.onRender()](#module_ShortcutTileBehavior+onRender)*
        * *[.onClick()](#module_ShortcutTileBehavior+onClick)*
    * _static_
        * [.extend(protoProperties, staticProperties)](#module_ShortcutTileBehavior.extend) ⇒ <code>function</code>
    * _inner_
        * [~ShortcutBehaviorType](#module_ShortcutTileBehavior..ShortcutBehaviorType) : <code>Object</code>

<a name="module_ShortcutTileBehavior+view"></a>

### shortcutTileBehavior.view : [<code>Marionette.View</code>](https://marionettejs.com/docs/v2.4.7/marionette.view.html)
reference to the shortcut's presentation

**Kind**: instance property of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
<a name="module_ShortcutTileBehavior+viewModel"></a>

### shortcutTileBehavior.viewModel : [<code>Backbone.Model</code>](https://backbonejs.org/#Model)
the model controlling the shortcut view

**Kind**: instance property of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
<a name="module_ShortcutTileBehavior+context"></a>

### shortcutTileBehavior.context : <code>Context</code>
active application context

**Kind**: instance property of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
**See**: [Context](../../utils/contexts/context.utils#Context)  
<a name="module_ShortcutTileBehavior+options"></a>

### shortcutTileBehavior.options : <code>Object</code>
Holder of parameters passed during shortcut construction. Has a normalized referece to the D2 Smart View landing page configuration behind this shortcut tile.

**Kind**: instance property of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
<a name="module_ShortcutTileBehavior+onRender"></a>

### *shortcutTileBehavior.onRender()*
Marionette view lifecycle hook. It can be overriden to make any changes to the DOM element of the underlying shortcut view.If overriden, it should call the `onRender` method of super type as the first thing. See example above.

**Kind**: instance abstract method of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
<a name="module_ShortcutTileBehavior+onClick"></a>

### *shortcutTileBehavior.onClick()*
Business logic to define what happens on click. It can be overriden to provide custom logic.

**Kind**: instance abstract method of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
<a name="module_ShortcutTileBehavior.extend"></a>

### ShortcutTileBehavior.extend(protoProperties, staticProperties) ⇒ <code>function</code>
To define a derived type from `ShortcutTileBehavior`.

**Kind**: static method of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
**Returns**: <code>function</code> - The derived type.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>protoProperties</td><td><code>object</code></td><td><p>Properties attached to the prototype of derived type.</p>
</td>
    </tr><tr>
    <td>protoProperties.constructor</td><td><code>function</code></td><td><p>The function to be used to construct instance of the derived type.</p>
</td>
    </tr><tr>
    <td>staticProperties</td><td><code>object</code></td><td><p>Properties statically attached to the derived type.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ShortcutTileBehavior..ShortcutBehaviorType"></a>

### ShortcutTileBehavior~ShortcutBehaviorType : <code>Object</code>
Extension definition to link a shortcut behavior implementation with a shortcut tile type.

**Kind**: inner typedef of [<code>ShortcutTileBehavior</code>](#module_ShortcutTileBehavior)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>string</code></td><td></td><td><p>Type of the shortcut tile.</p>
</td>
    </tr><tr>
    <td>behaviorClass</td><td><code>ShortcutTileBehavior</code></td><td></td><td><p>The behavior implementation.</p>
</td>
    </tr><tr>
    <td>[sequence]</td><td><code>number</code></td><td><code>1000</code></td><td><p>Priority of this behavior for the given <code>type</code>. Among all behaviors with matching <code>type</code> value, the one with lowest <code>sequence</code> is finally used.</p>
</td>
    </tr>  </tbody>
</table>

