<a name="module_ListTileView"></a>

## ListTileView : <code>d2/sdk/widget/list.tile/list.tile.view</code>
Base class for all list type of tiles in D2 Smart View landing perspective.

**Extends**: [<code>Marionette.CollectionView</code>](https://marionettejs.com/docs/v2.4.7/marionette.collectionview.html)  
**Example** *(A minimum sample list derivative may look like)*  
```js
//In mybundle/widgets/my.tile/my.tile.view.js
define([
 'd2/sdk/widgets/list.tile/list.tile.view',
 'mybundle/utils/contexts/factories/sample.tile.collection.factory'
], function(ListTileView, SampleTileCollectionFactory){
   'use strict';

   var SampleListTileView = ListTileView.extend({
     constructor: function SampleListTileView(options) {
       options = options || {};
       options.collectionFactory = SampleTileCollectionFactory;

       SampleListTileView.__super__.constructor.call(this, options);
     }
   });

   return SampleListTileView;
});
```
**Example** *(To link this definition to the list of tiles looked up at landing page resolution time)*  
```js
//in mybundle/extensions/landing.tiles.js
define([], function(){
 'use strict';

 return [{
   ...
   tileView: 'mybundle/widgets/my.tile/my.tile.view',
   ...
 }];
});
```
**Example** *(Then finally the extension is registered in extension.json as)*  
```js
{
  "d2/sdk/utils/landingpage/tiles": {
     "extensions": {
        "mybundle": {
           "mybundle/extensions/landing.tiles"
        }
     }
   }
}
```

* [ListTileView](#module_ListTileView) : <code>d2/sdk/widget/list.tile/list.tile.view</code>
    * _instance_
        * [.getFactoryParam()](#module_ListTileView+getFactoryParam) ⇒ <code>string</code>
        * [.onClickItem(target)](#module_ListTileView+onClickItem) ⇒ <code>void</code>
        * [.onClickHeader()](#module_ListTileView+onClickHeader) ⇒ <code>void</code>
    * _static_
        * [.extend(protoProperties, staticProperties)](#module_ListTileView.extend) ⇒ <code>function</code>
    * _inner_
        * [~ListTileView](#module_ListTileView..ListTileView)
            * [new ListTileView(options)](#new_module_ListTileView..ListTileView_new)
        * [~ListTileData](#module_ListTileView..ListTileData) : <code>Object</code>

<a name="module_ListTileView+getFactoryParam"></a>

### listTileView.getFactoryParam() ⇒ <code>string</code>
Get the name of factory param if this list view is backed by a collection factory.For easy coupling factory param should be given a name same as the `propertyPrefix` of the backing collection facatory.

**Kind**: instance method of [<code>ListTileView</code>](#module_ListTileView)  
**Returns**: <code>string</code> - Should return the factory param name.  
<a name="module_ListTileView+onClickItem"></a>

### listTileView.onClickItem(target) ⇒ <code>void</code>
Handles what happens when a list item is clicked or equivalne user interaction happens.The default implementation executes default action associated with the list item.

**Kind**: instance method of [<code>ListTileView</code>](#module_ListTileView)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>target</td><td><code>Marionette.View</code></td><td><p>View instance associated with the item being clicked.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ListTileView+onClickHeader"></a>

### listTileView.onClickHeader() ⇒ <code>void</code>
Handles what happens when the tile's header is clicked. The default implementation tries to openup linked application scope perspective, if any. Setting a special constructor option `avoidOpenPerspectiveOnHeader` to `true` prevents thedefault behavior from happening.

**Kind**: instance method of [<code>ListTileView</code>](#module_ListTileView)  
<a name="module_ListTileView.extend"></a>

### ListTileView.extend(protoProperties, staticProperties) ⇒ <code>function</code>
To define a derived type from `ListTileView`.

**Kind**: static method of [<code>ListTileView</code>](#module_ListTileView)  
**Returns**: <code>function</code> - The derived type.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>protoProperties</td><td><code>object</code></td><td><p>Properties attached to the prototype of derived type.</p>
</td>
    </tr><tr>
    <td>protoProperties.constructor</td><td><code>function</code></td><td><p>The function to be used to construct instance of the derived type.</p>
</td>
    </tr><tr>
    <td>staticProperties</td><td><code>object</code></td><td><p>Properties statically attached to the derived type.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ListTileView..ListTileView"></a>

### ListTileView~ListTileView
**Kind**: inner class of [<code>ListTileView</code>](#module_ListTileView)  
<a name="new_module_ListTileView..ListTileView_new"></a>

#### new ListTileView(options)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>constructor options</p>
</td>
    </tr><tr>
    <td>[options.data]</td><td><code>ListTileData</code></td><td><p>Generic purpose data. Defaults to nothing.</p>
</td>
    </tr><tr>
    <td>[options.collectionFactory]</td><td><code>Factory</code></td><td><p>A collection factory reference that will be used to create the collection that holds the data for this tile.
See <a href="../../utils/contexts/factories/factory">Factory</a></p>
</td>
    </tr><tr>
    <td>[options.collection]</td><td><code>Backbone.Collection</code></td><td><p>Reference to a collection instance to be used as data holder. Either one of <code>collectionFactory</code> or <code>collection</code> is required
If both are present in <code>options</code> then precedence is given to <code>collection</code>.</p>
</td>
    </tr><tr>
    <td>[options.lang]</td><td><code>object</code></td><td><p>Translation keys to be used for displaying labels associated with this tile.</p>
</td>
    </tr><tr>
    <td>[options.orderBy]</td><td><code>string</code></td><td><p>Default sorting order for the data to be displayed in this tile. Defaults to <code>name asc</code> i.e sort by item names in ascending order.</p>
</td>
    </tr><tr>
    <td>[options.showTitleIcon]</td><td><code>Boolean</code></td><td><p>Whether to show/hide icon for this tile. Defaults to <code>true</code>.</p>
</td>
    </tr><tr>
    <td>[options.context]</td><td><code>Context</code></td><td><p>The application context. See <a href="../../utils/contexts/context.utils#Context">Context</a></p>
</td>
    </tr><tr>
    <td>[options.showInlineActionBar]</td><td><code>Boolean</code></td><td><p>Whether to show menu actions on this tile&#39;s items. Defaults to <code>true</code>. However, actual display of menu
depends on <code>toolbarItems</code> configuration and availability of actions against each list item.</p>
</td>
    </tr><tr>
    <td>[options.toolbarItems]</td><td><code>ToolItemsFactory</code></td><td><p>The menu configuration for this tile&#39;s items. Default to Doclist menu configuration.
See <a href="../../controls/toolbar/toolitems.factory">ToolItemsFactory</a></p>
</td>
    </tr><tr>
    <td>[options.applicationScope]</td><td><code>string</code></td><td><p>Name of the application scope perspective which this tile may expand into. The value for this property
will be matched against all the registered application scopes. The relevant header button is enabled only if a match is found.</p>
</td>
    </tr><tr>
    <td>[options.hideOpenPerspective]</td><td><code>Boolean</code></td><td><p>Whether to hide the header button that makes this tile expand into the linked application scope perspective.
Defaults to <code>false</code>.</p>
</td>
    </tr><tr>
    <td>[options.hideSearch]</td><td><code>Boolean</code></td><td><p>Whether to hide the search box from tile header. Defaults to <code>false</code>.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ListTileView..ListTileData"></a>

### ListTileView~ListTileData : <code>Object</code>
Generic purpose data for the tile. Properties other than `icon` and `title` are directly merged with other constructor options.

**Kind**: inner typedef of [<code>ListTileView</code>](#module_ListTileView)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>icon</td><td><code>string</code></td><td><p>CSS class name of the tile header icon</p>
</td>
    </tr><tr>
    <td>title</td><td><code>string</code></td><td><p>Header label for the tile</p>
</td>
    </tr>  </tbody>
</table>

