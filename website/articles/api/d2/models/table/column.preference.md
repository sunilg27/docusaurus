<a name="module_ColumnPreferenceModel"></a>

## ColumnPreferenceModel : <code>d2/sdk/models/table/column.preference</code>
Fetch or save column configuration associated with a particular widget config.

**Extends**: [<code>Backbone.Model</code>](https://backbonejs.org/#Model)  

* [ColumnPreferenceModel](#module_ColumnPreferenceModel) : <code>d2/sdk/models/table/column.preference</code>
    * _instance_
        * [.columns](#module_ColumnPreferenceModel+columns) : <code>Backbone.Collection</code>
        * [.fetch()](#module_ColumnPreferenceModel+fetch) ⇒ <code>jQuery.Promise</code>
        * [.save()](#module_ColumnPreferenceModel+save) ⇒ <code>jQuery.Promise</code>
    * _inner_
        * [~constructor(attributes, options)](#module_ColumnPreferenceModel..constructor)

<a name="module_ColumnPreferenceModel+columns"></a>

### columnPreferenceModel.columns : <code>Backbone.Collection</code>
Parsed column configuration such that each model in this collection represent a single column.

**Kind**: instance property of [<code>ColumnPreferenceModel</code>](#module_ColumnPreferenceModel)  
<a name="module_ColumnPreferenceModel+fetch"></a>

### columnPreferenceModel.fetch() ⇒ <code>jQuery.Promise</code>
Fetch from server the column configuration of the widget identified by the set options

**Kind**: instance method of [<code>ColumnPreferenceModel</code>](#module_ColumnPreferenceModel)  
<a name="module_ColumnPreferenceModel+save"></a>

### columnPreferenceModel.save() ⇒ <code>jQuery.Promise</code>
Save this column configuration to server

**Kind**: instance method of [<code>ColumnPreferenceModel</code>](#module_ColumnPreferenceModel)  
<a name="module_ColumnPreferenceModel..constructor"></a>

### ColumnPreferenceModel~constructor(attributes, options)
Create a new instance.

**Kind**: inner method of [<code>ColumnPreferenceModel</code>](#module_ColumnPreferenceModel)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>attributes</td><td><code>Object</code></td><td><p>column attributes</p>
</td>
    </tr><tr>
    <td>options</td><td><code>Object</code></td><td><p>instance specific options</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>AJAX transport. See <a href="../../utils/contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>options.widgetType</td><td><code>string</code></td><td><p>Type of the widget config</p>
</td>
    </tr><tr>
    <td>options.widgetName</td><td><code>string</code></td><td><p>Name of the widget config.</p>
</td>
    </tr><tr>
    <td>[options.defaultType]</td><td><code>string</code></td><td><p>Default column types.</p>
</td>
    </tr>  </tbody>
</table>

