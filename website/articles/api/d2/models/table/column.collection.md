<a name="module_ColumnCollection"></a>

## ColumnCollection : <code>d2/sdk/models/table/column.collection</code>
A generic Column collection. Used in the context of a [BrowsableMixin](../mixins/browsable.collection.mixin) to contain and define column definitions for a table like view.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
