<a name="module_NodeCollection"></a>

## NodeCollection : <code>d2/sdk/models/node.collection</code>
Represents an arbitrary collection of [NodeModel](./node.model)s. Mostly used in the context of command execution.Even if the collection is instantiated with liternal properties, it maps those propoerties into instance of `NodeModel`.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example**  
```js
define([   'd2/sdk/models/node.collection'], function(NodeCollection){   'use strict';   var nodeCollection = new NodeCollection([     {id: '0c0015fa8000291e'},     {id: '0b0015fa80005598'}   ]);   console.log('There are ' + nodeCollection.length + ' models');   console.log('First model is with id : ' + nodeCollection.at(0).get('id'));});
```
