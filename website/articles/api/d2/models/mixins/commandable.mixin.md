<a name="module_CommandableMixin"></a>

## CommandableMixin : <code>d2/sdk/models/mixins/commandable.mixin</code>
Commandable mixin. Add action capability to a `Backbone.Collection` or `Backbone.Model` such that when the collection or model instance is synced with server, it fetchesmenu actions for the inidividual models.promoted actions are fetched immediately but non-promoted actions are delayed until an external trigger.

**Extends**: <code>Object</code>  
**Example** *(A typical collection using this mixin may look like)*  
```js
define([
 'nuc/lib/backbone',
 'd2/sdk/models/mixins/commandable.mixin'
], function(Backbone, CommandableMixin){
 'use strict';

 var MyCollection = Backbone.Collection.extend({
   constructor: function MyCollection(models, options) {
     options = options || {};
     options.d2ActionsOptions = options.d2ActionsOptions || {
       menuType: 'MenuContext',
       widgetType: 'DoclistWidget'
     };

     MyCollection.__super__.constructor.call(this, models, options);

     this.makeCommandable(options);
   }
 });

 CommandableMixin.mixin(MyCollection.ptototype); //Can be equally mixed with a Backbone.Model prototype

 return MyCollection;
});
```

* [CommandableMixin](#module_CommandableMixin) : <code>d2/sdk/models/mixins/commandable.mixin</code>
    * _instance_
        * [.makeCommandable(options)](#module_CommandableMixin+makeCommandable) ⇒ <code>\*</code>
    * _static_
        * [.mixin(prototype)](#module_CommandableMixin.mixin) ⇒ <code>void</code>
    * _inner_
        * [~ActionOption](#module_CommandableMixin..ActionOption) : <code>Object</code>

<a name="module_CommandableMixin+makeCommandable"></a>

### commandableMixin.makeCommandable(options) ⇒ <code>\*</code>
Does the necessary setup on an instance of `Backbone.Model` or `Backbone.Collection` so that it can fetchassociated menu actions.

**Kind**: instance method of [<code>CommandableMixin</code>](#module_CommandableMixin)  
**Returns**: <code>\*</code> - the model/collection instance itself.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Options hash</p>
</td>
    </tr><tr>
    <td>[options.delayedActionsOptions]</td><td><code>ActionOption</code></td><td><p>Controls options for the delayed action fetch behavior</p>
</td>
    </tr><tr>
    <td>[options.nonPromotedActionsOptions]</td><td><code>ActionOption</code></td><td><p>Controls options for the non-promoted action fetch behavior</p>
</td>
    </tr><tr>
    <td>[options.additionalActionsOptions]</td><td><code>ActionOption</code></td><td><p>Controls options for the additional action fetch behavior</p>
</td>
    </tr><tr>
    <td>[options.d2ActionsOptions]</td><td><code>ActionOption</code></td><td><p>Controls options for the delayed, non-promoted &amp; additional action fetch behavior simultaneously
unless the above corresponding options are also explicitly specified.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandableMixin.mixin"></a>

### CommandableMixin.mixin(prototype) ⇒ <code>void</code>
Fuse the mixin methods to a prototype

**Kind**: static method of [<code>CommandableMixin</code>](#module_CommandableMixin)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>Object</code></td><td><p>The target prototype where the mixin methods will be fused.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandableMixin..ActionOption"></a>

### CommandableMixin~ActionOption : <code>Object</code>
**Kind**: inner typedef of [<code>CommandableMixin</code>](#module_CommandableMixin)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>menuType</td><td><code>string</code></td><td><p>Controls the <code>type</code> query parameter sent during D2-REST <code>actions</code> endpoint invocation.</p>
</td>
    </tr><tr>
    <td>widgetType</td><td><code>string</code></td><td><p>Controls the <code>widgetType</code> query parameter sent during D2-REST <code>actions</code> endpoint invocation.</p>
</td>
    </tr>  </tbody>
</table>

