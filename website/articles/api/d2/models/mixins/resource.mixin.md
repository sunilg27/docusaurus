<a name="module_ResourceMixin"></a>

## ResourceMixin : <code>d2/sdk/models/mixins/resource.mixin</code>
Resource mixin. Can be mixed with any `Backbone.Model` or `Backbone.Collection` to make it AJAX worthy.

**Extends**: <code>Object</code>  
**Example** *(A typical collection using this mixin may look like)*  
```js
define([
 'nuc/lib/backbone',
 'd2/sdk/models/mixins/resource.mixin'
], function(Backbone, ResourceMixin){
 'use strict';

 var MyModel = Backbone.Model.extend({
   constructor: function MyModel(attr, options) {
     options = options || {};
     MyModel.__super__.constructor.call(this, attr, options);

     this.makeResource(options);
   }
 });

 ResourceMixin.mixin(MyModel.ptototype); //Can be equally mixed with a Backbone.Collection prototype

 return MyModel;
});
```

* [ResourceMixin](#module_ResourceMixin) : <code>d2/sdk/models/mixins/resource.mixin</code>
    * _instance_
        * [.makeResource(options)](#module_ResourceMixin+makeResource) ⇒ <code>\*</code>
    * _static_
        * [.mixin(prototype)](#module_ResourceMixin.mixin) ⇒ <code>void</code>

<a name="module_ResourceMixin+makeResource"></a>

### resourceMixin.makeResource(options) ⇒ <code>\*</code>
Does the necessary setup on an instance of `Backbone.Model` or `Backbone.Collection` so that it can connect & fetchresource from server using AJAX.

**Kind**: instance method of [<code>ResourceMixin</code>](#module_ResourceMixin)  
**Returns**: <code>\*</code> - the model/collection instance itself.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Options hash</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>The transport that lets the model or collection to connect to server. See <a href="../../utils/contexts/factories/connector">ConnectorFactory</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ResourceMixin.mixin"></a>

### ResourceMixin.mixin(prototype) ⇒ <code>void</code>
Fuse the mixin methods to a prototype

**Kind**: static method of [<code>ResourceMixin</code>](#module_ResourceMixin)  
**Mixes**: [<code>ConnectableMixin</code>](../../../nuc/models/mixins/connectable/connectable.mixin), [<code>FetchableMixin</code>](../../../nuc/models/mixins/fetchable/fetchable.mixin)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>Object</code></td><td><p>The target prototype where the mixin methods will be fused.</p>
</td>
    </tr>  </tbody>
</table>

