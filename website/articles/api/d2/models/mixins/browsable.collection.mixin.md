<a name="module_BrowsableCollectionMixin"></a>

## BrowsableCollectionMixin : <code>d2/sdk/models/mixins/browsable.collection.mixin</code>
BrowsableCollection mixin. Can be mixed with any `Backbone.Collection` to make it paginated. Pagination happens on the clientside by slicing & maintaining the models in it. But when it syncs to the server, it always fetches the full list of available objects.Besides pagination it also enables sorting & filtering.

**Extends**: <code>Object</code>  
**See**: [Browsable Support](../../../nuc/models/browsable/browsable)  
**Example** *(A typical usage infuses the mixin with a collection prototype)*  
```js
define([
 'nuc/lib/backbone',
 'd2/sdk/models/mixins/browsable.collection.mixin'
], function(Backbone, BrowsableCollectionMixin){
 'use strict';

 var MyCollection = Backbone.Collection.extend({
   constructor: function MyCollection(models, options) {
     options = options || {};
     MyCollection.__super__.constructor.call(this, models, options);

     this.makeBrowsableCollection(options);
   }
 });

 BrowsableCollectionMixin.mixin(MyCollection.ptototype);

 return MyCollection;
});
```

* [BrowsableCollectionMixin](#module_BrowsableCollectionMixin) : <code>d2/sdk/models/mixins/browsable.collection.mixin</code>
    * _instance_
        * [.makeBrowsableCollection(options)](#module_BrowsableCollectionMixin+makeBrowsableCollection) ⇒ <code>\*</code>
    * _static_
        * [.mixin(prototype)](#module_BrowsableCollectionMixin.mixin) ⇒ <code>void</code>

<a name="module_BrowsableCollectionMixin+makeBrowsableCollection"></a>

### browsableCollectionMixin.makeBrowsableCollection(options) ⇒ <code>\*</code>
Does the necessary setup so that a `Backbone.Collection` instance could become client-side paginated along with support for property based sorting & filtering.

**Kind**: instance method of [<code>BrowsableCollectionMixin</code>](#module_BrowsableCollectionMixin)  
**Returns**: <code>\*</code> - the collection instance itself.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td><p>The options hash.</p>
</td>
    </tr><tr>
    <td>[options.skip]</td><td><code>number</code></td><td><p>Number of elements to skip from original collection. Needs to be set only if the collection needs to start from a page other the page-1.</p>
</td>
    </tr><tr>
    <td>options.top</td><td><code>number</code></td><td><p>Number of elements per page.</p>
</td>
    </tr><tr>
    <td>[options.filter]</td><td><code>string</code></td><td><p>Any filter expression that the collection should begin with. Can be changed later with <code>setFilter</code>.</p>
</td>
    </tr><tr>
    <td>[options.orderBy]</td><td><code>string</code></td><td><p>Any sorting criteria that the collection should begin with. Can be changed later with <code>serOrderBy</code>.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_BrowsableCollectionMixin.mixin"></a>

### BrowsableCollectionMixin.mixin(prototype) ⇒ <code>void</code>
Fuse the mixin methods to a prototype

**Kind**: static method of [<code>BrowsableCollectionMixin</code>](#module_BrowsableCollectionMixin)  
**Mixes**: [<code>ClientSideBrowsableMixin</code>](../../../nuc/models/browsable/client-side.mixin), [<code>BrowsableMixin</code>](../../../nuc/models/browsable/browsable.mixin), <code>module:DelayedCollectionDataMixin</code>  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>Object</code></td><td><p>The target prototype where the mixin methods will be fused.</p>
</td>
    </tr>  </tbody>
</table>

