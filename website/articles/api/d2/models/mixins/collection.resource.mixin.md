<a name="module_CollectionResourceMixin"></a>

## CollectionResourceMixin : <code>d2/sdk/models/mixins/collection.resource.mixin</code>
CollectionResource mixin. This is an utility to conveniently enable AJAX, Browsable & Commandable capabilities onto a `Backbone.Collection`.

**Extends**: <code>Object</code>  
**Example** *(A typical collection using this mixin may look like)*  
```js
define([
 'nuc/lib/backbone',
 'd2/sdk/models/mixins/collection.resource.mixin'
], function(Backbone, CollectionResourceMixin){
 'use strict';

 var MyCollection = Backbone.Collection.extend({
   constructor: function MyCollection(models, options) {
     options = options || {};
     MyCollection.__super__.constructor.call(this, models, options);

     this.makeCollectionResource(options);
   }
 });

 CollectionResourceMixin.mixin(MyCollection.ptototype);

 return MyCollection;
});
```

* [CollectionResourceMixin](#module_CollectionResourceMixin) : <code>d2/sdk/models/mixins/collection.resource.mixin</code>
    * _instance_
        * [.makeCollectionResource(options)](#module_CollectionResourceMixin+makeCollectionResource) ⇒ <code>\*</code>
    * _static_
        * [.mixin(prototype)](#module_CollectionResourceMixin.mixin) ⇒ <code>void</code>

<a name="module_CollectionResourceMixin+makeCollectionResource"></a>

### collectionResourceMixin.makeCollectionResource(options) ⇒ <code>\*</code>
Does the necessary setup on an instance of `Backbone.Collection` so that it is AJAX enabled. Also adds thecapability to autofetch menu actions associated with each model and then finally represent the collection asa client-side paginated one.

**Kind**: instance method of [<code>CollectionResourceMixin</code>](#module_CollectionResourceMixin)  
**Returns**: <code>\*</code> - the collection instance itself.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td><p>The options hash. See the original mixin options for supported parameters.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CollectionResourceMixin.mixin"></a>

### CollectionResourceMixin.mixin(prototype) ⇒ <code>void</code>
Fuse the mixable methods to a prototype

**Kind**: static method of [<code>CollectionResourceMixin</code>](#module_CollectionResourceMixin)  
**Mixes**: [<code>ResourceMixin</code>](./resource.mixin), [<code>CommandableMixin</code>](./commandable.mixin), [<code>BrowsableCollectionMixin</code>](./browsable.collection.mixin)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>Object</code></td><td><p>The target prototype where the mixin methods will be fused.</p>
</td>
    </tr>  </tbody>
</table>

