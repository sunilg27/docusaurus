<a name="module_DelayedCollectionDataMixin"></a>

## DelayedCollectionDataMixin : <code>d2/sdk/models/mixins/collection.delayed.data.mixin</code>
DelayedCollectionData mixin. Can be mixed with any `Backbone.Collection` to create an interface that can be used toretrieve additional data like permission, favorited status, location, etc. and associated those with each model to fullyrepresent them in any view context.This additional data retrieval happens only after the primary collection data has been fetched and thus the name DelayedCollectionData.Collections using this mixin must override `fetchDelayedData()` method to actually fetch the additional data and must call `enableDelayedDataFetch()`post construction to enable the lifycycle hooks associated with this mixin.

**Extends**: <code>Object</code>  
**See**: [ServerQueryUtils](../../utils/server.query.utils)  
**Example** *(A typical collection using this mixin may look like)*  
```js
define([
 'nuc/lib/jquery',
 'nuc/lib/backbone',
 'd2/sdk/models/mixins/collection.delayed.data.mixin'
], function($, Backbone, DelayedCollectionDataMixin){
 'use strict';

 var MyCollection = Backbone.Collection.extend({
   constructor: function MyCollection(models, options) {
     options = options || {};
     MyCollection.__super__.constructor.call(this, models, options);

     this.enableDelayedDataFetch(options);
   },

   fetchDelayedData: function(options) {
     var dfd = $.Deferred();

     //Custom logic to fetch the delayed data
     //once done, `dfd` should be resolved with the retrieved data.

     return dfd.promise();
   }
 });

 DelayedCollectionDataMixin.mixin(MyCollection.ptototype);

 return MyCollection;
});
```

* [DelayedCollectionDataMixin](#module_DelayedCollectionDataMixin) : <code>d2/sdk/models/mixins/collection.delayed.data.mixin</code>
    * _instance_
        * [.enableDelayedDataFetch(options)](#module_DelayedCollectionDataMixin+enableDelayedDataFetch) ⇒ <code>\*</code>
        * *[.fetchDelayedData(options)](#module_DelayedCollectionDataMixin+fetchDelayedData) ⇒ <code>jQuery.Promise</code>*
    * _static_
        * [.mixin(prototype)](#module_DelayedCollectionDataMixin.mixin) ⇒ <code>void</code>

<a name="module_DelayedCollectionDataMixin+enableDelayedDataFetch"></a>

### delayedCollectionDataMixin.enableDelayedDataFetch(options) ⇒ <code>\*</code>
To enable binding on the collection lifecyle hooks that will later trigger `fetchDelayedData()` on every collection sync.

**Kind**: instance method of [<code>DelayedCollectionDataMixin</code>](#module_DelayedCollectionDataMixin)  
**Returns**: <code>\*</code> - Reference to the same instance of the collection on which it is called.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>constructor options as created and passed during instantiation of the collection.</p>
</td>
    </tr><tr>
    <td>options.mergeDelayedDataSilently</td><td><code>boolean</code></td><td><p>This controls whether or not to merge the additional data on individual models in silent manner.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DelayedCollectionDataMixin+fetchDelayedData"></a>

### *delayedCollectionDataMixin.fetchDelayedData(options) ⇒ <code>jQuery.Promise</code>*
Triggered every time the collection fetches its primary data. An implementation of this function enables the actual additional data fetch.The default implementation basically does nothing.

**Kind**: instance abstract method of [<code>DelayedCollectionDataMixin</code>](#module_DelayedCollectionDataMixin)  
**Returns**: <code>jQuery.Promise</code> - The returned promise should be resolved with fetched additional data to indicate successful retrieval or should be rejected otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Input paramters for the invocation.</p>
</td>
    </tr><tr>
    <td>options.nodes</td><td><code>Array.&lt;string&gt;</code></td><td><p>The list of id of all the models currently inside this collection.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DelayedCollectionDataMixin.mixin"></a>

### DelayedCollectionDataMixin.mixin(prototype) ⇒ <code>void</code>
Fuse the mixin methods to a collection prototype

**Kind**: static method of [<code>DelayedCollectionDataMixin</code>](#module_DelayedCollectionDataMixin)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>prototype</td><td><code>Object</code></td><td><p>Target collection prototype where the mixin methods will be fused.</p>
</td>
    </tr>  </tbody>
</table>

