<a name="module_NodeModel"></a>

## NodeModel : <code>d2/sdk/models/node.model</code>
NodeModel. Represents a Documentum object. Holds data attriutes that correspond to equivalent Documentum object properties.Smartview core framework uses a similar interface. See [NodeModel](../../nuc/models/node/node.model)

**Extends**: [<code>Backbone.Model</code>](https://backbonejs.org/#Model)  
**Example**  
```js
define([   'd2/sdk/models/node.model',   'd2/sdk/utils/contexts/context.utils',   'd2/sdk/utils/contexts/factories/factory'], function(NodeModel, contextUtils, ConnectorFactory){ 'use strict'; //To instantiate a NodeModel and fetch details of a Documentum object with id '090015fa8000441a' var context = contextUtils.getPerspectiveContext(),   connector = context.getObject(ConnectorFactory); var node = new NodeModel({id: '090015fa8000441a'}, {connector: connector}); //connector instance must be passed in the constructor options for all fetchable nodes. node.fetch().done(function(){   console.log('Node name: ' + node.get('name'); }).fail(function(){   console.log('Failed fetching node with id:' + node.get('id')); }); //NodeModel could also be extended to suit particular Docuemntum type specific usage var TaskModel = NodeModel.extend({   constructor: function TaskModel(attr, options) {     TaskModel.__super__.constructor.call(this, attr, options);   },   parse: function(response, options) {     var data = TaskModel.__super__.parse.call(this, response, options);     //Custom logic to parse task specific attributes     return data;   } });});
```

* [NodeModel](#module_NodeModel) : <code>d2/sdk/models/node.model</code>
    * [NodeModel](#exp_module_NodeModel--NodeModel) ⏏
        * [new NodeModel([attributes], [options])](#new_module_NodeModel--NodeModel_new)

<a name="exp_module_NodeModel--NodeModel"></a>

### NodeModel ⏏
**Kind**: Exported class  
**Mixes**: [<code>ResourceMixin</code>](./mixins/resource.mixin), [<code>CommandableMixin</code>](./mixins/commandable.mixin)  
<a name="new_module_NodeModel--NodeModel_new"></a>

#### new NodeModel([attributes], [options])
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[attributes]</td><td><code>object</code></td><td><p>An object hash containing set of initial attributes for the NodeModel.</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>object</code></td><td><p>Constructor options. Mixin specific options are supported as-is through constructor options.</p>
</td>
    </tr><tr>
    <td>[options.connector]</td><td><code>Connector</code></td><td><p>Mandatory if the NodeModel is to be synced with server.</p>
</td>
    </tr><tr>
    <td>[options.ignoreObjectData]</td><td><code>Boolean</code></td><td><p>Whether to fetch additional data like menu actions, favorited status etc. when this node syncs with server.
Defaults to <code>false</code>. This flag acts as a master flag for other addtional data specific flags like <code>fetchAction</code>, <code>fetchFavorites</code>, <code>fetchPreviewFormats</code>,
<code>fetchRelationsCount</code> &amp; <code>fetchExternalShare</code>.</p>
</td>
    </tr><tr>
    <td>[options.fetchActions]</td><td><code>Boolean</code></td><td><p>Whether to fetch menu actions after this node syncs with server. Defaults to <code>true</code>.</p>
</td>
    </tr><tr>
    <td>[options.fetchFavorites]</td><td><code>Boolean</code></td><td><p>Whether to fetch &#39;favorited&#39; status for this nodes after sync with server. Defaults to <code>true</code>.</p>
</td>
    </tr><tr>
    <td>[options.fetchPreviewFormats]</td><td><code>Boolean</code></td><td><p>Whether to fetch preview formats for the node after it has synced with server. Defautls to <code>true</code>.</p>
</td>
    </tr><tr>
    <td>[options.fetchRelationsCount]</td><td><code>Boolean</code></td><td><p>Whether to fetch relations count for the node after it has synced with server. Defaults to <code>true</code>.</p>
</td>
    </tr><tr>
    <td>[options.fetchExternalShare]</td><td><code>Boolean</code></td><td><p>Whether to fetch external share status for the node after it has synced with server. Defaults to <code>true</code>.</p>
</td>
    </tr>  </tbody>
</table>

