<a name="module_ModuleCollection"></a>

## ModuleCollection : <code>d2/sdk/models/module.collection</code>
Server module collection. Lists all the plugins registered with the D2 Smartview runtime as well as the underlying framework.By convention it is required for any new dynamic component(plugins), that adds functionalities to the D2SV runtime, is required to register itselfwith the runtime by declaring itself as an extension.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(To declare a D2SV extension component named &quot;someplugin&quot;, add in extension.json)*  
```js
{
  "d2/sdk/models/module.collection": {
     "modules": {
       "someplugin": { //Javascript namespace of the D2 Plugin which is supplying the D2SV component
         "title": "This is a new cool plugin that shows hello world in an iframe",
         "version": "1.0.0"
       }
     }
   }
}
```
