<a name="module_CreateFolderTypeFilter"></a>

## CreateFolderTypeFilter : <code>d2/sdk/commands/create.folder.type.filters</code>
Type filter collection. Defines which Documentum types should be considered while creating a folder.This collection augments the default functionality of `D2-CreateFolder` command.`dm_folder` is the root Documentum type that represents a folder, any sub-type of it can also represent a folder.New types could be introduced by extension

**Extends**: <code>Object</code>  
**Example** *(to register an extension, add in extension.json)*  
```js

{
  "d2/sdk/commands/create.folder.type.filters": {
     "extensions": {
        "mybundle": {
           "mybundle/commands/my.folder.type.filter"
        }
     }
   }
}
```
**Example** *(whereas my.folder.type.filter.js is defined as)*  
```js

define([], function(){
   'use strict';

   return function(typeData) {
     // This is the function that implements a custom filter condition
     return typeData.name === 'my_dctm_custom_folder_type';
   };
});
```

* [CreateFolderTypeFilter](#module_CreateFolderTypeFilter) : <code>d2/sdk/commands/create.folder.type.filters</code>
    * _static_
        * [.addFilters(newFilters)](#module_CreateFolderTypeFilter.addFilters)
        * [.filter(types)](#module_CreateFolderTypeFilter.filter) ⇒ <code>Array.&lt;object&gt;</code>
    * _inner_
        * [~FolderTypeFilterCondition](#module_CreateFolderTypeFilter..FolderTypeFilterCondition) ⇒ <code>boolean</code>

<a name="module_CreateFolderTypeFilter.addFilters"></a>

### CreateFolderTypeFilter.addFilters(newFilters)
To manually register new type filters without using the 'extension' way.

**Kind**: static method of [<code>CreateFolderTypeFilter</code>](#module_CreateFolderTypeFilter)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>newFilters</td><td><code>Array.&lt;FolderTypeFilterCondition&gt;</code></td><td><p>Array of callbacks that returns <code>true</code> or <code>false</code> to include or exclude a type respectively.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CreateFolderTypeFilter.filter"></a>

### CreateFolderTypeFilter.filter(types) ⇒ <code>Array.&lt;object&gt;</code>
To retrieve the list of filtered types that can be used while creating a folder.

**Kind**: static method of [<code>CreateFolderTypeFilter</code>](#module_CreateFolderTypeFilter)  
**Returns**: <code>Array.&lt;object&gt;</code> - The filtered types.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>types</td><td><code>Array.&lt;object&gt;</code></td><td><p>Documentum types.</p>
</td>
    </tr><tr>
    <td>types[].name</td><td><code>string</code></td><td><p>Name of a Documentum type.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CreateFolderTypeFilter..FolderTypeFilterCondition"></a>

### CreateFolderTypeFilter~FolderTypeFilterCondition ⇒ <code>boolean</code>
This is a callback type that implements the condition of a filter.

**Kind**: inner typedef of [<code>CreateFolderTypeFilter</code>](#module_CreateFolderTypeFilter)  
**Returns**: <code>boolean</code> - Should return `true` if the type should be included, `false` otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>typeData</td><td><code>object</code></td><td><p>Data representing a Documentum type</p>
</td>
    </tr><tr>
    <td>typeData.name</td><td><code>string</code></td><td><p>Name of the documentum type</p>
</td>
    </tr>  </tbody>
</table>

