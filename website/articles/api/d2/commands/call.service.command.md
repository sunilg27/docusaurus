<a name="module_CallServiceCommand"></a>

## CallServiceCommand : <code>d2/sdk/commands/call.service.command</code>
Base class for implementing command/menu that invokes a D2FS serive method. D2 Smartview runtime already provides an OOTB bindingfor such a command implementation. However, to suit particular business needs where an OOTB binding does not suffice, a new implementationextending from this module can be used to define such custom bindings.

**Extends**: <code>CommandModel</code>  
**See**

- [CommandModel](./command)
- [NodeActionsRules](./node.actions.rules)

**Example** *(to register binding for a custom implementation, add in extension.json)*  
```js

{
  "d2/sdk/commands/node.actions.rules": {
     "extensions": {
        "mybundle": {
           "mybundle/commands/my.node.actions.rules"
        }
     }
   }
}
```
**Example** *(whereas my.node.actions.rules.js is defined similar to)*  
```js

define([], function(){
   'use strict';

   return [
     {
       command_key: 'D2-CustomCallService',
       decides: function(item) {
         return item.action === 'D2_ACTION_EXECUTE' &&
           item.actionParameters && item.actionParameters.eService === 'PermissionActionService';
       }
       iconName: 'some_icon'
     }
   ];
});
```
**Example** *( to register an implementation with command collection, in extension.json )*  
```js
{
  "d2/sdk/utils/commands": {
     "extensions": {
       "mybundle": {
         "mybundle/commands/my.custom.call.service"
       }
     }
   }
}
```
**Example** *(module mybundle/commands/my.custom.call.service can be defined as)*  
```js

define([
   'nuc/lib/jquery',
   'd2/sdk/commands/call.service.command'
], function($, CallServiceCommandImpl){
 var MyCustomCallService = CallServiceCommandImpl.extend({
    defaults: {
       command_key: 'D2-CustomCallService',
       signature: 'D2-CustomCallService',
       name: 'Custom call service'
    },

    constructor: function MyCustomCallService(attr, options) {
       MyCustomCallService.__super__.constructor.call(this, attr, options);
    }

    beforeServiceCall: function(nodes, commandData) {
       var requestProperties = {};
       //Custom logic to populate
       return requestProperties; //It will be mixed with other default properties as part of request body while sending d2-generic-action-service
    },

    afterServiceCall: function(nodes, commandData, responseProperties) {
       //Custom logic to modify responseProperties
       //Later this modified responseProperties becomes input for final JS action invocation.
    }
 });

 return MyCustomCallService;
});
```

* [CallServiceCommand](#module_CallServiceCommand) : <code>d2/sdk/commands/call.service.command</code>
    * [~beforeServiceCall(nodes, commandData)](#module_CallServiceCommand..beforeServiceCall) ⇒ <code>Object</code>
    * [~afterServiceCall(nodes, commandData, responseProperties)](#module_CallServiceCommand..afterServiceCall) ⇒

<a name="module_CallServiceCommand..beforeServiceCall"></a>

### CallServiceCommand~beforeServiceCall(nodes, commandData) ⇒ <code>Object</code>
Make changes to the request properties sent out for underlying D2-REST API call.

**Kind**: inner method of [<code>CallServiceCommand</code>](#module_CallServiceCommand)  
**Returns**: <code>Object</code> - Returns the final propoerties set to be used as part of request body. Will also be mixed with the default properties.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>nodes</td><td><code>Array.&lt;NodeModel&gt;</code></td><td><p>Selected NodeModel objects upon which the command is being run.</p>
</td>
    </tr><tr>
    <td>commandData</td><td><code>Object</code></td><td><p>Object hash containing the default invocation properties</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CallServiceCommand..afterServiceCall"></a>

### CallServiceCommand~afterServiceCall(nodes, commandData, responseProperties) ⇒
Change invocation data for final JS action

**Kind**: inner method of [<code>CallServiceCommand</code>](#module_CallServiceCommand)  
**Returns**: void  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>nodes</td><td><code>Array.&lt;NodeModel&gt;</code></td><td><p>Selected NodeModel objects upon which the command is being run.</p>
</td>
    </tr><tr>
    <td>commandData</td><td><code>Object</code></td><td><p>Object hash containing the default invocation properties</p>
</td>
    </tr><tr>
    <td>responseProperties</td><td><code>Object</code></td><td><p>Raw response from underlying D2-REST API call. The same object is also used to invoke final JS action.</p>
</td>
    </tr>  </tbody>
</table>

