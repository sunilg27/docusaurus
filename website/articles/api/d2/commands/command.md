<a name="module_CommandModel"></a>

## CommandModel : <code>d2/sdk/commands/command</code>
Interface for classes that implement a business action over one or more [NodeModel](../models/node.model).

**Extends**: [<code>Backbone.Model</code>](https://backbonejs.org/#Model)  
**See**: [ProgressiveNodesComamnd](./progressive.nodes.command)An implementation should be registered with the [CommandCollection](../utils/commands) by an extension configuration.  
**Example** *(module mybundle/commands/my.command.js can be defined as)*  
```js

define([
   'nuc/lib/jquery',
   'd2/sdk/commands/command'
], function($, CommandModel){
 var MyCommandModel = CommandModel.extend({
    defaults: {
       signature: 'MyCommand',
       name: 'Example name',
       scope: 'single' // Indicates that this action could be executed on a selection of atmost one node, other possible value is 'multiple'
    },

    enabled: function(status, options) {
       return true; // enabled always
    },

    execute: function(status, options) {
       var firstNode = status.nodes.get(0);
       console.log("The first node object is: " + firstNode.get('name');
       return $.Deferred().resolve().promise(); // Here this command execution will be considered complete as soon as execute() returns
    }
 });

 return MyCommandModel;
});
```
**Example** *( to register the command with command collection, in extension.json )*  
```js
{
  "d2/sdk/utils/commands": {
     "extensions": {
       "mybundle": {
         "mybundle/commands/my.command"
       }
     }
   }
}
```

* [CommandModel](#module_CommandModel) : <code>d2/sdk/commands/command</code>
    * _instance_
        * [.attributes](#module_CommandModel+attributes) : <code>AttributeType</code>
        * [.getCommandKey()](#module_CommandModel+getCommandKey) ⇒ <code>string</code>
        * [.isNonPromoted()](#module_CommandModel+isNonPromoted) ⇒ <code>boolean</code>
        * [.enabled(status, [options])](#module_CommandModel+enabled) ⇒ <code>boolean</code>
        * *[.execute(status, [options])](#module_CommandModel+execute) ⇒ <code>jQuery.Promise</code>*
    * _static_
        * [.extend(protoProperties, staticProperties)](#module_CommandModel.extend) ⇒ <code>function</code>
    * _inner_
        * [~CommandStatus](#module_CommandModel..CommandStatus) : <code>Object</code>
        * [~AttributeType](#module_CommandModel..AttributeType) : <code>Object</code>

<a name="module_CommandModel+attributes"></a>

### commandModel.attributes : <code>AttributeType</code>
Required attributes. Can be defined using `defaults` hash, checkout the example.

**Kind**: instance property of [<code>CommandModel</code>](#module_CommandModel)  
**See**: [Backbone.Model#attributes](https://backbonejs.org/#Model-attributes) for more details.  
<a name="module_CommandModel+getCommandKey"></a>

### commandModel.getCommandKey() ⇒ <code>string</code>
Get key associated wtih this command instance.

**Kind**: instance method of [<code>CommandModel</code>](#module_CommandModel)  
**Returns**: <code>string</code> - The key/signature of this command.  
<a name="module_CommandModel+isNonPromoted"></a>

### commandModel.isNonPromoted() ⇒ <code>boolean</code>
Whether this command is non-promoted.

**Kind**: instance method of [<code>CommandModel</code>](#module_CommandModel)  
**Returns**: <code>boolean</code> - `true` if non-promoted, `false` otherwise.  
<a name="module_CommandModel+enabled"></a>

### commandModel.enabled(status, [options]) ⇒ <code>boolean</code>
Whether this command should be enabled for a particular toolbar.

**Kind**: instance method of [<code>CommandModel</code>](#module_CommandModel)  
**Returns**: <code>boolean</code> - `true` if the command should be considered as enabled, `false` otherwise  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>status</td><td><code>CommandStatus</code></td><td><p>Object hash containing the contextual data</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>object</code></td><td><p>Additional options that are not part of contextual data like settings</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandModel+execute"></a>

### *commandModel.execute(status, [options]) ⇒ <code>jQuery.Promise</code>*
Business logic that executes as part of user interacting with this command (usually through a toolbar action).A toast message is usually (unless suppressed) shown in the UI when execution completes and based on the state of the returned promise it can either be a success or an error taost.

**Kind**: instance abstract method of [<code>CommandModel</code>](#module_CommandModel)  
**Returns**: <code>jQuery.Promise</code> - The return promise object should be resolved to mark completion of the command execution successfully, whereas a rejection would indicate otherwise  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>status</td><td><code>CommandStatus</code></td><td><p>Object hash containing the contextual data</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>object</code></td><td><p>Additional options that are not part of contextual data like settings</p>
</td>
    </tr>  </tbody>
</table>

**Example** *(Sample implementation of &#x60;execute&#x60; could look like)*  
```js
var MyCommand = CommandModel.extend({
 ...

 execute: function(status, options) {
   var promise = $.Deferred().promise();
   if(status.say === 'hello') {
     console.log('I completed successfully');
     promise.resolve();
   } else {
     console.log('I completed with failure');
     promise.reject();
   }

   return promise;
 }
});
```
<a name="module_CommandModel.extend"></a>

### CommandModel.extend(protoProperties, staticProperties) ⇒ <code>function</code>
To define a derived type from `CommandModel`.

**Kind**: static method of [<code>CommandModel</code>](#module_CommandModel)  
**Returns**: <code>function</code> - The derived type.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>protoProperties</td><td><code>object</code></td><td><p>Properties attached to the prototype of derived type.</p>
</td>
    </tr><tr>
    <td>protoProperties.constructor</td><td><code>function</code></td><td><p>The function to be used to construct instance of the derived type.</p>
</td>
    </tr><tr>
    <td>staticProperties</td><td><code>object</code></td><td><p>Properties statically attached to the derived type.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandModel..CommandStatus"></a>

### CommandModel~CommandStatus : <code>Object</code>
Type of data passed as first argument for any command execution.

**Kind**: inner typedef of [<code>CommandModel</code>](#module_CommandModel)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[suppressSuccessMessage]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Setting it to <code>true</code> suppresses display of a success toast when this command execution completes successfully.</p>
</td>
    </tr><tr>
    <td>[suppressFailMessage]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Setting it to <code>true</code> suppresses display of any error toast even if the command completes with error.</p>
</td>
    </tr><tr>
    <td>nodes</td><td><code>NodeCollection</code></td><td></td><td><p>instance of <a href="../models/node.collection">NodeCollection</a>, on which node object(s) this command is being executed.</p>
</td>
    </tr><tr>
    <td>[context]</td><td><code>Context</code></td><td></td><td><p>Active application context</p>
</td>
    </tr><tr>
    <td>[toolitem]</td><td><code>ToolitemModel</code></td><td></td><td><p>The menu behind this action, if any.</p>
</td>
    </tr><tr>
    <td>[originatingView]</td><td><code>Marionette.View</code></td><td></td><td><p>The associated UI component where this command is being executed.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandModel..AttributeType"></a>

### CommandModel~AttributeType : <code>Object</code>
Attribute types.

**Kind**: inner typedef of [<code>CommandModel</code>](#module_CommandModel)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>signature</td><td><code>string</code></td><td></td><td><p>A unique identifier.</p>
</td>
    </tr><tr>
    <td>name</td><td><code>string</code></td><td></td><td><p>Display name. Usually gets overriden by menu item&#39;s label if this <code>CommandModel</code> is bound to a toolbar menu item.</p>
</td>
    </tr><tr>
    <td>[scope]</td><td><code>string</code></td><td><code>&quot;single&quot;</code></td><td><p>How many <code>NodeModel</code>s this command can execute on simultaneously. Possible values <code>single</code> and <code>multi</code>.</p>
</td>
    </tr>  </tbody>
</table>

