<a name="module_DefaultActionsRules"></a>

## DefaultActionsRules : <code>d2/sdk/commands/default.actions.rules</code>
Default action rules collection. It is an association of command signatures with predicate conditionssuch that when a condition is met, a command implementing the corresponding signature is identified as an action.Collectively these set of rules define what happens when end user clicks on an itemin a list or tabular display of data. Usually an item indicates with a pointer cursor when a defaultaction is matched against the item.NOTE: Actual availability of a default action for an item depends on matching a rule from this default actions rules collectionas well as it depends on actual availability of the command in the item's own action collection.D2SV application runtime pre-defines a bunch of rules that opens a document preview, opens details of a task/wrokflowetc.One or more rules, to define default action for an item type, could be added to this collection by means of extension

**Extends**: <code>Array</code>  
**See**: [RulesMatchingMixin](../../nuc/models/mixins/rules.matching/rules.matching.mixin) for a complete guide on how to write rules.  
**Example** *(to register an extension, add in extension.json)*  
```js

{
  "d2/sdk/commands/default.actions.rules": {
     "extensions": {
        "mybundle": {
           "mybundle/commands/my.default.actions.rules"
        }
     }
   }
}
```
**Example** *(whereas my.default.actions.rules.js is defined as)*  
```js

define([], function(){
   'use strict';

   return [
     {
       signature: 'D2-OpenTask', //`signature` from a CommandModel implementation
       type: constants.Types.TASK
     },
     {
       signature: 'D2-OpenVDocSnapshot',
       decides: function(node) {
         return node.get('r_object_type') === 'd2c_snapshot';
       }
     }
   ];
});
```
