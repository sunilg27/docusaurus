<a name="module_ProgressiveNodesCommand"></a>

## ProgressiveNodesCommand : <code>d2/sdk/commands/progressive.nodes.command</code>
Interface for classes that implements business actions over one or more [NodeModel](../models/node.model) such thatprogress of the action is collectively portrayed on a UI component called ProgressPanel.Normal behavior flow is as follows:1. The progress panel is displayed, and progress gets updated based on the completion of individual node processing.2. In case the processing for any node fails, or if user aborts the processing (either using *Stop* button for all pending items, or using Cancel icon against each item), or if `doneCommand` options are enabled and there are multiple nodes and at least one node is not within the `commandContainer`, then the progress panel will remain visible after all processing is complete. User can expand it to see individual item status.3. In case the processing is successful, then progress panel is removed, and toast message is displayed with success details. If it was a single node and `doneCommand` was applicable, then the toast message will also include done command link.An implementation should be registered with the command collection.

**Extends**: [<code>CommandModel</code>](./command)  
**Example** *(A sample implementation delete.js could look like)*  
```js
define([
   'nuc/lib/jquery',
   'd2/sdk/commands/progressive.nodes.command',
   'i18n!mybundle/commands/commands.lang'
], function($, ProgressiveNodesCommand, lang){
   var DeleteCommand = ProgressiveNodesCommand.extend({
     defaults: {
       signature: 'Delete',
       command_key: 'Delete',
       name: lang.CommandNameDelete,
       pageLeavingWarning: lang.DeletePageLeavingWarning,
       scope: 'multiple'
     },

     preProcess: function (status, options) {
       options || (options = {});
       var me = this,
       deferred = $.Deferred();

       // Do something...

       me._confirmAction(status, options).done(function (deleteOptions) {
         options.deleteOptions = deleteOptions;
         deferred.resolve();
       }).fail(function () {
         deferred.reject();
       });

       return deferred.promise();
    },

    doAction: function (node, options) {
       var deferred = $.Deferred();
       node.destroy({
         wait: true,
         deleteOptions: options.deleteOptions,
         container: options.container
       }).done(function () {
         deferred.resolve(node);
       }).fail(deferred.reject);

       return deferred.promise();
     },

     handleResults: function (results, options) {
       var sourceCollection = options.originatingView && options.originatingView.collection,
       deletedIds = _.compact(_.map(results || [], function (result) {
         if (result instanceof Backbone.Model) {
           return result.get('id');
         }
       }));

       if (deletedIds.length > 0) {
         options.context.trigger('d2:objects:deleted', deletedIds);
       }

       return sourceCollection.fetch({reload: true});
     },

     getMessageOptions: function () {
       return {
         oneFilePending: lang.DeletingOneItem,
         multiFilePending: lang.DeletingManyItems,
         oneFileSuccess: lang.DeleteOneItemSuccessMessage,
         multiFileSuccess: lang.DeleteManyItemsSuccessMessage,
         oneFileFailure: lang.DeleteOneItemFailMessage,
         multiFileOneFailure: lang.DeleteManyItemsOneFailMessage,
         multiFileFailure: lang.DeleteManyItemsFailMessage,
         someItemsStopped: lang.DeleteStopped,
         actionType: 'DELETE'
       };
     },

     // Other methods...
   });
});
```
**Example** *( to register the command with command collection, in extension.json )*  
```js
{
  "d2/sdk/utils/commands": {
     "extensions": {
       "mybundle": {
         "mybundle/commands/delete"
       }
     }
   }
}
```

* [ProgressiveNodesCommand](#module_ProgressiveNodesCommand) : <code>d2/sdk/commands/progressive.nodes.command</code>
    * _instance_
        * *[.preProcess(status, options)](#module_ProgressiveNodesCommand+preProcess) ⇒ <code>PromiseReturnType</code>*
        * *[.doAction(node, options)](#module_ProgressiveNodesCommand+doAction) ⇒ <code>PromiseReturnType</code>*
        * *[.handleResults(results, options)](#module_ProgressiveNodesCommand+handleResults) ⇒ <code>PromiseReturnType</code>*
        * *[.getMessageOptions(options)](#module_ProgressiveNodesCommand+getMessageOptions) ⇒ <code>MessageOptionsType</code>*
    * _static_
        * [.extend(protoProperties, staticProperties)](#module_ProgressiveNodesCommand.extend) ⇒ <code>function</code>
    * _inner_
        * [~DoneCommandOption](#module_ProgressiveNodesCommand..DoneCommandOption) : <code>Object</code>
        * [~CommandStatus](#module_ProgressiveNodesCommand..CommandStatus) : <code>Object</code>
        * [~PromiseReturnType](#module_ProgressiveNodesCommand..PromiseReturnType) : <code>jQuery.Promise</code>
        * [~MessageOptionsType](#module_ProgressiveNodesCommand..MessageOptionsType) : <code>Object</code>

<a name="module_ProgressiveNodesCommand+preProcess"></a>

### *progressiveNodesCommand.preProcess(status, options) ⇒ <code>PromiseReturnType</code>*
Apply a preprocessing logic before starting off the actual business action to any node

**Kind**: instance abstract method of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
**See**: [CommandModel#execute](external:CommandModel#execute)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>status</td><td><code>CommandStatus</code></td><td><p>Object hash containing the contextual data</p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>Additional options that are not part of contextual data like settings</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand+doAction"></a>

### *progressiveNodesCommand.doAction(node, options) ⇒ <code>PromiseReturnType</code>*
Execute the business action on a single `node` from within the `status.nodes` of [preProcess](#module_ProgressiveNodesCommand+preProcess)

**Kind**: instance abstract method of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>node</td><td><code>NodeModel</code></td><td><p>The curent NodeModel being processed</p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>Combined <code>status</code> and <code>options</code> from <a href="#module_ProgressiveNodesCommand+preProcess">preProcess</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand+handleResults"></a>

### *progressiveNodesCommand.handleResults(results, options) ⇒ <code>PromiseReturnType</code>*
Apply post processing

**Kind**: instance abstract method of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>results</td><td><code>Array.&lt;object&gt;</code></td><td><p>Elements of the argument correspond to each input node. The element is of type <code>NodeModel</code> if the command execution for this node was successful.
It is <code>undefined</code> if the command execution was aborted. In case of failure the type depends on whatever type was used to <code>reject()</code> the associated <code>jQuery.Promise</code> from <a href="#module_ProgressiveNodesCommand+doAction">doAction</a></p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>Combined <code>status</code> and <code>options</code> from <a href="#module_ProgressiveNodesCommand+preProcess">preProcess</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand+getMessageOptions"></a>

### *progressiveNodesCommand.getMessageOptions(options) ⇒ <code>MessageOptionsType</code>*
Customize different messages shown in the progress panel UI.

**Kind**: instance abstract method of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Combined <code>status</code> and <code>options</code> from <a href="#module_ProgressiveNodesCommand+preProcess">preProcess</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand.extend"></a>

### ProgressiveNodesCommand.extend(protoProperties, staticProperties) ⇒ <code>function</code>
To define a derived type from `ProgressiveNodesCommand`.

**Kind**: static method of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
**Returns**: <code>function</code> - The derived type.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>protoProperties</td><td><code>object</code></td><td><p>Properties attached to the prototype of derived type.</p>
</td>
    </tr><tr>
    <td>protoProperties.constructor</td><td><code>function</code></td><td><p>The function to be used to construct instance of the derived type.</p>
</td>
    </tr><tr>
    <td>staticProperties</td><td><code>object</code></td><td><p>Properties statically attached to the derived type.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand..DoneCommandOption"></a>

### ProgressiveNodesCommand~DoneCommandOption : <code>Object</code>
**Kind**: inner typedef of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>signature</td><td><code>string</code></td><td><p>Signature of command to execute on click</p>
</td>
    </tr><tr>
    <td>label</td><td><code>string</code></td><td><p>Label to be shown for the link.</p>
</td>
    </tr><tr>
    <td>tooltip</td><td><code>string</code></td><td><p>Tooltip for the link.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand..CommandStatus"></a>

### ProgressiveNodesCommand~CommandStatus : <code>Object</code>
Type of data passed as first argument for any command execution.

**Kind**: inner typedef of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[hideSuccessMessage]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Setting it to <code>true</code> suppresses display of a success toast when this command execution completes successfully.</p>
</td>
    </tr><tr>
    <td>[hideFailureMessage]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Setting it to <code>true</code> suppresses display of any error toast even if the command completes with error.</p>
</td>
    </tr><tr>
    <td>[suppressProgress]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Setting it to <code>true</code> hides the progress panel UI.</p>
</td>
    </tr><tr>
    <td>[doneCommand]</td><td><code>DoneCommandOption</code></td><td></td><td><p>To show additional link on the success toast message upon command completion.</p>
</td>
    </tr><tr>
    <td>[commandContainer]</td><td><code>NodeModel</code></td><td></td><td><p>This must be provided if <code>doneCommand</code> link has to be shown. The link is displayed if given node is not within this container.</p>
</td>
    </tr><tr>
    <td>nodes</td><td><code>NodeCollection</code></td><td></td><td><p>Instance of <a href="../models/node.collection">NodeCollection</a>, on which node object(s) this command is being executed.</p>
</td>
    </tr><tr>
    <td>[context]</td><td><code>Context</code></td><td></td><td><p>Active application context</p>
</td>
    </tr><tr>
    <td>[toolitem]</td><td><code>ToolitemModel</code></td><td></td><td><p>The menu behind this action, if any.</p>
</td>
    </tr><tr>
    <td>[originatingView]</td><td><code>Marionette.View</code></td><td></td><td><p>The associated UI component where this command is being executed.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ProgressiveNodesCommand..PromiseReturnType"></a>

### ProgressiveNodesCommand~PromiseReturnType : <code>jQuery.Promise</code>
Return type. Associated `jQuery.Promise` instance should be `resolve()`-ed or `reject()`-ed to mark either successful completion or failed completion respectively.

**Kind**: inner typedef of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
<a name="module_ProgressiveNodesCommand..MessageOptionsType"></a>

### ProgressiveNodesCommand~MessageOptionsType : <code>Object</code>
Formattable message to be used in progress panel.

**Kind**: inner typedef of [<code>ProgressiveNodesCommand</code>](#module_ProgressiveNodesCommand)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>oneFilePending</td><td><code>string</code></td><td><p>Message to be shown for <code>single</code> scoped command during processing, &#x7B;0&#x7D; is substituted with node name.</p>
</td>
    </tr><tr>
    <td>actionType</td><td><code>string</code></td><td><p>To group several executions of the same command into a single progress panel UI whereever applicable. It is a future scoped option.</p>
</td>
    </tr>  </tbody>
</table>

