<a name="module_NodeActionsRules"></a>

## NodeActionsRules : <code>d2/sdk/commands/node.actions.rules</code>
Action rules collection. Each rule binds a D2SV menu action to a [CommandModel](./command) by mapping through it's signature.So that when the menu action is selected by end user from a menu toolbar, business logic from the mapped `CommandModel` is executed.If a `NodeModel` is found to have a menu action that is not mapped to any `CommandModel`'s signature, the menu action is ignored.D2SV application runtime pre-defines many such mappings for OOTB business actions.One or more rules, to map new actions, could be added to this collection by means of extension.

**Extends**: <code>Array</code>  
**See**: [RulesMatchingMixin](../../nuc/models/mixins/rules.matching/rules.matching.mixin) for a complete guide on how to write rules.  
**Example** *(to register an extension, add in extension.json)*  
```js

{
  "d2/sdk/commands/node.actions.rules": {
     "extensions": {
        "mybundle": {
           "mybundle/commands/my.node.actions.rules"
        }
     }
   }
}
```
**Example** *(whereas my.node.actions.rules.js is defined as)*  
```js

define([], function(){
   'use strict';

   return [
     {
       command_key: 'D2-OpenUrl', //`signature` from a CommandModel implementation
       equals: {action: 'D2_ACTION_OPEN_URL'},
       iconName: 'd2_action_open_url'
     },
     {
       command_key: 'Delete',
       decides: function(item) {
         return item.action === 'D2_ACTION_EXECUTE_MANAGER' &&
           item.actionParameters && item.actionParameters.MANAGER === 'DestroyManager';
       },
       iconName: 'd2_action_delete' // The icon to use when this action is shown in a toolbar with icon.
     }
   ];
});
```
