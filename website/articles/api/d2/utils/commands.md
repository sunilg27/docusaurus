<a name="module_CommandCollection"></a>

## CommandCollection : <code>d2/sdk/utils/commands</code>
CommandCollection. A collection of all the registered commands, used by a toolbar to link executable actions for menus.can also be used to look up and run the executable code in a particular context without a backing toolbar.To implement a buisiness logic as command and get it registered with this collection, see -* [CommandModel](../commands/command)* [ProgressiveNodesCommand](../commands/progressive.nodes.command)

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(A particular comamnd within this colleciton could be looked up, tried and executed as)*  
```js

define([
   'd2/sdk/utils/commands',
   'd2/sdk/models/node.model',
   'd2/sdk/models/node.colletion',
   'd2/sdk/utils/contexts/context.utils',
   'd2/sdk/utils/contexts/factories/connector'
], function(commands, NodeModel, NodeCollection, contextUtil, ConnectorFactory){
   'use strict';

   var node = new NodeModel({
     id: '0b0015fa80005598'
   },{
     connector: contextUtil.getPerspectiveContext().getObject(ConnectorFactory)
   });

   node.fetch().done(function(){
     var status = {
       nodes: [node]
     };

     var myCommand = commands.get('MyCommand');
     if(myCommand && myCommand.enabled(status)) {
       myCommand.execute(status)
         .then(function(){
           console.log('MyCommand completed successfully');
         })
         .catch(function(){
           console.log('MyCommand has failed to execute');
         });
     }
   });

});
```
