<a name="module_Session"></a>

## Session : <code>d2/sdk/utils/session</code>
Session utility. It is a singleton entity in D2SV runtime, used to check state of D2SV session and avail the authenticated connector instanceassociated with the session.Can also return a reference to the application state data holder a.k.a context associated with the D2SV runtime.

**Extends**: [<code>Marionette.Object</code>](https://marionettejs.com/docs/v2.4.7/marionette.object.html)  
**Emits**: [<code>init</code>](#module_Session+event_init), [<code>begin</code>](#module_Session+event_begin), [<code>end</code>](#module_Session+event_end)  

* [Session](#module_Session) : <code>d2/sdk/utils/session</code>
    * _instance_
        * [.isInitialized()](#module_Session+isInitialized) ⇒ <code>Boolean</code>
        * [.isActive()](#module_Session+isActive) ⇒ <code>Boolean</code>
        * [.getContext()](#module_Session+getContext) ⇒ <code>\*</code>
        * ["init"](#module_Session+event_init)
        * ["begin"](#module_Session+event_begin)
        * ["end"](#module_Session+event_end)
    * _static_
        * [.get()](#module_Session.get) ⇒ <code>Session</code>

<a name="module_Session+isInitialized"></a>

### session.isInitialized() ⇒ <code>Boolean</code>
Check if the session is initialized or not. Right after a session object is instantiated, it is put to an un-initialized state.The uninitialized state represents that the session is still setting up context, connector and an appropriate authenticator.

**Kind**: instance method of [<code>Session</code>](#module_Session)  
**Returns**: <code>Boolean</code> - Whether the session is initialized.  
<a name="module_Session+isActive"></a>

### session.isActive() ⇒ <code>Boolean</code>
Checks if the session is active or not. An `initialized` session automatically tries to validate itself against an authenticatedconnection. If the validation goes through, then the session becomes an `active` seesion otherwise it stays `inactive` and waitsfor the end user to login into D2SV.

**Kind**: instance method of [<code>Session</code>](#module_Session)  
**Returns**: <code>Boolean</code> - Whether the session is active  
<a name="module_Session+getContext"></a>

### session.getContext() ⇒ <code>\*</code>
Get a reference to the application data holder associated with this session. The context can be used to create or accessdata created by different data factories.

**Kind**: instance method of [<code>Session</code>](#module_Session)  
**Returns**: <code>\*</code> - Returns the session context.  
**See**: [Context](./contexts/context.utils#Context)  
**Example** *(Code sample to get the connector instance inside any module)*  
```js

define([
 'd2/sdk/utils/session',
 'd2/sdk/utils/contexts/factories/connector',
 'd2/sdk/models/node.model'
], function(Session, ConnectorFactory, NodeModel){
 'use strict';

 var context;

 function createNodeModel(attributes) {
   if(!context) {
     context = Session.get().getContext();
   }
   return new NodeModel(attributes, {connector: context.getObject(ConnectorFactory)});
 }

 return {
   cteateNodeModel: createNodeModel
 };
});
```
<a name="module_Session+event_init"></a>

### "init"
Marks the initialization of a session. The event is emitted when the session has createdits pre-requisite objects and it's ready to check authentication status

**Kind**: event emitted by [<code>Session</code>](#module_Session)  
<a name="module_Session+event_begin"></a>

### "begin"
Marks the beginning of a session. The event is emitted when session reaches validated stateafter fresh login or page reload.

**Kind**: event emitted by [<code>Session</code>](#module_Session)  
<a name="module_Session+event_end"></a>

### "end"
Marks the end of a session. The event is emitted when session couldn't reach a validated stateafter page reload or the underlying connection authneticator has detected logout or session expiry.

**Kind**: event emitted by [<code>Session</code>](#module_Session)  
<a name="module_Session.get"></a>

### Session.get() ⇒ <code>Session</code>
Get a reference to the singleton instance.

**Kind**: static method of [<code>Session</code>](#module_Session)  
**Returns**: <code>Session</code> - Returns the session instance.  
