<a name="module_UriTemplates"></a>

## UriTemplates : <code>d2/sdk/utils/uri.templates</code>
Utility to help with parameter substiution for templatized URIs while turning it into anaccessible URI.

**Example** *(Sample use)*  
```js
define([
 'd2/sdk/utils/rest.linkrels',
 'd2/sdk/utils/rest.resources',
 'd2/sdk/utils/uri.templates'
], function(RestLinkrels, RestResources, UriTemplates){
 'use strict';

 var LINKREL_SEND_MAIL_CFG = 'http://identifiers.emc.com/linkrel/d2-sendmail-config';

 function getMailConfigURL() {
   var templateUrl = RestLinkrels.getLinkTemplate(RestResources.getCurrentRepository().get('links'), LINKREL_SEND_MAIL_CFG);
   if (templateUrl) {
     var params = {
       'id': "default?"
     }, template = new UriTemplates(templateUrl);

     return template.fill(params);
   }
 }

 return {
   getMailConfigURL: getMailConfigURL
 };
});
```

* [UriTemplates](#module_UriTemplates) : <code>d2/sdk/utils/uri.templates</code>
    * _instance_
        * [.fill(templateParams)](#module_UriTemplates+fill) ⇒ <code>String</code>
    * _inner_
        * [~UriTemplates](#module_UriTemplates..UriTemplates)
            * [new UriTemplates(templatizedUri)](#new_module_UriTemplates..UriTemplates_new)

<a name="module_UriTemplates+fill"></a>

### uriTemplates.fill(templateParams) ⇒ <code>String</code>
Substitute template parameters with their values.

**Kind**: instance method of [<code>UriTemplates</code>](#module_UriTemplates)  
**Returns**: <code>String</code> - Returns the accessible URI.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>templateParams</td><td><code>object</code></td><td><p>An object hash containing key-value pairs. All matching keys from the URI is replaced with the key&#39;s corresponding value.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_UriTemplates..UriTemplates"></a>

### UriTemplates~UriTemplates
**Kind**: inner class of [<code>UriTemplates</code>](#module_UriTemplates)  
<a name="new_module_UriTemplates..UriTemplates_new"></a>

#### new UriTemplates(templatizedUri)
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>templatizedUri</td><td><code>string</code></td><td><p>Templatized URI, which will later be turned into accessible URI.</p>
</td>
    </tr>  </tbody>
</table>

