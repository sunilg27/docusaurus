<a name="module_ApplicationScopePerspectives"></a>

## ApplicationScopePerspectives : <code>d2/sdk/utils/perspectives/app.scope.perspectives</code>
Application scope collection. Each navigable area within the D2 Smart View application page is called a perspective.E.g the area shown immediate after a user login is called landing perpsective.An application scope is the immediate next navigable area from the landing perspective. It can be activated by user interacting with a tilein landing perspective or by direct URL. Once activated, depending on how the application scope is defined, one or several view components are going to take overthe screen space and provide a holistic UI with related functional features for the perspective.This collection is a registry of all such scope definitions and could be extended by extension

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**Example** *(to register an extension, add in extension.json)*  
```js
{
  "d2/sdk/utils/perspectives/app.scope.perspectives": {
     "extensions": {
        "mybundle": {
           "mybundle/utils/perspectives/my.app.scopes"
        }
     }
   }
}
```
**Example** *(whereas my.app.scopes.js is defined as)*  
```js
define(['i18n!mybundle/utils/landingpage/impl/nls/lang'], function(lang){
   'use strict';

   function openURLWidgetConfigValidator(widgetConfig, options) {
       var validation = {status: true};
       if (!widgetConfig.widgetParams && !widgetConfig.widgetParams.open_url) {
         validation.status = false; // It will be considered as a validation error
         validation.errorMessage = lang.openUrlWidgetValidationFailed;
       }
       return validation;
   }

   // See 'AppScope' documentation for all possible properties with their usage
   return [
     {applicationScope: 'sample1'}, // widget-less application scope where the perspective descriptor location is inferred
     {applicationScope: 'sample2', perspectiveJson: 'sample2.perspective.json'}, // widget-less application scope where the perspective descriptor location is partially inferred
     {
       // scope that requires widget config and declares explicit location of perspective descriptor
       applicationScope: 'sample3',
       perspectiveJson: 'mybundle/utils/perspectives/sample3.perspective.json',
       widgetType: 'OpenURLWidget',
       validateConfig: openURLWidgetConfigValidator
     }
     // Any other definition
   ];
});
```

* [ApplicationScopePerspectives](#module_ApplicationScopePerspectives) : <code>d2/sdk/utils/perspectives/app.scope.perspectives</code>
    * [~AppScope](#module_ApplicationScopePerspectives..AppScope) : <code>Object</code>
    * [~ConfigValidatorCallback](#module_ApplicationScopePerspectives..ConfigValidatorCallback) ⇒ <code>ConfigValidatorReturnType</code>
    * [~ConfigValidatorReturnType](#module_ApplicationScopePerspectives..ConfigValidatorReturnType) : <code>Object</code>

<a name="module_ApplicationScopePerspectives..AppScope"></a>

### ApplicationScopePerspectives~AppScope : <code>Object</code>
**Kind**: inner typedef of [<code>ApplicationScopePerspectives</code>](#module_ApplicationScopePerspectives)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[sequence]</td><td><code>number</code></td><td><code>1000</code></td><td><p>Priority of this definition. For matching <code>applicationScope</code> property values, the definition with least value for <code>sequence</code> will be used.</p>
</td>
    </tr><tr>
    <td>applicationScope</td><td><code>string</code></td><td></td><td><p>ID of this scope. Also used in forming the URL that links to this application scope directly.</p>
</td>
    </tr><tr>
    <td>[perspectiveJson]</td><td><code>string</code></td><td></td><td><p>Path to the descriptor JSON file for this perspective. It can be specified explicitly as <code>mybundle/utils/perspectives/abc.json</code> or partially as <code>a.perspective.json</code>.
If specified partially then the complete path is resolved as <code>&lt;bundle_src_folder&gt;/utils/perspectives/a.perspective.json</code>. As another alternative, this property can be omitted altogether, in that case
the value of <code>applicationScope</code> property is used to resolve e.g if <code>applicationScope</code> is specified as <code>sample</code> then, <code>perspectiveJson</code> defaults to <code>sample.json</code> and that in-turn gets expanded
into the complete path <code>&lt;bundle_src_folder&gt;/utils/perspectives/sample.json</code>.</p>
</td>
    </tr><tr>
    <td>[defaultTitle]</td><td><code>string</code></td><td></td><td><p>The value to be passed as <code>title</code> to the outermost view component of this perspective. Ignored if this application scope is driven by a widget configuration, in that
case the label/name of the widget config is used as title.</p>
</td>
    </tr><tr>
    <td>[defaultWidgetType]</td><td><code>string</code></td><td></td><td><p>If activating this application scope demands a default widget configuration, then specify the type in this property. This should be specified
only if an application scope requires a widget configuration and allows direct URL navigation. A value from the OOTB default widget types could be used, see <a href="../widget.constants#DEFAULT">DEFAULT</a> or
any new default widget types introduced by a D2 plugin could be used.</p>
</td>
    </tr><tr>
    <td>[widgetType]</td><td><code>string</code></td><td></td><td><p>Type of the D2-Config widget associated with this application scope. Needs to be specified only if this scope requires a widget configuration. If omitted even
when this scope requires a widget configuration, value for this property is deduced from <code>defaultWidgetType</code> property. A value from the OOTB widget types could be used, <a href="../widget.constants#D2">D2</a> or
any valid widget type from D2-Config could be used.</p>
</td>
    </tr><tr>
    <td>[validateConfig]</td><td><code>ConfigValidatorCallback</code></td><td></td><td><p>Callback to validate the resolved config for this scope. Applicable only if this scope has specified one of <code>defaultWidgetType</code> or <code>widgetType</code>.</p>
</td>
    </tr><tr>
    <td>[beforeActivateScope]</td><td><code>callback</code></td><td></td><td><p>A no-argument callback invoked right before this application scope is just about to get activated. The callback should return <code>false</code> if it does not want to
get activated.</p>
</td>
    </tr><tr>
    <td>[afterDeactivateScope]</td><td><code>callback</code></td><td></td><td><p>A no-argument callback invoked if this application scope was enabled earlier and now being swapped out because a different application scope has been activated.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ApplicationScopePerspectives..ConfigValidatorCallback"></a>

### ApplicationScopePerspectives~ConfigValidatorCallback ⇒ <code>ConfigValidatorReturnType</code>
Callback function that validates widget configuration for an application scope

**Kind**: inner typedef of [<code>ApplicationScopePerspectives</code>](#module_ApplicationScopePerspectives)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>widgetConfig</td><td><code>Object</code></td><td><p>The widget configuration being validated</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>Object</code></td><td><p>Any additional context specific data that is not part of <code>widgetConfig</code> but might be relevant for validation</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ApplicationScopePerspectives..ConfigValidatorReturnType"></a>

### ApplicationScopePerspectives~ConfigValidatorReturnType : <code>Object</code>
**Kind**: inner typedef of [<code>ApplicationScopePerspectives</code>](#module_ApplicationScopePerspectives)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>status</td><td><code>boolean</code></td><td><p><code>true</code> indicates validation succeeded, <code>false</code> otherwise.</p>
</td>
    </tr><tr>
    <td>[errorMessage]</td><td><code>string</code></td><td><p>Error message to be shown in UI if the validation failed.</p>
</td>
    </tr>  </tbody>
</table>

