<a name="module_ContextUtil"></a>

## ContextUtil : <code>d2/sdk/utils/contexts/context.utils</code>
Utility to access `Context` object(s) associated with runtime.

**Extends**: <code>Object</code>  
**Todo**

- Examples regular & unit test.


* [ContextUtil](#module_ContextUtil) : <code>d2/sdk/utils/contexts/context.utils</code>
    * _static_
        * [.createPageContext()](#module_ContextUtil.createPageContext) ⇒ <code>PageContext</code>
        * [.getPerspectiveContext()](#module_ContextUtil.getPerspectiveContext) ⇒ <code>PerspectiveContext</code>
    * _inner_
        * [~PageContext](#module_ContextUtil..PageContext) ⇐ <code>Context</code>
        * [~PerspectiveContext](#module_ContextUtil..PerspectiveContext) ⇐ <code>Context</code>
        * [~Context](#module_ContextUtil..Context) ⇐ <code>IContext</code>

<a name="module_ContextUtil.createPageContext"></a>

### ContextUtil.createPageContext() ⇒ <code>PageContext</code>
Create and return a new `PageContext`. Useful while writing unit tests, where settin-up a `PerspectiveContext` is superflous.

**Kind**: static method of [<code>ContextUtil</code>](#module_ContextUtil)  
<a name="module_ContextUtil.getPerspectiveContext"></a>

### ContextUtil.getPerspectiveContext() ⇒ <code>PerspectiveContext</code>
Get the `PerspectiveContext` instance associated with runtime.

**Kind**: static method of [<code>ContextUtil</code>](#module_ContextUtil)  
<a name="module_ContextUtil..PageContext"></a>

### ContextUtil~PageContext ⇐ <code>Context</code>
**Kind**: inner interface of [<code>ContextUtil</code>](#module_ContextUtil)  
**Extends**: <code>Context</code>  
<a name="module_ContextUtil..PerspectiveContext"></a>

### ContextUtil~PerspectiveContext ⇐ <code>Context</code>
**Kind**: inner interface of [<code>ContextUtil</code>](#module_ContextUtil)  
**Extends**: <code>Context</code>  
<a name="module_ContextUtil..Context"></a>

### ContextUtil~Context ⇐ <code>IContext</code>
**Kind**: inner interface of [<code>ContextUtil</code>](#module_ContextUtil)  
**Extends**: <code>IContext</code>  
