<a name="module_BaseFactory"></a>

## BaseFactory : <code>d2/sdk/utils/contexts/factories/factory</code>
Interface for classes that want to implement a factory to create a `Backbone.Model` or `Backbone.Collection` or just a plain `Object`.The created model/collection/object is attached to the applicable `context` and all such instances are fetched when the `context` is fetched.

**Extends**: <code>Object</code>  
**Example** *(To create a collection factory)*  
```js

define([
  'module',
  'nuc/lib/underscore',
  'nuc/lib/backbone',
  'd2/sdk/utils/contexts/factories/factory',
  'd2/sdk/utils/contexts/factories/connector',
  'd2/sdk/utils/commands',
  'mybundle/models/sample.collection'
  ], function (module, _, Backbone, CollectionFactory, ConnectorFactory,
   commands, SampleCollection) {
     'use strict';

     var SampleCollectionFactory = CollectionFactory.extend({

         propertyPrefix: 'sampleCollection',

         constructor: function SampleCollectionFactory(context, options) {
           CollectionFactory.prototype.constructor.apply(this, arguments);

           var collection = this.options.widget || this.options.sampleCollection || {}; //It is customary to use the same name as propertyPrefix
           if (!(collection instanceof Backbone.Collection)) {
             var connector = context.getObject(ConnectorFactory, options),
               config = module.config();

             collection = new SampleCollection(collection.models, _.extend(
             { connector: connector },
             collection.options, config.options,
             SampleCollectionFactory.getDefaultResourceScope(),
             { autoreset: true }));
          }

          this.property = collection;
        },

        fetch: function (options) {
           return this.property.fetch(options);
        }

     },
     {

         getDefaultResourceScope: function () {
           return _.deepClone({
             commands: commands.getAllSignatures()
           });
         }

     });

     return SampleCollectionFactory;

});
```
**Example** *(Whereas the sample collection could be simply defined as)*  
```js

define([
  'nuc/lib/backbone',
  'd2/sdk/models/mixins/collection.resource.mixin'
], function(Backbone, CollectionResourceMixin){
 'use strict';

 var SampleCollection = Backbone.Collection.extend({
   constructor: function SampleCollection(models, options) {
     options = options || {};
     this.options = options;

     SampleCollection.__super__.constructor.call(this, models, options);

     this.makeCollectionResource(options);
   }
 });

 CollectionResourceMixin.mixin(SampleCollection.prototype);

 return SampleCollection;
});
```

* [BaseFactory](#module_BaseFactory) : <code>d2/sdk/utils/contexts/factories/factory</code>
    * *[.propertyPrefix](#module_BaseFactory+propertyPrefix) : <code>string</code>*
    * *[.isFetchable()](#module_BaseFactory+isFetchable) ⇒ <code>boolean</code>*
    * *[.fetch()](#module_BaseFactory+fetch) ⇒ <code>void</code>*

<a name="module_BaseFactory+propertyPrefix"></a>

### *baseFactory.propertyPrefix : <code>string</code>*
Name against which the created model/collection/object is to be saved.

**Kind**: instance abstract property of [<code>BaseFactory</code>](#module_BaseFactory)  
<a name="module_BaseFactory+isFetchable"></a>

### *baseFactory.isFetchable() ⇒ <code>boolean</code>*
Whether this factory instance is fetchable or not.

**Kind**: instance abstract method of [<code>BaseFactory</code>](#module_BaseFactory)  
**Returns**: <code>boolean</code> - should return `true` when the model associated with this factory is fetchable, `false` otherwise.  
<a name="module_BaseFactory+fetch"></a>

### *baseFactory.fetch() ⇒ <code>void</code>*
Should delegate this fetch to actually fetch the model/collection/object created by this factory.

**Kind**: instance abstract method of [<code>BaseFactory</code>](#module_BaseFactory)  
