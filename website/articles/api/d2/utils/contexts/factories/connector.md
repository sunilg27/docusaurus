<a name="module_ConnectorFactory"></a>

## ConnectorFactory : <code>d2/sdk/utils/contexts/factories/connector</code>
Factory to create/access a `connector` object. A `connector` object acts as anauthenticated transport for connecting to D2-REST resources through AJAX apis.D2 Smartview runtime uses a single connector object created through this factory.Such a `connector` can be used standalone with `jQuery` AJAX apis, or it can be set as thetransport for a `Backbone.Model` or `Backbone.Collection` instance.

**Extends**: [<code>BaseFactory</code>](./factory)  
**Example** *(To get the active connector instance in any module)*  
```js
define([
 'd2/utils/contexts/context.utils',
 'd2/utils/contexts/factories/connector'
], function(contextUtils, ConnectorFactory){
 'use strict';

 var context = contextUtils.getPerspectiveContext(),
   connector = context.getObject(ConnectorFactory);

 // Do whatever with the connector instance
});
```
