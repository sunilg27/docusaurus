<a name="module_RestLinkrels"></a>

## RestLinkrels : <code>d2/sdk/utils/rest.linkrels</code>
Utility to handle link relations of a D2-REST resource.Also defined named constants for several link relation identifiers.

**Extends**: <code>Object</code>  

* [RestLinkrels](#module_RestLinkrels) : <code>d2/sdk/utils/rest.linkrels</code>
    * [.ACTIONS](#module_RestLinkrels.ACTIONS) : <code>string</code>
    * [.BATCHES](#module_RestLinkrels.BATCHES) : <code>string</code>
    * [.CABINETS](#module_RestLinkrels.CABINETS) : <code>string</code>
    * [.FOLDERS](#module_RestLinkrels.FOLDERS) : <code>string</code>
    * [.USERS](#module_RestLinkrels.USERS) : <code>string</code>
    * [.GROUPS](#module_RestLinkrels.GROUPS) : <code>string</code>
    * [.USER_PROFILE_IMAGE](#module_RestLinkrels.USER_PROFILE_IMAGE) : <code>string</code>
    * [.CANCEL_CHECKOUT](#module_RestLinkrels.CANCEL_CHECKOUT) : <code>string</code>
    * [.CANONICAL](#module_RestLinkrels.CANONICAL) : <code>string</code>
    * [.CHECKOUT](#module_RestLinkrels.CHECKOUT) : <code>string</code>
    * [.CHECKOUT_PRECHECK](#module_RestLinkrels.CHECKOUT_PRECHECK) : <code>string</code>
    * [.CHECKEDOUT_OBJECTS](#module_RestLinkrels.CHECKEDOUT_OBJECTS) : <code>string</code>
    * [.CHILD_LINKS](#module_RestLinkrels.CHILD_LINKS) : <code>string</code>
    * [.COMMENTS](#module_RestLinkrels.COMMENTS) : <code>string</code>
    * [.CURRENT_VERSION](#module_RestLinkrels.CURRENT_VERSION) : <code>string</code>
    * [.CURRENT_USER](#module_RestLinkrels.CURRENT_USER) : <code>string</code>
    * [.D2_CONFIGURATIONS](#module_RestLinkrels.D2_CONFIGURATIONS) : <code>string</code>
    * [.D2_RELATION_TYPES](#module_RestLinkrels.D2_RELATION_TYPES) : <code>string</code>
    * [.CONVERT_TO_VIRTUAL_DOCUMENT](#module_RestLinkrels.CONVERT_TO_VIRTUAL_DOCUMENT) : <code>string</code>
    * [.ADD_VIRTUAL_DOCUMENT_NODES](#module_RestLinkrels.ADD_VIRTUAL_DOCUMENT_NODES) : <code>string</code>
    * [.D2_VD_INSERT_INHERITED_COMPONENT](#module_RestLinkrels.D2_VD_INSERT_INHERITED_COMPONENT) : <code>string</code>
    * [.D2_VD_TEMPLATES](#module_RestLinkrels.D2_VD_TEMPLATES) : <code>string</code>
    * [.FOLDER_STRUCTURE_CONVERSION_CONFIG](#module_RestLinkrels.FOLDER_STRUCTURE_CONVERSION_CONFIG) : <code>string</code>
    * [.REPLACE_VIRTUAL_DOCUMENT_NODES](#module_RestLinkrels.REPLACE_VIRTUAL_DOCUMENT_NODES) : <code>string</code>
    * [.DEFAULT_FOLDER](#module_RestLinkrels.DEFAULT_FOLDER) : <code>string</code>
    * [.DELETE](#module_RestLinkrels.DELETE) : <code>string</code>
    * [.DOWNLOAD_URLS](#module_RestLinkrels.DOWNLOAD_URLS) : <code>string</code>
    * [.DQL](#module_RestLinkrels.DQL) : <code>string</code>
    * [.D2_FAVORITES](#module_RestLinkrels.D2_FAVORITES) : <code>string</code>
    * [.D2_USER_RECENTLY_ACCESSED](#module_RestLinkrels.D2_USER_RECENTLY_ACCESSED) : <code>string</code>
    * [.D2_RELATIONS](#module_RestLinkrels.D2_RELATIONS) : <code>string</code>
    * [.D2_OBJECTS_RELATIONS_COUNT](#module_RestLinkrels.D2_OBJECTS_RELATIONS_COUNT) : <code>string</code>
    * [.D2_RENDITIONS](#module_RestLinkrels.D2_RENDITIONS) : <code>string</code>
    * [.D2_WIDGETS](#module_RestLinkrels.D2_WIDGETS) : <code>string</code>
    * [.D2_SAVED_SEARCH](#module_RestLinkrels.D2_SAVED_SEARCH) : <code>string</code>
    * [.D2_DISPLAY_TYPES](#module_RestLinkrels.D2_DISPLAY_TYPES) : <code>string</code>
    * [.D2_THEMES](#module_RestLinkrels.D2_THEMES) : <code>string</code>
    * [.D2_DEFAULT_THEME](#module_RestLinkrels.D2_DEFAULT_THEME) : <code>string</code>
    * [.D2_OPTIONS](#module_RestLinkrels.D2_OPTIONS) : <code>string</code>
    * [.D2_THUMBNAIL](#module_RestLinkrels.D2_THUMBNAIL) : <code>string</code>
    * [.D2_CREATION_PROFILES](#module_RestLinkrels.D2_CREATION_PROFILES) : <code>string</code>
    * [.D2_CREATION_PROFILE](#module_RestLinkrels.D2_CREATION_PROFILE) : <code>string</code>
    * [.D2_CREATION_PROFILE_VALUE_CONFIGS](#module_RestLinkrels.D2_CREATION_PROFILE_VALUE_CONFIGS) : <code>string</code>
    * [.D2_CREATION_PROFILE_VALUE_CONFIG](#module_RestLinkrels.D2_CREATION_PROFILE_VALUE_CONFIG) : <code>string</code>
    * [.D2_CREATION_PRESET_PROFILES](#module_RestLinkrels.D2_CREATION_PRESET_PROFILES) : <code>string</code>
    * [.D2_CREATION_PRESET_VALUE_CONFIGS](#module_RestLinkrels.D2_CREATION_PRESET_VALUE_CONFIGS) : <code>string</code>
    * [.D2_CREATION_PRESET_RECENTLY_USED](#module_RestLinkrels.D2_CREATION_PRESET_RECENTLY_USED) : <code>string</code>
    * [.D2_CREATION_PROPERTIES](#module_RestLinkrels.D2_CREATION_PROPERTIES) : <code>string</code>
    * [.D2_OBJECT_CREATION](#module_RestLinkrels.D2_OBJECT_CREATION) : <code>string</code>
    * [.D2_CONTENT_UPLOAD_URLS](#module_RestLinkrels.D2_CONTENT_UPLOAD_URLS) : <code>string</code>
    * [.D2_DOCUMENT_TEMPLATES](#module_RestLinkrels.D2_DOCUMENT_TEMPLATES) : <code>string</code>
    * [.D2_DOCUMENT_TEMPLATE](#module_RestLinkrels.D2_DOCUMENT_TEMPLATE) : <code>string</code>
    * [.D2_RECENTLY_USED_TEMPLATES](#module_RestLinkrels.D2_RECENTLY_USED_TEMPLATES) : <code>string</code>
    * [.D2_LINKED_DOCUMENT_CONFIG](#module_RestLinkrels.D2_LINKED_DOCUMENT_CONFIG) : <code>string</code>
    * [.D2_CHECKIN_CONFIG](#module_RestLinkrels.D2_CHECKIN_CONFIG) : <code>string</code>
    * [.D2_CHECKIN_URL](#module_RestLinkrels.D2_CHECKIN_URL) : <code>string</code>
    * [.D2_VD_SILENT_CHECKIN](#module_RestLinkrels.D2_VD_SILENT_CHECKIN) : <code>string</code>
    * [.D2_FOLDER_CREATION_PROPERTY_PAGE](#module_RestLinkrels.D2_FOLDER_CREATION_PROPERTY_PAGE) : <code>string</code>
    * [.D2_FOLDERS](#module_RestLinkrels.D2_FOLDERS) : <code>string</code>
    * [.D2_CABINETS](#module_RestLinkrels.D2_CABINETS) : <code>string</code>
    * [.D2_EXTRACT_O2_PROPERTIES_URLS](#module_RestLinkrels.D2_EXTRACT_O2_PROPERTIES_URLS) : <code>string</code>
    * [.D2_EXTRACT_EMAIL_ATTACHMENTS](#module_RestLinkrels.D2_EXTRACT_EMAIL_ATTACHMENTS) : <code>string</code>
    * [.D2_SILENT_LINKED_DOCUMENT_CREATION](#module_RestLinkrels.D2_SILENT_LINKED_DOCUMENT_CREATION) : <code>string</code>
    * [.D2_QUERYFORM_DIALOG](#module_RestLinkrels.D2_QUERYFORM_DIALOG) : <code>string</code>
    * [.D2_SET_TEMPLATE](#module_RestLinkrels.D2_SET_TEMPLATE) : <code>string</code>
    * [.D2_FOLDER_STRUCTURE_CONFIGS](#module_RestLinkrels.D2_FOLDER_STRUCTURE_CONFIGS) : <code>string</code>
    * [.D2_FOLDER_STRUCTURE_IMPORT_URLS](#module_RestLinkrels.D2_FOLDER_STRUCTURE_IMPORT_URLS) : <code>string</code>
    * [.D2_FETCH_VERSION_VD](#module_RestLinkrels.D2_FETCH_VERSION_VD) : <code>string</code>
    * [.D2_SET_BINDING_VD](#module_RestLinkrels.D2_SET_BINDING_VD) : <code>string</code>
    * [.D2_CONVERT_TO_SIMPLE_DOC](#module_RestLinkrels.D2_CONVERT_TO_SIMPLE_DOC) : <code>string</code>
    * [.D2_REMOVE_VD_CHILDREN](#module_RestLinkrels.D2_REMOVE_VD_CHILDREN) : <code>string</code>
    * [.D2_PREVIEW_FORMATS](#module_RestLinkrels.D2_PREVIEW_FORMATS) : <code>string</code>
    * [.D2_INBOX](#module_RestLinkrels.D2_INBOX) : <code>string</code>
    * [.D2_TASK_CATEGORIES](#module_RestLinkrels.D2_TASK_CATEGORIES) : <code>string</code>
    * [.D2_TASK_SUPPORTING_DOCUMENTS](#module_RestLinkrels.D2_TASK_SUPPORTING_DOCUMENTS) : <code>string</code>
    * [.D2_TASK_WORKING_DOCUMENTS](#module_RestLinkrels.D2_TASK_WORKING_DOCUMENTS) : <code>string</code>
    * [.D2_NOTES](#module_RestLinkrels.D2_NOTES) : <code>string</code>
    * [.D2_NOTES_COUNT](#module_RestLinkrels.D2_NOTES_COUNT) : <code>string</code>
    * [.D2_TASK_STATUS](#module_RestLinkrels.D2_TASK_STATUS) : <code>string</code>
    * [.D2_TASK_DELEGATE](#module_RestLinkrels.D2_TASK_DELEGATE) : <code>string</code>
    * [.D2_TASK_DELEGATE_USER](#module_RestLinkrels.D2_TASK_DELEGATE_USER) : <code>string</code>
    * [.D2_TASK_PERFORMERS](#module_RestLinkrels.D2_TASK_PERFORMERS) : <code>string</code>
    * [.D2_TASK_PERFORMERS_DIALOG](#module_RestLinkrels.D2_TASK_PERFORMERS_DIALOG) : <code>string</code>
    * [.D2_TASK_PERFORMERS_ALIAS_PARTICIPANTS](#module_RestLinkrels.D2_TASK_PERFORMERS_ALIAS_PARTICIPANTS) : <code>string</code>
    * [.D2_TASK_INTENTION_DICTIONARY_FORWARD](#module_RestLinkrels.D2_TASK_INTENTION_DICTIONARY_FORWARD) : <code>string</code>
    * [.D2_TASK_INTENTION_DICTIONARY_REJECT](#module_RestLinkrels.D2_TASK_INTENTION_DICTIONARY_REJECT) : <code>string</code>
    * [.D2_TASK_PROPERTY_PAGE_FORWARD](#module_RestLinkrels.D2_TASK_PROPERTY_PAGE_FORWARD) : <code>string</code>
    * [.D2_TASK_PROPERTY_PAGE_REJECT](#module_RestLinkrels.D2_TASK_PROPERTY_PAGE_REJECT) : <code>string</code>
    * [.D2_WORKFLOW_STATUS](#module_RestLinkrels.D2_WORKFLOW_STATUS) : <code>string</code>
    * [.D2_TASK_LIFECYCLE_CHECKER](#module_RestLinkrels.D2_TASK_LIFECYCLE_CHECKER) : <code>string</code>
    * [.D2_TASK_CHECK_LOCK_STATE](#module_RestLinkrels.D2_TASK_CHECK_LOCK_STATE) : <code>string</code>
    * [.D2_WORKFLOWS](#module_RestLinkrels.D2_WORKFLOWS) : <code>string</code>
    * [.D2_WORKFLOW_CONFIG](#module_RestLinkrels.D2_WORKFLOW_CONFIG) : <code>string</code>
    * [.D2_WORKFLOW_ENTRY_CONDITION_CHECKER](#module_RestLinkrels.D2_WORKFLOW_ENTRY_CONDITION_CHECKER) : <code>string</code>
    * [.D2_WORKFLOW_PACKAGETYPE_CHECKER](#module_RestLinkrels.D2_WORKFLOW_PACKAGETYPE_CHECKER) : <code>string</code>
    * [.D2_WORKFLOW_LAUNCH_PEROFMERS_DIALOG](#module_RestLinkrels.D2_WORKFLOW_LAUNCH_PEROFMERS_DIALOG) : <code>string</code>
    * [.D2_MY_WORKFLOWS](#module_RestLinkrels.D2_MY_WORKFLOWS) : <code>string</code>
    * [.D2_ALL_WORKFLOWS](#module_RestLinkrels.D2_ALL_WORKFLOWS) : <code>string</code>
    * [.D2_MY_WORKFLOWS_STATUS_SUMMARY](#module_RestLinkrels.D2_MY_WORKFLOWS_STATUS_SUMMARY) : <code>string</code>
    * [.D2_WORKFLOWS_TEMPLATES](#module_RestLinkrels.D2_WORKFLOWS_TEMPLATES) : <code>string</code>
    * [.D2_WORKFLOW_OVERVIEW_WIDGET_USERS](#module_RestLinkrels.D2_WORKFLOW_OVERVIEW_WIDGET_USERS) : <code>string</code>
    * [.D2_CHANGE_WORKFLOW_SUPERVISOR_USERS](#module_RestLinkrels.D2_CHANGE_WORKFLOW_SUPERVISOR_USERS) : <code>string</code>
    * [.D2_WORKFLOW_NOTES_CONFIG](#module_RestLinkrels.D2_WORKFLOW_NOTES_CONFIG) : <code>string</code>
    * [.D2_WORKFLOW_TASK_AUDITS](#module_RestLinkrels.D2_WORKFLOW_TASK_AUDITS) : <code>string</code>
    * [.D2_WORKFLOW_EVENT_AUDITS](#module_RestLinkrels.D2_WORKFLOW_EVENT_AUDITS) : <code>string</code>
    * [.D2_WORKFLOW_TASKS](#module_RestLinkrels.D2_WORKFLOW_TASKS) : <code>string</code>
    * [.D2_WORKFLOWS_COUNT](#module_RestLinkrels.D2_WORKFLOWS_COUNT) : <code>string</code>
    * [.EDIT](#module_RestLinkrels.EDIT) : <code>string</code>
    * [.EXPORT_URLS](#module_RestLinkrels.EXPORT_URLS) : <code>string</code>
    * [.FAVORITES](#module_RestLinkrels.FAVORITES) : <code>string</code>
    * [.FORMATS](#module_RestLinkrels.FORMATS) : <code>string</code>
    * [.OBJECTS](#module_RestLinkrels.OBJECTS) : <code>string</code>
    * [.OBJECTS_LOCATIONS](#module_RestLinkrels.OBJECTS_LOCATIONS) : <code>string</code>
    * [.OBJECTS_FACETS](#module_RestLinkrels.OBJECTS_FACETS) : <code>string</code>
    * [.DQL_DOCLIST_OBJECTS](#module_RestLinkrels.DQL_DOCLIST_OBJECTS) : <code>string</code>
    * [.OBJECTS_THUMBNAIL_URL](#module_RestLinkrels.OBJECTS_THUMBNAIL_URL) : <code>string</code>
    * [.PARENT](#module_RestLinkrels.PARENT) : <code>string</code>
    * [.PARENT_LINKS](#module_RestLinkrels.PARENT_LINKS) : <code>string</code>
    * [.D2_PREFERENCES](#module_RestLinkrels.D2_PREFERENCES) : <code>string</code>
    * [.D2_COLUMN_PREFERENCES](#module_RestLinkrels.D2_COLUMN_PREFERENCES) : <code>string</code>
    * [.PREVIEW_URLS](#module_RestLinkrels.PREVIEW_URLS) : <code>string</code>
    * [.PRINT_URL](#module_RestLinkrels.PRINT_URL) : <code>string</code>
    * [.PROPERTY_PAGE](#module_RestLinkrels.PROPERTY_PAGE) : <code>string</code>
    * [.QUICK_SEARCH](#module_RestLinkrels.QUICK_SEARCH) : <code>string</code>
    * [.REPOSITORIES](#module_RestLinkrels.REPOSITORIES) : <code>string</code>
    * [.SELF](#module_RestLinkrels.SELF) : <code>string</code>
    * [.SEND_MAIL](#module_RestLinkrels.SEND_MAIL) : <code>string</code>
    * [.VERSION_HISTORY](#module_RestLinkrels.VERSION_HISTORY) : <code>string</code>
    * [.VD_NODE_CHILDREN](#module_RestLinkrels.VD_NODE_CHILDREN) : <code>string</code>
    * [.VD_SNAPSHOT_NODE_CHILDREN](#module_RestLinkrels.VD_SNAPSHOT_NODE_CHILDREN) : <code>string</code>
    * [.VD_MOVE_NODES](#module_RestLinkrels.VD_MOVE_NODES) : <code>string</code>
    * [.FINISH_ACTION](#module_RestLinkrels.FINISH_ACTION) : <code>string</code>
    * [.ABORT_ACTION](#module_RestLinkrels.ABORT_ACTION) : <code>string</code>
    * [.ASYNC_OPERATION](#module_RestLinkrels.ASYNC_OPERATION) : <code>string</code>
    * [.D2_DUMP](#module_RestLinkrels.D2_DUMP) : <code>string</code>
    * [.D2_SIGNOFF_VALIDATION](#module_RestLinkrels.D2_SIGNOFF_VALIDATION) : <code>string</code>
    * [.D2_SYSOBJECTS](#module_RestLinkrels.D2_SYSOBJECTS) : <code>string</code>
    * [.getLink(links, linkrel)](#module_RestLinkrels.getLink) ⇒ <code>string</code>
    * [.getLinkTemplate(links, linkrel)](#module_RestLinkrels.getLinkTemplate) ⇒ <code>string</code>

<a name="module_RestLinkrels.ACTIONS"></a>

### RestLinkrels.ACTIONS : <code>string</code>
Link ID of D2-REST API to fetch actions.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.BATCHES"></a>

### RestLinkrels.BATCHES : <code>string</code>
Link ID of D2-REST API to execute batch operations.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CABINETS"></a>

### RestLinkrels.CABINETS : <code>string</code>
Link ID of D2-REST API to fetch cabinets.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.FOLDERS"></a>

### RestLinkrels.FOLDERS : <code>string</code>
Link ID of D2-REST API to fetch folder of cabinet/folder.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.USERS"></a>

### RestLinkrels.USERS : <code>string</code>
Link ID of D2-REST API to fetch users.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.GROUPS"></a>

### RestLinkrels.GROUPS : <code>string</code>
Link ID of D2-REST API to fetch groups.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.USER_PROFILE_IMAGE"></a>

### RestLinkrels.USER\_PROFILE\_IMAGE : <code>string</code>
Link ID of D2-REST API to fetch user's profile image.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CANCEL_CHECKOUT"></a>

### RestLinkrels.CANCEL\_CHECKOUT : <code>string</code>
Link ID of D2-REST API to cancel checkout of reserved object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CANONICAL"></a>

### RestLinkrels.CANONICAL : <code>string</code>
Link ID of D2-REST API to fetch canonical object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CHECKOUT"></a>

### RestLinkrels.CHECKOUT : <code>string</code>
Link ID of D2-REST API to checkout object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CHECKOUT_PRECHECK"></a>

### RestLinkrels.CHECKOUT\_PRECHECK : <code>string</code>
Link ID of D2-REST API to check if object can be reserved.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CHECKEDOUT_OBJECTS"></a>

### RestLinkrels.CHECKEDOUT\_OBJECTS : <code>string</code>
Link ID of D2-REST API to fetch objects checked-out by user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CHILD_LINKS"></a>

### RestLinkrels.CHILD\_LINKS : <code>string</code>
Link ID of D2-REST API to get child links of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.COMMENTS"></a>

### RestLinkrels.COMMENTS : <code>string</code>
Link ID of D2-REST API to get comments of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CURRENT_VERSION"></a>

### RestLinkrels.CURRENT\_VERSION : <code>string</code>
Link ID of D2-REST API to get current version of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CURRENT_USER"></a>

### RestLinkrels.CURRENT\_USER : <code>string</code>
Link ID of D2-REST API to get logged-in user's details.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CONFIGURATIONS"></a>

### RestLinkrels.D2\_CONFIGURATIONS : <code>string</code>
Link ID of D2-REST API to get conguration objects avilable to logged in user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_RELATION_TYPES"></a>

### RestLinkrels.D2\_RELATION\_TYPES : <code>string</code>
Link ID of D2-REST API to get possible relation types.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.CONVERT_TO_VIRTUAL_DOCUMENT"></a>

### RestLinkrels.CONVERT\_TO\_VIRTUAL\_DOCUMENT : <code>string</code>
Link ID of D2-REST API to convert a document to virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.ADD_VIRTUAL_DOCUMENT_NODES"></a>

### RestLinkrels.ADD\_VIRTUAL\_DOCUMENT\_NODES : <code>string</code>
Link ID of D2-REST API to add children to a virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_VD_INSERT_INHERITED_COMPONENT"></a>

### RestLinkrels.D2\_VD\_INSERT\_INHERITED\_COMPONENT : <code>string</code>
Link ID of D2-REST API to add children to a virtual document by inheriting from another virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_VD_TEMPLATES"></a>

### RestLinkrels.D2\_VD\_TEMPLATES : <code>string</code>
Link ID of D2-REST API to get virtual document creation templates.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.FOLDER_STRUCTURE_CONVERSION_CONFIG"></a>

### RestLinkrels.FOLDER\_STRUCTURE\_CONVERSION\_CONFIG : <code>string</code>
Link ID of D2-REST API to get configuration for converting folder structure to virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.REPLACE_VIRTUAL_DOCUMENT_NODES"></a>

### RestLinkrels.REPLACE\_VIRTUAL\_DOCUMENT\_NODES : <code>string</code>
Link ID of D2-REST API to replace a child of a virtual document with other objects.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.DEFAULT_FOLDER"></a>

### RestLinkrels.DEFAULT\_FOLDER : <code>string</code>
Link ID of D2-REST API to get user's default folder.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.DELETE"></a>

### RestLinkrels.DELETE : <code>string</code>
Link ID of D2-REST API to delete object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.DOWNLOAD_URLS"></a>

### RestLinkrels.DOWNLOAD\_URLS : <code>string</code>
Link ID of D2-REST API to get content download URLs of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.DQL"></a>

### RestLinkrels.DQL : <code>string</code>
Link ID of D2-REST API to execute a DQL query.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_FAVORITES"></a>

### RestLinkrels.D2\_FAVORITES : <code>string</code>
Link ID of D2-REST API to get favorited(subscribed) objects of user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_USER_RECENTLY_ACCESSED"></a>

### RestLinkrels.D2\_USER\_RECENTLY\_ACCESSED : <code>string</code>
Link ID of D2-REST API to get list of recently accessed objects of user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_RELATIONS"></a>

### RestLinkrels.D2\_RELATIONS : <code>string</code>
Link ID of D2-REST API to get related objects of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_OBJECTS_RELATIONS_COUNT"></a>

### RestLinkrels.D2\_OBJECTS\_RELATIONS\_COUNT : <code>string</code>
Link ID of D2-REST API to get related objects' count of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_RENDITIONS"></a>

### RestLinkrels.D2\_RENDITIONS : <code>string</code>
Link ID of D2-REST API to get renditions of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WIDGETS"></a>

### RestLinkrels.D2\_WIDGETS : <code>string</code>
Link ID of D2-REST API to get widget configuration accessible to the user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_SAVED_SEARCH"></a>

### RestLinkrels.D2\_SAVED\_SEARCH : <code>string</code>
Link ID of D2-REST API to get saved search categories.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_DISPLAY_TYPES"></a>

### RestLinkrels.D2\_DISPLAY\_TYPES : <code>string</code>
Link ID of D2-REST API to get displayable information for different Documentum types.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_THEMES"></a>

### RestLinkrels.D2\_THEMES : <code>string</code>
Link ID of D2-REST API to get available theme configurations.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_DEFAULT_THEME"></a>

### RestLinkrels.D2\_DEFAULT\_THEME : <code>string</code>
Link ID of D2-REST API to get default theme config for the D2 Smart View runtime.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_OPTIONS"></a>

### RestLinkrels.D2\_OPTIONS : <code>string</code>
Link ID of D2-REST API to get different D2 Smart View runtime options.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_THUMBNAIL"></a>

### RestLinkrels.D2\_THUMBNAIL : <code>string</code>
Link ID of D2-REST API to get thumbnail of a document object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PROFILES"></a>

### RestLinkrels.D2\_CREATION\_PROFILES : <code>string</code>
Link ID of D2-REST API to get list of content creation profiles.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PROFILE"></a>

### RestLinkrels.D2\_CREATION\_PROFILE : <code>string</code>
Link ID of D2-REST API to get details of a content creation profile.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PROFILE_VALUE_CONFIGS"></a>

### RestLinkrels.D2\_CREATION\_PROFILE\_VALUE\_CONFIGS : <code>string</code>
Link ID of D2-REST API to get value configurations of a content creation profile.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PROFILE_VALUE_CONFIG"></a>

### RestLinkrels.D2\_CREATION\_PROFILE\_VALUE\_CONFIG : <code>string</code>
Link ID of D2-REST API to get details of a creation profile's value configuration.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PRESET_PROFILES"></a>

### RestLinkrels.D2\_CREATION\_PRESET\_PROFILES : <code>string</code>
Link ID of D2-REST API to get preset type of content creation profiles or preset profile in short.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PRESET_VALUE_CONFIGS"></a>

### RestLinkrels.D2\_CREATION\_PRESET\_VALUE\_CONFIGS : <code>string</code>
Link ID of D2-REST API to value confgurations of a preset profile.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PRESET_RECENTLY_USED"></a>

### RestLinkrels.D2\_CREATION\_PRESET\_RECENTLY\_USED : <code>string</code>
Link ID of D2-REST API to get recently used content creation profile be it preset or not.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CREATION_PROPERTIES"></a>

### RestLinkrels.D2\_CREATION\_PROPERTIES : <code>string</code>
Link ID of D2-REST API to get creation time property page.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_OBJECT_CREATION"></a>

### RestLinkrels.D2\_OBJECT\_CREATION : <code>string</code>
Link ID of D2-REST API to create new object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CONTENT_UPLOAD_URLS"></a>

### RestLinkrels.D2\_CONTENT\_UPLOAD\_URLS : <code>string</code>
Link ID of D2-REST API to get URL that lets upload content for a newly created object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_DOCUMENT_TEMPLATES"></a>

### RestLinkrels.D2\_DOCUMENT\_TEMPLATES : <code>string</code>
Link ID of D2-REST API to get list of document templates while creating new content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_DOCUMENT_TEMPLATE"></a>

### RestLinkrels.D2\_DOCUMENT\_TEMPLATE : <code>string</code>
Link ID of D2-REST API to get details of a document template.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_RECENTLY_USED_TEMPLATES"></a>

### RestLinkrels.D2\_RECENTLY\_USED\_TEMPLATES : <code>string</code>
Link ID of D2-REST API to get list of recently used document templates.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_LINKED_DOCUMENT_CONFIG"></a>

### RestLinkrels.D2\_LINKED\_DOCUMENT\_CONFIG : <code>string</code>
Link ID of D2-REST API to get linkable document configuration for a newly created object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CHECKIN_CONFIG"></a>

### RestLinkrels.D2\_CHECKIN\_CONFIG : <code>string</code>
Link ID of D2-REST API to get configuration to control check-in of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CHECKIN_URL"></a>

### RestLinkrels.D2\_CHECKIN\_URL : <code>string</code>
Link ID of D2-REST API to get URL that lets upload new content as part of check-in.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_VD_SILENT_CHECKIN"></a>

### RestLinkrels.D2\_VD\_SILENT\_CHECKIN : <code>string</code>
Link ID of D2-REST API to check-in a virtual document silently.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_FOLDER_CREATION_PROPERTY_PAGE"></a>

### RestLinkrels.D2\_FOLDER\_CREATION\_PROPERTY\_PAGE : <code>string</code>
Link ID of D2-REST API to get property page to be shown while creating a folder.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_FOLDERS"></a>

### RestLinkrels.D2\_FOLDERS : <code>string</code>
Link ID of D2-REST API to get folders.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CABINETS"></a>

### RestLinkrels.D2\_CABINETS : <code>string</code>
Link ID of D2-REST API to get cabinets.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_EXTRACT_O2_PROPERTIES_URLS"></a>

### RestLinkrels.D2\_EXTRACT\_O2\_PROPERTIES\_URLS : <code>string</code>
Link ID of D2-REST API to extract properties using O2 plugin while importing new content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_EXTRACT_EMAIL_ATTACHMENTS"></a>

### RestLinkrels.D2\_EXTRACT\_EMAIL\_ATTACHMENTS : <code>string</code>
Link ID of D2-REST API to extract attachments from email while importing new content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_SILENT_LINKED_DOCUMENT_CREATION"></a>

### RestLinkrels.D2\_SILENT\_LINKED\_DOCUMENT\_CREATION : <code>string</code>
Link ID of D2-REST API to create a linked docuement silently while creating new content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_QUERYFORM_DIALOG"></a>

### RestLinkrels.D2\_QUERYFORM\_DIALOG : <code>string</code>
Link ID of D2-REST API to get dialog configuration for a query form.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_SET_TEMPLATE"></a>

### RestLinkrels.D2\_SET\_TEMPLATE : <code>string</code>
Link ID of D2-REST API to set a document template to a newly created object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_FOLDER_STRUCTURE_CONFIGS"></a>

### RestLinkrels.D2\_FOLDER\_STRUCTURE\_CONFIGS : <code>string</code>
Link ID of D2-REST API to get folder structure config while importing folder.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_FOLDER_STRUCTURE_IMPORT_URLS"></a>

### RestLinkrels.D2\_FOLDER\_STRUCTURE\_IMPORT\_URLS : <code>string</code>
Link ID of D2-REST API to get URL that lets import folder structure.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_FETCH_VERSION_VD"></a>

### RestLinkrels.D2\_FETCH\_VERSION\_VD : <code>string</code>
Link ID of D2-REST API to get bound version of a node in virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_SET_BINDING_VD"></a>

### RestLinkrels.D2\_SET\_BINDING\_VD : <code>string</code>
Link ID of D2-REST API to set binding versions of a node in virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CONVERT_TO_SIMPLE_DOC"></a>

### RestLinkrels.D2\_CONVERT\_TO\_SIMPLE\_DOC : <code>string</code>
Link ID of D2-REST API to convert a virtual document to a simple document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_REMOVE_VD_CHILDREN"></a>

### RestLinkrels.D2\_REMOVE\_VD\_CHILDREN : <code>string</code>
Link ID of D2-REST API to remove children from a virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_PREVIEW_FORMATS"></a>

### RestLinkrels.D2\_PREVIEW\_FORMATS : <code>string</code>
Link ID of D2-REST API to fetch content preivew format of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_INBOX"></a>

### RestLinkrels.D2\_INBOX : <code>string</code>
Link ID of D2-REST API to get task assigments of user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_CATEGORIES"></a>

### RestLinkrels.D2\_TASK\_CATEGORIES : <code>string</code>
Link ID of D2-REST API to get different categoris applicable to user inbox.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_SUPPORTING_DOCUMENTS"></a>

### RestLinkrels.D2\_TASK\_SUPPORTING\_DOCUMENTS : <code>string</code>
Link ID of D2-REST API to get supporting documents of a taks.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_WORKING_DOCUMENTS"></a>

### RestLinkrels.D2\_TASK\_WORKING\_DOCUMENTS : <code>string</code>
Link ID of D2-REST API to get working documents of a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_NOTES"></a>

### RestLinkrels.D2\_NOTES : <code>string</code>
Link ID of D2-REST API to get notes/user comments of a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_NOTES_COUNT"></a>

### RestLinkrels.D2\_NOTES\_COUNT : <code>string</code>
Link ID of D2-REST API to count number of notes of a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_STATUS"></a>

### RestLinkrels.D2\_TASK\_STATUS : <code>string</code>
Link ID of D2-REST API to change status of a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_DELEGATE"></a>

### RestLinkrels.D2\_TASK\_DELEGATE : <code>string</code>
Link ID of D2-REST API to delegate a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_DELEGATE_USER"></a>

### RestLinkrels.D2\_TASK\_DELEGATE\_USER : <code>string</code>
Link ID of D2-REST API to get list of users applicable for a task delegation.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_PERFORMERS"></a>

### RestLinkrels.D2\_TASK\_PERFORMERS : <code>string</code>
Link ID of D2-REST API to get performers of a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_PERFORMERS_DIALOG"></a>

### RestLinkrels.D2\_TASK\_PERFORMERS\_DIALOG : <code>string</code>
Link ID of D2-REST API to get dialog config to udpate performers of a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_PERFORMERS_ALIAS_PARTICIPANTS"></a>

### RestLinkrels.D2\_TASK\_PERFORMERS\_ALIAS\_PARTICIPANTS : <code>string</code>
Link ID of D2-REST API to get data to get resolved participant details of task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_INTENTION_DICTIONARY_FORWARD"></a>

### RestLinkrels.D2\_TASK\_INTENTION\_DICTIONARY\_FORWARD : <code>string</code>
Link ID of D2-REST API to get intention dictionary while completing a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_INTENTION_DICTIONARY_REJECT"></a>

### RestLinkrels.D2\_TASK\_INTENTION\_DICTIONARY\_REJECT : <code>string</code>
Link ID of D2-REST API to get intentions dictionary while rejecting a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_PROPERTY_PAGE_FORWARD"></a>

### RestLinkrels.D2\_TASK\_PROPERTY\_PAGE\_FORWARD : <code>string</code>
Link ID of D2-REST API to get property-page while completing a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_PROPERTY_PAGE_REJECT"></a>

### RestLinkrels.D2\_TASK\_PROPERTY\_PAGE\_REJECT : <code>string</code>
Link ID of D2-REST API to get property-page while rejecting a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_STATUS"></a>

### RestLinkrels.D2\_WORKFLOW\_STATUS : <code>string</code>
Link ID of D2-REST API to change workflow status.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_LIFECYCLE_CHECKER"></a>

### RestLinkrels.D2\_TASK\_LIFECYCLE\_CHECKER : <code>string</code>
Link ID of D2-REST API to check lifecycle status while completing/rejecting a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_TASK_CHECK_LOCK_STATE"></a>

### RestLinkrels.D2\_TASK\_CHECK\_LOCK\_STATE : <code>string</code>
Link ID of D2-REST API to check locked status of working documents while completing/rejecting a task.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOWS"></a>

### RestLinkrels.D2\_WORKFLOWS : <code>string</code>
Link ID of D2-REST API to launch a new workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_CONFIG"></a>

### RestLinkrels.D2\_WORKFLOW\_CONFIG : <code>string</code>
Link ID of D2-REST API to get configuration to launch a new workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_ENTRY_CONDITION_CHECKER"></a>

### RestLinkrels.D2\_WORKFLOW\_ENTRY\_CONDITION\_CHECKER : <code>string</code>
Link ID of D2-REST API to validate entry conditions against a workflow config before starting a new workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_PACKAGETYPE_CHECKER"></a>

### RestLinkrels.D2\_WORKFLOW\_PACKAGETYPE\_CHECKER : <code>string</code>
Link ID of D2-REST API to validate package types against a workflow config.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_LAUNCH_PEROFMERS_DIALOG"></a>

### RestLinkrels.D2\_WORKFLOW\_LAUNCH\_PEROFMERS\_DIALOG : <code>string</code>
Link ID of D2-REST API to get performer dialog config while launching workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_MY_WORKFLOWS"></a>

### RestLinkrels.D2\_MY\_WORKFLOWS : <code>string</code>
Link ID of D2-REST API to get all workflows that the user is supervisor of.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_ALL_WORKFLOWS"></a>

### RestLinkrels.D2\_ALL\_WORKFLOWS : <code>string</code>
Link ID of D2-REST API to get all workflows running system-wide.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_MY_WORKFLOWS_STATUS_SUMMARY"></a>

### RestLinkrels.D2\_MY\_WORKFLOWS\_STATUS\_SUMMARY : <code>string</code>
Link ID of D2-REST API to get a summary of worklfows supervised by user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOWS_TEMPLATES"></a>

### RestLinkrels.D2\_WORKFLOWS\_TEMPLATES : <code>string</code>
Link ID of D2-REST API to get avilable workflow templates.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_OVERVIEW_WIDGET_USERS"></a>

### RestLinkrels.D2\_WORKFLOW\_OVERVIEW\_WIDGET\_USERS : <code>string</code>
Link ID of D2-REST API to get list of users to be shown for workflow filtering.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_CHANGE_WORKFLOW_SUPERVISOR_USERS"></a>

### RestLinkrels.D2\_CHANGE\_WORKFLOW\_SUPERVISOR\_USERS : <code>string</code>
Link ID of D2-REST API to get list of user to shown while changing supervisor of a workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_NOTES_CONFIG"></a>

### RestLinkrels.D2\_WORKFLOW\_NOTES\_CONFIG : <code>string</code>
Link ID of D2-REST API to get comment configuration of a workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_TASK_AUDITS"></a>

### RestLinkrels.D2\_WORKFLOW\_TASK\_AUDITS : <code>string</code>
Link ID of D2-REST API to get event details of tasks of a workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_EVENT_AUDITS"></a>

### RestLinkrels.D2\_WORKFLOW\_EVENT\_AUDITS : <code>string</code>
Link ID of D2-REST API to get event details of a workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOW_TASKS"></a>

### RestLinkrels.D2\_WORKFLOW\_TASKS : <code>string</code>
Link ID of D2-REST API to get all tasks of a worklfow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_WORKFLOWS_COUNT"></a>

### RestLinkrels.D2\_WORKFLOWS\_COUNT : <code>string</code>
Link ID of D2-REST API to get number of notes attached to a workflow.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.EDIT"></a>

### RestLinkrels.EDIT : <code>string</code>
Link ID of D2-REST API to edit object properties.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.EXPORT_URLS"></a>

### RestLinkrels.EXPORT\_URLS : <code>string</code>
Link ID of D2-REST API to get URL to download content of a object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.FAVORITES"></a>

### RestLinkrels.FAVORITES : <code>string</code>
Link ID of D2-REST API to get favorited objects of user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.FORMATS"></a>

### RestLinkrels.FORMATS : <code>string</code>
Link ID of D2-REST API to get list of supported content formats.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.OBJECTS"></a>

### RestLinkrels.OBJECTS : <code>string</code>
Link ID of D2-REST API to get objects within a folder/cabinet.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.OBJECTS_LOCATIONS"></a>

### RestLinkrels.OBJECTS\_LOCATIONS : <code>string</code>
Link ID of D2-REST API to get location of an object within the repository.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.OBJECTS_FACETS"></a>

### RestLinkrels.OBJECTS\_FACETS : <code>string</code>
Link ID of D2-REST API to get objects within a folder/cabinet after applying facet filters.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.DQL_DOCLIST_OBJECTS"></a>

### RestLinkrels.DQL\_DOCLIST\_OBJECTS : <code>string</code>
Link ID of D2-REST API to get objects of a DQL doclist.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.OBJECTS_THUMBNAIL_URL"></a>

### RestLinkrels.OBJECTS\_THUMBNAIL\_URL : <code>string</code>
Link ID of D2-REST API to get thumbanil of object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.PARENT"></a>

### RestLinkrels.PARENT : <code>string</code>
Link ID of D2-REST API to get immediate parent of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.PARENT_LINKS"></a>

### RestLinkrels.PARENT\_LINKS : <code>string</code>
Link ID of D2-REST API to link an object to several folders.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_PREFERENCES"></a>

### RestLinkrels.D2\_PREFERENCES : <code>string</code>
Link ID of D2-REST API to get general preference data of the user.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_COLUMN_PREFERENCES"></a>

### RestLinkrels.D2\_COLUMN\_PREFERENCES : <code>string</code>
Link ID of D2-REST API to get column preference data of a widget.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.PREVIEW_URLS"></a>

### RestLinkrels.PREVIEW\_URLS : <code>string</code>
Link ID of D2-REST API to get URL that lets preview a content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.PRINT_URL"></a>

### RestLinkrels.PRINT\_URL : <code>string</code>
Link ID of D2-REST API to get URL that lets access to a printable content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.PROPERTY_PAGE"></a>

### RestLinkrels.PROPERTY\_PAGE : <code>string</code>
Link ID of D2-REST API to get property page of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.QUICK_SEARCH"></a>

### RestLinkrels.QUICK\_SEARCH : <code>string</code>
Link ID of D2-REST API to execute a quick search.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.REPOSITORIES"></a>

### RestLinkrels.REPOSITORIES : <code>string</code>
Link ID of D2-REST API to get list of repositories.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.SELF"></a>

### RestLinkrels.SELF : <code>string</code>
Link ID of D2-REST API that point to the resource itself.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.SEND_MAIL"></a>

### RestLinkrels.SEND\_MAIL : <code>string</code>
Link ID of D2-REST API to get configuration to send email.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.VERSION_HISTORY"></a>

### RestLinkrels.VERSION\_HISTORY : <code>string</code>
Link ID of D2-REST API to get versions' history of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.VD_NODE_CHILDREN"></a>

### RestLinkrels.VD\_NODE\_CHILDREN : <code>string</code>
Link ID of D2-REST API to get children of a virtual document.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.VD_SNAPSHOT_NODE_CHILDREN"></a>

### RestLinkrels.VD\_SNAPSHOT\_NODE\_CHILDREN : <code>string</code>
Link ID of D2-REST API to get children of a virtual document's snapshot.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.VD_MOVE_NODES"></a>

### RestLinkrels.VD\_MOVE\_NODES : <code>string</code>
Link ID of D2-REST API to move children of a virtual document from one sub-path to another.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.FINISH_ACTION"></a>

### RestLinkrels.FINISH\_ACTION : <code>string</code>
Link ID of D2-REST API to commit all in-process object while creating new content.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.ABORT_ACTION"></a>

### RestLinkrels.ABORT\_ACTION : <code>string</code>
Link ID of D2-REST API to roll back all newly created in-process objects that are not committed yet.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.ASYNC_OPERATION"></a>

### RestLinkrels.ASYNC\_OPERATION : <code>string</code>
Link ID of D2-REST API to perform an async operation.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_DUMP"></a>

### RestLinkrels.D2\_DUMP : <code>string</code>
Link ID of D2-REST API to get all metadata of an object.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_SIGNOFF_VALIDATION"></a>

### RestLinkrels.D2\_SIGNOFF\_VALIDATION : <code>string</code>
Link ID of D2-REST API to validate electronic signature credentials.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.D2_SYSOBJECTS"></a>

### RestLinkrels.D2\_SYSOBJECTS : <code>string</code>
Link ID of D2-REST API to fetch Documentum dm_sysobject type attributes for a bunch of objects.

**Kind**: static constant of [<code>RestLinkrels</code>](#module_RestLinkrels)  
<a name="module_RestLinkrels.getLink"></a>

### RestLinkrels.getLink(links, linkrel) ⇒ <code>string</code>
Get complete URL from a link relation

**Kind**: static method of [<code>RestLinkrels</code>](#module_RestLinkrels)  
**Returns**: <code>string</code> - the formed URL  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>links</td><td><code>Array.&lt;D2RestLinkObject&gt;</code></td><td><p>A set of related link objects.</p>
</td>
    </tr><tr>
    <td>linkrel</td><td><code>string</code></td><td><p>Identifier of the link relation whose URL is to be formed.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_RestLinkrels.getLinkTemplate"></a>

### RestLinkrels.getLinkTemplate(links, linkrel) ⇒ <code>string</code>
Get complete URL from a templated link relation

**Kind**: static method of [<code>RestLinkrels</code>](#module_RestLinkrels)  
**Returns**: <code>string</code> - the formed URL  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>links</td><td><code>Array.&lt;D2RestLinkObject&gt;</code></td><td><p>A set of related link objects.</p>
</td>
    </tr><tr>
    <td>linkrel</td><td><code>string</code></td><td><p>Identifier of the link relation whose URL is to be formed. The link object identified should have <code>hrefTemplate</code> in it.</p>
</td>
    </tr>  </tbody>
</table>

