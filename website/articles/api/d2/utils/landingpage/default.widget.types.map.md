<a name="module_DefaultWidgetsMap"></a>

## DefaultWidgetsMap : <code>d2/sdk/utils/landingpage/default.widget.types.map</code>
An extensible map that assigns a D2-Config widget type to a D2 Smart View default widget typeNew mapping entries could be added using extension

**Extends**: <code>Object</code>  
**Example** *(to register an extension, add in extension.json)*  
```js
{
  "d2/sdk/utils/landingpage/default.widget.types.map": {
     "extensions": {
        "mybundle": {
           "mybundle/utils/landingpage/my.default.widget.types"
        }
     }
   }
}
```
**Example** *(whereas my.default.widget.types.js is defined as)*  
```js
define([], function(){
   'use strict';

   return [
     {defaultType: 'svcustomtype1', d2WidgetType: 'FavoritesWidget'}, // uses default error if the widget configuration can't be resolved
     {defaultType: 'svcustomtype2', d2WidgetType: 'FavoritesWidget', errMissingWidgetConfig: 'Widget for svcustomtype2 is not found'}, //custom error
     // Got more maps ? keep appending here.
   ];
});
```

* [DefaultWidgetsMap](#module_DefaultWidgetsMap) : <code>d2/sdk/utils/landingpage/default.widget.types.map</code>
    * _static_
        * [.addMaps(newEntries)](#module_DefaultWidgetsMap.addMaps) ⇒
        * [.getMaps()](#module_DefaultWidgetsMap.getMaps) ⇒ <code>MapEntryReturnType</code>
    * _inner_
        * [~MapEntry](#module_DefaultWidgetsMap..MapEntry) : <code>Object</code>
        * [~MapEntryReturnType](#module_DefaultWidgetsMap..MapEntryReturnType) : <code>Object</code>

<a name="module_DefaultWidgetsMap.addMaps"></a>

### DefaultWidgetsMap.addMaps(newEntries) ⇒
Add new mapping entries. Recommended way is to get it added via extension mechanism, see example from above.

**Kind**: static method of [<code>DefaultWidgetsMap</code>](#module_DefaultWidgetsMap)  
**Returns**: void  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>newEntries</td><td><code>Array.&lt;MapEntry&gt;</code></td><td><p>Entries to be added</p>
</td>
    </tr>  </tbody>
</table>

**Example** *(New types could be added as)*  
```js
// It has to be made sure that this AMD module is loaded by some other module.
define(['d2/sdk/utils/landingpage/default.widget.types.map'], function(widgetTypesMap){
   'use strict';

   widgetTypesMap.addMaps([
     {defaultType: 'svcustomtype1', d2WidgetType: 'FavoritesWidget'},
     {defaultType: 'svcustomtype2', d2WidgetType: 'FavoritesWidget', errMissingWidgetConfig: 'Widget for svcustomtype2 is not found'}
     // More entries could be added.
   ]);

   // some other code
});
```
<a name="module_DefaultWidgetsMap.getMaps"></a>

### DefaultWidgetsMap.getMaps() ⇒ <code>MapEntryReturnType</code>
Get existing map entries

**Kind**: static method of [<code>DefaultWidgetsMap</code>](#module_DefaultWidgetsMap)  
<a name="module_DefaultWidgetsMap..MapEntry"></a>

### DefaultWidgetsMap~MapEntry : <code>Object</code>
**Kind**: inner typedef of [<code>DefaultWidgetsMap</code>](#module_DefaultWidgetsMap)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>defaultType</td><td><code>string</code></td><td><p>Name of D2 Smart View default widget type</p>
</td>
    </tr><tr>
    <td>d2WidgetType</td><td><code>string</code></td><td><p>Name of D2-Config widget type</p>
</td>
    </tr><tr>
    <td>errMissingWidgetConfig</td><td><code>string</code></td><td><p>Error to be shown in the UI if the mapped widget type from D2-Config is not resolved.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_DefaultWidgetsMap..MapEntryReturnType"></a>

### DefaultWidgetsMap~MapEntryReturnType : <code>Object</code>
Return type. Each key in this object represents a default widget type name in D2 Smart View.

**Kind**: inner typedef of [<code>DefaultWidgetsMap</code>](#module_DefaultWidgetsMap)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>key</td><td><code>string</code></td><td><p>Default widget type name</p>
</td>
    </tr><tr>
    <td>value</td><td><code>Object</code></td><td><p>The map</p>
</td>
    </tr><tr>
    <td>value.type</td><td><code>string</code></td><td><p>D2 widget type name</p>
</td>
    </tr><tr>
    <td>value.errMissingWidgetConfig</td><td><code>string</code></td><td><p>Error to be shown if widget config is not resolved</p>
</td>
    </tr>  </tbody>
</table>

