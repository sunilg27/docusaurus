<a name="module_TileCollection"></a>

## TileCollection : <code>d2/sdk/utils/landingpage/tiles</code>
Tile definition collection. Each tile definition describes its priority, association type, pre-requisite and finally a view that is used as therepresentation of the tile. Based on priority a particular definition is picked from this collection to finally render D2 Smart View landing page.

**Extends**: [<code>Backbone.Collection</code>](https://backbonejs.org/#Collection)  
**See**: [WidgetConstants](../widget.constants) for some related pre-defined constants.New definitions could be added to this collection by means of extension.  
**Example** *(to register an extension, add in extension.json)*  
```js
{
  "d2/sdk/utils/landingpage/tiles": {
     "extensions": {
        "mybundle": {
           "mybundle/utils/landingpage/my.tiles"
        }
     }
   }
}
```
**Example** *(whereas my.tiles.js is defined as)*  
```js
define(['d2/sdk/utils/widget.constants', 'i18n!mybundle/utils/landingpage/impl/nls/lang'], function(widgetConstants, lang){
   'use strict';

   function validateURLShortcutConfig(widgetConfig, options) {
       var validation = {status: true};
       if (!widgetConfig.widgetParams && !widgetConfig.widgetParams.open_url) {
         validation.status = false; // It will be considered as a validation error
         validation.errorMessage = lang.openUrlWidgetMissing;
       }
       return validation;
   }

   // See 'LandingTile' documentation for all possible properties with their usage
   return [
     {type: 'CreateFile', icon: 'icon-new', isShortcut: true, tileConfigType: widgetConstants.TileConfigTypes.NONE}, // widget configuration-less shortcut tile
     {type: 'OpenURLWidget', icon: 'icon-url', isShortcut: true, validateConfig: validateURLShortcutConfig}, // shortcut tile that needs & validates a widget configuration
     {
       // A widget tile that relies on the default widget validation
       type: 'CheckoutDocumentWidget',
       icon: 'title-checkout',
       tileView: 'mybundle/widgets/newcheckout',
       errMissingWidgetConfig: lang.checkoutWidgetMissing,
       applicationScope: 'checkedout'
     }
     // Any other definition
   ];
});
```

* [TileCollection](#module_TileCollection) : <code>d2/sdk/utils/landingpage/tiles</code>
    * [~ShortcutType](#module_TileCollection..ShortcutType) : <code>enum</code>
    * [~LandingTile](#module_TileCollection..LandingTile) : <code>Object</code>
    * [~ConfigValidatorCallback](#module_TileCollection..ConfigValidatorCallback) ⇒ <code>ConfigValidatorReturnType</code>
    * [~ConfigValidatorReturnType](#module_TileCollection..ConfigValidatorReturnType) : <code>Object</code>
    * [~TileOptionBuilderCallback](#module_TileCollection..TileOptionBuilderCallback) ⇒ <code>void</code>
    * [~ShortcutClickHandler](#module_TileCollection..ShortcutClickHandler) ⇒ <code>void</code>

<a name="module_TileCollection..ShortcutType"></a>

### TileCollection~ShortcutType : <code>enum</code>
OOTB D2SV shortcut types

**Kind**: inner enum of [<code>TileCollection</code>](#module_TileCollection)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>CreateFile</td><td><p>New file shortcut</p>
</td>
    </tr><tr>
    <td>UploadFile</td><td><p>Import files shortcut</p>
</td>
    </tr><tr>
    <td>CreateUploadFile</td><td><p>Combined New/Import file shortcut</p>
</td>
    </tr><tr>
    <td>UploadFolder</td><td><p>Import folder shortcut</p>
</td>
    </tr><tr>
    <td>QueryFormObject</td><td><p>Query form shortcur</p>
</td>
    </tr><tr>
    <td>QueryObject</td><td><p>Saved query/search shortcut</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TileCollection..LandingTile"></a>

### TileCollection~LandingTile : <code>Object</code>
**Kind**: inner typedef of [<code>TileCollection</code>](#module_TileCollection)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[sequence]</td><td><code>number</code></td><td><code>1000</code></td><td><p>Priority of this definition. For matching <code>type</code> property values, the definition with least value for <code>sequence</code> will be used.</p>
</td>
    </tr><tr>
    <td>type</td><td><code>string</code></td><td></td><td><p>Association type of this tile. It should be set with a value from the enumeration <code>ShortcutType</code>, or any D2 widget type name.
Additional shortcut types or widget types declared by a D2 plugin also could be used.</p>
</td>
    </tr><tr>
    <td>icon</td><td><code>string</code></td><td></td><td><p>CSS class name of the icon to be shown on the tile. For non-shortcut tiles, the icon is shown in the tile header.</p>
</td>
    </tr><tr>
    <td>[isShortcut]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether it&#39;s shortcut tile or widget tile? <code>true</code> indicates a shortcut.</p>
</td>
    </tr><tr>
    <td>[tileConfigType]</td><td><code>TileConfigTypes</code></td><td><code>TileConfigTypes.WIDGET</code></td><td><p>Tiles that don&#39;t require a widget config should set it to <code>TileConfigTypes.NONE</code>. See <a href="../widget.constants#TileConfigTypes">TileConfigTypes</a>.</p>
</td>
    </tr><tr>
    <td>[tileView]</td><td><code>string</code></td><td></td><td><p>Not required for shortcut tiles. For other tiles it should point to a folder implementing the view to be associated with this tile. E.g For a path <code>mybundle/widget/somefolder</code> the runtime
will look for a <code>somefolder.view.js</code> within the <code>somefolder</code> directory. Alternatively it can be specified as just a name like <code>mytile</code>, in that case the tile&#39;s view will be resolved as
<code>&lt;bundle_src_folder&gt;/widgets/mytile/mytile.view.js</code>.</p>
</td>
    </tr><tr>
    <td>[clickHandler]</td><td><code>ShortcutClickHandler</code></td><td></td><td><p>Callback to be executed only for those shortcut tiles that neither specifies an <code>applicationScope</code> (see below) nor a matching shortcut behavior
is found in the registry. See <a href="../../widgets/shortcut.tile/shortcut.tile.behaviors">ShortcutBehaviors</a>.</p>
</td>
    </tr><tr>
    <td>[validateConfig]</td><td><code>ConfigValidatorCallback</code></td><td></td><td><p>Callback to validate the resolved widget config.</p>
</td>
    </tr><tr>
    <td>[buildTileOptions]</td><td><code>TileOptionBuilderCallback</code></td><td></td><td><p>Callback to build tile options that would be passed to the <code>tileView</code> while constructing.</p>
</td>
    </tr><tr>
    <td>[errMissingWidgetName]</td><td><code>string</code></td><td></td><td><p>The error to be shown if a tile definition requires a widget config but the corresponding D2 Smart View landing page configuraiton
fails to specify a name for it. If unspecified, a generic error message is shown.</p>
</td>
    </tr><tr>
    <td>[errMissingWidgetConfig]</td><td><code>string</code></td><td></td><td><p>The error to be shown if a tile definition requires a widget config but a suitable widget configuration could not be resolved for some reason.</p>
</td>
    </tr><tr>
    <td>[applicationScope]</td><td><code>string</code></td><td></td><td><p>The application scope in which this tile will expand into. Tile expansion is enabled only if this value matches one of the registered application scope perspectives&#39;s id.
See <a href="../perspectives/app.scope.perspectives">ApplicationScopePerspectives</a>. For shortcut tiles, clicking anywhere in the tile activates the expansion. For list type of tiles, the tile header or
a dedicated &#39;Expand&#39; button could be clicked to activate the expansion.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TileCollection..ConfigValidatorCallback"></a>

### TileCollection~ConfigValidatorCallback ⇒ <code>ConfigValidatorReturnType</code>
Callback function that validates a widget configuration for a tile

**Kind**: inner typedef of [<code>TileCollection</code>](#module_TileCollection)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>widgetConfig</td><td><code>Object</code></td><td><p>The widget configuration being validated</p>
</td>
    </tr><tr>
    <td>[options]</td><td><code>Object</code></td><td><p>Any additional context specific data that is not part of <code>widgetConfig</code> but might be relevant for validation</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TileCollection..ConfigValidatorReturnType"></a>

### TileCollection~ConfigValidatorReturnType : <code>Object</code>
**Kind**: inner typedef of [<code>TileCollection</code>](#module_TileCollection)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>status</td><td><code>boolean</code></td><td><p><code>true</code> indicates validation succeeded, <code>false</code> otherwise.</p>
</td>
    </tr><tr>
    <td>[errorMessage]</td><td><code>string</code></td><td><p>Error message to be shown in UI if the validation failed.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TileCollection..TileOptionBuilderCallback"></a>

### TileCollection~TileOptionBuilderCallback ⇒ <code>void</code>
Callback function that builds options for a tile

**Kind**: inner typedef of [<code>TileCollection</code>](#module_TileCollection)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>tile</td><td><code>Object</code></td><td><p>Parsed tile object from D2 Smart View landing page configuration. New properties could be attached to this object as part of forming additional option.</p>
</td>
    </tr><tr>
    <td>[widgetConfig]</td><td><code>Object</code></td><td><p>Any relevant wieget configuration, applicable only if the tile requires a widget configuration.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_TileCollection..ShortcutClickHandler"></a>

### TileCollection~ShortcutClickHandler ⇒ <code>void</code>
Callback to handle click event for a shortcut tile

**Kind**: inner typedef of [<code>TileCollection</code>](#module_TileCollection)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>The options container.</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td><p><code>context</code> object associated with the runtime. See <a href="../contexts/context.utils#Context">Context</a>.</p>
</td>
    </tr><tr>
    <td>options.widgetName</td><td><code>string</code></td><td><p>Name of the widget configuration if the shortcut is of type <code>TileConfigTypes.WIDGET</code>. See <a href="../widget.constants#module_WidgetConstants.TileConfigTypes">TileConfigTypes</a>.</p>
</td>
    </tr><tr>
    <td>options.widgetParams</td><td><code>object</code></td><td><p>Any parameters associated with the resolved widget configuration. Applicable only if the shortcut is of type <code>TileConfigTypes.WIDGET</code>. See <a href="../widget.constants#TileConfigTypes">TileConfigTypes</a>.</p>
</td>
    </tr>  </tbody>
</table>

