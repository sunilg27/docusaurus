<a name="module_CommonMock"></a>

## CommonMock : <code>d2/sdk/utils/test/common.mock</code>
Not a part of D2 Smart View application runtime, rather it helps in writing unit tests.CommonMock abstracts out few common setup & tear-down for every unit test case written.

**Extends**: <code>Object</code>  
**Example** *(With common.mock, structure of a unit test mymodule.spec.js could be look like)*  
```js
define(['d2/sdk/utils/test/common.mock'], function(commonMock){
   'use strict';

   describe("My module", function(){

     // Outermost setup
     beforeAll(function(){
       commonMock.enable();

       //ohter setup code
     });

     // Outermost teardown
     afterAll(function(){
       commonMock.disable();

       //ohter teardown code
     });

     describe("some group", function(){
        it("test1", function(){
           // test code
        });

        it("test2", function(){
           // test code
        });

        // any other test for this group could go here
     });

     it("test3", function(){
       // this test is not grouped
       // test code
     });
   });
});
```

* [CommonMock](#module_CommonMock) : <code>d2/sdk/utils/test/common.mock</code>
    * [.enable()](#module_CommonMock.enable)
    * [.disable()](#module_CommonMock.disable)

<a name="module_CommonMock.enable"></a>

### CommonMock.enable()
Enables the common mock APIs. Should be called while setting-up unit test.

**Kind**: static method of [<code>CommonMock</code>](#module_CommonMock)  
<a name="module_CommonMock.disable"></a>

### CommonMock.disable()
Disables the common mock APIs. Should be called while tearing-down a unit test.

**Kind**: static method of [<code>CommonMock</code>](#module_CommonMock)  
