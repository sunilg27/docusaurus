<a name="module_CommandHelper"></a>

## CommandHelper : <code>d2/sdk/utils/commandhelper</code>
A few frequetnly used functions that helps reduce code duplication across [CommandModel](../commands/command) implementations.

**Extends**: <code>Object</code>  

* [CommandHelper](#module_CommandHelper) : <code>d2/sdk/utils/commandhelper</code>
    * [.getJustOneNode(status)](#module_CommandHelper.getJustOneNode) ⇒ [<code>NodeModel</code>](../models/node.model)
    * [.getOriginalAction(node, signature, [commandData])](#module_CommandHelper.getOriginalAction) ⇒ <code>ActionModel</code>
    * [.getOriginalActionLink(node, signature, linkrel, [params], [commandData])](#module_CommandHelper.getOriginalActionLink) ⇒ <code>String</code>
    * [.refreshNode(node, [widgetConfig])](#module_CommandHelper.refreshNode) ⇒ <code>jQuery.Promise</code>
    * [.showErrorPanel(node, errMessage, detailMessage)](#module_CommandHelper.showErrorPanel) ⇒ <code>\*</code>
    * [.blockD2ViewActions(view)](#module_CommandHelper.blockD2ViewActions) ⇒ <code>void</code>
    * [.unblockD2ViewActions(view)](#module_CommandHelper.unblockD2ViewActions) ⇒ <code>void</code>

<a name="module_CommandHelper.getJustOneNode"></a>

### CommandHelper.getJustOneNode(status) ⇒ [<code>NodeModel</code>](../models/node.model)
Get exactly one node from the given `status`

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
**Returns**: [<code>NodeModel</code>](../models/node.model) - Returns the first node from `status.nodes`.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>status</td><td><code>object</code></td><td><p>The <code>status</code> parameter from <a href="../commands/command">CommandModel</a>&#39;s <code>enabled</code> or <code>execute</code> methods.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandHelper.getOriginalAction"></a>

### CommandHelper.getOriginalAction(node, signature, [commandData]) ⇒ <code>ActionModel</code>
Get the most matching action of a node for a given command signature. Usued for disambigutation of an action when it may be present multiple times due to repetitive menu config.

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
**Returns**: <code>ActionModel</code> - Returns the action that matches most. if `commandData` is not passed, the first action matching the `signature` is returned.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>node</td><td><code><a href="../models/node.model">NodeModel</a></code></td><td><p>The node whose actions are being looked up</p>
</td>
    </tr><tr>
    <td>signature</td><td><code>string</code></td><td><p>The command singnature being looked up</p>
</td>
    </tr><tr>
    <td>[commandData]</td><td><code>object</code></td><td><p><code>commandData</code> associated with the <code>status</code> parameter from <a href="../commands/command">CommandModel</a>&#39;s <code>enabled</code> or <code>execute</code> methods.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandHelper.getOriginalActionLink"></a>

### CommandHelper.getOriginalActionLink(node, signature, linkrel, [params], [commandData]) ⇒ <code>String</code>
Get the URL for given linkrel ID from the most matching action of a node. Internally calls [getOriginalAction](#module_CommandHelper.getOriginalAction) to lookup the matching action.

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
**Returns**: <code>String</code> - the URL  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>node</td><td><code><a href="../models/node.model">NodeModel</a></code></td><td><p>The node whose actions are being looked up</p>
</td>
    </tr><tr>
    <td>signature</td><td><code>string</code></td><td><p>The command singnature being looked up</p>
</td>
    </tr><tr>
    <td>linkrel</td><td><code>string</code></td><td><p>Linkrel identifier being looked up.</p>
</td>
    </tr><tr>
    <td>[params]</td><td><code>object</code></td><td><p>If the link relation points to an href-template, then resolve the template using these params.</p>
</td>
    </tr><tr>
    <td>[commandData]</td><td><code>object</code></td><td><p><code>commandData</code> associated with the <code>status</code> parameter from <a href="../commands/command">CommandModel</a>&#39;s <code>enabled</code> or <code>execute</code> methods.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandHelper.refreshNode"></a>

### CommandHelper.refreshNode(node, [widgetConfig]) ⇒ <code>jQuery.Promise</code>
Refresh the attributes of a node from server in the context of a widget

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
**Returns**: <code>jQuery.Promise</code> - it is `resolve()`-ed only if the refresh is successful, `reject()`-ed otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>node</td><td><code><a href="../models/node.model">NodeModel</a></code></td><td><p>The node being refreshed</p>
</td>
    </tr><tr>
    <td>[widgetConfig]</td><td><code>object</code></td><td><p>Widet configuration to set the context of refresh</p>
</td>
    </tr><tr>
    <td>widgetConfig.widgetName</td><td><code>string</code></td><td><p>Name of the widget</p>
</td>
    </tr><tr>
    <td>widgetConfig.widgetType</td><td><code>string</code></td><td><p>Type of the widget</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandHelper.showErrorPanel"></a>

### CommandHelper.showErrorPanel(node, errMessage, detailMessage) ⇒ <code>\*</code>
Show an error toast associated with a node.

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
**Returns**: <code>\*</code> - the toast instance  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>node</td><td><code><a href="../models/node.model">NodeModel</a></code></td><td><p>The node to associate the message with</p>
</td>
    </tr><tr>
    <td>errMessage</td><td><code>string</code></td><td><p>Brief message to be shown</p>
</td>
    </tr><tr>
    <td>detailMessage</td><td><code>string</code></td><td><p>Verbose detail of the <code>errMessage</code>. The detail is shown in an expandable region of the toast.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandHelper.blockD2ViewActions"></a>

### CommandHelper.blockD2ViewActions(view) ⇒ <code>void</code>
Enable blocker UI.

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>view</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>A view instance infused with the progress blocker API. See <a href="../controls/blocker/blocker">BlockingView</a>.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_CommandHelper.unblockD2ViewActions"></a>

### CommandHelper.unblockD2ViewActions(view) ⇒ <code>void</code>
Disable blocker UI.

**Kind**: static method of [<code>CommandHelper</code>](#module_CommandHelper)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>view</td><td><code><a href="https://marionettejs.com/docs/v2.4.7/marionette.view.html">Marionette.View</a></code></td><td><p>A view instance infused with the progress blocker API. See <a href="../controls/blocker/blocker">BlockingView</a>.</p>
</td>
    </tr>  </tbody>
</table>

