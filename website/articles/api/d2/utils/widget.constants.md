<a name="module_WidgetConstants"></a>

## WidgetConstants : <code>d2/sdk/utils/widget.constants</code>
A few utility constants related to widgets

**Extends**: <code>Object</code>  

* [WidgetConstants](#module_WidgetConstants) : <code>d2/sdk/utils/widget.constants</code>
    * [.TileConfigTypes](#module_WidgetConstants.TileConfigTypes) : <code>Object</code>
    * [.D2](#module_WidgetConstants.D2) : <code>Object</code>
    * [.DEFAULT](#module_WidgetConstants.DEFAULT) : <code>Object</code>

<a name="module_WidgetConstants.TileConfigTypes"></a>

### WidgetConstants.TileConfigTypes : <code>Object</code>
Tile config types.

**Kind**: static property of [<code>WidgetConstants</code>](#module_WidgetConstants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>NONE</td><td><code>string</code></td><td><p>This tile requires a widget config</p>
</td>
    </tr><tr>
    <td>WIDGET</td><td><code>string</code></td><td><p>This tile requires a widget config</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_WidgetConstants.D2"></a>

### WidgetConstants.D2 : <code>Object</code>
OOTB D2 widget types that has an implementation within the D2 Smart View runtime

**Kind**: static property of [<code>WidgetConstants</code>](#module_WidgetConstants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>RECENTLY_ACCESSED</td><td><code>string</code></td><td><p>RecentlyAccessedWidget</p>
</td>
    </tr><tr>
    <td>CHECKOUT</td><td><code>string</code></td><td><p>CheckoutDocumentWidget</p>
</td>
    </tr><tr>
    <td>DOCLIST</td><td><code>string</code></td><td><p>DoclistWidget</p>
</td>
    </tr><tr>
    <td>FAVORITES</td><td><code>string</code></td><td><p>FavoritesWidget</p>
</td>
    </tr><tr>
    <td>BRAVACSR</td><td><code>string</code></td><td><p>BravaCSRViewerWidget</p>
</td>
    </tr><tr>
    <td>OPENURL</td><td><code>string</code></td><td><p>OpenURLWidget</p>
</td>
    </tr><tr>
    <td>RELATIONS</td><td><code>string</code></td><td><p>DetailsRelationsWidget</p>
</td>
    </tr><tr>
    <td>RENDITIONS</td><td><code>string</code></td><td><p>DetailsRenditionsWidget</p>
</td>
    </tr><tr>
    <td>VERSIONS</td><td><code>string</code></td><td><p>DetailsVersionsWidget</p>
</td>
    </tr><tr>
    <td>PROPERTIES</td><td><code>string</code></td><td><p>Properties</p>
</td>
    </tr><tr>
    <td>SEARCH</td><td><code>string</code></td><td><p>SearchWidget</p>
</td>
    </tr><tr>
    <td>MYSHARES</td><td><code>string</code></td><td><p>SharedDocumentWidget</p>
</td>
    </tr><tr>
    <td>VDOC</td><td><code>string</code></td><td><p>VirtualDocWidget</p>
</td>
    </tr><tr>
    <td>TASKS</td><td><code>string</code></td><td><p>TasksWidget</p>
</td>
    </tr><tr>
    <td>WORKFLOWS</td><td><code>string</code></td><td><p>WorkflowOverviewWidget</p>
</td>
    </tr><tr>
    <td>TASK_ATTACHMENTS</td><td><code>string</code></td><td><p>TaskAttachmentWidget</p>
</td>
    </tr><tr>
    <td>BRAVAENTERPRISE</td><td><code>string</code></td><td><p>BravaEnterpriseViewerWidget</p>
</td>
    </tr><tr>
    <td>BRAVAIV</td><td><code>string</code></td><td><p>OpenTextIntelligentViewingWidget</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_WidgetConstants.DEFAULT"></a>

### WidgetConstants.DEFAULT : <code>Object</code>
OOTB Default widet types available in D2 Smart View runtime.All of these default widget type names(including any new ones introduced by a D2 Plugin) could be calculated by normalizingthe default widget's XML tagname from the Smart View landing page xml configuration. In the normalization process any specialnon-word character from the tagname needs to be removed.For an example if the xml tag name is `<my-custom-wdiget></my-custom-widget>`, it normalizes into `mycustomwidget` after removing all the `'-'` characters.Another example `<task-attachments-widget></task-attachments-widget>` to `taskattachmentswidget`.

**Kind**: static property of [<code>WidgetConstants</code>](#module_WidgetConstants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>VERSIONS</td><td><code>string</code></td><td><p>versionwidget</p>
</td>
    </tr><tr>
    <td>RENDITIONS</td><td><code>string</code></td><td><p>renditionwidget</p>
</td>
    </tr><tr>
    <td>RELATIONS</td><td><code>string</code></td><td><p>relationwidget</p>
</td>
    </tr><tr>
    <td>SEARCH</td><td><code>string</code></td><td><p>doclistsearchresultswidget</p>
</td>
    </tr><tr>
    <td>DOCLIST</td><td><code>string</code></td><td><p>doclistwidget</p>
</td>
    </tr><tr>
    <td>FAVORITES</td><td><code>string</code></td><td><p>favoriteswidget</p>
</td>
    </tr><tr>
    <td>BRAVACSR</td><td><code>string</code></td><td><p>bravacsrwidget</p>
</td>
    </tr><tr>
    <td>VDOC</td><td><code>string</code></td><td><p>vdocwidget</p>
</td>
    </tr><tr>
    <td>TASKS</td><td><code>string</code></td><td><p>taskswidget</p>
</td>
    </tr><tr>
    <td>TASK_ATTACHMENTS</td><td><code>string</code></td><td><p>taskattachmentswidget</p>
</td>
    </tr><tr>
    <td>MY_WORKFLOWS</td><td><code>string</code></td><td><p>myworkflowswidget</p>
</td>
    </tr><tr>
    <td>ALL_WORKFLOWS</td><td><code>string</code></td><td><p>allworkflowswidget</p>
</td>
    </tr><tr>
    <td>BRAVAENTERPRISE</td><td><code>string</code></td><td><p>bravaenterprisewidget</p>
</td>
    </tr><tr>
    <td>BRAVAIV</td><td><code>string</code></td><td><p>otivwidget</p>
</td>
    </tr>  </tbody>
</table>

