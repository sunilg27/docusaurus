<a name="module_OAuthLoginDialog"></a>

## OAuthLoginDialog : <code>d2/sdk/utils/oauth.login.dialog</code>
Authenticate with an external IDP using OAuth2 and acquire access token. Requires popup to be enabled in browser.

**Extends**: <code>Object</code>  
**Example** *(A sample authentication with OTDS could look like)*  
```js
define([
   'd2/sdk/utils/contexts/context.utils',
   'd2/sdk/utils/contexts/factories/connector'
   'd2/sdk/utils/oauth.login.dialog'
], function(contextUtil, ConnectorFactory, OAuthLoginDialog){
   'use strict';

   var dlg = new OAuthLoginDialog({
     connector: contextUtil.getPerspectiveContext().getObject(ConnectorFactory),
     url: 'https://<OTDS_SERVER_IP>:8443/otdsws/oauth2/auth',
     clientId: 'sample_system',
     strings: {
       title: 'Sign in to Sample System', //could be localized using i18n
       authError: 'Authentication failed for Sample System' //could be localized using i18n
     }
   });

   dlg
     .login()
     .then(function(accessToken, expiresIn){
       console.log('Acquired token:' + accessToken + ' that is valid for ' + expiresIn + ' ms.'); //Even though console logging sensitive data is a bad idea, but it's always handy for a code snippet :-)
     })
     .catch(function(){
       console.log('Auth failed');
     });
});
```

* [OAuthLoginDialog](#module_OAuthLoginDialog) : <code>d2/sdk/utils/oauth.login.dialog</code>
    * [OAuthLoginDialog](#exp_module_OAuthLoginDialog--OAuthLoginDialog) ⏏
        * [new OAuthLoginDialog(options)](#new_module_OAuthLoginDialog--OAuthLoginDialog_new)
        * _instance_
            * [.login()](#module_OAuthLoginDialog--OAuthLoginDialog+login) ⇒ <code>jQyery.Promise</code>
        * _inner_
            * [~OAuthLoginSuccessCallback](#module_OAuthLoginDialog--OAuthLoginDialog..OAuthLoginSuccessCallback) : <code>function</code>

<a name="exp_module_OAuthLoginDialog--OAuthLoginDialog"></a>

### OAuthLoginDialog ⏏
**Kind**: Exported class  
<a name="new_module_OAuthLoginDialog--OAuthLoginDialog_new"></a>

#### new OAuthLoginDialog(options)
**Throws**:

- Error if any of `options.connector` & `options.url` is missing.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>Construction options holder</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>options.url</td><td><code>string</code></td><td><p>Base URL of IDP</p>
</td>
    </tr><tr>
    <td>options.clientId</td><td><code>string</code></td><td><p>Client ID of the system for which token is being requested.</p>
</td>
    </tr><tr>
    <td>[options.strings]</td><td><code>Object.&lt;string, string&gt;</code></td><td><p>To control different displayable labels associated with the dialog.</p>
</td>
    </tr><tr>
    <td>[options.strings.title]</td><td><code>string</code></td><td><p>Login dialog&#39;s title.</p>
</td>
    </tr><tr>
    <td>[options.strings.message]</td><td><code>string</code></td><td><p>Login dialog&#39;s message.</p>
</td>
    </tr><tr>
    <td>[options.strings.cancelBtn]</td><td><code>string</code></td><td><p>Login dialog&#39;s Cancel button label.</p>
</td>
    </tr><tr>
    <td>[options.strings.authError]</td><td><code>string</code></td><td><p>Message to be shown if authentication fails.</p>
</td>
    </tr><tr>
    <td>[options.strings.popupTitle]</td><td><code>string</code></td><td><p>Alert dialog&#39;s title if popup is diabled.</p>
</td>
    </tr><tr>
    <td>[options.strings.popupWarning]</td><td><code>string</code></td><td><p>Alert dialog&#39;s message if popup is diabled.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_OAuthLoginDialog--OAuthLoginDialog+login"></a>

#### oAuthLoginDialog.login() ⇒ <code>jQyery.Promise</code>
Perform login to the external IDP. It checks existence of a valid access token first, if not found then login screen is shown.

**Kind**: instance method of [<code>OAuthLoginDialog</code>](#exp_module_OAuthLoginDialog--OAuthLoginDialog)  
**Returns**: <code>jQyery.Promise</code> - resolves and invokes `OAuthLoginSuccessCallback` on successful login, rejects otherwise  
<a name="module_OAuthLoginDialog--OAuthLoginDialog..OAuthLoginSuccessCallback"></a>

#### OAuthLoginDialog~OAuthLoginSuccessCallback : <code>function</code>
**Kind**: inner typedef of [<code>OAuthLoginDialog</code>](#exp_module_OAuthLoginDialog--OAuthLoginDialog)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>accessToken</td><td><code>string</code></td><td><p>The access token</p>
</td>
    </tr><tr>
    <td>expiresIn</td><td><code>number</code></td><td><p>Token expiration time in milliseconds.</p>
</td>
    </tr>  </tbody>
</table>

