<a name="module_RestResources"></a>

## RestResources : <code>d2/sdk/utils/rest.resources</code>
Lets read-only access to some long lived D2-REST resources that are loaded during application startup.

**Extends**: <code>Object</code>  

* [RestResources](#module_RestResources) : <code>d2/sdk/utils/rest.resources</code>
    * [.getHomeDocLinks()](#module_RestResources+getHomeDocLinks) ⇒ <code>Array.&lt;D2RestLinkObject&gt;</code>
    * [.getProductInfo()](#module_RestResources+getProductInfo) ⇒ <code>ProductInfoModel</code>
    * [.getCurrentRepository()](#module_RestResources+getCurrentRepository) ⇒ <code>RepositoryModel</code>
    * [.getCurrentUser()](#module_RestResources+getCurrentUser) ⇒ <code>UserModel</code>
    * [.getFormats()](#module_RestResources+getFormats) ⇒ <code>FormatCollection</code>
    * [.getD2Options()](#module_RestResources+getD2Options) ⇒ <code>Object</code>
    * [.getWigetConfigs()](#module_RestResources+getWigetConfigs) ⇒ <code>WidgetConfigsMap</code>
    * [.getDctmTypeData()](#module_RestResources+getDctmTypeData) ⇒ <code>DctmTypesHolder</code>

<a name="module_RestResources+getHomeDocLinks"></a>

### restResources.getHomeDocLinks() ⇒ <code>Array.&lt;D2RestLinkObject&gt;</code>
Get the root resource links of D2-REST APIs.

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
**Returns**: <code>Array.&lt;D2RestLinkObject&gt;</code> - resource links  
<a name="module_RestResources+getProductInfo"></a>

### restResources.getProductInfo() ⇒ <code>ProductInfoModel</code>
Get the metadata of D2-REST services. It returns build information, authentication type, repository config etc.

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
<a name="module_RestResources+getCurrentRepository"></a>

### restResources.getCurrentRepository() ⇒ <code>RepositoryModel</code>
Get the curernt repository that the user has logged into

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
<a name="module_RestResources+getCurrentUser"></a>

### restResources.getCurrentUser() ⇒ <code>UserModel</code>
Get data of the logged in user.

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
<a name="module_RestResources+getFormats"></a>

### restResources.getFormats() ⇒ <code>FormatCollection</code>
Get collection of all content formats supported by the repository

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
<a name="module_RestResources+getD2Options"></a>

### restResources.getD2Options() ⇒ <code>Object</code>
Get D2 rumtime options for the logged in user

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
<a name="module_RestResources+getWigetConfigs"></a>

### restResources.getWigetConfigs() ⇒ <code>WidgetConfigsMap</code>
Get all widget configs data available to the logged in user

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
<a name="module_RestResources+getDctmTypeData"></a>

### restResources.getDctmTypeData() ⇒ <code>DctmTypesHolder</code>
Get Documentum types available in the repository

**Kind**: instance method of [<code>RestResources</code>](#module_RestResources)  
