<a name="module_Constants"></a>

## Constants : <code>d2/sdk/utils/constants</code>
Some utility constants.

**Extends**: <code>Object</code>  

* [Constants](#module_Constants) : <code>d2/sdk/utils/constants</code>
    * [.MediaTypes](#module_Constants.MediaTypes) : <code>Object</code>
    * [.Types](#module_Constants.Types) : <code>Object</code>
    * [.MenuTypes](#module_Constants.MenuTypes) : <code>Object</code>
    * [.ISODateFormats](#module_Constants.ISODateFormats) : <code>Object</code>

<a name="module_Constants.MediaTypes"></a>

### Constants.MediaTypes : <code>Object</code>
Supported AJAX media types.

**Kind**: static property of [<code>Constants</code>](#module_Constants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>DCTM_JSON</td><td><code>string</code></td><td><p>&quot;application/vnd.emc.documentum+json&quot;.</p>
</td>
    </tr><tr>
    <td>DCTM_XML</td><td><code>string</code></td><td><p>&quot;application/vnd.emc.documentum+xml&quot;.</p>
</td>
    </tr><tr>
    <td>DCTM_D2_V1_JSON</td><td><code>string</code></td><td><p>&quot;application/vnd.documentum.d2.v1+json&quot;.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Constants.Types"></a>

### Constants.Types : <code>Object</code>
Object types. Used for type equality comparison for [NodeModel](../models/node.model)s.

**Kind**: static property of [<code>Constants</code>](#module_Constants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>FOLDER</td><td><code>number</code></td>
    </tr><tr>
    <td>OBJECT</td><td><code>number</code></td>
    </tr><tr>
    <td>REPOSITORY</td><td><code>number</code></td>
    </tr><tr>
    <td>CABINET</td><td><code>number</code></td>
    </tr><tr>
    <td>DOCUMENT</td><td><code>number</code></td>
    </tr><tr>
    <td>RENDITION</td><td><code>number</code></td>
    </tr><tr>
    <td>WORKFLOW_STEP</td><td><code>number</code></td>
    </tr><tr>
    <td>TASK</td><td><code>number</code></td>
    </tr><tr>
    <td>TASK_PERFORMER</td><td><code>number</code></td>
    </tr><tr>
    <td>WORKFLOW</td><td><code>number</code></td>
    </tr><tr>
    <td>WORKFLOW_EVENT</td><td><code>number</code></td>
    </tr><tr>
    <td>TASK_EVENT</td><td><code>number</code></td>
    </tr><tr>
    <td>EMAIL</td><td><code>number</code></td>
    </tr><tr>
    <td>DQL</td><td><code>number</code></td>
    </tr><tr>
    <td>ACCESSOR</td><td><code>number</code></td>
    </tr>  </tbody>
</table>

**Example**  
```js
define(['d2/sdk/utils/constants'], function(constants){   //supposing 'node' is an instance of NodeModel   if(node.get('type') === constants.Types.FOLDER) {     console.log('This is a folder');   }});
```
<a name="module_Constants.MenuTypes"></a>

### Constants.MenuTypes : <code>Object</code>
Values used as 'type' query-parameter while making AJAX call to retrieve menu actions associated with a [NodeModel](../models/node.model)

**Kind**: static property of [<code>Constants</code>](#module_Constants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>CONTEXT</td><td><code>string</code></td><td><p>&#39;MenuContext&#39;</p>
</td>
    </tr><tr>
    <td>NEW_OBJECT</td><td><code>string</code></td><td><p>&#39;MenuNewObject&#39;</p>
</td>
    </tr><tr>
    <td>USER</td><td><code>string</code></td><td><p>&#39;MenuUser&#39;</p>
</td>
    </tr><tr>
    <td>VDOC</td><td><code>string</code></td><td><p>&#39;MenuContextVD&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_VD_ADD_CHILD</td><td><code>string</code></td><td><p>&#39;MenuContextVDAddChildOption&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_VD_REPLACE_CHILD</td><td><code>string</code></td><td><p>&#39;MenuContextVDReplaceChildOption&#39;</p>
</td>
    </tr><tr>
    <td>TASKS</td><td><code>string</code></td><td><p>&#39;MenuContextTasksList&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_CHANGE_TASK_PRIORITY</td><td><code>string</code></td><td><p>&#39;MenuContextTaskPriority&#39;</p>
</td>
    </tr><tr>
    <td>TASK_DOCUMENTS</td><td><code>string</code></td><td><p>&#39;MenuContextTaskDocument&#39;</p>
</td>
    </tr><tr>
    <td>WORKFLOWS</td><td><code>string</code></td><td><p>&#39;MenuContextWorkflowOverview&#39;</p>
</td>
    </tr><tr>
    <td>SAVED_SEARCH</td><td><code>string</code></td><td><p>&#39;MenuContextSavedSearchObject&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_LAUNCH_WORKFLOW</td><td><code>string</code></td><td><p>&#39;MenuDocumentWorkflow&#39;</p>
</td>
    </tr><tr>
    <td>DETAIL_RELATIONS</td><td><code>string</code></td><td><p>&#39;MenuContextDetailRelations&#39;</p>
</td>
    </tr><tr>
    <td>DETAIL_RENDITIONS</td><td><code>string</code></td><td><p>&#39;MenuContextDetailRenditions&#39;</p>
</td>
    </tr><tr>
    <td>DETAIL_VERSIONS</td><td><code>string</code></td><td><p>&#39;MenuContextDetailVersions&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_SHARE</td><td><code>string</code></td><td><p>&#39;MenuDocumentShare&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_MASS_UPDATE</td><td><code>string</code></td><td><p>&#39;MenuToolsMassUpdate&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_DOCUMENT_LIFECYCLE</td><td><code>string</code></td><td><p>&#39;MenuDocumentLifeCycle&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_DOCUMENT_LIFECYCLE_ATTACH</td><td><code>string</code></td><td><p>&#39;MenuDocumentLifeCycleAttach&#39;</p>
</td>
    </tr><tr>
    <td>SUBMENU_DOCUMENT_D2_LIFECYCLE_ATTACH</td><td><code>string</code></td><td><p>&#39;MenuDocumentD2LifeCycleAttach&#39;</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Constants.ISODateFormats"></a>

### Constants.ISODateFormats : <code>Object</code>
Date formats used by D2-REST APIs.

**Kind**: static property of [<code>Constants</code>](#module_Constants)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>DATE_ONLY</td><td><code>string</code></td><td><p>&#39;YYYY-MM-DD&#39;</p>
</td>
    </tr><tr>
    <td>DATE_TIME</td><td><code>string</code></td><td><p>&#39;YYYY-MM-DDTHH:mm:ss.SSSZZ&#39;</p>
</td>
    </tr>  </tbody>
</table>

