<a name="module_Utils"></a>

## Utils : <code>d2/sdk/utils/utils</code>
Utility functions.

**Extends**: <code>Object</code>  

* [Utils](#module_Utils) : <code>d2/sdk/utils/utils</code>
    * [.getAppContext()](#module_Utils.getAppContext) ⇒ <code>string</code>
    * [.getStoreKey(key)](#module_Utils.getStoreKey) ⇒ <code>string</code>
    * [.getLocalStore(key)](#module_Utils.getLocalStore) ⇒ <code>NamedStorage</code>
    * [.getSessionStore(key)](#module_Utils.getSessionStore) ⇒ <code>NamedStorage</code>
    * [.isMobile()](#module_Utils.isMobile) ⇒ <code>boolean</code>
    * [.isAppworks()](#module_Utils.isAppworks) ⇒ <code>boolean</code>
    * [.openUrlInWindow(url, [wnd], [focus], [keepLink])](#module_Utils.openUrlInWindow) ⇒ <code>void</code>
    * [.makeBatchAjaxCall(options)](#module_Utils.makeBatchAjaxCall) ⇒ <code>jQuery.Promise</code>
    * [.buildLinkUrl(link, isTemplate)](#module_Utils.buildLinkUrl) ⇒ <code>string</code>
    * [.parseAjaxError(xhr, [textStatus], [errorThrown], [includeDetails])](#module_Utils.parseAjaxError) ⇒ <code>ParsedErrorType</code>
    * [.parseAjaxErrorAsString(xhr, [textStatus], [errorThrown], [includeDetails])](#module_Utils.parseAjaxErrorAsString) ⇒ <code>string</code>
    * [.parseError(responseType, responseData)](#module_Utils.parseError) ⇒ <code>ParsedErrorType</code>
    * [.parseErrorAsString(responseType, responseData)](#module_Utils.parseErrorAsString) ⇒ <code>string</code>
    * [.copyToClipboard(clipboardText)](#module_Utils.copyToClipboard) ⇒ <code>boolean</code>
    * [.isJqueryPromise(value)](#module_Utils.isJqueryPromise) ⇒ <code>boolean</code>
    * [.isNonEmptyString(str)](#module_Utils.isNonEmptyString) ⇒ <code>boolean</code>
    * [.areSameFunctions(fn1, fn2)](#module_Utils.areSameFunctions) ⇒ <code>boolean</code>

<a name="module_Utils.getAppContext"></a>

### Utils.getAppContext() ⇒ <code>string</code>
Get context path name of D2 Smart View deployment

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>string</code> - the context path name  
<a name="module_Utils.getStoreKey"></a>

### Utils.getStoreKey(key) ⇒ <code>string</code>
Get a properly namespaced storage name for the given key. Used while creating a boundary within `localStorage` or `sessionStorage`.See `getLocalStore` and `getSessionStore` below.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>string</code> - namespaced key name  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>key</td><td><code>string</code></td><td><p>original key name</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.getLocalStore"></a>

### Utils.getLocalStore(key) ⇒ <code>NamedStorage</code>
Get a local storage area identified by the given key. A new storage area gets created if doesn't exist already.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>NamedStorage</code> - local store instance  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>key</td><td><code>string</code></td><td><p>Storage identifier</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.getSessionStore"></a>

### Utils.getSessionStore(key) ⇒ <code>NamedStorage</code>
Get a session storage area identified by the given key. A new storage area gets created if doesn't exist already.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>NamedStorage</code> - session store instance  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>key</td><td><code>string</code></td><td><p>Storage identifier</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.isMobile"></a>

### Utils.isMobile() ⇒ <code>boolean</code>
Detects whether the D2 Smart View runtime platform is a mobile

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>boolean</code> - `true` if it's being run on mobile, `false` otherwise.  
<a name="module_Utils.isAppworks"></a>

### Utils.isAppworks() ⇒ <code>boolean</code>
Detects whether the D2 Smart View runtime platform is appworks.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>boolean</code> - `true` if it's Appworks.  
<a name="module_Utils.openUrlInWindow"></a>

### Utils.openUrlInWindow(url, [wnd], [focus], [keepLink]) ⇒ <code>void</code>
Open the given URL in window.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>url</td><td><code>string</code></td><td></td><td><p>The URL to open</p>
</td>
    </tr><tr>
    <td>[wnd]</td><td><code>Window</code></td><td></td><td><p>The window where <code>url</code> will be opened. Defaults to current global <code>window</code> object.</p>
</td>
    </tr><tr>
    <td>[focus]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether to focus the window</p>
</td>
    </tr><tr>
    <td>[keepLink]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether to keep the <code>opener</code> reference. <code>true</code> keeps it.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.makeBatchAjaxCall"></a>

### Utils.makeBatchAjaxCall(options) ⇒ <code>jQuery.Promise</code>
Make a single D2-REST AJAX call with a batch of operations.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>jQuery.Promise</code> - The returned promise fails on AJAX error. It resolves otherwise invoking `BatchDoneCallback` as documented below.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td></td><td><p>Holder of different options</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td></td><td><p>Transport for the AJAX call. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>options.operations</td><td><code>Array.&lt;BatchOperationRequest&gt;</code></td><td></td><td><p>Operations to be perfomred in this batch.</p>
</td>
    </tr><tr>
    <td>[options.transactional]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether this batch is to be executed as a transaction. A transactional batch operates with &#39;Either all or none&#39; paradigm.</p>
</td>
    </tr><tr>
    <td>[options.sequential]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>For a non-transactional batch, whether the operations are to be run sequentially or parrallely.</p>
</td>
    </tr><tr>
    <td>[options.continueOnError]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether remaining operations are to be run if one fails.</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
define([   'd2/sdk/utils/contexts/context.utils',   'd2/sdk/utils/utils',   'd2/sdk/utils/contexts/factories/connector'], function(contextUtil, util, ConnectorFactory){   'use strict';   // Retrieve document menu actions and favorited status for object '0c0015fa8000291e' in a single batch call   util.makeBatchAjaxCall({     connector: contextUtil.getPerspectiveContext().getObject(ConnectorFactory),     continueOnError: true,     operations: [       {         id: 'favorites',         request: {           method: 'GET',           uri: '/repositories/d2repo/favorites-checker?object-ids=0c0015fa8000291e'         }       },       {         id: 'actions',         request: {           method: 'GET',           uri: '/repositories/d2repo/actions?type=MenuContext&widgetType=DoclistWidget&include-subtype=true&object-ids=0c0015fa8000291e'         }       }     ]   })     .done(function(){       console.log('batch call success');     })     .fail(function(){       console.log('batch call failed');     });});
```
<a name="module_Utils.buildLinkUrl"></a>

### Utils.buildLinkUrl(link, isTemplate) ⇒ <code>string</code>
Build complete URL from a D2-REST link

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>string</code> - the complete URL.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>link</td><td><code>Object</code></td><td><p>The link as is from a D2-REST response</p>
</td>
    </tr><tr>
    <td>isTemplate</td><td><code>boolean</code></td><td><p>Is this D2-REST link a templated one or regular.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.parseAjaxError"></a>

### Utils.parseAjaxError(xhr, [textStatus], [errorThrown], [includeDetails]) ⇒ <code>ParsedErrorType</code>
Parse AJAX error happened as a result of D2-REST API call and return a normalized error structure.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>ParsedErrorType</code> - the parsed error  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>xhr</td><td><code>external:XMLHttpRequest</code></td><td></td><td><p>The XHR transport</p>
</td>
    </tr><tr>
    <td>[textStatus]</td><td><code>string</code></td><td></td><td><p>Textual status associated with the error</p>
</td>
    </tr><tr>
    <td>[errorThrown]</td><td><code>string</code></td><td></td><td><p>Custom error message in case the XHR response could not be parsed.</p>
</td>
    </tr><tr>
    <td>[includeDetails]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether to include a <code>errorDetails</code> field in the prased error structure.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.parseAjaxErrorAsString"></a>

### Utils.parseAjaxErrorAsString(xhr, [textStatus], [errorThrown], [includeDetails]) ⇒ <code>string</code>
Parse AJAX error happened as a result of D2-REST API call and return a compact string representing it.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>string</code> - the parsed error  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>xhr</td><td><code>external:XMLHttpRequest</code></td><td></td><td><p>The XHR transport</p>
</td>
    </tr><tr>
    <td>[textStatus]</td><td><code>string</code></td><td></td><td><p>Textual status associated with the error</p>
</td>
    </tr><tr>
    <td>[errorThrown]</td><td><code>string</code></td><td></td><td><p>Custom error message in case the XHR response could not be parsed.</p>
</td>
    </tr><tr>
    <td>[includeDetails]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether to include a <code>errorDetails</code> field in the prased error structure.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.parseError"></a>

### Utils.parseError(responseType, responseData) ⇒ <code>ParsedErrorType</code>
Extract error from custom object and format it similar to return format of `parseAjaxError`.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>ParsedErrorType</code> - the parsed error  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>responseType</td><td><code>string</code></td><td><p>Hint of how to parse <code>responseData</code>. Can be any of &#39;xml&#39;, &#39;json&#39; or &#39;object&#39;</p>
</td>
    </tr><tr>
    <td>responseData</td><td><code>JSON</code> | <code>string</code></td><td><p>If <code>responseType</code> is &#39;object&#39; then a JSON object, otherwise a string serilized error structure.
Irrespective of its type, every input is expected to have a <code>code</code>, <code>message</code> and <code>details</code> field in it.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.parseErrorAsString"></a>

### Utils.parseErrorAsString(responseType, responseData) ⇒ <code>string</code>
Extract error from custom object and return a compact string representing it.

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>string</code> - the parsed error  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>responseType</td><td><code>string</code></td><td><p>Hint of how to parse <code>responseData</code>. Can be any of &#39;xml&#39;, &#39;json&#39; or &#39;object&#39;</p>
</td>
    </tr><tr>
    <td>responseData</td><td><code>JSON</code> | <code>string</code></td><td><p>If <code>responseType</code> is &#39;object&#39; then a JSON object, otherwise a string serilized error structure.
Irrespective of its type, every input is expected to have a <code>code</code>, <code>message</code> and <code>details</code> field in it.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.copyToClipboard"></a>

### Utils.copyToClipboard(clipboardText) ⇒ <code>boolean</code>
Set some text content to the browser clipboard

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>boolean</code> - `true` indicates the copy is successful.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>clipboardText</td><td><code>string</code></td><td><p>The content to be copied.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.isJqueryPromise"></a>

### Utils.isJqueryPromise(value) ⇒ <code>boolean</code>
Checks whether a given value is a `jQuery.Promise` instance or not

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>boolean</code> - `true` if it's a `jQuery.Promise`, `false` otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>value</td><td><code>*</code></td><td><p>The test value</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.isNonEmptyString"></a>

### Utils.isNonEmptyString(str) ⇒ <code>boolean</code>
Checks whether a given value is a non-empty `string`

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>boolean</code> - `true` if it's a `string`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>str</td><td><code>*</code></td><td><p>The test value</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_Utils.areSameFunctions"></a>

### Utils.areSameFunctions(fn1, fn2) ⇒ <code>boolean</code>
Compare two functions for equality

**Kind**: static method of [<code>Utils</code>](#module_Utils)  
**Returns**: <code>boolean</code> - `true` only if Left hand side is equal to right hand side.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>fn1</td><td><code>function</code></td><td><p>Left hand side</p>
</td>
    </tr><tr>
    <td>fn2</td><td><code>function</code></td><td><p>Right hand side</p>
</td>
    </tr>  </tbody>
</table>

