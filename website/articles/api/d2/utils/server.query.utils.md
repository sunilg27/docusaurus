<a name="module_ServerQueryUtils"></a>

## ServerQueryUtils : <code>d2/sdk/utils/server.query.utils</code>
A general utility to access/process some commonly used D2-REST resources outside of a `Backbone.Model` or `Backbone.Collection`.

**Extends**: <code>Object</code>  

* [ServerQueryUtils](#module_ServerQueryUtils) : <code>d2/sdk/utils/server.query.utils</code>
    * [.appendQueryParams(url, options)](#module_ServerQueryUtils.appendQueryParams) ⇒ <code>String</code>
    * [.parseResponse(response, options)](#module_ServerQueryUtils.parseResponse) ⇒ <code>JSON</code>
    * [.fetchColumnPreferences(options)](#module_ServerQueryUtils.fetchColumnPreferences) ⇒ <code>jQuery.Promise</code>
    * [.saveColumnPreferences(options)](#module_ServerQueryUtils.saveColumnPreferences) ⇒ <code>jQuery.Promise</code>
    * [.getWidgetConfig(widgetType, widgetName)](#module_ServerQueryUtils.getWidgetConfig) ⇒ <code>object</code>
    * [.fetchPermissions(options)](#module_ServerQueryUtils.fetchPermissions) ⇒ <code>jQuery.Promise</code>
    * [.fetchFavorites(options)](#module_ServerQueryUtils.fetchFavorites) ⇒ <code>jQuery.Promise</code>
    * [.fetchActions(options)](#module_ServerQueryUtils.fetchActions) ⇒ <code>jQuery.Promise</code>
    * [.fetchLocations(options)](#module_ServerQueryUtils.fetchLocations) ⇒ <code>jQuery.Promise</code>
    * [.fetchDataForObjects(options)](#module_ServerQueryUtils.fetchDataForObjects) ⇒ <code>jQuery.Promise</code>
    * [.fetchDataAsPropertiesForObjects(options)](#module_ServerQueryUtils.fetchDataAsPropertiesForObjects) ⇒

<a name="module_ServerQueryUtils.appendQueryParams"></a>

### ServerQueryUtils.appendQueryParams(url, options) ⇒ <code>String</code>
Form a query appended URL from a base URL, thus facilitates to make AJAX calls.

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>String</code> - The formed URL.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>url</td><td><code>string</code></td><td><p>The base URL.</p>
</td>
    </tr><tr>
    <td>options</td><td><code>Object</code></td><td><p>The options hash to control URL generation.</p>
</td>
    </tr><tr>
    <td>options.scope</td><td><code>string</code></td><td><p>The scope of the target D2-REST resource</p>
</td>
    </tr><tr>
    <td>options.paginate</td><td><code>boolean</code></td><td><p>Whether to paginate the URL.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.parseResponse"></a>

### ServerQueryUtils.parseResponse(response, options) ⇒ <code>JSON</code>
Parses response from D2-REST resource.

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>JSON</code> - The parsed response.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>response</td><td><code>JSON</code></td><td><p>Raw response from server.</p>
</td>
    </tr><tr>
    <td>options</td><td><code>Object</code></td><td><p>The options hash to control parsing.</p>
</td>
    </tr><tr>
    <td>options.scope</td><td><code>string</code></td><td><p>The scope of the target D2-REST resource</p>
</td>
    </tr><tr>
    <td>options.paginate</td><td><code>boolean</code></td><td><p>Whether to extract page information from the response.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchColumnPreferences"></a>

### ServerQueryUtils.fetchColumnPreferences(options) ⇒ <code>jQuery.Promise</code>
Fetch column preferences for a widget. It caches the preference data for a given widget type and name,the first time it is fetched, and returns the data from this cache next time onwards.

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - resolves & invokes `ColumnPreferenceDoneCallback` on success, rejects otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>options.scope</td><td><code>*</code></td><td><p>Scope of the request, usually should point to the related data model. E.g. when calling in the context of a collection, it should be the collection itself.</p>
</td>
    </tr><tr>
    <td>options.widgetType</td><td><code>string</code></td><td><p>Type of the widget config.</p>
</td>
    </tr><tr>
    <td>options.widgetName</td><td><code>string</code></td><td><p>Name of the widget config.</p>
</td>
    </tr><tr>
    <td>[options.defaultType]</td><td><code>string</code></td><td><p>Default column types.</p>
</td>
    </tr><tr>
    <td>[options.columnOptions]</td><td><code>Object</code></td><td><p>Options for the columns data, used while converting server columns to client columns</p>
</td>
    </tr><tr>
    <td>[options.columnOptions.typeColumn]</td><td><code>boolean</code></td><td><p>Include type column</p>
</td>
    </tr><tr>
    <td>[options.columnOptions.reservedColumn]</td><td><code>boolean</code></td><td><p>Include object lock status column</p>
</td>
    </tr><tr>
    <td>[options.columnOptions.commentColumn]</td><td><code>boolean</code></td><td><p>Include comments column</p>
</td>
    </tr><tr>
    <td>[options.columnOptions.favoriteColumn]</td><td><code>boolean</code></td><td><p>Include favorite status column</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.saveColumnPreferences"></a>

### ServerQueryUtils.saveColumnPreferences(options) ⇒ <code>jQuery.Promise</code>
Save column preferences for a widget

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - resolves and invokes `ColumnPreferenceDoneCallback` on success, rejects otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.widgetType</td><td><code>string</code></td><td><p>Type of the widget config.</p>
</td>
    </tr><tr>
    <td>options.widgetName</td><td><code>string</code></td><td><p>Name of the widget config.</p>
</td>
    </tr><tr>
    <td>[options.defaultType]</td><td><code>string</code></td><td><p>Default column types.</p>
</td>
    </tr><tr>
    <td>options.columnPreference</td><td><code>external:ColumnPreferenceModel</code></td><td><p>The new preference data.</p>
</td>
    </tr><tr>
    <td>options.sortOrder</td><td><code>string</code></td><td><p>Specify a sort order.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.getWidgetConfig"></a>

### ServerQueryUtils.getWidgetConfig(widgetType, widgetName) ⇒ <code>object</code>
Get widget configuration object.

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>object</code> - The widget config  
**Throws**:

- `Error` if any of the paramters are `null` or `undefined` or empty.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>widgetType</td><td><code>string</code></td><td><p>Type of the widget config.</p>
</td>
    </tr><tr>
    <td>widgetName</td><td><code>string</code></td><td><p>Name of the widget config.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchPermissions"></a>

### ServerQueryUtils.fetchPermissions(options) ⇒ <code>jQuery.Promise</code>
Fetch user's permission level for the objects

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - on success it resolves and invokes callback with list of object specific responses  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.ids</td><td><code>Array.&lt;string&gt;</code></td><td><p>Documentum object IDs.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>options.permissionLevel</td><td><code>string</code></td><td><p>Level of permission to check. Any of Documentum permission i.e &#39;browse&#39;, &#39;read&#39;, &#39;relate&#39;, &#39;version&#39;, &#39;write&#39;, &#39;delete&#39;</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchFavorites"></a>

### ServerQueryUtils.fetchFavorites(options) ⇒ <code>jQuery.Promise</code>
Fetch user's favorite(substription) status of the objects

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - on success it resolves and invokes callback with list of object specific responses  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.ids</td><td><code>Array.&lt;string&gt;</code></td><td><p>Documentum object IDs.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchActions"></a>

### ServerQueryUtils.fetchActions(options) ⇒ <code>jQuery.Promise</code>
Fetch allowed actions for the objects

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - on success it resolves and invokes callback with list of object specific responses  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.ids</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>Documentum object IDs.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>options.menuType</td><td><code>string</code></td><td></td><td><p>Type of actions to be fetched. See <code>MenuTypes</code> from <a href="./constants">Constants</a>.</p>
</td>
    </tr><tr>
    <td>options.widgetType</td><td><code>string</code></td><td></td><td><p>Type of widget for which menu is required (e.g. DoclistWidget). See <code>D2</code> from <a href="./widget.constants#module_WidgetConstants.D2">WidgetConstants.D2</a></p>
</td>
    </tr><tr>
    <td>options.queryAction</td><td><code>string</code></td><td></td><td><p>Types of actions to request. Possible values are PROMOTED, NON-PROMOTED and ALL. Default is ALL</p>
</td>
    </tr><tr>
    <td>[options.parentIds]</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>List of parent ids in case the objects are renditions. It has to have a positional correspondence with <code>ids</code> i.e <code>parentIds[0]</code> should be the parent of <code>id[0]</code>.</p>
</td>
    </tr><tr>
    <td>[options.associateIds]</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>List of associated object ids in case the objects are relations. It has to have a positional correspondence.</p>
</td>
    </tr><tr>
    <td>[options.rulesType]</td><td><code>string</code></td><td><code>&quot;node&quot;</code></td><td><p>Rule collection to be used for filtering the actions. Can be one of &#39;user&#39;, &#39;node&#39;.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchLocations"></a>

### ServerQueryUtils.fetchLocations(options) ⇒ <code>jQuery.Promise</code>
Fetch locations of the given objects

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - on success it resolves and invokes callback with list of object specific responses  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.ids</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>Documentum object IDs.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>[options.fullPath]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Whether to parse full path or just include the immediate parent.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchDataForObjects"></a>

### ServerQueryUtils.fetchDataForObjects(options) ⇒ <code>jQuery.Promise</code>
A single call to fetch different aspects of the objects. It can be used to fetch permission, favorited status, actions, locations, external share staus,preview formats and relation count for multiple objects

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: <code>jQuery.Promise</code> - on success it resolves and invokes callback with separate maps representing permissions, favorites, actions etc. depending on what was requested.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td></td><td><p>Request options holder.</p>
</td>
    </tr><tr>
    <td>options.ids</td><td><code>Array.&lt;string&gt;</code></td><td></td><td><p>Documentum object IDs.</p>
</td>
    </tr><tr>
    <td>options.connector</td><td><code>Connector</code></td><td></td><td><p>AJAX transport. See <a href="./contexts/factories/connector">Connector</a>.</p>
</td>
    </tr><tr>
    <td>[options.permissions]</td><td><code>boolean</code> | <code>Object</code></td><td></td><td><p>Fetch permissions.</p>
</td>
    </tr><tr>
    <td>options.permissions.permissionLevel</td><td><code>string</code></td><td></td><td><p>Level of permission to check. Any of Documentum permission i.e &#39;browse&#39;, &#39;read&#39;, &#39;relate&#39;, &#39;version&#39;, &#39;write&#39;, &#39;delete&#39;</p>
</td>
    </tr><tr>
    <td>[options.favorites]</td><td><code>boolean</code></td><td></td><td><p>Fetch favorited status.</p>
</td>
    </tr><tr>
    <td>[options.actions]</td><td><code>ActionRequestType</code></td><td></td><td><p>Fetch actions as per specified options.</p>
</td>
    </tr><tr>
    <td>[options.locations]</td><td><code>boolean</code> | <code>Object</code></td><td></td><td><p>Fetch locations.</p>
</td>
    </tr><tr>
    <td>[options.locations.fullPath]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Parse full path of each location.</p>
</td>
    </tr><tr>
    <td>[options.externalShare]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Fetch external share status.</p>
</td>
    </tr><tr>
    <td>[options.previewFormats]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Fetch preview formats.</p>
</td>
    </tr><tr>
    <td>[options.relationsCount]</td><td><code>boolean</code></td><td><code>false</code></td><td><p>Fetch relations count.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ServerQueryUtils.fetchDataAsPropertiesForObjects"></a>

### ServerQueryUtils.fetchDataAsPropertiesForObjects(options) ⇒
Works the same as fetchDataForObjects(), additionally collects related data for every objects individually into asingle object hash, as per most commonly used property naming conventions for NodeModel, so that the data can be easily mergedwith any target NodeModel.

**Kind**: static method of [<code>ServerQueryUtils</code>](#module_ServerQueryUtils)  
**Returns**: jQuery.Promise  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><p>Same as the options parameter for fetchDataForObjects().</p>
</td>
    </tr>  </tbody>
</table>

