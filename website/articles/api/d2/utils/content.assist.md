<a name="module_ContentAssistDialogUtil"></a>

## ContentAssistDialogUtil : <code>d2/sdk/utils/content.assist</code>
Utility to fetch ContentAssist dialog related options

**Extends**: <code>Object</code>  

* [ContentAssistDialogUtil](#module_ContentAssistDialogUtil) : <code>d2/sdk/utils/content.assist</code>
    * [.getContentAssistConfig(options)](#module_ContentAssistDialogUtil.getContentAssistConfig) ⇒ <code>jQuery.Promise</code>
    * [.getContentAssistOptions(options)](#module_ContentAssistDialogUtil.getContentAssistOptions) ⇒ <code>jQuery.Promise</code>

<a name="module_ContentAssistDialogUtil.getContentAssistConfig"></a>

### ContentAssistDialogUtil.getContentAssistConfig(options) ⇒ <code>jQuery.Promise</code>
Get content assist dialog configuration

**Kind**: static method of [<code>ContentAssistDialogUtil</code>](#module_ContentAssistDialogUtil)  
**Returns**: <code>jQuery.Promise</code> - resolves & invokes `ContentAssistConfigCallback` on success, rejects otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>options holder</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td><p>Application runtime context. See <a href="./contexts/context.utils#Context">Context</a>.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_ContentAssistDialogUtil.getContentAssistOptions"></a>

### ContentAssistDialogUtil.getContentAssistOptions(options) ⇒ <code>jQuery.Promise</code>
Gets and caches the configuration for Content Assist dialog.

**Kind**: static method of [<code>ContentAssistDialogUtil</code>](#module_ContentAssistDialogUtil)  
**Returns**: <code>jQuery.Promise</code> - resolves & invokes `ContentAssistOptionsCallback` on success, rejects otherwise.  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>object</code></td><td><p>options holder</p>
</td>
    </tr><tr>
    <td>options.context</td><td><code>Context</code></td><td><p>Application runtime context. See <a href="./contexts/context.utils#Context">Context</a>.</p>
</td>
    </tr>  </tbody>
</table>

