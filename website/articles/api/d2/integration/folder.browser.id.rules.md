<a name="module_FolderBrowserIDRules"></a>

## FolderBrowserIDRules : <code>d2/sdk/integration/folder.browser.id.rules</code>
A rule collection that defines how to parse id attribute for the FolderBrowser widget in D2SV embedded mode.This collection allows extension registrations and each of those extensions are treated as root object's id providerfor the FolderBrowser widget.

**Extends**: <code>Array</code>  
**Note**: Each rule is supposed to define a `fetcher` callback function that should return a `jquery.Promise` object. See `IDRuleFetcher` below.The returned promise should be resolved with a Documentum ID which will be used as the root container for FolderBrowser. Or, alternativelythe returned promise could be rejected to inidate an error.  
**See**: [RulesMatchingMixin](../../nuc/models/mixins/rules.matching/rules.matching.mixin) for a complete guide on how to write rules.  
**Example** *(To register a new type of id provider)*  
```js
//in src/extensions/embed/folder.browser.id.rules.js
define([
 'nuc/lib/jquery'
], function($){
 'use strict';

 return [{
   equals: {
     id_source: 'abcd' //Match this rule when id_source=abcd is present as part of query parameters while accessing FolderBrowser widget using iURL
   },
   fetcher: function(options) {
     var dfd = $.Deferred();

     var nodeId = options.params.my_id;
     if(!!nodeId) {
       dfd.resolve(nodeId);
       //This example shows a very trivial case where 'my_id' parameter itself holds the Documentum ID.
       //However, depending on requirement complex business integration logic could also be written here to
       //retrieve the Documentum ID from an external system
     } else {
       dfd.reject('my_id is missing from url');
     }

     return dfd.promise();
   }
 }];
});
```
<a name="module_FolderBrowserIDRules..IDRuleFetcher"></a>

### FolderBrowserIDRules~IDRuleFetcher ⇒ <code>jQuery.Promise</code>
**Kind**: inner typedef of [<code>FolderBrowserIDRules</code>](#module_FolderBrowserIDRules)  
**Returns**: <code>jQuery.Promise</code> - The promise should be either resolved with a Documentum ID(r_object_id) or rejected with an error string.  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>options</td><td><code>Object</code></td><td></td>
    </tr><tr>
    <td>options.params</td><td><code>Object</code></td><td><p>JSON object representing query params.</p>
</td>
    </tr>  </tbody>
</table>

