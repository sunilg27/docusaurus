
# Overview

The API includes classes, objects, methods & extension points that could be used to enhance/alter existing components
 of D2SV runtime or write brand new components for it.
 
The D2SV runtime is comprised of

:::info front-end
  Runs on Internet browsers, written using Javascript, HTML, CSS. 
  
  Components of the application are loaded using RequireJS framwork that comes bundled with the runtime.
  A bunch of other open-source libraries are pre-packed as part of the runtime. Below is a list of libraries
  and their RequireJS module dependency path - 
  
* BackboneJS (nuc/lib/backbone)
* MarionetteJS (nuc/lib/marionette)
* UnderscoreJS (nuc/lib/underscore) 
* jQyery (nuc/lib/jquery)
* MomentJS (nuc/lib/moment)
* jQuery Fancy tree (d2/lib/fancytree/jquery.fancytree)
* D3 JS (csui/lib/d3)
  
  
  Available requirejs plugins -
  
  
  * i18n - Loads a localization module
  * hbs - Loads handlebar template files (*.hbs)
  * json - Loads JSON files (*.json)
  * css - Loads CSS files (*.css)
:::

:::info back-end
  Runs on Web application container, Written in Java.
:::
