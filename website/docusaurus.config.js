'use strict';

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'D2 SDK',
  tagline: 'D2 made-easy for customizations',
  url: 'http://localhost:3000',
  baseUrl: '/sdk/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'images/favicon.ico',
  organizationName: 'opentext',
  projectName: 'd2sdk',
  baseUrlIssueBanner: true,
  trailingSlash: false,
  themeConfig: {
    hideableSidebar: true,
    colorMode: {
      defaultMode: 'light',
      respectPrefersColorScheme: true
    },
    navbar: {
      title: 'D2 SDK',
      logo: {
        alt: 'D2 SDK Logo',
        src: 'images/icon.png',
      },
      hideOnScroll: false,
      items: [
        {
          type: 'doc',
          docId: 'general/architecture',
          position: 'left',
          label: 'Docs'
        },
        {
          type: 'doc',
          docId: 'api_overview',
          position: 'left',
          label: 'API'
        },
        /*{ to: 'pathname://sdoc/sample.html', label: 'Samples', position: 'left', target: '_self' },*/
      ],
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} Open Text. All rights reserved. Trademarks owned by Open Text.`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
    presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          path: 'articles',
          routeBasePath: '/',
          include: ['**/*.md', '**/*.mdx'],
          sidebarPath: require.resolve('./sidebars.js')
        },
        blog: false,
        pages: {
        },
        theme: {
          customCss: require.resolve('./css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    [
      require.resolve('@easyops-cn/docusaurus-search-local'),
      {
        hashed: false,
        indexDocs: true,
        docsRouteBasePath: '/',
        docsDir: 'articles',
        indexBlog: false,
        indexPages: true,
        highlightSearchTermsOnTargetPage: true,
        language: "en"
      },
    ],
  ],
};
